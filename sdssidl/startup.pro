; SDSSIDL startup file used to get all SDSSIDL routines without C/C++ ext.
;
DEFSYSV, '!SDSSIDL_DIR', GETENV("SDSSIDL_DIR")
!PATH = !PATH + ":"+ EXPAND_PATH('+'+!SDSSIDL_DIR+'/pro')
PRINT,"SDSS IDL (v 1.7.0) now available"
