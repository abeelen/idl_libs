IAS IDL LIBS
============

Series of commonly used IDL libraries in astronomy

* **astron**  
  THE IDL Astronomy User's Library  
  http://idlastro.gsfc.nasa.gov/  
  25-May-2016
* **colortools**  
  usefull library for simple colors
* **Healpix**  
  The Full Healpix library (to be compiled)  
  http://healpix.sourceforge.net/  
  3.10
* **idlutils**  
  idlutils is a collection of IDL utilities useful for astronomical applications   https://www.sdss3.org/dr8/software/idlutils.php  
  v5_5_5
* **markwardt**  
  The Markwardt IDL Library (including MPFIT)  
  http://cow.physics.wisc.edu/~craigm/idl/idl.html  
  v1.3 2016-05-19
* **PIP**  
  The Practical IDL Programming programs (including IMDISP)  
  http://www.gumley.com/PIP/Free_Software.html  
  v1.47 2002/06/05
* **slugs**  
  The SLUGs library (including SPLIT_FOR)  
  http://slugidl.pbworks.com/w/page/29199259/Child%20Processes  
  3-14-11
* **textoidl**  
  Matt Craig TexToIDL routine  
  http://physics.mnstate.edu/craig/textoidl/  
  v2.1.2 2004-05-22 June
* **atv**  
  ATV: an interactive display tool for astronomical images  
  http://www.physics.uci.edu/~barth/atv/  
  v3.0b6 2016-01-31
* **coyote**  
  The Coyote Library from the Coyote's Guide to IDL Programming  
  http://www.idlcoyote.com/  
  2016-01-14
* **idl_aladin**  
  IDL/ Aladin interface  
  http://aladin.u-strasbg.fr/java/FAQ.htx#ToC160  
  Version 0.8 : February 2007
* **IRSA**  
  IRSA IDL Tools (including read_ipac_table)  
  http://irsa.ipac.caltech.edu/tools/irsa_idl.html  
  v1.5 2016-01-05
* **OAR**  
  IDL Library of the Rome Astronomical Observatory  (including FFT_CONVOLUTION & RADIAL_PROFILE)  
  http://www.oa-roma.inaf.it/facilities/IDL_Library/    
  Version 1.01
* **sdssidl**  
  University of Michigan/Chicago/NYU SDSS IDL libraries
  https://github.com/esheldon/sdssidl  
  v2.* 2007-03-26
* **starfinder**  
  A code for stellar field analysis
  http://www.bo.astro.it/StarFinder/paper6.htm#To%20obtain%20the%20code
  v1.8.2
* **UcCC**  
  Unit conversion and Color correction - Planck PLA 2015 Wiki  
  https://wiki.cosmos.esa.int/planckpla2015/index.php/Unit_conversion_and_Color_correction


* **idl_start** : the idl startup script which include all the above library
* **idlrc.sh** : setup all shell variables for the idl_start script, must be launch once beforehand.
