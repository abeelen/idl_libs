This README file provides basic instruction on using the related IDL scripts in generating various HFI [/LFI] spectral coefficients.

The following scripts should be available:

hfi_lfi_test_script.pro - This routine provides examples of the use of the other IDL scripts provided (see descriptions below).  
There are also test outputs provided to compare with parameters set to print to screen during execution.  
Default directoriesa on magique3 are used, change these as required to locate input files of interest.

hfi_lfi_test_script_PLA_RIMO.pro - a sample script providing examples of calling the various routines with the PLA RIMO file (without the detector spectra).

hfi_read_bandpass.pro - Get spectra from the IMO or RIMO in an IDL structure format which the other scripts are expecting.
hfi_read_Avg_bandpass.pro - The same as above for the band-average spectra.

lfi_read_bandpass.pro - Get the LFI spectra, similar to the corresponding HFI script.  
	Note: the /RIMO keyword should be set, and a LFI RIMO.fits file should be available.
lfi_read_average_bandpass.pro - Get the band-average LFI bandpass spectra in an IDL structure format expecteds by the other routines.

get_hfibolo_list.pro - Generates an array of detector names to be used by the scripts which output the transmission spectra.

hfi_unit_conversion.pro - calculate the various conversion factors for the spectra provided as input (i.e. HFI or LFI).

hfi_colour_correction.pro - Calculate the colour correction factors for the provided spectra.  
A user-supplied power law index is used, or a used specified blackbody temperature/emisivity, or a user-specified spectrum.

hfi_co_correction.pro - Generates CO conversion coefficients for the provided spectra.
Note: This outputs nothing for LFI spectra as there are no relevant CO rotational in-band.

LFI_fastcc.pro - the LFI CC routine which has quadratic coefficients hard-coded for the LFI RIMO spectra.  It is faster as it does not need the spectra resotred or any integration.

