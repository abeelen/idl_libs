;+
; NAME:
;
;   hfi_read_avg_bandpass
;
; PURPOSE:
;
;   Read band-average HFI bandpass info
;
; CATEGORY:
;
;
;
; CALLING SEQUENCE:
;
;   bp = hfi_read_avg_bandpass(+ Keywords)
;
; OPTIONAL INPUTS:
;
;   version :  HFI bandpass version
;              v101
;              v201
;
; KEYWORD PARAMETERS:
;
;   IMO:        If an IMO label is to be read.
;   LBL_IMO:    The label for the desired IMO, e.g. '2_67'.
;   PATH_IMO:   The file path for the desired IMO, e.g. '/data/dmc/MISS03/METADATA/'. 
;   RIMO:       If a RIMO fits file is to be read.
;   PATH_RIMO:  The location of the RIMO .fits file, e.g. '/wrk/lspencer/mywork2/test/'.
;   NAME_RIMO:  The name of the RIMO .fits file, e.g. 'LFI_RIMO_03062011_DX7.fits'.
;	FLAG : If set to 1, it will remove the over-sampled CO points from the spectra, only in the case that the IMO keyword is also set.
;			This is recommended for unit conversion and colour correction, but is not recommended for the CO correction (i.e. keep the CO data in).
;	  FLG_INFO:   A structure similar to the BP_INFO containing the CO flag instead of spectral transmission.
;	  ER_INFO:    A structure similar to the BP_INFO containing the spectral uncertainty instead of spectral transmission.
;
;
; OUTPUTS:
;
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;    Planck collaboration only.
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;   abp = hfi_read_avg_bandpass('v101', /imo)
;   abp = hfi_read_avg_bandpass('v101', /rimo)
;
; MODIFICATION HISTORY:
;
;   Created by L Spencer (August 2011) based on code from S. Hildebrandt and J.F. Macias-Perez, Oct, 2010
;   Modified by L. Spencer (2012/09/17) to revise default conditions to current/DX9 data products.
;   Modified by L. Spencer (2013/01/24) to have the FLAG=1 setting by default.
;
;
;   
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   A copy of the GNU General Public License is available at 
;   <http://www.gnu.org/licenses/>.
;   
;   Copyright Locke D. Spencer, 2013
;   
;-

FUNCTION hfi_read_avg_bandpass, IMO = IMO, LBL_IMO=LBL_IMO, PATH_IMO=PATH_IMO, $
  RIMO = RIMO, PATH_RIMO=PATH_RIMO, NAME_RIMO=NAME_RIMO, $
  FLAG=FLAG, FLG_INFO=FLG_INFO, ER_INFO=ER_INFO
  
  ;
  ; -- Definitions
  CONST = { C: 2.99792458d+08, H: 6.6260755d-34, HBAR: 1.0545726691251021d-34, K: 1.3806580d-23, TCMB: 2.7255d0 }
  ;
  IF N_ELEMENTS(FLAG) EQ 0 THEN FLAG = 1 ; the default flag setting is 1, i.e. remove the CO points, explicitly set FLAG=0 to include them.
  ;
  N_DETECT = 6 ; Planck HFI
  ;
  N_MEASUREMENTS = 17000l
  IF N_ELEMENTS(VERSION) EQ 0 THEN VERSION = ''
  ;
  BP_INFO = replicate( { NAME: '', FREQ: dblarr( n_measurements), TRANS: dblarr( n_measurements) }, n_detect )
  FLG_INFO = BP_INFO
  ER_INFO = FLG_INFO
  If Keyword_set( RIMO ) then begin
    If NOT Keyword_set( PATH_RIMO ) then PATH_RIMO =''
    IF ~KEYWORD_SET(NAME_RIMO) THEN NAME_RIMO = 'HFI-RIMO-20120618.fits'	;	'HFI-RIMO-20110617.fits'
    RIMO_FITS = NAME_RIMO
    ;FITS_RIMO = mrdfits( path_rimo + rimo_fits, 'FREQUENCY MAP PARAMETERS', HDR_RIMO )
    ;  The above line does not work for the delta-DX9 RIMO files, so it is changed to the below one.
    FITS_RIMO = mrdfits( path_rimo + rimo_fits, 'MAP_PARAMS', HDR_RIMO, /SILENT )
    IF N_ELEMENTS(FITS_RIMO) LT 2 THEN FITS_RIMO = mrdfits( path_rimo + rimo_fits, 'FREQUENCY MAP PARAMETERS', HDR_RIMO, /SILENT )
    ;  Assume the RIMO files are either delta-DX9 (default) or pre-delta-DX9 format, future versions may need an additional fix.
    FITS_RIMO_1 = FITS_RIMO
    ;stop
    ;If strpos( hdr_rimo[ 4 ], '6' ) eq -1 then stop,'Is it an HFI RIMO file?'
    FITS_RIMO_det = STRING(fits_rimo.FREQUENCY)
    FITS_RIMO_det = STRTRIM(FITS_RIMO_DET,2)
    BOLO_ID_TMP = ['avg100','avg143','avg217','avg353','avg545','avg857']
    For i = 0, N_DETECT - 1 do begin
      NAME_RIMO_TMP = BOLO_ID_TMP[i]    ; strmid( fits_rimo[ i ].detector, 4  )
      Q_NAME = where( FITS_RIMO_det eq STRMID(NAME_RIMO_TMP,3) )
      BP_INFO[ i ].NAME = NAME_RIMO_TMP    ;   bolo_id_tmp[ q_name[ 0 ] ]  ; completing the name of the detector (e.g. 100-1a in the RIMO)
      FLG_INFO[i].name = BP_INFO[i].name
      ER_INFO[i].name = BP_INFO[i].name
    Endfor
  ; Joining the information of the bandpasses in the bp_info structure
    For i = 0, n_detect - 1 do begin
      ;
      FITS_RIMO = mrdfits( path_rimo + rimo_fits, 'BANDPASS_F'+STRMID(BP_INFO[i].NAME,3), hdri, /SILENT)
      ;
      Nms = TAG_NAMES(FITS_RIMO)
      Ns = N_TAGS(FITS_RIMO)
      NmStr = ''
      FOR ii = 0, Ns - 1 DO NmStr = NmStr+Nms[ii]
      FND_FLG = MAX([STRPOS(NmStr,'FLAG'),STRPOS(NmStr,'flag'),STRPOS(NmStr,'Flag')])
      FND_Er  = MAX([STRPOS(NmStr,'UNCERTAINTY'),STRPOS(NmStr,'uncertainty'),STRPOS(NmStr,'Uncertainty')])
      ;
      wn = fits_rimo.wavenumber   ;   in cm-1
      trans = fits_rimo.transmission
      IF FND_FLG GE 0 THEN BEGIN
      ;stop
        flg = fits_rimo.flag
        flgF = WHERE(flg EQ 'F', NF, COMPLEMENT=flgT, NCOMPLEMENT=NT)
        flgInt = INTARR(N_ELEMENTS(flg))
        IF NF GT 0 THEN flgInt[flgF] = 0
        IF NT GT 0 THEN flgInt[flgT] = 1
        flg = flgInt
      ENDIF
      IF FND_ER GE 0 THEN yer = fits_rimo.uncertainty
      IF KEYWORD_SET(FLAG) THEN BEGIN
        IF FND_FLG GE 0 THEN BEGIN
          noFlag = WHERE(flg EQ 0, Nnoflag)
          ;stop
          If Nnoflag GT 10 THEN BEGIN
            wn = wn[noFlag]
            trans = trans[noFlag]
            flg = flg[noFlag]
            IF FND_ER Ge 0 THEN yer = yer[noFlag]
          ENDIF
        ENDIF ; If flag was not found then the flagged data have already been removed, so this is fine...
      ENDIF ; If FLAG was not set then leave the flagged data (if any) in.
      BP_INFO[ i ].FREQ  = wn * 1d2 * const.c ; wave number given in cm^-1
      FLG_INFO[i].freq = BP_INFO[i].freq
      ER_INFO[i].freq = BP_INFO[i].freq
      BP_INFO[ i ].TRANS = trans  ; values of the transmission (normalized later)
      IF FND_FLG GE 0 THEN FLG_INFO[i].trans = flg
      IF FND_Er GE 0 THEN ER_INFO[i].trans = yer
      ;
    Endfor
  ENDIF   ; END RIMO portion
  ;
  ; IMO
  ;
  If Keyword_set( IMO ) THEN BEGIN
    IF KEYWORD_SET(LBLIMO) THEN IMO_LBL = LBL_IMO ELSE IMO_LBL = '3_20_detilt_t2_ptcor6'	;	'2_67'
    IF KEYWORD_SET(PATH_IMO) THEN ImoFileMD = PATH_IMO ELSE ImoFileMD = '/data/dmc/MISS03/METADATA'
    ImoFile = ImoFileMD + '%lbl:IMO_' + imo_lbl
    ; Open Imo
    IMO_GROUP = PIOOpenIMOFile(ImoFile, 'r')
    print,'IMO_GROUP: ',imo_group
    IF imo_group LE 0 THEN pioerrmess, imo_group
    ; Joining the information of the bandpasses in the bp_info structure
    G_TYPE = 'PIOSTRING'
    TAIL_X = 'SpectralResp:SpecTransmissions:VectX'
    Tail_Flag = 'SpectralResp:SpecTransmissions:VectYFlag'
    Tail_Y = 'SpectralResp:SpecTransmissions:VectY'
    Tail_YError = 'SpectralResp:SpecTransmissions:VectYError'
    ;IF KEYWORD_SET(ERR) THEN Tail_Y = Tail_YError 		;	= 'SpectralResp:SpecTransmissions:VectYError'
    CHANNEL_TMP = [ '100', '143', '217', '353', '545', '857' ]
    N_DETECT = 0
    For i = 0, 5 do begin
		  ;BOLO_LIST = get_hfibolo_list( CHANNEL = channel_tmp[ i ] )
      bolo_ch = CHANNEL_TMP[i]
	    N_LIST = n_elements( bolo_ch )
      For j = 0, n_list - 1 DO BEGIN
        BP_INFO[ N_DETECT ].NAME = 'avg'+bolo_ch
        ;G_NAME_X = 'IMO:HFI:DET:Phot_Pixel Name="' + bolo_list[ j ]  + '":' + tail_x
        G_NAME_X = 'IMO:SkyMap:f' + bolo_ch + ':' + tail_x
        DUMMY  = PIOGetValue( G_VALUE, gerror, g_type, gunit, gcomment, $
		                            g_name_x, imo_group)
		    Q = strpos( g_value, ':' , /reverse_s)
		    G_VALUE_READ = strmid( g_value, 0, q )
		    WN = pioread( g_value_read ) ; Wave number in cm^-1
		    ;G_NAME_Y = 'IMO:HFI:DET:Phot_Pixel Name="' + bolo_list[ j ]  + '":' + tail_y
		    G_NAME_Y = 'IMO:SkyMap:f' + bolo_ch + ':' + tail_y
		    DUMMY  = PIOGetValue( G_VALUE, gerror, g_type, gunit, gcomment, $
		                            g_name_y, imo_group)
		    Q = strpos( g_value, ':' , /reverse_s)
		    G_VALUE_READ = strmid( g_value, 0, q )
		    trans = pioread( g_value_read )
		    ;
		    G_NAME_YER = 'IMO:SkyMap:f' + bolo_ch + ':' + tail_yError
        DUMMY  = PIOGetValue( G_VALUE, gerror, g_type, gunit, gcomment, $
                                g_name_yEr, imo_group)
        ;stop
        Q = strpos( g_value, ':' , /reverse_s)
        G_VALUE_READ = strmid( g_value, 0, q )
        yerror = pioread( g_value_read )
        ;
        G_NAME_flag = 'IMO:SkyMap:f' + bolo_ch + ':' + tail_Flag
        DUMMY  = PIOGetValue( G_VALUE, gerror, g_type, gunit, gcomment, $
                                    g_name_flag, imo_group)
        Q = strpos( g_value, ':' , /reverse_s)
        G_VALUE_READ = strmid( g_value, 0, q )
        flg = pioread( g_value_read ) ; Wave number in cm^-1
        ;
		    IF KEYWORD_SET(FLAG) THEN BEGIN
				  ;IF (VERSION EQ 'v101') THEN BEGIN
          ;  ;	It is not possible to remove the flagged data from v101 as there was no CO interpolation flag present.
          ;  ;	Leave the data as-is as a result.
			    ;ENDIF ELSE BEGIN		;	IF ((VERSION EQ 'v201') OR (VERSION EQ 'v202')) THEN BEGIN
            ;
            noFlag = WHERE(flg EQ 0, Nnoflag)
            If Nnoflag GT 10 THEN BEGIN
              wn = wn[noFlag]
              trans = trans[noFlag]
              flg = flg[noFlag]
              yerror = yerror[noFlag]
            ENDIF
          ;ENDELSE		;	ENDIF
        ENDIF
        ;
		    BP_INFO[ n_detect + j ].FREQ = const.c *1d2 * wn ; freq in Hz
        FLG_INFO[ n_detect + j ].FREQ = const.c *1d2 * wn ; freq in Hz
        ER_INFO[ n_detect + j ].FREQ = const.c *1d2 * wn ; freq in Hz
        ;
        BP_INFO[ n_detect + j ].TRANS = trans
        FLG_INFO[ n_detect + j ].TRANS = flg
        ER_INFO[ n_detect + j ].TRANS = yerror
      Endfor
	    N_DETECT = n_detect + n_list
    Endfor
  Endif
  ;
  ;
  return, bp_info
END
