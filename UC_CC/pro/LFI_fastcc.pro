FUNCTION LFI_fastcc, freq, alpha, detector=detector, pair=pair,debug=debug
    ; Apply colour corrections to Planck LFI data
    ; freq should be one of 28.4, 44.1 or 70.4
	; detector (optional) should be between 18 and 28 inclusive
	; set debug to see debug messages
	; 
	; Version history:
	; Mike Peel   01-Feb-2013   v1.0 Initial version
       ; Mike Peel   04-Feb-2013   v1.1 Correct convention
       ; Locke Spencer 05-Feb-2013:  v1.2 Changed name to LFI_fastCC from planckcc (it only works for LFI)
       ;                             Changed nested IF..ELSE groups to case statements
       ;                             Changed invalid detector/frequency output to zero and removed internal stop in the code.
    
IF (keyword_set(detector)) THEN BEGIN
  IF (keyword_set(debug)) THEN print,'Using detector number ',detector
  CASE detector OF
  18: cc = [0.98836, 0.0123556, -0.00394]
  19: cc = [0.93933, 0.0375844, -0.00225]
  20: cc = [0.95663, 0.0285644, -0.00273]
  21: cc = [0.97140, 0.0209690, -0.00318]
  22: cc = [1.02220,-0.0077263, -0.00327]
  23: cc = [1.00098, 0.0029940, -0.00240]
  24: cc = [0.99571, 0.0053247, -0.00175]
  25: cc = [0.98988, 0.0082248, -0.00161]
  26: cc = [0.98557, 0.0107023, -0.00175]
  27: cc = [0.98513, 0.0129780, -0.00288]
  28: cc = [0.98516, 0.0134605, -0.00318]
  ELSE: BEGIN
    print,'Invalid detector specified for LFI_fastcc, returning zero.'
    return, 0d
  ENDELSE
  ENDCASE
ENDIF ELSE BEGIN ; individual detectors
  IF (keyword_set(debug)) THEN print,'Using frequency ',freq
  CASE freq OF
    28.4: cc = [0.98520, 0.0131778, -0.00302]
    44.1: cc = [0.99059, 0.0079600, -0.00169]
    70.4: cc = [0.98149, 0.0152737, -0.00325]
    ELSE: BEGIN
      print,'Invalid frequency specified for planckcc, returning zero.'
      return, 0d
    ENDELSE
  ENDCASE
ENDELSE
;
fastCC = cc[0] + cc[1]*alpha + cc[2]*alpha*alpha
RETURN, fastCC
;
END
