;
;   
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   A copy of the GNU General Public License is available at 
;   <http://www.gnu.org/licenses/>.
;   
;   Copyright Locke D. Spencer, 2013
;   
pro unit_conversion, nu_0, freq_d, trans_d, kcmb2mjysr, mjysr2tb, kcmb2ysz, BT=BT, TCMB=TCMB, $
                     ERR_D=ERR_D, GETERR=GETERR, NITER=NITER, ER_MJY=ER_kcmb2mjysr, ER_YSZ=ER_kcmb2ysz, MSG=MSG
;
;	The old unit conversions were as follows: kcmb2mjysr, krj2mjysr, krj2kcmb, kcmb2ysz, krj2ysz
;	The new unit conversion are as follows:   kcmb2mjysr, mjysr2tb, kcmb2ysz
;mkcmb2mjysr,mkcmb2mkrj,mkcmb2ysz <--- these are no longer included
;
;  Modified by L. Spencer 2013/02/05 -- Added the ability to generate coefficient uncertainties.

; -- Definitions
  CONST = { C: 2.99792458d+08, H: 6.6260755d-34, HBAR: 1.0545726691251021d-34, K: 1.3806580d-23, TCMB: 2.7255d0 }
IF KEYWORD_SET(TCMB) THEN CONST.TCMB = TCMB
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
IF N_ELEMENTS(BT) EQ 0 THEN BT = 1d-7
IF N_ELEMENTS(GETERR) EQ 0 THEN GETERR = 0
IF N_ELEMENTS(NITER) EQ 0 THEN NITER = 2 ; min number, ignored unless GETERR is set.
IF N_ELEMENTS(ERR_D) EQ 0 THEN ERR_D = DBLARR(N_ELEMENTS(freq_d))
IF N_ELEMENTS(MSG) Eq 0 THEN MSG = '' ELSE MSG = STRING(MSG)
;
ERR_D_orig = ERR_D
FREQ_D_orig = FREQ_D
TRANS_D_orig = TRANS_D
;
IF KEYWORD_SET(GETERR) THEN BEGIN
  ;
  IF N_ELEMENTS(ERR_D) LT N_ELEMENTS(trans_d) THEN BEGIN
    GETERR = 0 ; err_d is not valid
    MSG = [MSG,'GETERR was set, but ERR_D was invalid.  GETERR was set to zero and the uncertainties returned are set to zero.']
  ENDIF
  ER_kcmb2mjysr = 0d    ; set the error to zero to start.
  ER_kcmb2ysz = 0d ; set the error to zero to start.
ENDIF
;
posF = WHERE((freq_d GT 0d), Npos)
IF Npos GT 0 THEN BEGIN
  freq_d = freq_d[posF]
  trans_d = trans_d[posF]
  err_d = err_d[posF]
ENDIF
;
;	restrict to amplitudes greater than 1d-7
HighAmp = WHERE(trans_d GE BT, Ngood)
IF Ngood GT 0 THEN BEGIN
	trans_d = trans_d[HighAmp]
	freq_d = freq_d[HighAmp]
	err_d = err_D[HighAmp]
ENDIF
wn = freq_d/(const.c*1d2)
NUC = nu_0*1d9	;	Hz
NWN = N_ELEMENTS(wn)
Nu = freq_d ; Hz
Lam = const.C/Nu
Tcmb = const.TCMB	;	2.725d ; K
  ; dB/dT = (2hnu^3/c^2)(e^(hnu/kT))(1/(e(hnu/kT) - 1))^2(hnu/kT^2)
a = EXP(const.H*Nu/const.K/Tcmb)
b = 1d/(a - 1d)
c = (2d*const.H*Nu^3d)/(const.C^2d)
d = (2d*const.H^2d*Nu^4d)/(const.C^2d*const.K*Tcmb^2d)
e = (2d*Nu^2d*const.K)/(const.C^2d) ; This one is for the RJ dB/dT
f = a*d*b^2d*Tcmb*((const.H*Nu/const.K/Tcmb)*((a + 1d)*b) - 4d)
y0 = c*b  ; the Planck function
y1 = e        ; The RJ-Planck Temp. deriv.
y2 = a*d*b^2d ; The Planck Temperature deriv.
y3 = f          ;   The ySZ equation in units of DeltaI(Wm-2sr-1Hz-1)/ySZ
y4 = NUC/Nu
  ;IF (b_[0] EQ 0d) THEN b_[0] = 0d
spc = trans_d
  ;
;yNumRJ = y1;*spc
yNumCMB = y2;*spc
  ;*EFs[i] ; The Planck Func. times spec trans, times AOmega ---> output of integral is in W
  ;IntNum = INT_TABULATED(Nu, yNum, /DOUBLE);*1d17 ; ; Now in units of MJy/sr Hz / mKCMB
IntnumCMB = TSUM(Nu, yNumCMB*spc) ; .../KRJ
;IntnumRJ = TSUM(Nu, yNumRJ*spc) ; .../KCMB
  ;
yDenMJ = y4;*spc
yDenSZ = y3;*spc
  ;IntDen = INT_TABULATED(Nu, yDen, /DOUBLE)
IntDenMJ = TSUM(Nu, yDenMJ*spc)/1d20 ; .../MJy/sr
IntDenSZ = TSUM(Nu, yDenSZ*spc) ; .../ySZ
;IntDenCMB = IntNumCMB
  ;
UcCMBMJ = IntNumCMB/IntDenMJ    ;;;;;   MJy/sr/KCMB
;UcRJMJ = IntNumRJ/IntDenMJ      ;;;;;   MJy/sr/KRJ
;UcRJCMB = IntNumRJ/IntDenCMB    ;;;;;   KRJ/KCMB
UcCMBSZ = IntNumCMB/IntDenSZ    ;;;;;   ySZ/KCMB
;UcRJSZ = IntNumRJ/IntDenSZ      ;;;;;   ySZ/KRJ
;
UcMJTB = const.c^2d/2d20/NUC^2d/const.k	;;;;;	Should be K_RJ/(MJy/sr)
;
kcmb2mjysr = UcCMBMJ
mjysr2tb = UcMJTB
kcmb2ysz = UcCMBSZ
;
IF KEYWORD_SET(GETERR) THEN BEGIN
  ;  
  ;  Run NITER MC-realizations to get an uncertainty estimate of the coefficients provided.
  Nspec = N_ELEMENTS(trans_d)
  ;         
  ;  Set up some arrays for uncertainty
  UC_arr1 = DBLARR(NITER)  ;  The kcmb2mjysr array
  UC_arr2 = DBLARR(NITER)  ;  The kcmb2ysz array
  ;
  FOR ii = 0, NITER - 1 DO BEGIN
    ;
    noise_spec = RANDOMN(seed,Nspec)
    sp_ = spc + noise_spec*ERR_D
    ;
    IntnumCMB = TSUM(Nu, yNumCMB*sp_) ; .../KRJ
    IntDenMJ = TSUM(Nu, yDenMJ*sp_)/1d20 ; .../MJy/sr
    IntDenSZ = TSUM(Nu, yDenSZ*sp_) ; .../ySZ
    UC_arr1[ii] = IntNumCMB/IntDenMJ    ;;;;;   MJy/sr/KCMB
    UC_arr2[ii] = IntNumCMB/IntDenSZ    ;;;;;   ySZ/KCMB
    ;
  ENDFOR
  ;
  ER_kcmb2mjysr = STDEV(UC_arr1)
  ER_kcmb2ysz = STDEV(UC_arr2)
  ;
ENDIF
;
ERR_D = ERR_D_orig
FREQ_D = FREQ_D_orig
TRANS_D = TRANS_D_orig
;
return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;    INTTRANS = int_tabulated(freq_d, trans_d , /double)
;    TRANS_1 = trans_d / inttrans ; normalizing the transmission areas
;
;; Derivative of Planck function
;    NU_CMB = const.k * const.tcmb / const.h/ 1d9 ; 56.78011261545 GHz
;    ALPHA = 2d0 * const.k^3 * const.tcmb^2 / const.h^2 / const.c^2
;                                ; ALPHA = 9.9052896e-19 ** W Hz^-1 /m^2 / K
;                                ; (thermo - black body) = 9.9052896e7
;                                ; Jy/K_thermo
;;Computing the value
;    X = freq_d/nu_cmb
;    DB_DT = alpha * x^4d0 * exp( x ) / ( exp( x ) - 1d0 )^2d0
;    DB_DT_RJ = 2.0d0  * freq_d * 1d9* freq_d * 1d9* const.k/const.c^2.0
;    DB_Y = DB_DT * const.tcmb * (x *  (exp( x) + 1) / (exp ( x) - 1) - 4)
;
;; Conversion of thermodynamic temperature to flux (FIRAS convention) mkCMB2MJy (as DX4 delivered)
;      mkcmb2mjysr = 1d17 * int_tabulated( freq_d, db_dt * trans_1 , /double ) / $
;      int_tabulated( freq_d, nu_0 / freq_d * trans_1 , /double)
;; Conversion of thermodynamic temperature to flux (FIRAS convention) Kcmb_2_K_RJ (as DX4 delivered)
;      mkcmb2mkrj  =  int_tabulated( freq_d, db_dt * trans_1 , /double ) / $
;      int_tabulated( freq_d, db_dt_rj * trans_1 , /double)
;; Conversion of thermodynamic temperature to Compton Parameter ySZ
;        mkcmb2ysz =  1D-3 * $
;        int_tabulated( freq_d, db_dt * trans_1 , /double ) / $
;        int_tabulated( freq_d, db_y * trans_1 , /double)
;
; return
end

;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;   
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   A copy of the GNU General Public License is available at 
;   <http://www.gnu.org/licenses/>.
;   
;   Copyright Locke D. Spencer, 2013
;   
;
FUNCTION hfi_unit_conversion, bp_info=BP_INFO, channel, bolo_uc, lfi=lfi, CH_UC=CH_UC, ABP=ABP, EABP=EABP, AVG_UC=AVG_UC, BT=BT, TCMB=TCMB,$
                              BP_ERR=BP_ERR, GETERR=GETERR, NITER=NITER, ER_MJY=ER_MJY, ER_YSZ=ER_YSZ, MSG=MSG

;+
; NAME:
;
;   hfi_unit_conversion
;
; PURPOSE:
;
;   Compute unit conversions for the HFI channels accounting for the
;   spectral bandpass
;
; CATEGORY:
;
;
;
; CALLING SEQUENCE:
;
;    Result = hfi_unit_conversion( bp_info=bp_info, channel, bolo_uc, lfi=lfi, CH_UC=CH_UC, ABP=ABP, AVG_UC=AVG_UC)
;
; INPUTS:
;
;   bp_info :  structure containing the bandpass information (from hfi_read_bandpass script) (now an optional keyword input)
;   channel :  HFI channel to consider
;
; OPTIONAL INPUTS:
;
;
;
; KEYWORD PARAMETERS:
; 
;   INPUT:
;     BP_INFO:  see above, now this is optional however.
;     ABP :     structure containing the band-average bandpass information (from hfi_read_avg_bandpass script)
;     BT  :     The bandpass threshold, above which values are included in he integrations (default = 1d-7), 
;               i.e. tranmission values lower than BT are excluded from the integrals 
;
;   OUTPUT:
;     CH_UC :   Structure containing the average coefficients from each detector in the desired channel, the same as is output via the function return.
;     AVG_UC:   Structure containing the coefficients determined from the average spectra (i.e. not the average of the detector values).  This is also the default output of the function.
;
; OUTPUTS:
;
;   avg_uc      : structure containing the mean output unit conversion for the channel
;                 .chname:        The string name of the frequency channel.
;                 .kcmb2mjysr:    The K_CMB to MJy/sr conversion coefficient.
;                 .mjysr2tb:      The MJy/sr to Tb [K_RJ] conversion coefficient.
;                 .kcmb2ysz:      The K_CMB to y_SZ   conversion coefficient.
;   bolo_uc : structure containing the output unit conversion coefficients
;              for all bolometers in the input channel:
;                 .boloname:      The string name of the bolometer.
;                 .kcmb2mjysr:    The K_CMB to MJy/sr conversion coefficient.
;                 .mjysr2tb:      The MJy/sr to Tb [K_RJ] conversion coefficient.
;                 .kcmb2ysz:      The K_CMB to y_SZ   conversion coefficient.
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;   Planck collaboration only.
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;  bp = hfi_read_bandpass( /imo, LBLIMO='2_61')     ;   I select an older version intentionally.
;  abp = hfi_read_avg_bandpass(,/IMO, LBLIMO='2_61')
;  uc = hfi_unit_conversion(bp_info=bp,'100',bolo_uc, CH_UC=CH_UC, ABP=ABP, AVG_UC=AVG_UC)
;  help, uc, /str
;  nbolo =n_elements(bolo_uc)
;  for ibolo=0,nbolo-1 do print, bolo_uc[ibolo].boloname+':  ' +strtrim(bolo_uc[ibolo].kcmb2mjysr,2)
;  print, AVG_UC.chname+':  '+strtrim(AVG_UC.kcmb2mjysr,2)
;  print, CH_UC.chname+':  '+strtrim(CH_UC.kcmb2mjysr,2)
;
; ** Structure <a4e328>, 3 tags, length=32, data length=26, refs=5:
;   CHNAME      STRING    '100'
;   KCMB2MJYSR  DOUBLE    243.59881
;   MJYSR2TB    DOUBLE    xxxxxxxxx
;   KCMB2YSZ    DOUBLE    -0.24815717
;
;   00_100_1a:  238.67326
;   01_100_1b:  242.05680
;   20_100_2a:  244.10386
;   21_100_2b:  243.35771
;   40_100_3a:  246.06779
;   41_100_3b:  240.18871
;   80_100_4a:  246.62432
;   81_100_4b:  247.71806
;
;   100:  243.85909
;   100:  243.59881
;
; MODIFICATION HISTORY:
; Created by S. Hildebrandt and J.F. Macias-Perez, Oct, 2010
; Modified by L. Spencer August 31, 2011
;   Include conversion for the band-average spectra (not just averaging the detector coefficients)
;   Added additional conversion coefficients, the coefficients determined now are as follows:
;     K_CMB to MJy/sr
;     K_RJ  to MJy/sr 
;     K_RJ  to K_CMB 
;     K_CMB to y_SZ 
;     K_RJ  to y_SZ
;  Modified by L. Spencer 14 Jan. 2013
;    Revised the included coefficients following discussions with LFI.  Now we include:
;     K_CMB  to MJy/sr
;     MJy/sr to Tb [K_RJ] 
;     K_CMB  to y_SZ 
;  Modified by L. Spencer 18 Jan. 2013
;    Changed bp_info from input variable to a keyword, in case the RIMO file does not have detector spectra.
;    Added a check for zer-valued frequency data points in the passed spectra.
;  Modified by L. Spencer 05 Feb. 2013
;    Added MSG keyword to pass any debug comments to user (optional).
;    Added the ability to output uncertainty estimates for the coefficients.
;                                     - With GETERR=1 as an option, the NITER keyword was added to control the number of iterations used to determine the uncertainty (default of 10 iterations).
;                                     - Also added the bp_err keyoword to house the spectral uncertainty (same structure as bp_info).
;                                     - Also added the EABP keyoword to house the band-average spectral uncertainty (same structure as ABP).
;    
;
;-
  ;
  ; -- Definitions
  CONST = { C: 2.99792458d+08, H: 6.6260755d-34, HBAR: 1.0545726691251021d-34, K: 1.3806580d-23, TCMB: 2.725d0 }
  ;
  IF N_ELEMENTS(BT) EQ 0 THEN BT = 1d-7
  ;
  ;  Check if the GETERR keyword has been set, if so check that the required uncertainties are also set.
  ;
  IF N_ELEMENTS(NITER) EQ 0 THEN NITER = 2 ; default of 2 iterations if the number of iterations is not set, only valid if GETERR is set, otherwise only one calculation is done.
  IF N_ELEMENTS(GETERR) EQ 0 THEN GETERR = 0 ; set this to not get the errors.
  ERR_DETS = 0  ; internal flag to determine uncertainty of detector coefficients.
  ERR_BNDS = 0  ; internal flag to determine uncertainty of band-average coefficients.  
  IF KEYWORD_SET(GETERR) THEN BEGIN
    ;
    ERR_DETS = 1
    ERR_BNDS = 1
    ;  check if the uncertainties on the spectra are provided.
    IF N_ELEMENTS(BP_ERR) LT N_ELEMENTS(BP_INFO) THEN ERR_DETS = 0
    IF N_ELEMENTS(EABP) LT N_ELEMENTS(ABP) THEN ERR_BNDS = 0
    ;
  ENDIF
  ;
  ; --- Check channel name first
  if not keyword_set(lfi) then begin
    channels = ['100','143','217','353','545','857']
    nuc = [100.0d0, 143.0d0, 217.0d0,353.0d0,545.0d0,857.0d0]	;	GHz
  endif else begin
    channels =['30','44','70']
    ;nuc = [28.5d0, 44.1d0,70.3d0]
    nuc = [28.4d0, 44.1d0,70.4d0]
  endelse
  ;
  nchannels =  n_elements(channels)
  okchannel = 0
  ichwork = -1
  FOR ich = 0,nchannels-1 DO BEGIN
    IF (channel EQ channels[ich]) THEN BEGIN
      okchannel = 1
      ichwork = ich
    ENDIF
  ENDFOR
  IF (okchannel EQ 0) THEN BEGIN
    print, 'Wrong channel name. Available channels are '
    print,  channels
    return, -1
  ENDIF
  ;
  IF KEYWORD_SET(BP_INFO) THEN BEGIN
  ;
  ; Getting data from bandpass structure
  NAME   = bp_info.name      ; name of the detector
  FREQ   = bp_info.freq  ; frequencies measured in Hz
  TRANS  = bp_info.trans     ; values of the transmission (normalized later)
  ;
  IF KEYWORD_SET(ERR_DETS) THEN BEGIN
    NAME_E = bp_err.name
    FREQ_E = bp_err.freq
    TRANS_E = bp_err.trans ; I should check that this is on the same grid as the bp_info structure (should be, but may not be).
  ENDIF
  ;
  IF KEYWORD_SET(ERR_DETS) THEN ch_uc =  {chname:channel, kcmb2mjysr:0d, mjysr2tb:0d, kcmb2ysz:0d,erkcmb2mjysr:0d, erkcmb2ysz:0d} ELSE ch_uc = {chname:channel, kcmb2mjysr:0d, mjysr2tb:0d, kcmb2ysz:0d}
  IF KEYWORD_SET(ERR_BNDS) THEN avg_uc = {chname:channel, kcmb2mjysr:0d, mjysr2tb:0d, kcmb2ysz:0d,erkcmb2mjysr:0d, erkcmb2ysz:0d} ELSE avg_uc =  {chname:channel, kcmb2mjysr:0d, mjysr2tb:0d, kcmb2ysz:0d}
  ;
  ; GET Channel bolometer and create structure
  BOLO_LIST = get_hfibolo_list( CHANNEL = channel, LFI=LFI )
  N_BOLO_LIST = n_elements( bolo_list )
  ;
  IF KEYWORD_SET(ERR_DETS) THEN bolo_uc = {boloname:'', kcmb2mjysr:0d, mjysr2tb:0d, kcmb2ysz:0d,erkcmb2mjysr:0d, erkcmb2ysz:0d} ELSE bolo_uc = {boloname:'', kcmb2mjysr:0d, mjysr2tb:0d, kcmb2ysz:0d}
  bolo_uc = replicate(bolo_uc, n_bolo_list)
  bolo_uc.boloname = bolo_list
  ;
  NU_0 = nuc[ ichwork ]         ; in GHz
  For jdet = 0, n_bolo_list - 1 do begin
    Q_DET = where(bolo_list[ jdet ] eq name)
    Q_DET = q_det(0)
    ;stop
    Q_TRANS = where( trans( * , q_det ) gt BT )
    FREQ_D = freq( q_trans, q_det ) ; already in GHz
    TRANS_D = trans( q_trans, q_det )
    PosFreq = WHERE(FREQ_D GT 0d, Npos)
    FREQ_D = FREQ_D[PosFreq]
    TRANS_D = TRANS_D[PosFreq]
    ;
    IF KEYWORD_SET(ERR_DETS) THEN BEGIN
      Q_DET_E = where(bolo_list[ jdet ] eq name_E)
      Q_DET_E = q_det_E(0)
      FREQ_DE = freq_E( *, q_det_E ) ; already in Hz
      TRANS_DE = trans_E( *, q_det_E )
      ERR_D = INTERPOL(TRANS_DE, FREQ_DE, FREQ_D) ; now on same grid as the transmission spectrum.
    ENDIF
    ;
    unit_conversion, nu_0, freq_d, trans_d, kcmb2mjysr, mjysr2tb, kcmb2ysz, TCMB=TCMB, $
                     ERR_D=ERR_D, GETERR=ERR_DETS, NITER=NITER, ER_MJY=ER_MJY, ER_YSZ=ER_YSZ, MSG=MSG
    Bolo_uc[jdet].kcmb2mjysr = kcmb2mjysr
    bolo_uc[jdet].mjysr2tb = mjysr2tb
    bolo_uc[jdet].kcmb2ysz = kcmb2ysz
    ;
    IF KEYWORD_SET(ERR_DETS) THEN BEGIN
      Bolo_uc[jdet].erkcmb2mjysr = ER_MJY
      bolo_uc[jdet].erkcmb2ysz = ER_YSZ
    ENDIF
    ;
    ;;    INTTRANS = int_tabulated(freq_d, trans_d , /double)
    ;;     TRANS_1 = trans_d / inttrans ; normalizing the transmission areas
    
    ;; ; Derivative of Planck function
    ;;     NU_CMB = const.k * const.tcmb / const.h/ 1d9 ; 56.78011261545 GHz
    ;;     ALPHA = 2d0 * const.k^3 * const.tcmb^2 / const.h^2 / const.c^2
    ;;                                 ; ALPHA = 9.9052896e-19 ** W Hz^-1 /m^2 / K
    ;;                                 ; (thermo - black body) = 9.9052896e7
    ;;                                 ; Jy/K_thermo
    ;; ;Computing the value
    ;;     X = freq_d/nu_cmb
    ;;     DB_DT = alpha * x^4d0 * exp( x ) / ( exp( x ) - 1d0 )^2d0
    ;;     DB_DT_RJ = 2.0d0  * freq_d * 1d9* freq_d * 1d9* const.k/const.c^2.0
    ;;     DB_Y = DB_DT * const.tcmb * (x *  (exp( x) + 1) / (exp ( x) - 1) - 4)
    
    ;; ; Conversion of thermodynamic temperature to flux (FIRAS convention) mkCMB2MJy (as DX4 delivered)
    ;;       bolo_ucc[jdet].mkcmb2mjysr = 1d17 * int_tabulated( freq_d, db_dt * trans_1 , /double ) / $
    ;;       int_tabulated( freq_d, nu_0 / freq_d * trans_1 , /double)
    ;; ; Conversion of thermodynamic temperature to flux (FIRAS convention) Kcmb_2_K_RJ (as DX4 delivered)
    ;;       bolo_ucc[jdet].mkcmb2mkrj =  int_tabulated( freq_d, db_dt * trans_1 , /double ) / $
    ;;       int_tabulated( freq_d, db_dt_rj * trans_1 , /double)
    ;; ; Conversion of thermodynamic temperature to Compton Parameter ySZ
    ;;       bolo_ucc[jdet].mkcmb2ysz =  1D-3 * $
    ;;         int_tabulated( freq_d, db_dt * trans_1 , /double ) / $
    ;;         int_tabulated( freq_d, db_y * trans_1 , /double)
    ;
    ; RTS_DETECTORS (v41)
    bolo_uccaux = bolo_uc
    If (bolo_list[ jdet ] eq '70_143_8') OR ( bolo_list[ jdet ] eq '55_545_3' ) then BEGIN
	;	OR ( bolo_list[ jdet ] eq '74_857_4' ) ... removed for DX9 consistency.
      bolo_uccaux[ jdet ].kcmb2mjysr = -1.0d9
      bolo_uccaux[ jdet ].mjysr2tb = -1.0d9
      bolo_uccaux[ jdet ].kcmb2ysz = -1.0d9
      ;
      bolo_uc[jdet] = bolo_uccaux[jdet]
    ENDIF
  Endfor
  ;
  ; Computing an average (first approximation to map making)
  ;	FIXME: I do not like this as we should be calculating the coefficients from the average spectra, not averaging the coefficients.
  Q_D = where( bolo_uccaux.kcmb2mjysr NE (-1.0D9))
  ch_uc.kcmb2mjysr =  mean(bolo_uccaux[ q_d ].kcmb2mjysr )
  ch_uc.mjysr2tb =  mean(bolo_uccaux[ q_d ].mjysr2tb )
  ch_uc.kcmb2ysz =  mean(bolo_uccaux[ q_d ].kcmb2ysz )
  IF KEYWORD_SET(ERR_DETS) THEN BEGIN
    ch_uc.erkcmb2mjysr = MAX( [ SQRT(TOTAL( (bolo_uc[ q_d ].erkcmb2mjysr)^2d ))/(DOUBLE(N_ELEMENTS(bolo_uc[ q_d ].kcmb2mjysr))), $
                                STDEV(bolo_uc[ q_d ].kcmb2mjysr)/SQRT(DOUBLE(N_ELEMENTS(bolo_uc[ q_d ].kcmb2mjysr)))] ) 
  ; either take the individual uncertainties spread into the mean, or take the spread of the coefficients, whichever is greater.
    ch_uc.erkcmb2ysz = MAX( [ SQRT(TOTAL( (bolo_uc[ q_d ].erkcmb2ysz)^2d ))/(DOUBLE(N_ELEMENTS(bolo_uc[ q_d ].kcmb2ysz))), $
                              STDEV(bolo_uc[ q_d ].kcmb2ysz)/SQRT(DOUBLE(N_ELEMENTS(bolo_uc[ q_d ].kcmb2ysz)))] ) 
  ; either take the individual uncertainties spread into the mean, or take the spread of the coefficients, whichever is greater.
    ;
  ENDIF
  ;
  ENDIF ELSE BEGIN
    ;
    ;
    ;
    IF KEYWORD_SET(ERR_DETS) THEN ch_uc = {chname:channel, kcmb2mjysr:0d, mjysr2tb:0d, kcmb2ysz:0d, erkcmb2mjysr:0d,erkcmb2ysz:0d} ELSE ch_uc = {chname:channel, kcmb2mjysr:0d, mjysr2tb:0d, kcmb2ysz:0d}
    IF KEYWORD_SET(ERR_BNDS) THEN avg_uc = {chname:channel, kcmb2mjysr:0d, mjysr2tb:0d, kcmb2ysz:0d, erkcmb2mjysr:0d,erkcmb2ysz:0d} ELSE avg_uc = {chname:channel, kcmb2mjysr:0d, mjysr2tb:0d, kcmb2ysz:0d}
    ;
    ; GET Channel bolometer and create structure
    BOLO_LIST = get_hfibolo_list( CHANNEL = channel, LFI=LFI )
    ;
    N_BOLO_LIST = n_elements( bolo_list )
    bolo_uc = {boloname:'', kcmb2mjysr:0d, mjysr2tb:0d, kcmb2ysz:0d}
    bolo_uc = replicate(bolo_uc, n_bolo_list)
    bolo_uc.boloname = bolo_list
    ;
    NU_0 = nuc[ ichwork ]         ; in GHz
    For jdet = 0, n_bolo_list - 1 do begin
      ;
      ; RTS_DETECTORS (v41)
      bolo_uccaux = bolo_uc
      If (bolo_list[ jdet ] eq '70_143_8') OR ( bolo_list[ jdet ] eq '55_545_3' ) then BEGIN
  	;	OR ( bolo_list[ jdet ] eq '74_857_4' ) ... removed for DX9 consistency.
        bolo_uccaux[ jdet ].kcmb2mjysr = -1.0d9
        bolo_uccaux[ jdet ].mjysr2tb = -1.0d9
        bolo_uccaux[ jdet ].kcmb2ysz = -1.0d9
        ;
        bolo_uc[jdet] = bolo_uccaux[jdet]
      ENDIF
    Endfor
    ;
    ; Computing an average (first approximation to map making)
    ;	FIXME: I do not like this as we should be calculating the coefficients from the average spectra, not averaging the coefficients.
    Q_D = where( bolo_uccaux.kcmb2mjysr NE (-1.0D9))
    ch_uc.kcmb2mjysr =  mean(bolo_uccaux[ q_d ].kcmb2mjysr )
    ch_uc.mjysr2tb =  mean(bolo_uccaux[ q_d ].mjysr2tb )
    ch_uc.kcmb2ysz =  mean(bolo_uccaux[ q_d ].kcmb2ysz )  ;  The above few lines will just be empty as there are no detector spectra provided.
    ;
  ENDELSE
  ;
  ;   Now calculate the coefficients for the average spectra
  IF KEYWORD_SET(abp) THEN BEGIN    ; abp is the average spectra for the given channel.
    trans_b = abp[ichwork].trans
    freq_b = abp[ichwork].freq
    PosFreq = WHERE(FREQ_b GT 0d, Npos)
    FREQ_b = FREQ_b[PosFreq]
    TRANS_b = TRANS_b[PosFreq]
    ;stop
    Q_TRANS = where( trans_b gt BT ) ; This is done later just in case the CO line falls at a low amplitude...
    FREQ_b = freq_b[q_trans] ; already in Hz
    TRANS_b = trans_b[q_trans]
    ;
    IF KEYWORD_SET(ERR_BNDS) THEN BEGIN
      FREQ_BE = EABP[ichwork].freq
      TRANS_BE = EABP[ichwork].trans ; I should check that this is on the same grid as the bp_info structure (should be, but may not be).
      ERR_b = INTERPOL(Trans_BE, FREQ_BE, FREQ_b)
    ENDIF
    ;
    unit_conversion, nu_0, freq_b, trans_b, kcmb2mjysr, mjysr2tb, kcmb2ysz, TCMB=TCMB, $
                     ERR_D=ERR_D, GETERR=ERR_BNDS, NITER=NITER, ER_MJY=ER_MJY, ER_YSZ=ER_YSZ, MSG=MSG
    avg_uc.kcmb2mjysr = kcmb2mjysr
    avg_uc.mjysr2tb = mjysr2tb
    avg_uc.kcmb2ysz = kcmb2ysz
    IF KEYWORD_SET(ERR_BNDS) THEN BEGIN
      avg_uc.erkcmb2mjysr = ER_MJY
      avg_uc.erkcmb2ysz = ER_YSZ
    ENDIF
    ;return, avg_uc	;	ch_uc
  ENDIF ELSE BEGIN
    avg_uc = 0d
  ENDELSE
  return, avg_uc	;	ch_uc
END