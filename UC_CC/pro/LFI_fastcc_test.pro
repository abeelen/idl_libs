; Test code for planckcc
; Run with @planckcc_test -- changed this to a procedure (see below) so now it runs as `LFI_fastcc_test'.
; 
; Version history:
; Mike Peel   01-Feb-2013   v1.0 Initial version
; Mike Peel   04-Feb-2013   v1.1 Update format
; Locke Spencer 05-Feb-2013:  v1.2 changed planckcc to LFI_fastcc within this code to follow changes to LFI_fastcc
;                             renamed this routine LFI_fastcc_test, from planckcc_test, to follow convention of other changes.
;                             changed this routine from a script to a procedure so that it can be included within the hfi_lfi_test_script example routine also.
;
PRO LFI_fastcc_test

spectra = [-2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0]

print,'Detector	alpha',spectra
print,'LFI-18',LFI_fastcc(70.4,spectra,detector=18)
print,'LFI-19',LFI_fastcc(70.4,spectra,detector=19)
print,'LFI-20',LFI_fastcc(70.4,spectra,detector=20)
print,'LFI-21',LFI_fastcc(70.4,spectra,detector=21)
print,'LFI-22',LFI_fastcc(70.4,spectra,detector=22)
print,'LFI-23',LFI_fastcc(70.4,spectra,detector=23)
print,'70GHz',LFI_fastcc(70.4,spectra)

print,'LFI-24',LFI_fastcc(44.1,spectra,detector=24)
print,'LFI-25',LFI_fastcc(44.1,spectra,detector=25)
print,'LFI-26',LFI_fastcc(44.1,spectra,detector=26)
print,'44GHz',LFI_fastcc(44.1,spectra)

print,'LFI-27',LFI_fastcc(28.4,spectra,detector=27)
print,'LFI-28',LFI_fastcc(28.4,spectra,detector=28)
print,'30GHz',LFI_fastcc(28.4,spectra)


;STOP

END