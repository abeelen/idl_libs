;+
; NAME:
;
;   hfi_read_bandpass
;
; PURPOSE:
;
;   Read HFI bandpass info from official IMO/RIMO files
;
; CATEGORY:
;
;
;
; CALLING SEQUENCE:
;
;   bp = hfi_read_bandpass(+Keywords)
;
; INPUTS:
;
;				If the version string is different from the above, then the LBL_IMO and IN_PATH keywords should be set.
;				For the RIMO input file option, the version setting is superseded by the NAME_RIMO keyword (if set).
;				The version input is included for legacy use, and is depricated by the PATH_IMO and LBL_IMO keywords.  
;				Use of the PATH_IMO and LBL_IMO keywords is preffered. 
;
; OPTIONAL INPUTS:
;
;   None
;
; KEYWORD PARAMETERS:
;  INPUT:
;   IMO:          If an IMO label is to be read. 
;   LBL_IMO:      The IMO version, in the format 'x_yy', to be used (default 3_20_detilt_t2_ptcor6).
;   PATH_IMO:     The path to use for the IMO, default is '/data/dmc/MISS03/METADATA'
;   RIMO: If a RIMO fits file is to read
;   PATH_RIMO:    The file path to the RIMO fits file
;   NAME_RIMO:    The file name for the RIMO fits file, including the .fits suffix.
;	  FLAG : If set to 1, it will remove the over-sampled CO points from the spectra.
;			This is recommended for unit conversion and colour correction, but is not recommended for the CO correction 
;			(i.e. keep the CO-interpolated, over-sampled, data in).
;	 OUTPUT:
;	  FLG_INFO:     The same info structure as BP_INFO, but with the CO-interpolation flag values set in place of the transmission values.
;	  ER_INFO:      The same info structure as BP_INFO, but with the uncertainty values in place of the transmission values.
;
;
; OUTPUTS:
;
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;    Planck collaboration only.
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;	bp = hfi_read_bandpass( /IMO, LBL_IMO='2_67')		;	This reads the spectra from IMO_2_67
;	bp = hfi_read_bandpass(/RIMO, PATH_RIMO='/path/to/RIMO/fits/file/',NAME_RIMO='RIMO_fileNAME.fits')	;	Gets the spectra from the RIMO .fits file in RIMO_PATH
;	bp = hfi_read_bandpass(/IMO, LBL_IMO='2_67',PATH_IMO='/data/dmc/MISS01/METADATA')	;	Gets the data from MISS01 as specified.
;	bp = hfi_read_bandpass(/IMO, ER_INFO=er)					;	Restores the spectral transmission uncertainty as well as the spectral transmission.
;	bp = hfi_read_bandpass(/IMO, /FLAG, FLG_INFO=flg)					;	Removes the interpolated points (around the CO regions) from the restored spectra.
;	                                                            flg contains the flag data in place of transmission (all flagged points have been removed however.)
; bp = hfi_read_bandpass(/IMO, FLG_INFO=flg)         ; Removes the interpolated points (around the CO regions) from the restored spectra.
;                                                    ; flg contains the flag data in place of transmission (including flagged data this time).
;	                                                          
;
; MODIFICATION HISTORY:
;
;   Created by S. Hildebrandt and J.F. Macias-Perez, Oct, 2010
;	Added the v202 save file, and the call to a more recent IMO by L. Spencer June 29, 2011.
;	Added the LBLIMO keyword to call a more current IMO version by L. Spencer Aug. 30, 2011.
;	Added the ERR keyword which restores the unctertainty in the spectrum rather than the spectrum itself by L. Spencer Aug. 30, 2011.
;	Changed defaults to better reflect DX9 products -- L. Spencer 2012/09/17.
;      Switched the default setting for the FLAG keyword. -- L. Spencer 2013/01/24.
;   
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   A copy of the GNU General Public License is available at 
;   <http://www.gnu.org/licenses/>.
;   
;   Copyright Locke D. Spencer, 2013
;   
;-

FUNCTION hfi_read_bandpass, IMO = IMO, LBL_IMO=LBL_IMO, PATH_IMO=PATH_IMO, $
  RIMO = RIMO, PATH_RIMO=PATH_RIMO, NAME_RIMO=NAME_RIMO, $
	FLAG=FLAG, FLG_INFO=FLG_INFO, ER_INFO=ER_INFO
  ;
  ; -- Definitions
  CONST = { C: 2.99792458d+08, H: 6.6260755d-34, HBAR: 1.0545726691251021d-34, K: 1.3806580d-23, TCMB: 2.7255d0 }
  ;
  N_DETECT = 52 ; Planck HFI
  ;
  N_MEASUREMENTS = 17000l
  ;
  IF N_ELEMENTS(FLAG) EQ 0 THEN FLAG = 1 ; the default flag setting is 1, i.e. remove the CO points, explicitly set FLAG=0 to include them.
  ;
  ;IF N_ELEMENTS(VERSION) EQ 0 THEN VERSION = ''
  ;Case VERSION of	;	Just in case this may change in the future...
  ;  'v101': N_MEASUREMENTS =  17000l
  ;  'v201': N_MEASUREMENTS = 17000l
  ;  'v202': N_MEASUREMENTS = 17000l
  ;ELSE: N_MEASUREMENTS = 17000l
  ;Endcase
  ;
  BP_INFO = replicate( { NAME: '', FREQ: dblarr( n_measurements), TRANS: dblarr( n_measurements) }, n_detect )
  FLG_INFO = BP_INFO
  ER_INFO = FLG_INFO
  If Keyword_set( RIMO ) then begin
    If ~Keyword_set( PATH_RIMO ) then PATH_RIMO =''	;	This assumes the RIMO fits file is in the currenty workign IDL directory.
    IF N_ELEMENTS(NAME_RIMO) EQ 0 THEN RIMO_FITS = 'HFI-RIMO-20120618.fits' ELSE RIMO_FITS = NAME_RIMO	;	'HFI-RIMO-20110617.fits'
    ;IF ~KEYWORD_SET(NAME_RIMO) THEN RIMO_FITS = 'HFI-RIMO-20110617.fits' ELSE RIMO_FITS = NAME_RIMO
    ;FITS_RIMO = mrdfits( path_rimo + rimo_fits, 'CHANNEL PARAMETERS', HDR_RIMO )
    ;  The above line works for earlier RIMO files, but not for later ones, I'll change it to work with current at the cost of the older.
    FITS_RIMO = mrdfits( path_rimo + rimo_fits, 'DET_PARAMS', HDR_RIMO, /SILENT )
    IF N_ELEMENTS(FITS_RIMO) LT 2 THEN FITS_RIMO = mrdfits( path_rimo + rimo_fits, 'CHANNEL PARAMETERS', HDR_RIMO, /SILENT )
    ;  They both may not work, but for now I assume either the current, or the former RIMO file convention.
    FITS_RIMO_1 = FITS_RIMO
    ;If strpos( hdr_rimo[ 4 ], '52' ) eq -1 then stop,'Is it an HFI RIMO file?'
    BOLO_ID_TMP = [ '00_100_1a', '01_100_1b', '20_100_2a', '21_100_2b', '40_100_3a', '41_100_3b', $
                  '80_100_4a', '81_100_4b', '02_143_1a', '03_143_1b', '30_143_2a', '31_143_2b', $
                  '50_143_3a', '51_143_3b', '82_143_4a', '83_143_4b', '10_143_5',  '42_143_6',  $
                  '60_143_7',  '70_143_8',  '11_217_5a', '12_217_5b', '43_217_6a', '44_217_6b', $
                  '61_217_7a', '62_217_7b', '71_217_8a', '72_217_8b', '04_217_1',  '22_217_2',  $
                  '52_217_3',  '84_217_4',  '23_353_3a', '24_353_3b', '32_353_4a', '33_353_4b', $
                  '53_353_5a', '54_353_5b', '63_353_6a', '64_353_6b', '05_353_1',  '13_353_2',  $
                  '45_353_7',  '85_353_8',  '14_545_1',  '34_545_2',  '55_545_3',  '73_545_4',  $
                  '25_857_1',  '35_857_2',  '65_857_3',  '74_857_4' ]
    IF N_ELEMENTS(FITS_RIMO) LT 2 THEN BEGIN
      print, 'This RIMO file does not have detector spectra included, please run hfi_read_avg_bandpass to obtain the band-average spectra.'
    ENDIF ELSE BEGIN
    For i = 0, 51 do begin
      NAME_RIMO_TMP = strmid( fits_rimo[ i ].detector, 0, 3 ) + '_' + strmid( fits_rimo[ i ].detector, 4  )
      Q_NAME = where( strmid( bolo_id_tmp, 3) eq name_rimo_tmp )
      BP_INFO[ i ].NAME = bolo_id_tmp[ q_name[ 0 ] ]  ; completing the name of the detector (e.g. 100-1a in the RIMO)
      FLG_INFO[i].name = BP_INFO[i].name
      ER_INFO[i].name = BP_INFO[i].name
    Endfor
    ; Joining the information of the bandpasses in the bp_info structure
    For i = 0, n_detect - 1 do begin
      ;
      ;FITS_RIMO = mrdfits( path_rimo + rimo_fits, fitsshift + i )
      ;stop
      FITS_RIMO = mrdfits( path_rimo + rimo_fits, 'BANDPASS_'+fits_RIMO_1[i].detector, hdri, /SILENT);fitsshift + i )
      Nms = TAG_NAMES(FITS_RIMO)
      Ns = N_TAGS(FITS_RIMO)
      NmStr = ''
      FOR ii = 0, Ns - 1 DO NmStr = NmStr+Nms[ii]
      FND_FLG = MAX([STRPOS(NmStr,'FLAG'),STRPOS(NmStr,'flag'),STRPOS(NmStr,'Flag')])
      FND_Er  = MAX([STRPOS(NmStr,'UNCERTAINTY'),STRPOS(NmStr,'uncertainty'),STRPOS(NmStr,'Uncertainty')])
      ;
      wn = fits_rimo.wavenumber
      trans = fits_rimo.transmission
      IF FND_FLG GE 0 THEN BEGIN
        flg = fits_rimo.flag
        flgF = WHERE(flg EQ 'F', NF, COMPLEMENT=flgT, NCOMPLEMENT=NT)
        flgInt = INTARR(N_ELEMENTS(flg))
        IF NF GT 0 THEN flgInt[flgF] = 0
        IF NT GT 0 THEN flgInt[flgT] = 1
        flg = flgInt
      ENDIF
      IF FND_ER GE 0 THEN yer = fits_rimo.uncertainty
      IF KEYWORD_SET(FLAG) THEN BEGIN
        IF FND_FLG GE 0 THEN BEGIN
          noFlag = WHERE(flg EQ 0, Nnoflag)
          ;stop
          If Nnoflag GT 10 THEN BEGIN
            wn = wn[noFlag]
            trans = trans[noFlag]
            flg = flg[noFlag]
            IF FND_ER Ge 0 THEN yer = yer[noFlag]
          ENDIF
        ENDIF ; If flag was not found then the flagged data have already been removed, so this is fine...
      ENDIF ; If FLAG was not set then leave the flagged data (if any) in.
      ;
      ;stop
      BP_INFO[ i ].FREQ  = wn * 1d2 * const.c ; wave number given in cm^-1
      BP_INFO[ i ].TRANS = trans  ; values of the transmission (normalized later)
      FLG_INFO[i].FREQ = BP_INFO[i].freq
      IF FND_FLG GE 0 THEN FLG_INFO[i].TRANS = flg
      ER_INFO[i].freq = BP_INFO[i].freq
      IF FND_ER GE 0 THEN ER_INFO[i].trans = yer
      ;
    Endfor
    ENDELSE
    ; IF KEYWORD_SET(FLAG) THEN BP_INFO = FLG_INFO
    ;stop
  ENDIF   ; End the RIMO part...
  ;
  ; IMO
  ;
  IF Keyword_set( IMO ) THEN BEGIN
    IF KEYWORD_SET(LBL_IMO) THEN IMO_LBL = LBL_IMO ELSE IMO_LBL = '3_20_detilt_t2_ptcor6'	;	'2_67'
    IF KEYWORD_SET(PATH_IMO) THEN ImoFileMD = PATH_IMO ELSE ImoFileMD = '/data/dmc/MISS03/METADATA'
    ImoFile = ImoFileMD + '%lbl:IMO_' + imo_lbl
    ; Open Imo
    IMO_GROUP = PIOOpenIMOFile(ImoFile, 'r')
    print,'IMO_GROUP: ',imo_group
    IF imo_group LE 0 THEN pioerrmess, imo_group
    ; Joining the information of the bandpasses in the bp_info structure
    G_TYPE = 'PIOSTRING'
    TAIL_X = 'SpectralResp:SpecTransmissions:VectX'
    Tail_Flag = 'SpectralResp:SpecTransmissions:VectYFlag'
    Tail_Y = 'SpectralResp:SpecTransmissions:VectY'
    Tail_YError = 'SpectralResp:SpecTransmissions:VectYError'
    ;IF KEYWORD_SET(ERR) THEN Tail_Y = Tail_YError 		;	= 'SpectralResp:SpecTransmissions:VectYError'
    CHANNEL_TMP = [ '100', '143', '217', '353', '545', '857' ]
    N_DETECT = 0
    For i = 0, 5 do begin
		  BOLO_LIST = get_hfibolo_list( CHANNEL = channel_tmp[ i ] )
	    N_LIST = n_elements( bolo_list )
      For j = 0, n_list - 1 DO BEGIN
        BP_INFO[ n_detect + j ].NAME = bolo_list[ j ]
        FLG_INFO[n_detect + j].NAME = bolo_list[ j ]
        ER_INFO[n_detect + j].NAME = bolo_list[ j ]
        G_NAME_X = 'IMO:HFI:DET:Phot_Pixel Name="' + bolo_list[ j ]  + '":' + tail_x
        DUMMY  = PIOGetValue( G_VALUE, gerror, g_type, gunit, gcomment, $
		                            g_name_x, imo_group)
		    ;stop
		    Q = strpos( g_value, ':' , /reverse_s)
		    G_VALUE_READ = strmid( g_value, 0, q )
		    WN = pioread( g_value_read ) ; Wave number in cm^-1
		    G_NAME_Y = 'IMO:HFI:DET:Phot_Pixel Name="' + bolo_list[ j ]  + '":' + tail_y
		    DUMMY  = PIOGetValue( G_VALUE, gerror, g_type, gunit, gcomment, $
		                            g_name_y, imo_group)
		    Q = strpos( g_value, ':' , /reverse_s)
		    G_VALUE_READ = strmid( g_value, 0, q )
		    trans = pioread( g_value_read )
		    ;print, N_ELEMENTS(WN)
		    ;
		    ;   FIXME: must check that uncertainty is present in the IMO....(what happens if it is not?)
		    G_NAME_YER = 'IMO:HFI:DET:Phot_Pixel Name="' + bolo_list[ j ]  + '":' + tail_yError
        DUMMY  = PIOGetValue( G_VALUE, gerror, g_type, gunit, gcomment, $
                                g_name_yEr, imo_group)
        ;stop
        Q = strpos( g_value, ':' , /reverse_s)
        G_VALUE_READ = strmid( g_value, 0, q )
        yerror = pioread( g_value_read )
        ;
		    G_NAME_flag = 'IMO:HFI:DET:Phot_Pixel Name="' + bolo_list[ j ]  + '":' + tail_Flag
        DUMMY  = PIOGetValue( G_VALUE, gerror, g_type, gunit, gcomment, $
                                    g_name_flag, imo_group)
        Q = strpos( g_value, ':' , /reverse_s)
        G_VALUE_READ = strmid( g_value, 0, q )
        flg = pioread( g_value_read ) ; vs. Wave number in cm^-1
        ;
	IF KEYWORD_SET(FLAG) THEN BEGIN
		noFlag = WHERE(flg EQ 0, Nnoflag)
		;stop
		If Nnoflag GT 10 THEN BEGIN
		      	wn = wn[noFlag]
		      	trans = trans[noFlag]
		      	flg = flg[noFlag]
		      	yerror = yerror[noFlag]
		ENDIF
		;ENDELSE		;	ENDIF
        ENDIF
        ;print, N_ELEMENTS(WN)
        BP_INFO[ n_detect + j ].FREQ = const.c *1d2 * wn ; freq in Hz
        FLG_INFO[ n_detect + j ].FREQ = const.c *1d2 * wn ; freq in Hz
        ER_INFO[ n_detect + j ].FREQ = const.c *1d2 * wn ; freq in Hz
		    ;
		    BP_INFO[ n_detect + j ].TRANS = trans
		    FLG_INFO[ n_detect + j ].TRANS = flg
		    ER_INFO[ n_detect + j ].TRANS = yerror
      Endfor
	    N_DETECT = n_detect + n_list
    Endfor
  Endif   ;   End of the IMO section


  return, bp_info
END