function planck_rad_public, Ttemp, nu_or_lambda, dBdT, units=units, mjy=mjy, wcm2=wcm2
; SRH: Modified to work with arrays of Ttemp (so with arrays of nu_or_lambda and Ttemp)
;+NAME/ONE LINE DESCRIPTION OF ROUTINE:
;     PLANCK returns the spectral radiance of a blackbody.
;
;DESCRIPTION:  
;    IDL function to return the spectral radiance of a blackbody,
;    i.e. the Planck curve, in units of either MJy/steradian (I_nu)
;    or watts/cm^2/steradian (nu_I_nu).
;    The blackbody temperature and either frequency (in icm or GHz)
;    or wavelength (in microns) are inputs to the function.  The
;    routine also optionally returns the derivative with respect to 
;    temperature, in units of MJy/sr/K or W/cm^2/sr/K.
;
;CALLING SEQUENCE:  
;     RESULT = PLANCK (temperature, nu_or_lambda [,dBdT] $
;              [,UNITS=units], [/MJY], [/WCM2])
;
;ARGUMENTS (I = input, O = output, [] = optional):
;     RESULT        O   flt [arr]  Spectral radiance at each wavelength. 
;                                  Units: W/cm^2/sr/K if /WCM2 specified
;                                         MJy/sr      if /MJY specfied
;     TEMPERATURE   I   flt        Temperature of blackbody, in K.
;     NU_OR_LAMBDA  I   flt        Frequency or wavelength at which to 
;                                  calculate spectrum. Units are as 
;                                  specified with UNITS keyword.
;     dBdT         [O]  flt [arr]  Derivative of Planck with respect to 
;                                  temperature. 
;     UNITS        [I]  str        'Microns', 'icm', or 'GHz' to 
;                                  identify units of NU_OR_LAMBDA. Only 
;                                  first character is required.  If 
;                                  left out, default is 'microns'.
;     /MJY          I   key        Sets output units to MJy/sr
;     /WCM2         I   key        Sets output units to W/cm^2/sr
;
;WARNINGS:
;     1.  One of /MJY or /WCM2 MUST be specified.  
;     2.  Routine gives incorrect results for T < 1 microKelvin and
;            wavelengths shortward of 1.e-10 microns.
;
;EXAMPLE:
;     To produce a 35 K spectrum in MJy/sr at 2, 4, 6, 8, 10 microns:
;
;       wavelength = 2. + 2.*findgen(5)
;       temp = 35.
;       blackbody = planck(temp, wavelength, units='micron', /mjy)
;
;     One could also get back the derivative by including it in the
;     call:
;       blackbody = planck(temp, wavelength, deriv, units='m', /mjy)
;#
;COMMON BLOCKS:
;     None
;
;PROCEDURE (AND OTHER PROGRAMMING NOTES): 
;     Identifies units using the UNITS keyword, then converts the 
;     supplied independent variable into microns to evaluate the 
;     Planck function.  Uses Rayleigh-Jeans and Wien approximations 
;     for the low- and high-frequency end, respectively.  Reference: 
;     Allen, Astrophysical Quantities, for the Planck formula.
;
;PERTINENT ALGORITHMS, LIBRARY CALLS, ETC.:
;     None
;  
;MODIFICATION HISTORY:
;    Written by Rich Isaacman, General Sciences Corp.  17 April 1991
;    Revised by RBI 27 Jan 1992 to use updated fundamental constants 
;         (SPR 9449)
;    Revised by RBI 29 Jan 1992 to calculate derivatives only when 
;         necessary
;    Revised by RBI 3 Feb 1992 to redimension output to a scalar if only 
;       a single wavelength is supplied  (SPR 9459)
;    Revised by RBI 6 Mar 92 to return either MJy/sr or (!) W/cm^2/sr
;    Revised by RBI 1 Jun 92 to fix single-wavelength failure when no
;       derivative is requested (SPR 9738), and to use MESSAGE.
;    RBI corrected error in derivative calculation SPR 9817 (17 Jul 92)
;    RBI corrected error in Wien and RJ tails SPR 10392 (24 Dec 92)
;	 but didn't get it quite right (Piper/Kryszak, 28-Dec-92)
; July 2012 (Sergi R. Hildebrandt): Just added "_public" in order to ensure correct file is compiled.
;
; SPR 9616
;.TITLE
; Routine PLANCK
;-
;
; Check on input parameters
;
on_error, 2
if n_elements(nu_or_lambda) lt 1 or n_elements(Ttemp) lt 1 then $
     message,'CALLING SEQUENCE: bbflux = planck (temp,wavelength,units=<units>)'
if not keyword_set(mjy) and not keyword_set(wcm2) then $
     message, 'Either /MJy or /Wcm2 must be specified!'
if keyword_set(mjy) and keyword_set(wcm2) then $
     message, 'Only one of /MJy or /Wcm2 may be specified!'
;
makederiv = n_params() eq 3           ; see whether dBdT is requested
if n_elements(units) lt 1 then begin
   units = 'm'
   message, /continue, "Wavelength units are assumed to be microns."
endif
T = float(ttemp) > 1.e-06                ;force temperature to be real*4
; SRH -arrays of T
T = ttemp
;
; Define some necessary constants
;
c = 299792.458d0                         ;speed of light, Physics Today Aug 90
hck = 14387.69d0                         ;h*c/k             "      "
thcc = 1.1910439d0                       ;2*h/c^2           "     "  
coeff = thcc/hck^4 * 1.e04               ;Stephan-Boltzmann * 15/pi^5
;
; Convert nu_or_lambda into lambda (in microns) depending on specified units
;
units0 = strupcase (strmid (units,0,1))
case units0 of
   'M':   lambda = nu_or_lambda > 1.e-10                ; microns
   'I':   lambda = 1.e04 / (nu_or_lambda > 1.e-10)      ; icm, avoiding nu=0.
   'G':   lambda = c / (nu_or_lambda > 1.e-10)          ; GHz, avoiding nu=0.
   else:  message, "Units must be specified as 'microns', 'icm', or 'GHz'"
endcase
; 
;  Variable fhz is a scale factor used to go from units of nu_I_nu units to 
;  MJy/sr if the keyword is set.  In that case, its value is
;  frequency in Hz * w/cm^2/Hz ==> MJy
;
if keyword_set(wcm2) then fhz = 1. + fltarr(n_elements(lambda))
if keyword_set(mjy) then fhz = c/lambda * 1.e-15         
;
;  Introduce dimensionless variable chi, used to check whether we are on 
;  Wien or Rayleigh Jeans tails
;
chi = hck / lambda / T
val = fltarr(n_elements(chi))
if makederiv then dBdT = fltarr(n_elements(chi))
;
;  Start on Rayleigh Jeans side
;
rj = where (chi lt 0.001)
if rj(0) ne -1 then begin
    val(rj) = coeff * T^4 * chi(rj)^3 / fhz(rj)
    if makederiv then dBdT(rj) = val(rj) / T
endif
;
;  Now do nonapproximate part
;
exact = where (chi ge 0.001 and chi le 50)
if exact(0) ne -1 then begin
    chi_ex = chi(exact)
    val(exact) = coeff * T^4 * chi_ex^4 / (exp(chi_ex) - 1.) / fhz(exact)
    if makederiv then dBdT(exact) = $
	val(exact) * chi_ex / T / (1. - exp(-chi_ex))
endif
;
;  ...and finally the Wien tail
;
wien = where (chi gt 50.)
if wien(0) ne -1 then begin
    chi_wn = chi(wien)
    val(wien) = coeff * T^4 * chi_wn^4 * exp(-chi_wn) / fhz(wien)
    if makederiv then dBdT(wien) = $
	val(wien) * chi_wn / T / (1. - exp(-chi_wn))
endif
;
;  Redimension to a scalar if only 1 wavelength supplied, then return
;
if n_elements(nu_or_lambda) eq 1 then begin
    if makederiv then dBdT = dBdT(0)
    val = val(0)
endif
return, val
;
end
;DISCLAIMER:
;
;This software was written at the Cosmology Data Analysis Center in
;support of the Cosmic Background Explorer (COBE) Project under NASA
;contract number NAS5-30750.
;
;This software may be used, copied, modified or redistributed so long
;as it is not sold and this disclaimer is distributed along with the
;software.  If you modify the software please indicate your
;modifications in a prominent place in the source code.  
;
;All routines are provided "as is" without any express or implied
;warranties whatsoever.  All routines are distributed without guarantee
;of support.  If errors are found in this code it is requested that you
;contact us by sending email to the address below to report the errors
;but we make no claims regarding timely fixes.  This software has been 
;used for analysis of COBE data but has not been validated and has not 
;been used to create validated data sets of any type.
;
;Please send bug reports to CGIS@ZWICKY.GSFC.NASA.GOV.
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
FUNCTION mod_blackbody,  nu, beta, temp
;+
; NAME:
;
;  mod_blackbody
; 
; PURPOSE:
;
;  Compute the intensity inu for a modified black body
;  of the form :  inu = nu^beta x B_nu(temp)
;
; CATEGORY:
;
;
;
; CALLING SEQUENCE:
;
;
;
; INPUTS:
;
;   nu: frequency in GHz.
;   beta: emissivity exponent
;   temp: black body temperature
;
; OPTIONAL INPUTS:
;
;
;
; KEYWORD PARAMETERS:
;
;
;
; OUTPUTS:
;
;  inu : intensity
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;  inu = mod_blackbody(  nu, beta, temp)
;
; MODIFICATION HISTORY:
;     Created by S. Hildebrandt and J.F. Macias-Perez, Oct., 2010
;-



 tempn = double(temp[0])
 betan = beta[0]
 inu = (nu)^(betan)*planck_rad_public(tempn,nu,units='GHz',/MJy)
 return, inu

END
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;   
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   A copy of the GNU General Public License is available at 
;   <http://www.gnu.org/licenses/>.
;   
;   Copyright Locke D. Spencer, 2013
;   
;
PRO colour_correction, nu_0, freq_d, trans_d, bolo_cc, nu, inu,$
                           powerlaw = powerlaw, alpha = alpha, $
                           modblackbody =  modblackbody, tbd = tbd, betabd =  betabd, BT=BT, GETNUEFF=GETNUEFF, NUEFF=NUEFF, MSG=MSG, $
                           ERR_D=ERR_D, GETERR=GETERR, NITER=NITER, ERCC=ERCC, ERNUEFF=ERNUEFF, ER_ALPHA=ER_ALPHA, ER_tbd=ER_tbd, ER_betabd=ER_betabd, ER_INU=ER_INU
	;
	;	Nu_0 is the band central frequency, in GHz
	;	Freq_d is the freq, in Hz, associated with the spectral Transmission
	;	trans_d is the spectral transmission
	;	bolo_cc is the individual colour correction output
	;	nu is the freq for an arbitrary Inu input (see next),
	;		if left out and inu is the same size as freq_d, it is assumed that nu=freq_d is intended
	;	inu is an arbitrary input spectrum to correct for, it is only used if neither powerlaw nor modblackbody are used.
	;	powerlaw sets the correction to be Inu propto nu^alpha (default)
	;	alpha is powerlaw spectral index
	;	modblackbody sets the correction to a modified blackbody of the form (nu/nu_c)^Beta Bnu(T,nu)/Bnu(T,nu_c)
	;	tbd is the temperature of the modified blackbody
	;	betabd is the power law index of the modified blackbody,
	;		if modblackbody is set but betabd is not, then alpha is used (if set), otherwise betabd is assumed to be -1.
	;
	;
	;	BT is the band threshold, below which spectral transmission points are not included in the integrals.  Defautl is 10^-7.
       ;      GETERR is a keyword to return an uncertainty estimate on the CC factor on top of the CC factor itself, 
       ;        it also requires the NITER and  ERR_D keywords to have input values.  
       ;        ERR_D is the spectral uncertainty, NITER is the number of realizatiuons used in determining the uncertainty estimate.
       ;        ERCC is the CC error estimate, ERNUEFF is the NUEFF error estimate.
       ;        ER_alpha is the uncertainty on alpha (if desired, nominally this is kept at zero).
       ;        ER_tbd is the uncertainty on the modified blackbody temperature (nominally this is kept to zero).
       ;        ER_betabd is the uncertainty on the modified blackbody emissivity index beta (nominally this is kept to zero).
       ;        ER_INU is the uncertainty on the user specified spectral profile.  It is kept to zero if not specified as an input variable.
	;	
	;
	CONST = { C: 2.99792458d+08, H: 6.6260755d-34, HBAR: 1.0545726691251021d-34, K: 1.3806580d-23, TCMB: 2.7255d0 }
	;
	IF N_ELEMENTS(POWERLAW) EQ 0 THEN POWERLAW = 0
	IF N_ELEMENTS(MODBLACKBODY) EQ 0 THEN MODBLACKBODY = 0
	IF N_ELEMENTS(BT) EQ 0 THEN BT = 1d-7
       IF N_ELEMENTS(GETNUEFF) EQ 0 THEN GETNUEFF = 0
       IF N_ELEMENTS(MSG) Eq 0 THEN MSG = '' ELSE MSG = STRING(MSG)
       IF N_ELEMENTS(GETERR) EQ 0 THEN GETERR = 0 ; default is to not determine the uncertainty.
       IF N_ELEMENTS(NITER) EQ 0 THEN NITER = 2  ; default of 2 realizations if GETERR is set.
       IF N_ELEMENTS(ER_ALPHA) EQ 0 THEN ER_ALPHA = 0d
       IF N_ELEMENTS(ER_TBD) EQ 0 THEN ER_TBD = 0d
       IF N_ELEMENTS(ER_BETABD) EQ 0 THEN ER_BETABD = 0d
       IF N_ELEMENTS(ER_INU) EQ 0 THEN BEGIN
         IF N_ELEMENTS(INU) GT 0 THEN ER_INU = DBLARR(N_ELEMENTS(INU))
       ENDIF
       IF N_ELEMENTS(ERR_D) EQ 0 THEN ERR_D = DBLARR(N_ELEMENTS(freq_d))
       ERR_D_orig = ERR_D
       FREQ_D_orig = FREQ_D
       TRANS_D_orig = TRANS_D
       ;
       IF KEYWORD_SET(GETERR) THEN BEGIN
         ;
         IF N_ELEMENTS(ERR_D) LT N_ELEMENTS(trans_d) THEN BEGIN
           GETERR = 0 ; err_d is not valid
           MSG = [MSG,'GETERR was set, but ERR_D was invalid.  GETERR was set to zero and the uncertainties returned are set to zero.']
         ENDIF
         ERCC = 0d    ; set the error to zero to start.
         ERNUEFF = 0d ; set the error to zero to start.
       ENDIF
	;
	;	Check if the required variables are set for powerlaw
	;
	IF KEYWORD_SET(POWERLAW) THEN BEGIN
		;
		;	check if alpha was set
		MSG = [MSG, 'Running the powerlaw colour correction conversion']
		IF N_ELEMENTS(alpha) EQ 0 THEN ALPHA = -1d
	ENDIF
	;
	;	Check if the required variables are set for modblackbody
	;
	IF KEYWORD_SET(MODBLACKBODY) THEN BEGIN
		;
		;	check if beta  was set
		MSG = [MSG, 'Running the modblackbody colour correction conversion']
		IF N_ELEMENTS(betabd) EQ 0 THEN BEGIN
			;	beta was not set, check if alpha was set, otherwise use -1
			IF N_ELEMENTS(Alpha) GT 0 THEN betabd = alpha ELSE betabd = -1d
		ENDIF
		;
		;	Now check if BB temp was set, otherwise use the CMB temp as default
		;
		IF N_ELEMENTS(TBD) EQ 0 THEN tbd = const.tcmb
	ENDIF
	;
	;	Now check for the custom cc calc.
	;
	IF ((POWERLAW EQ 0) AND (MODBLACKBODY EQ 0)) THEN BEGIN
		MSG = [MSG, 'Running the user input spectrum colour correction conversion']
		;	check to see if nu is set, if not use freq_d
		Ninu = N_ELEMENTS(inu)
		Nnu = N_ELEMENTS(nu)
		Nfreq = N_ELEMENTS(freq_d)
		IF Nnu EQ 0 THEN BEGIN	;	nu was not set
			MSG = [MSG, 'nu was not set, checking inu and freq_d']
			IF Ninu EQ 0 THEN BEGIN
				MSG = [MSG, 'inu was not set either, returning -1']
				bolo_cc = -1d
				return
			ENDIF ELSE BEGIN	;	nu not set but inu was, check against freq_d
				MSG = [MSG, 'nu not set but inu was, checking against freq_d']
				IF Ninu LT Nfreq THEN BEGIN
					MSG = [MSG, 'inu is smaller then freq_d, setting nu=freq_d and attempting to pad inu with zeros, but result may not be correct']
					inu_ = dblarr(Nfreq)
					inu_[0:Ninu - 1] = inu
					inu = inu_
					nu = freq_d
				ENDIF ELSE BEGIN
					IF Ninu EQ Nfreq THEN BEGIN
						MSG = [MSG, 'inu and freq_d are the same size, setting nu = freq_d']
						nu = freq_d
					ENDIF ELSE BEGIN
						MSG = [MSG, 'inu is larger than freq_d, settign nu = freq_d and truncating inu to be the same size as nu, result may not be correct']
						nu = freq_d
						inu = inu[0:Nfreq - 1]
					ENDELSE
				ENDELSE
			ENDELSE
		ENDIF ELSE BEGIN	;	nu was set, now check it against inu
			IF Ninu EQ 0 THEN BEGIN	;	inu not set, this is a probelm
				MSG = [MSG, 'inu was not set, returning -1']
				bolo_cc = -1d
				return
			ENDIF ELSE BEGIN	;	both nu and inu are set, see if same size
				IF Nnu LT Ninu THEN BEGIN	;	cut inu down to match
					MSG = [MSG, "inu is larger than nu, trimming inu to nu's size"]
					inu = inu[0:Nnu - 1]
				ENDIF ELSE BEGIN
					IF Nnu GT Ninu THEN BEGIN	;	nu is bigger, shrink nu to match
						MSG = [MSG, 'nu is larger than inu, trimming nu to inu size']
						nu = nu[0:Ninu - 1]
					ENDIF ELSE BEGIN	;	they are both set and the same size
						;	do nothing, they are both set as they should be...
					ENDELSE
				ENDELSE
			ENDELSE
		ENDELSE
	ENDIF
	;
	;
	;		Now check that frequency is always positive
	;
	posF = WHERE((freq_d GT 0d), Npos)
	IF Npos GT 0 THEN BEGIN
		freq_d = freq_d[posF]
		trans_d = trans_d[posF]
              err_D = err_d[posF]
	ENDIF
	;
	;	And check that only the larger-amplitude regions are used...
	HighAmp = WHERE(trans_d GE BT, Ngood)
	IF Ngood GT 0 THEN BEGIN
		trans_d = trans_d[HighAmp]
		freq_d = freq_d[HighAmp]
              err_d = err_d[HighAmp]
	ENDIF
	;
	nu_0 = nu_0*1d9	;	in Hz from GHz now...
	; ------- choose option ----------------
     IF keyword_set(powerlaw) THEN BEGIN	;	POWERLAW is the default

         inuint = ( freq_d / nu_0 )^alpha

     ENDIF ELSE BEGIN
     	IF keyword_set(modblackbody) THEN BEGIN
     		inuint = mod_blackbody(freq_d/1d9,betabd , tbd)/ mod_blackbody( nu_0/1d9,betabd , tbd )
     	ENDIF ELSE BEGIN
     		inuint = INTERPOL( inu, nu, freq_d )
         	inuint0 = INTERPOL( inu, nu, [nu_0] )
         	inuint =  inuint/inuint0[0]
     	ENDELSE
     ENDELSE

     FLUX_INT = TSUM(freq_d, inuint*trans_d)	;	int_tabulated( freq_d, inuint * trans_1 , /double)
     IRAS_NOM = TSUM(freq_d, (nu_0/freq_d)*trans_d)	;	int_tabulated( freq_d, ( nu_nom / freq_d ) * trans_1, /double )
	;
	;nu_0 = nu_0/1d9
	;
	;bolo_cc = flux_int / iras_nom
	bolo_cc = iras_nom / flux_int	;	LSedit 2012_10_01 swapped the numerator and denominator to have CC coeff's consistent with UC coeffs in HFI CoPap.
       ;
       IF KEYWORD_SET(GETNUEFF) THEN BEGIN
         ;
         INT_NUM = TSUM(freq_d, inuint*trans_d*freq_d)
         INT_DEN = TSUM(freq_d, inuint*trans_d)
         NUEFF = INT_NUM/INT_DEN/1d9  ;  in GHz now...
       ENDIF
       ;
       ; Get the uncertainty of the above if desired. 
       ;
       IF KEYWORD_SET(GETERR) THEN BEGIN
         ;
         Nspec = N_ELEMENTS(trans_d)
         Ninu = N_ELEMENTS(Inu)
         ;         
         ;  Set up some arrays for uncertainty
         CC_arr = DBLARR(NITER)
         IF KEYWORD_SET(GETNUEFF) THEN NU_arr = DBLARR(NITER)
         ;
         IF Ninu GT 0 THEN USER_ERR = TOTAL(ABS(ER_INU))
         ;
         FOR ii = 0, NITER - 1 DO BEGIN
           ;
           noise_spec = RANDOMN(seed,Nspec)
           sp_ = trans_d + noise_spec*ERR_D
           IF KEYWORD_SET(POWERLAW) THEN BEGIN ; standard powerlaw spectral index
             IF ER_ALPHA GT 0d THEN BEGIN
               alpha_ = (alpha + RANDOMN(seed,1)*ER_alpha)[0]
               inuint = ( freq_d / nu_0)^alpha_
             ENDIF ELSE BEGIN
               inuint = inuint ; same as outside of the M-C loop.
             ENDELSE
           ENDIF ELSE BEGIN  ;  Either the modified blackbody, or the user-specified spectral profile.
             IF KEYWORD_SET(modblackbody) THEN BEGIN ; the modified blackbody spectral profile.
               IF (ER_TBD GT 0d) THEN BEGIN  ; There is also an uncertainty on the BB temperature.
                 TBD_ = (TBD + RANDOMN(seed,1)*ER_TBD)[0]
                 IF (ER_BETABD GT 0d) THEN BEGIN ; Both temp and beta have an uncertainty.
                   BETABD_ = (BETABD + RANDOMN(seed,1)*ER_BETABD)[0]
                 ENDIF ELSE BEGIN
                   BETABD_ = BETABD
                 ENDELSE
                 inuint = mod_blackbody(freq_d/1d9,betabd_ , tbd_)/ mod_blackbody( nu_0/1d9,betabd_ , tbd_ )
               ENDIF ELSE BEGIN  ; No temperature uncertainty, but perhaps on on the emissivity.
                 TBD_ = TBD
                 IF ER_BETABD GT 0d THEN BEGIN  ; emissivity uncertainty, no temperature uncertainty.
                   BETABD_ = (BETABD + RANDOMN(seed,1)*ER_BETABD)[0]
                   inuint = mod_blackbody(freq_d/1d9,betabd_ , tbd_)/ mod_blackbody( nu_0/1d9,betabd_ , tbd_ )
                 ENDIF ELSE BEGIN  ; no uncertainty in the emissivity or temperature of the modified blackbody.
                   inuint = inuint
                 ENDELSE
               ENDELSE  ; have inuint for the modified blackbody case
             ENDIF ELSE BEGIN ; This is the user provided spectral profile case.
               IF USER_ERR GT 0d THEN BEGIN ; there is an uncertainty to the user provided spectrum
                 Inu_ = Inu + RANDOMN(seed,Ninu)*ER_Inu
                 inuint = INTERPOL( inu_, nu, freq_d )
                 inuint0 = INTERPOL( inu_, nu, [nu_0] )
                 inuint =  inuint/inuint0[0]
               ENDIF ELSE BEGIN ; there is no uncertainty to the user provided spectrum, the above inuint variable can be left as-is.
                 inuint = inuint
               ENDELSE
             ENDELSE
           ENDELSE
           ;
           ;  I now have the revised spec-trans, and a revised inuint (if needed).  I can calculate the i-th-realisation CC factor.
           ;
           FLUX_INT = TSUM(freq_d, inuint*sp_)	;	int_tabulated( freq_d, inuint * trans_1 , /double)
           IRAS_NOM = TSUM(freq_d, (nu_0/freq_d)*sp_)	;	int_tabulated( freq_d, ( nu_nom / freq_d ) * trans_1, /double )
           ;
           CC_arr[ii] = iras_nom/flux_int
           IF KEYWORD_SET(GETNUEFF) THEN BEGIN
             ;
             INT_NUM = TSUM(freq_d, inuint*sp_*freq_d)
             INT_DEN = TSUM(freq_d, inuint*sp_)
             NU_arr[ii] = INT_NUM/INT_DEN/1d9  ;  in GHz
           ENDIF
           ;
           ;
         ENDFOR
         ;
         ERCC = STDEV(CC_arr);/SQRT(2d) ; there is uncertainty in the original spectrum, /etc also.  Having thought about it, I do not think that the root(2) term should be included.
         IF KEYWORD_SET(GETNUEFF) THEN ERNUEFF = STDEV(NU_arr);/sqrt(2d)
         ;
       ENDIF
       ;
       nu_0 = nu_0/1d9
       ;
       freq_d = freq_d_orig
       trans_d = trans_d_orig
       err_d = err_d_orig
       ;
       return
       ;bolo_cc[ idet].cc = flux_int / iras_nom
END
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
PRO color_correction, nu_0, freq_d, trans_d, bolo_cc, nu, inu,$
                           powerlaw = powerlaw, alpha = alpha, $
                           modblackbody =  modblackbody, tbd = tbd, betabd =  betabd, BT=BT, GETNUEFF=GETNUEFF, NUEFF=NUEFF, MSG=MSG, $
                           ERR_D=ERR_D, GETERR=ERR_DETS, NITER=NITER, ERCC=ERCC, ERNUEFF=ERNUEFF, ER_ALPHA=ER_ALPHA, ER_tbd=ER_tbd, ER_betabd=ER_betabd, ER_INU=ER_INU
	;
	;	Have this script in case of spelling problems.
	;	
	print, 'You have called color_correction instead of colour_correction.   ... redirecting to the current version.'
	;
	colour_correction, nu_0, freq_d, trans_d, bolo_cc, nu, inu,$
                           powerlaw = powerlaw, alpha = alpha, $
                           modblackbody =  modblackbody, tbd = tbd, betabd =  betabd, BT=BT, GETNUEFF=GETNUEFF, NUEFF=NUEFF, MSG=MSG, $
                           ERR_D=ERR_D, GETERR=ERR_DETS, NITER=NITER, ERCC=ERCC, ERNUEFF=ERNUEFF, ER_ALPHA=ER_ALPHA, ER_tbd=ER_tbd, ER_betabd=ER_betabd, ER_INU=ER_INU
	;
END
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;   
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   A copy of the GNU General Public License is available at 
;   <http://www.gnu.org/licenses/>.
;   
;   Copyright Locke D. Spencer, 2013
;   
;
FUNCTION hfi_colour_correction, channel, bolo_cc, nu, inu,bp_info=bp_info, LFI=LFI, $
                           powerlaw = powerlaw, alpha = alpha, $
                           modblackbody =  modblackbody, tbd = tbd, betabd =  betabd, $
                           ABP=ABP, CH_CC=CH_CC, AVG_CC=AVG_CC, BT=BT, $
                           GETNUEFF=GETNUEFF, AVG_NUEFF=AVG_NUEFF, CH_NUEFF=CH_NUEFF, BOLO_NUEFF=BOLO_NUEFF, MSG=MSG, DEBUG=DEBUG, $
                           GETERR=GETERR, BP_ERR=BP_ERR, EABP=EABP, NITER=NITER, ER_ALPHA=ER_ALPHA, ER_tbd=ER_tbd, ER_betabd=ER_betabd, ER_INU=ER_INU
;+
; NAME:
;
;    hfi_colour_correction
;
; PURPOSE:
;
; Quick tool to compute colour correction  for FLUX wrt nominal frequencies in HFI. No map-making involved.
; Data calibrated in CMB units and converted to IRAS convention, nu  I_nu = cte.
;
; CATEGORY:
;
;
;
; CALLING SEQUENCE:
;
;    result = hfi_colour_correction( bp_info=bp_info, channel, bolo_cc, nu, inu,LFI=LFI, $
;                           powerlaw = powerlaw, alpha = alpha, $
;                           modblackbody =  modblackbody, tbd = tbd, betabd =  betabd, $
;                           ABP=ABP, CH_CC=CH_CC, AVG_CC=AVG_CC)
;
;
; INPUTS:
;
;    channel : string for HFI channel to be considered
;    bp_info:  HFI bandpass
;
;
; OPTIONAL INPUTS:
;
;    nu: input frequency vector in GHz
;    inu: input intensity law
;    BT:  optional baseline threshold, above which values are included in any integrals.
;
; KEYWORD PARAMETERS:
;   INPUT:
;    powerlaw: assume a power law as input intensity :
;        I_{\nu} \propto \nu^{\alpha}
;        WARNING: the keyword alpha=alpha needs to be provided
;    alpha: The power law proportionality constant
;
;    modblackbody: assume a modified black body spectrum for the input
;                  intensity
;           I_{\nu} \propto \nu^{\beta} B_{\nu}(T)
;        WARNING: the keywords betabd=betabd and tbd=tbd need to be provided
;    betabd:    The beta in the modblackbody equation.
;    tbd:       The T in the modblackbody equation.
;    
;    BP_INFO:  The detector level spectral information (from hfi_read_bandpass script)
;    ABP:   The band-average spectrum information (from hfi_read_avg_bandpass script) 
;
;   GETNUEFF:  Set this keyword to 1 to also compute the effective frequencies for the specified final spectrum. 
;                This works for powerlaw, modblackbody, and user-specified spectral profile.
;    
;   OUTPUT:
;    CH_CC:   The structure for the average colour correction coefficients, derived as the average of the detector coefficients.
;    AVG_CC:  The structure for the colour correction coefficient derived from the band-average spectrum.
;    CH_NUEFF: The same for the effective frequency
;    AVG_NUEFF: The same for the effective frequency.
;    BOLO_NUEFF
;    
; OUTPUTS:
;
;   bolo_cc : structure containing
;            .name : bolometer name
;            .cc   : the colour correction coefficients per bolometer (-1 for
;                    RTS bolometers)
;   CH_CC:    structure containing the band-average coefficients (mean of detector coefficients, not the coefficient from the bandf-average spectrum)
;             .chname : channel name
;             .cc : colour correction coefficient
;
; OPTIONAL OUTPUTS:
;
;   See KEYWORDS
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;
;
; PROCEDURE:
;
;
;
; EXAMPLE:
; 
;  bp = hfi_read_bandpass( /imo, LBLIMO='2_61')     ;   I select an older version intentionally.
;  abp = hfi_read_avg_bandpass(,/IMO, LBLIMO='2_61')
;  cc = hfi_colour_correction(bp_INFO = bp,'100',bolo_cc, /POWERLAW, ALPHA=4.0, ABP=ABP, CH_CC=CH_CC, AVG_CC=AVG_CC)
;  help, cc, /str
;  nbolo =n_elements(bolo_cc)
;  for ibolo=0,nbolo-1 do print, bolo_cc[ibolo].boloname+':  ' +strtrim(bolo_cc[ibolo].cc,2)
;  print, AVG_CC.chname+':  '+strtrim(AVG_CC.cc,2)
;  print, CH_CC.chname+':  '+strtrim(CH_CC.cc,2)
;
; ** Structure <a4ed68>, 2 tags, length=42, data length=24, refs=3:
;   CHNAME      STRING    '100'
;   CC          DOUBLE    1.1199855
;
;   00_100_1a:  1.0698235
;   01_100_1b:  1.0976890
;   20_100_2a:  1.1262720
;   21_100_2b:  1.1190556
;   40_100_3a:  1.1435700
;   41_100_3b:  1.0866167
;   80_100_4a:  1.1533167
;   81_100_4b:  1.1635402
;
;   100:  1.1228059
;   100:  1.1199855
;
;  bp = hfi_read_bandpass( /imo, LBLIMO='2_61')     ;   I select an older version intentionally.
;  abp = hfi_read_avg_bandpass(,/IMO, LBLIMO='2_61')
;  cc = hfi_colour_correction(BP_INFO=bp,'100',bolo_cc, /modblackbody,betabd=2.0,tbd=17.0, ABP=ABP, CH_CC=CH_CC, AVG_CC=AVG_CC)
;  help, cc, /str
;  nbolo =n_elements(bolo_cc)
;  for ibolo=0,nbolo-1 do print, bolo_cc[ibolo].boloname+':  ' +strtrim(bolo_cc[ibolo].cc,2)
;  print, AVG_CC.chname+':  '+strtrim(AVG_CC.cc,2)
;  print, CH_CC.chname+':  '+strtrim(CH_CC.cc,2)
;
; ** Structure <a4ed68>, 2 tags, length=42, data length=24, refs=3:
;   CHNAME      STRING    '100'
;   CC          DOUBLE    1.1103309
;
;   00_100_1a:  1.0622812
;   01_100_1b:  1.0893660
;   20_100_2a:  1.1163948
;   21_100_2b:  1.1094302
;   40_100_3a:  1.1331014
;   41_100_3b:  1.0782955
;   80_100_4a:  1.1419800
;   81_100_4b:  1.1517978
;
;   100:  1.1130340

;   100:  1.1103309
; 
;  bp = hfi_read_bandpass( /imo, LBLIMO='2_61')     ;   I select an older version intentionally.
;  abp = hfi_read_avg_bandpass(,/IMO, LBLIMO='2_61')
;  nu = dindgen(200)*1d11 
;               ; in Hz, the range of nu must be greater than the range of the spectrum, i.e. from 0 to 20000 GHz if the spectra are not truncated
;  inu = nu^(4.0)
;  cc = hfi_colour_correction(BP_INFO=bp,'100',bolo_cc, /modblackbody,betabd=2.0,tbd=17.0, ABP=ABP, CH_CC=CH_CC, AVG_CC=AVG_CC)
;  help, cc, /str
;  nbolo =n_elements(bolo_cc)
;  for ibolo=0,nbolo-1 do print, bolo_cc[ibolo].boloname+':  ' +strtrim(bolo_cc[ibolo].cc,2)
;  print, AVG_CC.chname+':  '+strtrim(AVG_CC.cc,2)
;  print, CH_CC.chname+':  '+strtrim(CH_CC.cc,2)
;
; ** Structure <a2ab68>, 2 tags, length=42, data length=24, refs=3:
;   CHNAME      STRING    '100'
;   CC          DOUBLE    1.6925336
;
;   00_100_1a:  1.5810925
;   01_100_1b:  1.6270241
;   20_100_2a:  1.7132610
;   21_100_2b:  1.6990577
;   40_100_3a:  1.7467464
;   41_100_3b:  1.6154594
;   80_100_4a:  1.7701407
;   81_100_4b:  1.7874870
;
;   100:  1.6997710
;   100:  1.6925336 
;
; MODIFICATION HISTORY:
;   Created by S. Hildebrandt and J.F. Macias-Perez, Oct., 2010
;   Modifiead by L. Spencer, August 31 2011:
;     Calculate a single coefficient for the band-average spectrum, instead of averaging the individual detector coefficients. 
;   Modified by L. Spencer, 2012/09/17 to only require single compile (swithced function order), and added modblackbody and planck_rad_public routines.
;   Modified by L. Spencer, 2012/10/01 to make coefficient consistent with unit conversion.  (it converted from alpha to -1, instead of from -1 to alpha).  The 
;      coefficients can now be multiplied with the unit conversion (KCMB/KRJ tp MJy/sr) coefficients to maintain the same units for a different spectral index.
;   Modified by L.Spencer, 2013/01/18 for the bp_info input to be a keyword rather than a required variable.  This will allow the script to function with the 
;      external RIMO file which may not have detector level spectra included.
;   Modified by L.Spencer, 2013/01/23 Changed the default TCMB (should change anything here really0.  Added the NU_EFFECTIVE keyword and code.
;   Modified by L.Spencer, 2013/02/05 Added the GETERR keyword to obtain an uncertainty estimate of the CC coefficients (default GETERR=0).  
;                                     - With GETERR=1 as an option, the NITER keyword was added to control the number of iterations used to determine the uncertainty (default of 10 iterations).
;                                     - Also added the bp_err keyoword to house the spectral uncertainty (same structure as bp_info).
;                                     - Also added the EABP keyoword to house the band-average spectral uncertainty (same structure as ABP).
;                                     - Also added ER_alpha, ER_tbd, ER_betabd, and ER_Inu keywords for the errors associated (if any) with alpha, tbd, betabd, and Inu
;
;-
  ;
  ; -- Definitions
  CONST = { C: 2.99792458d+08, H: 6.6260755d-34, HBAR: 1.0545726691251021d-34, K: 1.3806580d-23, TCMB: 2.7255d0 }
  ;
  IF N_ELEMENTS(BT) EQ 0 THEN BT = 1d-7	;	Default value
  ;
  ;  Check if the GETERR keyword has been set, if so check that the required uncertainties are also set.
  ;
  IF N_ELEMENTS(NITER) EQ 0 THEN NITER = 2 ; default of 2 iterations if the number of iterations is not set, only valid if GETERR is set, otherwise only one calculation is done.
  IF N_ELEMENTS(GETERR) EQ 0 THEN GETERR = 0 ; set this to not get the errors.
  ERR_DETS = 0  ; internal flag to determine uncertainty of detector coefficients.
  ERR_BNDS = 0  ; internal flag to determine uncertainty of band-average coefficients.  
  IF KEYWORD_SET(GETERR) THEN BEGIN
    ;
    ERR_DETS = 1
    ERR_BNDS = 1
    ;  check if the uncertainties on the spectra are provided.
    IF N_ELEMENTS(BP_ERR) LT N_ELEMENTS(BP_INFO) THEN ERR_DETS = 0
    IF N_ELEMENTS(EABP) LT N_ELEMENTS(ABP) THEN ERR_BNDS = 0
    ;
  ENDIF
  ;
  ; --- Check channel name first
  if not keyword_set(LFI) then begin
    channels = ['100','143','217','353','545','857']
    nuc = [100.0d0, 143.0d0, 217.0d0,353.0d0,545.0d0,857.0d0] ; GHz
  endif else begin
    channels =['30','44','70']
    ;nuc = [28.5d0, 44.1d0,70.3d0]
    nuc = [28.4d0, 44.1d0,70.4d0]
  endelse
  ;
  ;channels = ['100','143','217','353','545','857']
  ;nuc = [100.0d0, 143.0d0, 217.0d0,353.0d0,545.0d0,857.0d0]
  nchannels =  n_elements(channels)
  okchannel = 0
  ichwork = -1
  FOR ich = 0,nchannels-1 DO BEGIN
    IF (channel EQ channels[ich]) THEN BEGIN
      okchannel = 1
      ichwork = ich
    ENDIF
  ENDFOR
  IF (okchannel EQ 0) THEN BEGIN
    print, 'Wrong channel name. Available channels are '
    print,  channels
    return, -1
  ENDIF
  ;
  ; Defining bandpass parameters
  ;
  IF KEYWORD_SET(BP_INFO) THEN BEGIN
  NAME   = bp_info.name      ; name of the detector
  FREQ   = bp_info.freq  	 ; frequencies measured in Hz
  TRANS  = bp_info.trans     ; values of the transmission (normalized later)
  IF KEYWORD_SET(ERR_DETS) THEN BEGIN
    NAME_E = bp_err.name
    FREQ_E = bp_err.freq
    TRANS_E = bp_err.trans ; I should check that this is on the same grid as the bp_info structure (should be, but may not be).
  ENDIF
  ;
  ; Defining channel parameters
  NU_NOM = double(nuc[ichwork])
  BOLO_LIST = get_hfibolo_list( channel = channel, LFI=LFI )
  N_BOLO_LIST = n_elements( bolo_list )
  ;
  ;  Define output variables
  IF KEYWORD_SET(ERR_DETS) THEN bolo_cc =  {boloname:'',cc:0.0d0, er:0.0d0} ELSE bolo_cc =  {boloname:'',cc:0.0d0}
  bolo_cc =  replicate(bolo_cc,n_bolo_list)
  bolo_cc.boloname = bolo_list
  IF KEYWORD_SET(ERR_DETS) THEN bolo_nueff =  {boloname:'',unit:'GHz',nueff:0.0d0, er:0.0d0} ELSE bolo_nueff =  {boloname:'',unit:'GHz',nueff:0.0d0}
  bolo_nueff =  replicate(bolo_nueff,n_bolo_list)
  bolo_nueff.boloname = bolo_list
  ;
  IF KEYWORD_SET(ERR_DETS) THEN ch_cc =  {chname:channel,cc:0.0d0, er:0.0d0} ELSE ch_cc =  {chname:channel,cc:0.0d0}
  IF KEYWORD_SET(ERR_DETS) THEN ch_nueff =  {chname:channel,unit:'GHz',nueff:0.0d0, er:0.0d0} ELSE ch_nueff =  {chname:channel,unit:'GHz',nueff:0.0d0}
  IF KEYWORD_SET(ERR_BNDS) THEN avg_cc =  {chname:channel,cc:0.0d0, er:0.0d0} ELSE avg_cc =  {chname:channel,cc:0.0d0}
  IF KEYWORD_SET(ERR_BNDS) THEN avg_nueff =  {chname:channel,unit:'GHz',nueff:0.0d0, er:0.0d0} ELSE avg_nueff =  {chname:channel,unit:'GHz',nueff:0.0d0}
  ;
  ; General case using nu and inu as inputs
  ; define min max in the band for further interpolation
  ;
  IF NOT keyword_set(powerlaw) AND NOT keyword_set(modblackbody) THEN BEGIN
    minfreq = -1d99
    maxfreq = 1d99
    for idet = 0, n_bolo_list-1 DO BEGIN
      Q_DET  = where(bolo_list[ idet ] eq name)
      Q_DET  = q_det(0)
      FREQ_D    = freq( * , q_det ) ; already in Hz
      IF minfreq LT min(freq_d) THEN minfreq = min(freq_d)
      IF maxfreq GT max(freq_d) THEN maxfreq = max(freq_d)
    ENDFOR
    IF (min(nu) GT minfreq) OR (max(nu) LT maxfreq) THEN BEGIN
      print, "Sorry, the frequency range is not sufficient for interpolation"
      print, "data needed from " + strtrim(minfreq,2) +  "  to  " +  strtrim(maxfreq,2) +  " Hz"
      return, -1
    ENDIF
  ENDIF
  ;
  ;  Case of a power law model
  IF keyword_set(powerlaw) THEN BEGIN
    IF N_ELEMENTS(alpha) EQ 0 THEN BEGIN		;		IF NOT keyword_set(alpha) THEN BEGIN
      print, "Sorry, you need to define the power law exponent"
      print, "using the keyword alpha = alpha"
      return, -1
    ENDIF
  ENDIF
  ;
  ; Case of a modified black body spectrum
  IF keyword_set(modblackbody) THEN BEGIN
    IF NOT keyword_set(tbd) OR NOT keyword_set(betabd) THEN BEGIN
      print, "Sorry, you need to define the temperature tbd= and emissivity betabd="
      print, "using the keywords tbd = tbd and betabd = betabd"
      return, -1
    ENDIF
  ENDIF
  ;
  ; starting computation
  For idet = 0 , n_bolo_list - 1 do begin
    ;
    ; ----- get transmission ------------
    Q_DET = where(bolo_list[ idet ] eq name)
    Q_DET = q_det(0)
    Q_TRANS = where( trans( * , q_det ) gt BT )
    FREQ_D = freq( q_trans, q_det ) ; already in Hz
    TRANS_D = trans( q_trans, q_det )
    ;  Check for zero frequency and get rid of it.
    PosFreq = WHERE(FREQ_D GT 0d, Npos)
    FREQ_D = FREQ_D[PosFreq]
    TRANS_D = TRANS_D[PosFreq]
    IF KEYWORD_SET(ERR_DETS) THEN BEGIN
      Q_DET_E = where(bolo_list[ idet ] eq name_E)
      Q_DET_E = q_det_E(0)
      FREQ_DE = freq_E( *, q_det_E ) ; already in Hz
      TRANS_DE = trans_E( *, q_det_E )
      ERR_D = INTERPOL(TRANS_DE, FREQ_DE, FREQ_D) ; now on same grid as the transmission spectrum.
    ENDIF
    ;INTTRANS = int_tabulated(freq_d, trans_d , /double)
    ;TRANS_1 = trans_d / inttrans ; normalizing the transmission areas
    ;
    colour_correction, nu_NOM, freq_d, trans_d, cc, nu, inu, powerlaw=powerlaw, alpha=alpha, modblackbody=modblackbody, tbd=tbd, betabd=betabd, $
      GETNUEFF=GETNUEFF, NUEFF=NUEFF, MSG=MSG, ERR_D=ERR_D, GETERR=ERR_DETS, NITER=NITER, ERCC=ERCC, ERNUEFF=ERNUEFF, $
      ER_ALPHA=ER_ALPHA, ER_tbd=ER_tbd, ER_betabd=ER_betabd, ER_INU=ER_INU
    ;; ------- choose option ----------------
    ;     IF keyword_set(powerlaw) THEN BEGIN
    ;
    ;         inuint = ( freq_d / nu_nom )^alpha
    ;
    ;     ENDIF ELSE IF keyword_set(modblackbody) THEN BEGIN
    ;
    ;         inuint = mod_blackbody(freq_d,betabd , tbd)/ mod_blackbody( nu_nom,betabd , tbd )
    ;
    ;     ENDIF ELSE BEGIN
    ;
    ;         inuint = INTERPOL( inu, nu, freq_d )
    ;         inuint0 = INTERPOL( inu, nu, [nu_nom] )
    ;         inuint =  inuint/inuint0[0]
    ;     ENDELSE
    ;
    ;     FLUX_INT = int_tabulated( freq_d, inuint * trans_1 , /double)
    ;     IRAS_NOM = int_tabulated( freq_d, ( nu_nom / freq_d ) * trans_1, /double )
    ;
    ;     bolo_cc[ idet].cc = flux_int / iras_nom
    bolo_cc[idet].cc = cc
    IF KEYWORD_SET(ERR_DETS) THEN bolo_cc[idet].er = ERCC
    IF KEYWORD_SET(GETNUEFF) THEN BEGIN
      bolo_NUEFF[idet].nueff = NUEFF
      IF KEYWORD_SET(ERR_DETS) THEN bolo_NUEFF[idet].er = ERNUEFF
    ENDIF
    ;
    ; RTS_DETECTORS (v41)
    If (bolo_list[ idet ] eq '70_143_8') OR ( bolo_list[ idet ] eq '55_545_3' ) then BEGIN
    ;  Removed `OR ( bolo_list[ idet ] eq '74_857_4' )' as 857- 4 is now included in DX9 (and later) data.
      bolo_cc[ idet ].cc = -1.0d9
      IF KEYWORD_SET(GETNUEFF) THEN bolo_NUEFF[idet].nueff = -1.0d9
    ENDIF
    ;
  ENDFOR
  ;
  ; Computing an average (first approximation to map making)
  Q_D = where( bolo_cc.cc GT -1.0)
  ch_cc.cc =  mean(bolo_cc[ q_d ].cc )
  ;IF KEYWORD_SET(ERR_DETS) THEN ch_cc.er = STDEV(bolo_cc[ q_d].cc) ; the standard deviation of the mean, rather than the individual uncertainties combined...is this correct?
  IF KEYWORD_SET(ERR_DETS) THEN ch_cc.er = MAX( [ SQRT(TOTAL( (bolo_cc[ q_d ].er)^2d ))/DOUBLE(N_ELEMENTS(bolo_cc[ q_d ].cc)) , $
                                                  STDEV(bolo_cc[ q_d ].cc)/SQRT(DOUBLE(N_ELEMENTS(bolo_cc[ q_d ].cc)))] ) 
; take either the combined individual uncertainties, or the spread of the coefficients, whichever is greater.
  IF KEYWORD_SET(GETNUEFF) THEN CH_NUEFF.nueff = MEAN(bolo_NUEFF[ q_d ].nueff)
  IF ( KEYWORD_SET(GETNUEFF) AND KEYWORD_SET(ERR_DETS) ) THEN CH_NUEFF.er = MAX( [ SQRT(TOTAL( (bolo_NUEFF[ q_d ].er)^2d ))/DOUBLE(N_ELEMENTS(bolo_cc[ q_d ].cc)) , $
                                                                                   STDEV(bolo_NUEFF[ q_d ].nueff)/SQRT(DOUBLE(N_ELEMENTS(bolo_cc[ q_d ].cc)))] ) 
; take either the combined individual uncertainties, or the spread of the coefficients, whichever is greater.
  ENDIF ELSE BEGIN
    ;
    ; Defining channel parameters
    NU_NOM = double(nuc[ichwork])
    BOLO_LIST = get_hfibolo_list( channel = channel, LFI=LFI )
    N_BOLO_LIST = n_elements( bolo_list )
    ;
    ;  Define output variables
    IF KEYWORD_SET(ERR_DETS) THEN bolo_cc =  {boloname:'',cc:0.0d0, er:0.0d0} ELSE bolo_cc =  {boloname:'',cc:0.0d0}
    bolo_cc =  replicate(bolo_cc,n_bolo_list)
    bolo_cc.boloname = bolo_list
    IF KEYWORD_SET(ERR_DETS) THEN bolo_nueff =  {boloname:'',unit:'GHz',nueff:0.0d0, er:0.0d0} ELSE bolo_nueff =  {boloname:'',unit:'GHz',nueff:0.0d0}
    bolo_nueff =  replicate(bolo_nueff,n_bolo_list)
    bolo_nueff.boloname = bolo_list
    IF KEYWORD_SET(ERR_DETS) THEN ch_cc =  {chname:channel,cc:0.0d0, er:0.0d0} ELSE ch_cc =  {chname:channel,cc:0.0d0}
    IF KEYWORD_SET(ERR_DETS) THEN ch_nueff =  {chname:channel,unit:'GHz',nueff:0.0d0, er:0.0d0} ELSE ch_nueff =  {chname:channel,unit:'GHz',nueff:0.0d0}
    IF KEYWORD_SET(ERR_BNDS) THEN avg_cc =  {chname:channel,cc:0.0d0, er:0.0d0} ELSE avg_cc =  {chname:channel,cc:0.0d0}
    IF KEYWORD_SET(ERR_BNDS) THEN avg_nueff =  {chname:channel,unit:'GHz',nueff:0.0d0, er:0.0d0} ELSE avg_nueff =  {chname:channel,unit:'GHz',nueff:0.0d0}
    ;
    ;bolo_cc =  {boloname:'',cc:0.0d0}
    ;bolo_cc =  replicate(bolo_cc,n_bolo_list)
    ;bolo_cc.boloname = bolo_list
    ;bolo_NUEFF = bolo_cc
    ;ch_cc =  {chname:channel,cc:0.0d0}
    ;AVG_CC= CH_CC
    ;ch_nueff = ch_cc
    ;AVG_NUEFF = ch_cc
    ;
    ; RTS_DETECTORS (v41)
    For idet = 0 , n_bolo_list - 1 do begin
      If (bolo_list[ idet ] eq '70_143_8') OR ( bolo_list[ idet ] eq '55_545_3' ) then BEGIN
      ;  Removed `OR ( bolo_list[ idet ] eq '74_857_4' )' as 857- 4 is now included in DX9 (and later) data.
        bolo_cc[ idet ].cc = -1.0d9
        IF KEYWORD_SET(GETNUEFF) THEN bolo_NUEFF[idet].nueff = -1.0d9
      ENDIF
    ENDFOR
    ;
  ENDELSE
  ;
  ;   Now calculate for the band-averqage spectrum
  IF KEYWORD_SET(abp) THEN BEGIN    ; abp is the average spectra for the given channel.
    trans_b = abp[ichwork].trans
    freq_b = abp[ichwork].freq
    PosFreq = WHERE(FREQ_b GT 0d, Npos)
    FREQ_b = FREQ_b[PosFreq]
    TRANS_b = TRANS_b[PosFreq]
    ;stop
    Q_TRANS = where( trans_b gt BT ) ; This is done later just in case the CO line falls at a low amplitude...
    FREQ_b = freq_b[q_trans] ; already in Hz
    TRANS_b = trans_b[q_trans]
    ;
    IF KEYWORD_SET(ERR_BNDS) THEN BEGIN
      FREQ_BE = EABP[ichwork].freq
      TRANS_BE = EABP[ichwork].trans ; I should check that this is on the same grid as the bp_info structure (should be, but may not be).
      ERR_b = INTERPOL(Trans_BE, FREQ_BE, FREQ_b)
    ENDIF
    ;
    colour_correction, nu_NOM, freq_b, trans_b, cc, nu, inu, powerlaw=powerlaw, alpha=alpha, modblackbody=modblackbody, tbd=tbd, betabd=betabd, $
      GETNUEFF=GETNUEFF, NUEFF=NUEFF, MSG=MSG, ERR_D=ERR_B, GETERR=ERR_BNDS, NITER=NITER, ERCC=ERCC, ERNUEFF=ERNUEFF, $
      ER_ALPHA=ER_ALPHA, ER_tbd=ER_tbd, ER_betabd=ER_betabd, ER_INU=ER_INU
    AVG_CC.cc = cc
    IF KEYWORD_SET(ERR_BNDS) THEN AVG_cc.er = ERCC
    IF KEYWORD_SET(GETNUEFF) THEN BEGIN
      AVG_NUEFF.nueff = NUEFF
      IF KEYWORD_SET(ERR_BNDS) THEN AVG_NUEFF.er = ERNUEFF
    ENDIF
  ENDIF ELSE BEGIN
    cc = 0d
  ENDELSE
  ;
  IF KEYWORD_SET(DEBUG) THEN FOR iii = 0, N_ELEMENTS(MSG) - 1 DO print, MSG[iii]
  ;
  return, cc	;	ch_cc
END
;
;
;
;
;
;
;
;
;
;
;
;
FUNCTION hfi_color_correction, channel, bolo_cc, nu, inu,bp_info=bp_info, LFI=LFI, $
                           powerlaw = powerlaw, alpha = alpha, $
                           modblackbody =  modblackbody, tbd = tbd, betabd =  betabd, $
                           ABP=ABP, CH_CC=CH_CC, AVG_CC=AVG_CC, BT=BT, $
                           GETNUEFF=GETNUEFF, AVG_NUEFF=AVG_NUEFF, CH_NUEFF=CH_NUEFF, BOLO_NUEFF=BOLO_NUEFF, MSG=MSG, DEBUG=DEBUG, $
                           GETERR=GETERR, BP_ERR=BP_ERR, EABP=EABP, NITER=NITER, ER_ALPHA=ER_ALPHA, ER_tbd=ER_tbd, ER_betabd=ER_betabd, ER_INU=ER_INU



	;
	;	Have this script in case of spelling problems.
	;	
	print, 'You have called "hfi_color_correction" instead of "hfi_colour_correction".   ... redirecting to the correct version.'
	;
	result = hfi_colour_correction(channel, bolo_cc, nu, inu,bp_info=bp_info, LFI=LFI, $
                           powerlaw = powerlaw, alpha = alpha, $
                           modblackbody =  modblackbody, tbd = tbd, betabd =  betabd, $
                           ABP=ABP, CH_CC=CH_CC, AVG_CC=AVG_CC, BT=BT, $
                           GETNUEFF=GETNUEFF, AVG_NUEFF=AVG_NUEFF, CH_NUEFF=CH_NUEFF, BOLO_NUEFF=BOLO_NUEFF, MSG=MSG, DEBUG=DEBUG, $
                           GETERR=GETERR, BP_ERR=BP_ERR, EABP=EABP, NITER=NITER, ER_ALPHA=ER_ALPHA, ER_tbd=ER_tbd, ER_betabd=ER_betabd, ER_INU=ER_INU)
	;
	return, result
	;
END
;