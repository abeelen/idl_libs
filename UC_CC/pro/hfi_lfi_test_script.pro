PRO hfi_lfi_test_script, FDIR=FDIR, HFI_RIMO_Name=HFI_RIMO_Name, LFI_RIMO_Name=LFI_RIMO_Name
;
;+
; NAME:
;
;   hfi_lfi_test_script
;
; PURPOSE:
;
;   Demonstrate the calling of and output from the various IDL unit conversion / colour correction scripts.
;
; CATEGORY:
;
;
;
; CALLING SEQUENCE:
;
;    hfi_lfi_test_script, FDIR=FDIR
;
; INPUTS:
;
;   
; OPTIONAL INPUTS:
;
;
; KEYWORD PARAMETERS:
; 
;   INPUT:
;     FDIR :     The location of a directory where the RIMO FITS files are located (a default is specified if not set).
;	HFI_RIMO_Name :	The name of the HFI RIMO .fits file.
;	LFI_RIMO_Name :	The name of the LFI RIMO .fits file.
;
;   OUTPUT:
;
;
; OUTPUTS:
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;   Planck collaboration only.
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;  hfi_lfi_test_script
;  ;   look at plot, look at output tables, compare printed numbers to those at the end of this file.
;  .continue
;
;
; MODIFICATION HISTORY:
; Created by L. Spencer August 31, 2011
; Modified by L. Spencer Sept. 17, 2012 to set default values to v300 HFI spectra, and DX9 RIMO .fits files.
; Modified by L. Spencer Jan. 2013 to change default values to v3.02 spectra and   delta-DX9 RIMO .fits files,
;  also following changes to Unit conversion script structure.
; Modified by L. Spencer 2013/02/05:  Added examples for coefficient uncertainties to be output from the code.
;                                     Added the LFI_testcc routine to the UcCC package, and added LFI_fastcc_test example to this example routine.
;
;   
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   A copy of the GNU General Public License is available at 
;   <http://www.gnu.org/licenses/>.
;   
;   Copyright Locke D. Spencer, 2013
;   
;
;-
;
  ; 
  ; This script runs through a basic test of all of the bandpass conversions/corrections, to demonstrate the use of them.
  ; 
  ; The following results, in addition to a plot of all of the bandpasses, should be printed to the screen upon running this script.
  ; I have moved the results printout from the top to the bottom of this script for readability.  
  ; This should run fine from magique3 as is.  Please copy the RIMO .fits files over to the (user selected) 
  ; FDIR keyword directory if beign run on another machine.
  ; 
  ; 
  ; 
  ; 
  ; 
  ; 
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;
  ;  The LFI_fastcc routine does not require any input spectra as it has quadratic coefficients hard-coded inside the program.
  ;
  print, ' LFI fastcc results: '
  LFI_fastcc_test ; this should print out some LFI CC coefficients.
  ;
  ;  Another few examples of using the LFI_fastcc routine directly (taken from the above test script).
  ;
  spectralIndex = [-2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0]
  print,'LFI-23',LFI_fastcc(70.4,spectralIndex,detector=23)
  print,'70GHz',LFI_fastcc(70.4,spectralIndex)
  ; 
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;
  IF ~KEYWORD_SET(FDIR) THEN fDIR = '/space/lspencer/UcCC_CodeUpdate_v302/'	;	'/wrk/lspencer/mywork2/test/'
  IF ~KEYWORD_SET(HFI_RIMO_Name) THEN HFI_RIMO_Name = 'HFI_RIMO_DX9v2.fits'	;	
  IF ~KEYWORD_SET(LFI_RIMO_Name) THEN LFI_RIMO_Name = 'LFI_RIMO_DX9v1.fits'
  ;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;
  ;;;; This bit is commented out as it is faster to read from the RIMO fits file.  I keep the code here as an example of how to restore from the IMO, however.
  ;;;; read in the hfi spectra from the IMO
  ;
  ;hfi_bp = hfi_read_bandpass(/IMO, LBL_IMO='2_67', PATH_IMO='/data/dmc/MISS03/METADATA/', FLG_INFO=flg, ER_INFO=er, /FLAG)
  ;hfi_bp_withCO = hfi_read_bandpass(/IMO, LBL_IMO='2_67', PATH_IMO='/data/dmc/MISS03/METADATA/', FLG_INFO=flg_withCO, ER_INFO=er_withCO)
  ;   the /FLAG keyword removes the sinc-interpolated oversampled CO regions of the spectra. 
  ;
  ;hfi_avg = hfi_read_avg_bandpass(/IMO, LBL_IMO='2_67', PATH_IMO='/data/dmc/MISS03/METADATA/', FLG_INFO=flg_avg, ER_INFO=er_avg, /FLAG)
  ;hfi_avg_withCO = hfi_read_avg_bandpass(/IMO, LBL_IMO='2_67', PATH_IMO='/data/dmc/MISS03/METADATA/', FLG_INFO=flg_avg_withCO, ER_INFO=er_avg_withCO)
  ;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;
  ;;    Get the spectra from the RIMO files instead.
  ;
  hfi_bp = hfi_read_bandpass(/RIMO, PATH_RIMO=fDIR, NAME_RIMO=HFI_RIMO_Name, FLG_INFO=flg, ER_INFO=hfi_er, /FLAG)
  hfi_bp_withCO = hfi_read_bandpass(/RIMO, PATH_RIMO=FDIR, NAME_RIMO=HFI_RIMO_Name, FLG_INFO=flg_withCO, ER_INFO=hfi_er_withCO, FLAG=0)
  ;   the /FLAG keyword removes the sinc-interpolated oversampled CO regions of the spectra. 
  ;
  hfi_avg = hfi_read_avg_bandpass(/RIMO, PATH_RIMO=fDIR, NAME_RIMO=HFI_RIMO_Name, FLG_INFO=flg_avg, ER_INFO=hfi_er_avg, /FLAG)
  hfi_avg_withCO = hfi_read_avg_bandpass(/RIMO, PATH_RIMO=fDIR, NAME_RIMO=HFI_RIMO_Name, FLG_INFO=flg_avg_withCO, ER_INFO=hfi_er_avg_withCO, FLAG=0)
  ;
  lfi_bp = lfi_read_bandpass(/RIMO, PATH_RIMO=fDIR, NAME_RIMO=LFI_RIMO_Name)
  lfi_avg= lfi_read_avg_bandpass(/RIMO, PATH_RIMO=fDIR, NAME_RIMO=LFI_RIMO_Name)
  ;
  loadct, 12
  plot, /xs, /ys, /XLOG, /YLOG, XR=[1d1,3d3], hfi_bp[0].freq/1d9, hfi_bp[0].trans, XTITLE='Frequency [GHz]', YTITLE='Trans. [a.u.]'
  FOR i = 0, N_ELEMENTS(hfi_bp) - 1 DO oplot, hfi_bp[i].freq/1d9, hfi_bp[i].trans, color=200
  FOR i = 0, N_ELEMENTS(hfi_avg) - 1 DO oplot, hfi_avg[i].freq/1d9, hfi_avg[i].trans, color=240, thick=2
  ;
  FOR i = 0, N_ELEMENTS(lfi_bp) - 1 DO oplot, lfi_bp[i].freq/1d9, lfi_bp[i].trans, color=40
  FOR i = 0, N_ELEMENTS(lfi_avg) - 1 DO oplot, lfi_avg[i].freq/1d9, lfi_avg[i].trans, color=100, thick=2
  ;
  ;
  ;   Now do a unit conversion calculation
  ;
  hfi_100_uc = hfi_unit_conversion(BP_INFO=hfi_bp, '100',hfibolo_100_uc, ABP=hfi_avg, AVG_UC=AVG_100_UC)
  hfi_143_uc = hfi_unit_conversion(BP_INFO=hfi_bp, '143',hfibolo_143_uc, ABP=hfi_avg, AVG_UC=AVG_143_UC)
  hfi_217_uc = hfi_unit_conversion(BP_INFO=hfi_bp, '217',hfibolo_217_uc, ABP=hfi_avg, AVG_UC=AVG_217_UC)
  hfi_353_uc = hfi_unit_conversion(BP_INFO=hfi_bp, '353',hfibolo_353_uc, ABP=hfi_avg, AVG_UC=AVG_353_UC)
  hfi_545_uc = hfi_unit_conversion(BP_INFO=hfi_bp, '545',hfibolo_545_uc, ABP=hfi_avg, AVG_UC=AVG_545_UC)
  hfi_857_uc = hfi_unit_conversion(BP_INFO=hfi_bp, '857',hfibolo_857_uc, ABP=hfi_avg, AVG_UC=AVG_857_UC)
  ;stop
  ;
  lfi_30_uc = hfi_unit_conversion(BP_INFO=lfi_bp, '30', lfibolo_30_uc, /lfi, ABP=lfi_avg, AVG_UC=AVG_30_UC)
  lfi_44_uc = hfi_unit_conversion(BP_INFO=lfi_bp, '44', lfibolo_44_uc, /lfi, ABP=lfi_avg, AVG_UC=AVG_44_UC)
  lfi_70_uc = hfi_unit_conversion(BP_INFO=lfi_bp, '70', lfibolo_70_uc, /lfi, ABP=lfi_avg, AVG_UC=AVG_70_UC)
  ;
  ;  The unit conversions may also be done without the detector spectra (just the band-average spectra) as follows:
  ;
  hfi_100_uc2 = hfi_unit_conversion('100',ABP=hfi_avg, AVG_UC=AVG_100_UC2)  ;  similar for the other bands.  
  ;
  ;  Show an example of unit conversion with an uncertainty estimate employing 100 iterations.
  ;
  hfi_100_uc3 = hfi_unit_conversion(BP_INFO=hfi_bp, '100',hfibolo_100_uc3, ABP=hfi_avg, AVG_UC=AVG_100_UC3, BP_ERR=hfi_er, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_143_uc3 = hfi_unit_conversion(BP_INFO=hfi_bp, '143',hfibolo_143_uc3, ABP=hfi_avg, AVG_UC=AVG_143_UC3, BP_ERR=hfi_er, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_217_uc3 = hfi_unit_conversion(BP_INFO=hfi_bp, '217',hfibolo_217_uc3, ABP=hfi_avg, AVG_UC=AVG_217_UC3, BP_ERR=hfi_er, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_353_uc3 = hfi_unit_conversion(BP_INFO=hfi_bp, '353',hfibolo_353_uc3, ABP=hfi_avg, AVG_UC=AVG_353_UC3, BP_ERR=hfi_er, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_545_uc3 = hfi_unit_conversion(BP_INFO=hfi_bp, '545',hfibolo_545_uc3, ABP=hfi_avg, AVG_UC=AVG_545_UC3, BP_ERR=hfi_er, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_857_uc3 = hfi_unit_conversion(BP_INFO=hfi_bp, '857',hfibolo_857_uc3, ABP=hfi_avg, AVG_UC=AVG_857_UC3, BP_ERR=hfi_er, EABP=hfi_er_avg, /GETERR, NITER=100)
  ;
  print, 'case 1 - both detector and band spectra: '
  help, avg_100_uc, /struct
  print, 'case 2 - only band spectra: '
  help, avg_100_uc2, /struct
  print, 'case 3 - case 1 with an uncertainty estimate: '
  help, avg_100_uc3, /struct
  print, 'Case 1 and 2 show the same for the band-spectrum.  Case 3 has an additional error component included with the output structure.'
  ;
  ;   Now do the colour correction example, powerlaw, alpha = 2.0
  ;
  hfi_100_a2_cc = hfi_colour_correction(BP_INFO=hfi_bp, '100', hfibolo_100_a2_cc, /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_100_a2_cc)
  hfi_143_a2_cc = hfi_colour_correction(BP_INFO=hfi_bp, '143', hfibolo_143_a2_cc, /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_143_a2_cc)
  hfi_217_a2_cc = hfi_colour_correction(BP_INFO=hfi_bp, '217', hfibolo_217_a2_cc, /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_217_a2_cc)
  hfi_353_a2_cc = hfi_colour_correction(BP_INFO=hfi_bp, '353', hfibolo_353_a2_cc, /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_353_a2_cc)
  hfi_545_a2_cc = hfi_colour_correction(BP_INFO=hfi_bp, '545', hfibolo_545_a2_cc, /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_545_a2_cc)
  hfi_857_a2_cc = hfi_colour_correction(BP_INFO=hfi_bp, '857', hfibolo_857_a2_cc, /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_857_a2_cc)
  ;
  lfi_30_a2_cc = hfi_colour_correction(BP_INFO=lfi_bp, '30', lfibolo_30_a2_cc, /POWERLAW, ALPHA=2.0, ABP=lfi_avg, AVG_CC=AVG_30_a2_cc, /LFI)
  lfi_44_a2_cc = hfi_colour_correction(BP_INFO=lfi_bp, '44', lfibolo_44_a2_cc, /POWERLAW, ALPHA=2.0, ABP=lfi_avg, AVG_CC=AVG_44_a2_cc, /LFI)
  lfi_70_a2_cc = hfi_colour_correction(BP_INFO=lfi_bp, '70', lfibolo_70_a2_cc, /POWERLAW, ALPHA=2.0, ABP=lfi_avg, AVG_CC=AVG_70_a2_cc, /LFI)
  ;
  ;  Repeat the colour correction for HFI with an uncertainty estimate
  hfi_100_a2_cc2 = hfi_colour_correction(BP_INFO=hfi_bp, '100', hfibolo_100_a2_cc2, /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_100_a2_cc2, BP_ERR=hfi_er, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_143_a2_cc2 = hfi_colour_correction(BP_INFO=hfi_bp, '143', hfibolo_143_a2_cc2, /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_143_a2_cc2, BP_ERR=hfi_er, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_217_a2_cc2 = hfi_colour_correction(BP_INFO=hfi_bp, '217', hfibolo_217_a2_cc2, /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_217_a2_cc2, BP_ERR=hfi_er, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_353_a2_cc2 = hfi_colour_correction(BP_INFO=hfi_bp, '353', hfibolo_353_a2_cc2, /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_353_a2_cc2, BP_ERR=hfi_er, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_545_a2_cc2 = hfi_colour_correction(BP_INFO=hfi_bp, '545', hfibolo_545_a2_cc2, /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_545_a2_cc2, BP_ERR=hfi_er, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_857_a2_cc2 = hfi_colour_correction(BP_INFO=hfi_bp, '857', hfibolo_857_a2_cc2, /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_857_a2_cc2, BP_ERR=hfi_er, EABP=hfi_er_avg, /GETERR, NITER=100)
  ;
  ;  Check the Effective Frequencies for the above colour correction, first get it for Spectral Index = -1, then for Spectral Index = +2.
  ;  You can also get the uncertainty of the effective frequency; do this for the alpha=+2 case below.
  ;
  hfi_100_am1_cc = hfi_colour_correction(BP_INFO=hfi_bp,'100',/POWERLAW,ALPHA=-1.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_100_am1_NU, CH_NUEFF=CH_100_am1_NU, BOLO_NUEFF=BOLO_100_am1_NU)
  hfi_143_am1_cc = hfi_colour_correction(BP_INFO=hfi_bp,'143',/POWERLAW,ALPHA=-1.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_143_am1_NU, CH_NUEFF=CH_143_am1_NU, BOLO_NUEFF=BOLO_143_am1_NU)
  hfi_217_am1_cc = hfi_colour_correction(BP_INFO=hfi_bp,'217',/POWERLAW,ALPHA=-1.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_217_am1_NU, CH_NUEFF=CH_217_am1_NU, BOLO_NUEFF=BOLO_217_am1_NU)
  hfi_353_am1_cc = hfi_colour_correction(BP_INFO=hfi_bp,'353',/POWERLAW,ALPHA=-1.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_353_am1_NU, CH_NUEFF=CH_353_am1_NU, BOLO_NUEFF=BOLO_353_am1_NU)
  hfi_545_am1_cc = hfi_colour_correction(BP_INFO=hfi_bp,'545',/POWERLAW,ALPHA=-1.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_545_am1_NU, CH_NUEFF=CH_545_am1_NU, BOLO_NUEFF=BOLO_545_am1_NU)
  hfi_857_am1_cc = hfi_colour_correction(BP_INFO=hfi_bp,'857',/POWERLAW,ALPHA=-1.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_857_am1_NU, CH_NUEFF=CH_857_am1_NU, BOLO_NUEFF=BOLO_857_am1_NU)
  ;
  lfi_30_am1_cc = hfi_colour_correction(BP_INFO=lfi_bp,'30',/POWERLAW,ALPHA=-1.0,ABP=lfi_avg,/GETNUEFF, AVG_NUEFF=AVG_30_am1_NU, CH_NUEFF=CH_30_am1_NU, BOLO_NUEFF=BOLO_30_am1_NU,/LFI)
  lfi_44_am1_cc = hfi_colour_correction(BP_INFO=lfi_bp,'44',/POWERLAW,ALPHA=-1.0,ABP=lfi_avg,/GETNUEFF, AVG_NUEFF=AVG_44_am1_NU, CH_NUEFF=CH_44_am1_NU, BOLO_NUEFF=BOLO_44_am1_NU,/LFI)
  lfi_70_am1_cc = hfi_colour_correction(BP_INFO=lfi_bp,'70',/POWERLAW,ALPHA=-1.0,ABP=lfi_avg,/GETNUEFF, AVG_NUEFF=AVG_70_am1_NU, CH_NUEFF=CH_70_am1_NU, BOLO_NUEFF=BOLO_70_am1_NU,/LFI)
  ;
  hfi_100_a2_cc = hfi_colour_correction(BP_INFO=hfi_bp,'100',/POWERLAW,ALPHA=2.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_100_a2_NU, CH_NUEFF=CH_100_a2_NU, BOLO_NUEFF=BOLO_100_a2_NU, /GETERR, NITER=100, BP_ERR=hfi_er, EABP=hfi_er_avg)
  hfi_143_a2_cc = hfi_colour_correction(BP_INFO=hfi_bp,'143',/POWERLAW,ALPHA=2.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_143_a2_NU, CH_NUEFF=CH_143_a2_NU, BOLO_NUEFF=BOLO_143_a2_NU, /GETERR, NITER=100)
  hfi_217_a2_cc = hfi_colour_correction(BP_INFO=hfi_bp,'217',/POWERLAW,ALPHA=2.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_217_a2_NU, CH_NUEFF=CH_217_a2_NU, BOLO_NUEFF=BOLO_217_a2_NU, /GETERR, NITER=100)
  hfi_353_a2_cc = hfi_colour_correction(BP_INFO=hfi_bp,'353',/POWERLAW,ALPHA=2.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_353_a2_NU, CH_NUEFF=CH_353_a2_NU, BOLO_NUEFF=BOLO_353_a2_NU, /GETERR, NITER=100)
  hfi_545_a2_cc = hfi_colour_correction(BP_INFO=hfi_bp,'545',/POWERLAW,ALPHA=2.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_545_a2_NU, CH_NUEFF=CH_545_a2_NU, BOLO_NUEFF=BOLO_545_a2_NU, /GETERR, NITER=100)
  hfi_857_a2_cc = hfi_colour_correction(BP_INFO=hfi_bp,'857',/POWERLAW,ALPHA=2.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_857_a2_NU, CH_NUEFF=CH_857_a2_NU, BOLO_NUEFF=BOLO_857_a2_NU, /GETERR, NITER=100)
  ;
  print, 'The /GETNUEFF keyword does not change the standard output, but provides the effective frequencies as an additional output.'
  help, bolo_100_am1_nu, /struct
  help, bolo_100_a2_nu, /struct
  ;
  lfi_30_a2_cc = hfi_colour_correction(BP_INFO=lfi_bp,'30',/POWERLAW,ALPHA=2.0,ABP=lfi_avg,/GETNUEFF, AVG_NUEFF=AVG_30_a2_NU, CH_NUEFF=CH_30_a2_NU, BOLO_NUEFF=BOLO_30_a2_NU,/LFI)
  lfi_44_a2_cc = hfi_colour_correction(BP_INFO=lfi_bp,'44',/POWERLAW,ALPHA=2.0,ABP=lfi_avg,/GETNUEFF, AVG_NUEFF=AVG_44_a2_NU, CH_NUEFF=CH_44_a2_NU, BOLO_NUEFF=BOLO_44_a2_NU,/LFI)
  lfi_70_a2_cc = hfi_colour_correction(BP_INFO=lfi_bp,'70',/POWERLAW,ALPHA=2.0,ABP=lfi_avg,/GETNUEFF, AVG_NUEFF=AVG_70_a2_NU, CH_NUEFF=CH_70_a2_NU, BOLO_NUEFF=BOLO_70_a2_NU,/LFI)
  ;
  ;
  ;   Now do a CO correction example, HFI only as no CO lines are found in LFI.
  ;
  vrad = 0d   ;   km/s
  hfi_100_co = hfi_co_correction( BP_INFO=hfi_bp_withCO, '100', hfibolo_100_co, hfibolo_100_13co, VRAD=vrad, ABP=hfi_avg_withCO, $
                                    AVG_CO=AVG_100_CO, AVG_13CO=AVG_100_13CO, CH_CO=CH_100_CO, CH_13CO=CH_100_13CO, $
                                    BP_FLG=flg_withCO, FABP=flg_avg_withCO, BP_ERR=hfi_er_withCO, EABP=hfi_er_avg_withCO)
  ;
  hfi_143_co = hfi_co_correction( BP_INFO=hfi_bp_withCO, '143', hfibolo_143_co, hfibolo_143_13co, VRAD=vrad, ABP=hfi_avg_withCO, $
                                    AVG_CO=AVG_143_CO, AVG_13CO=AVG_143_13CO, CH_CO=CH_143_CO, CH_13CO=CH_143_13CO, $
                                    BP_FLG=flg_withCO, FABP=flg_avg_withCO, BP_ERR=hfi_er_withCO, EABP=hfi_er_avg_withCO)
  ;
  hfi_217_co = hfi_co_correction( BP_INFO=hfi_bp_withCO, '217', hfibolo_217_co, hfibolo_217_13co, VRAD=vrad, ABP=hfi_avg_withCO, $
                                    AVG_CO=AVG_217_CO, AVG_13CO=AVG_217_13CO, CH_CO=CH_217_CO, CH_13CO=CH_217_13CO, $
                                    BP_FLG=flg_withCO, FABP=flg_avg_withCO, BP_ERR=hfi_er_withCO, EABP=hfi_er_avg_withCO)
  ;
  hfi_353_co = hfi_co_correction( BP_INFO=hfi_bp_withCO, '353', hfibolo_353_co, hfibolo_353_13co, VRAD=vrad, ABP=hfi_avg_withCO, $
                                    AVG_CO=AVG_353_CO, AVG_13CO=AVG_353_13CO, CH_CO=CH_353_CO, CH_13CO=CH_353_13CO, $
                                    BP_FLG=flg_withCO, FABP=flg_avg_withCO, BP_ERR=hfi_er_withCO, EABP=hfi_er_avg_withCO)
  ;
  hfi_545_co = hfi_co_correction( BP_INFO=hfi_bp_withCO, '545', hfibolo_545_co, hfibolo_545_13co, VRAD=vrad, ABP=hfi_avg_withCO, $
                                    AVG_CO=AVG_545_CO, AVG_13CO=AVG_545_13CO, CH_CO=CH_545_CO, CH_13CO=CH_545_13CO, $
                                    BP_FLG=flg_withCO, FABP=flg_avg_withCO, BP_ERR=hfi_er_withCO, EABP=hfi_er_avg_withCO)
  ;
  hfi_857_co = hfi_co_correction( BP_INFO=hfi_bp_withCO, '857', hfibolo_857_co, hfibolo_857_13co, VRAD=vrad, ABP=hfi_avg_withCO, $
                                    AVG_CO=AVG_857_CO, AVG_13CO=AVG_857_13CO, CH_CO=CH_857_CO, CH_13CO=CH_857_13CO, $
                                    BP_FLG=flg_withCO, FABP=flg_avg_withCO, BP_ERR=hfi_er_withCO, EABP=hfi_er_avg_withCO)
  ;
  ;
  ;
  ;   Now do a CO correction example with uncertainty, HFI only as no CO lines are found in LFI.
  ;   Use the spectral uncertainty, as well as a +/- 30 km/s radial velocity uncertainty (spacecraft velocity wrt line of sight).
  ;
  vrad = 0d   ;   km/s
  hfi_100_co = hfi_co_correction( BP_INFO=hfi_bp_withCO, '100', hfibolo_100_co2, hfibolo_100_13co2, VRAD=vrad, ABP=hfi_avg_withCO, $
                                    AVG_CO=AVG_100_CO2, AVG_13CO=AVG_100_13CO2, CH_CO=CH_100_CO2, CH_13CO=CH_100_13CO2, $
                                    BP_FLG=flg_withCO, FABP=flg_avg_withCO, BP_ERR=hfi_er_withCO, EABP=hfi_er_avg_withCO, /GETERR, NITER=100, ER_VRAD = 30d)
  ;
  hfi_143_co = hfi_co_correction( BP_INFO=hfi_bp_withCO, '143', hfibolo_143_co2, hfibolo_143_13co2, VRAD=vrad, ABP=hfi_avg_withCO, $
                                    AVG_CO=AVG_143_CO2, AVG_13CO=AVG_143_13CO2, CH_CO=CH_143_CO2, CH_13CO=CH_143_13CO2, $
                                    BP_FLG=flg_withCO, FABP=flg_avg_withCO, BP_ERR=hfi_er_withCO, EABP=hfi_er_avg_withCO, /GETERR, NITER=100, ER_VRAD = 30d)
  ;
  hfi_217_co = hfi_co_correction( BP_INFO=hfi_bp_withCO, '217', hfibolo_217_co2, hfibolo_217_13co2, VRAD=vrad, ABP=hfi_avg_withCO, $
                                    AVG_CO=AVG_217_CO2, AVG_13CO=AVG_217_13CO2, CH_CO=CH_217_CO2, CH_13CO=CH_217_13CO2, $
                                    BP_FLG=flg_withCO, FABP=flg_avg_withCO, BP_ERR=hfi_er_withCO, EABP=hfi_er_avg_withCO, /GETERR, NITER=100, ER_VRAD = 30d)
  ;
  hfi_353_co = hfi_co_correction( BP_INFO=hfi_bp_withCO, '353', hfibolo_353_co2, hfibolo_353_13co2, VRAD=vrad, ABP=hfi_avg_withCO, $
                                    AVG_CO=AVG_353_CO2, AVG_13CO=AVG_353_13CO2, CH_CO=CH_353_CO2, CH_13CO=CH_353_13CO2, $
                                    BP_FLG=flg_withCO, FABP=flg_avg_withCO, BP_ERR=hfi_er_withCO, EABP=hfi_er_avg_withCO, /GETERR, NITER=100, ER_VRAD = 30d)
  ;
  hfi_545_co = hfi_co_correction( BP_INFO=hfi_bp_withCO, '545', hfibolo_545_co2, hfibolo_545_13co2, VRAD=vrad, ABP=hfi_avg_withCO, $
                                    AVG_CO=AVG_545_CO2, AVG_13CO=AVG_545_13CO2, CH_CO=CH_545_CO2, CH_13CO=CH_545_13CO2, $
                                    BP_FLG=flg_withCO, FABP=flg_avg_withCO, BP_ERR=hfi_er_withCO, EABP=hfi_er_avg_withCO, /GETERR, NITER=100, ER_VRAD = 30d)
  ;
  hfi_857_co = hfi_co_correction( BP_INFO=hfi_bp_withCO, '857', hfibolo_857_co2, hfibolo_857_13co2, VRAD=vrad, ABP=hfi_avg_withCO, $
                                    AVG_CO=AVG_857_CO2, AVG_13CO=AVG_857_13CO2, CH_CO=CH_857_CO2, CH_13CO=CH_857_13CO2, $
                                    BP_FLG=flg_withCO, FABP=flg_avg_withCO, BP_ERR=hfi_er_withCO, EABP=hfi_er_avg_withCO, /GETERR, NITER=100, ER_VRAD = 30d)
  ;
  print, 'Compare the two CO values, the first without an uncertainty, and the second with one.'
  help, avg_100_co, /struct
  print, avg_100_co
  help, avg_100_co2, /struct
  print, avg_100_co2
  ;
  ;
  ;  By request, I am combining the CMB to IRAS conversion with a spectral index=2 conversion.
  ;  
  ;  Placeholders for a moment...
  lfibolo_30_uccc = lfibolo_30_a2_cc
  lfibolo_44_uccc = lfibolo_44_a2_cc
  lfibolo_70_uccc = lfibolo_70_a2_cc
  avg_30_uccc = avg_30_a2_cc
  avg_44_uccc = avg_44_a2_cc
  avg_70_uccc = avg_70_a2_cc
  ;
  hfibolo_100_uccc = hfibolo_100_a2_cc
  hfibolo_143_uccc = hfibolo_143_a2_cc
  hfibolo_217_uccc = hfibolo_217_a2_cc
  hfibolo_353_uccc = hfibolo_353_a2_cc
  hfibolo_545_uccc = hfibolo_545_a2_cc
  hfibolo_857_uccc = hfibolo_857_a2_cc
  avg_100_uccc = avg_100_a2_cc
  avg_143_uccc = avg_143_a2_cc
  avg_217_uccc = avg_217_a2_cc
  avg_353_uccc = avg_353_a2_cc
  avg_545_uccc = avg_545_a2_cc
  avg_857_uccc = avg_857_a2_cc
  ;
  cc_30_a2 = lfibolo_30_uccc.cc
  cc_44_a2 = lfibolo_44_uccc.cc
  cc_70_a2 = lfibolo_70_uccc.cc
  ;
  cc_30_a2_avg = avg_30_uccc.cc
  cc_44_a2_avg = avg_44_uccc.cc
  cc_70_a2_avg = avg_70_uccc.cc
  ;
  cc_100_a2 = hfibolo_100_uccc.cc
  cc_143_a2 = hfibolo_143_uccc.cc
  cc_217_a2 = hfibolo_217_uccc.cc
  cc_353_a2 = hfibolo_353_uccc.cc
  cc_545_a2 = hfibolo_545_uccc.cc
  cc_857_a2 = hfibolo_857_uccc.cc
  ;
  cc_100_a2_avg = avg_100_uccc.cc
  cc_143_a2_avg = avg_143_uccc.cc
  cc_217_a2_avg = avg_217_uccc.cc
  cc_353_a2_avg = avg_353_uccc.cc
  cc_545_a2_avg = avg_545_uccc.cc
  cc_857_a2_avg = avg_857_uccc.cc
  ;
  uc_30_kcmb = lfibolo_30_uc.KCMB2MJYSR
  uc_44_kcmb = lfibolo_44_uc.KCMB2MJYSR
  uc_70_kcmb = lfibolo_70_uc.KCMB2MJYSR
  uc_30_avg_kcmb = avg_30_uc.KCMB2MJYSR
  uc_44_avg_kcmb = avg_44_uc.KCMB2MJYSR
  uc_70_avg_kcmb = avg_70_uc.KCMB2MJYSR
  ;
  uc_100_kcmb = hfibolo_100_uc.KCMB2MJYSR
  uc_143_kcmb = hfibolo_143_uc.KCMB2MJYSR
  uc_217_kcmb = hfibolo_217_uc.KCMB2MJYSR
  uc_353_kcmb = hfibolo_353_uc.KCMB2MJYSR
  uc_545_kcmb = hfibolo_545_uc.KCMB2MJYSR
  uc_857_kcmb = hfibolo_857_uc.KCMB2MJYSR
  uc_100_avg_kcmb = avg_100_uc.KCMB2MJYSR
  uc_143_avg_kcmb = avg_143_uc.KCMB2MJYSR
  uc_217_avg_kcmb = avg_217_uc.KCMB2MJYSR
  uc_353_avg_kcmb = avg_353_uc.KCMB2MJYSR
  uc_545_avg_kcmb = avg_545_uc.KCMB2MJYSR
  uc_857_avg_kcmb = avg_857_uc.KCMB2MJYSR
  ;
  lfibolo_30_uccc.cc = uc_30_kcmb*cc_30_a2
  lfibolo_44_uccc.cc = uc_44_kcmb*cc_44_a2
  lfibolo_70_uccc.cc = uc_70_kcmb*cc_70_a2
  avg_30_uccc.cc = uc_30_avg_kcmb*cc_30_a2_avg
  avg_44_uccc.cc = uc_44_avg_kcmb*cc_44_a2_avg
  avg_70_uccc.cc = uc_70_avg_kcmb*cc_70_a2_avg
  ;
  hfibolo_100_uccc.cc = uc_100_kcmb*cc_100_a2
  hfibolo_143_uccc.cc = uc_143_kcmb*cc_143_a2
  hfibolo_217_uccc.cc = uc_217_kcmb*cc_217_a2
  hfibolo_353_uccc.cc = uc_353_kcmb*cc_353_a2
  hfibolo_545_uccc.cc = uc_545_kcmb*cc_545_a2
  hfibolo_857_uccc.cc = uc_857_kcmb*cc_857_a2
  avg_100_uccc.cc = uc_100_avg_kcmb*cc_100_a2_avg
  avg_143_uccc.cc = uc_143_avg_kcmb*cc_143_a2_avg
  avg_217_uccc.cc = uc_217_avg_kcmb*cc_217_a2_avg
  avg_353_uccc.cc = uc_353_avg_kcmb*cc_353_a2_avg
  avg_545_uccc.cc = uc_545_avg_kcmb*cc_545_a2_avg
  avg_857_uccc.cc = uc_857_avg_kcmb*cc_857_a2_avg
  ;
  ;
  ;
  ;   Print some of the numbers for verification.
  ;
  print, ';Unit conversion data:'
  print, ';Detector   Kcmb to MJy/sr      MJy/sr to Tb      Kcmb to ySZ'
  print,';LFI:'
  FOR i = 0, N_ELEMENTS(lfibolo_30_uc) - 1 DO print, ';',lfibolo_30_uc[i].(0),'   ',lfibolo_30_uc[i].(1),'   ',lfibolo_30_uc[i].(2),'   ',lfibolo_30_uc[i].(3);,'   ',lfibolo_30_uc[i].(4),'   ',lfibolo_30_uc[i].(5) 
  print,';', avg_30_uc.(0),'      ',avg_30_uc.(1),'   ',avg_30_uc.(2),'   ',avg_30_uc.(3);,'   ',avg_30_uc.(4),'   ',avg_30_uc.(5)
  FOR i = 0, N_ELEMENTS(lfibolo_44_uc) - 1 DO print,';', lfibolo_44_uc[i].(0),'   ',lfibolo_44_uc[i].(1),'   ',lfibolo_44_uc[i].(2),'   ',lfibolo_44_uc[i].(3);,'   ',lfibolo_44_uc[i].(4),'   ',lfibolo_44_uc[i].(5) 
  print, ';',avg_44_uc.(0),'      ',avg_44_uc.(1),'   ',avg_44_uc.(2),'   ',avg_44_uc.(3);,'   ',avg_44_uc.(4),'   ',avg_44_uc.(5)
  FOR i = 0, N_ELEMENTS(lfibolo_70_uc) - 1 DO print,';', lfibolo_70_uc[i].(0),'   ',lfibolo_70_uc[i].(1),'   ',lfibolo_70_uc[i].(2),'   ',lfibolo_70_uc[i].(3);,'   ',lfibolo_70_uc[i].(4),'   ',lfibolo_70_uc[i].(5) 
  print, ';',avg_70_uc.(0),'      ',avg_70_uc.(1),'   ',avg_70_uc.(2),'   ',avg_70_uc.(3);,'   ',avg_70_uc.(4),'   ',avg_70_uc.(5)
  print,';HFI:'
  a = hfibolo_100_uc
  aa = avg_100_uc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',a[i].(2),'   ',a[i].(3);,'   ',a[i].(4),'   ',a[i].(5) 
  print,';', aa.(0),'         ',aa.(1),'   ',aa.(2),'   ',aa.(3);,'   ',aa.(4),'   ',aa.(5)
  a = hfibolo_143_uc
  aa = avg_143_uc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',a[i].(2),'   ',a[i].(3);,'   ',a[i].(4),'   ',a[i].(5) 
  print,';', aa.(0),'         ',aa.(1),'   ',aa.(2),'   ',aa.(3);,'   ',aa.(4),'   ',aa.(5)
  a = hfibolo_217_uc
  aa = avg_217_uc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',a[i].(2),'   ',a[i].(3);,'   ',a[i].(4),'   ',a[i].(5) 
  print,';', aa.(0),'         ',aa.(1),'   ',aa.(2),'   ',aa.(3);,'   ',aa.(4),'   ',aa.(5)
  a = hfibolo_353_uc
  aa = avg_353_uc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',a[i].(2),'   ',a[i].(3);,'   ',a[i].(4),'   ',a[i].(5) 
  print,';', aa.(0),'         ',aa.(1),'   ',aa.(2),'   ',aa.(3);,'   ',aa.(4),'   ',aa.(5)
  a = hfibolo_545_uc
  aa = avg_545_uc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',a[i].(2),'   ',a[i].(3);,'   ',a[i].(4),'   ',a[i].(5) 
  print,';', aa.(0),'         ',aa.(1),'   ',aa.(2),'   ',aa.(3);,'   ',aa.(4),'   ',aa.(5)
  a = hfibolo_857_uc
  aa = avg_857_uc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',a[i].(2),'   ',a[i].(3);,'   ',a[i].(4),'   ',a[i].(5) 
  print,';', aa.(0),'         ',aa.(1),'   ',aa.(2),'   ',aa.(3);,'   ',aa.(4),'   ',aa.(5)
  ;
  ;stop
  ;
  print, ';'
  print, ';Colour Correction data, POWERLAW, ALPHA=2.0 (Spectral Index -1 to +2):'
  print, ';Detector       CC                  Nu_eff (-1) [GHz]  Nu_eff (+2) [GHz]'
  print,';LFI:'
  a = lfibolo_30_a2_cc
  aa = avg_30_a2_cc
  aaa = bolo_30_am1_Nu
  aaaa = bolo_30_a2_Nu
  aaaaa = avg_30_am1_nu
  aaaaaa = avg_30_a2_Nu
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',aaa[i].(2),'   ', aaaa[i].(2)
  print,';', aa.(0),'       ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  a = lfibolo_44_a2_cc
  aa = avg_44_a2_cc
  aaa =   bolo_44_am1_Nu
  aaaa =  bolo_44_a2_Nu
  aaaaa =  avg_44_am1_nu
  aaaaaa = avg_44_a2_Nu
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',aaa[i].(2),'   ', aaaa[i].(2) 
  print,';', aa.(0),'       ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  a = lfibolo_70_a2_cc
  aa = avg_70_a2_cc
  aaa =   bolo_70_am1_Nu
  aaaa =  bolo_70_a2_Nu
  aaaaa =  avg_70_am1_nu
  aaaaaa = avg_70_a2_Nu
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',aaa[i].(2),'   ', aaaa[i].(2) 
  print,';', aa.(0),'       ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  print, ';HFI:'
  a  = hfibolo_100_a2_cc
  aa = avg_100_a2_cc
  aaa =   bolo_100_am1_Nu
  aaaa =  bolo_100_a2_Nu
  aaaaa =  avg_100_am1_nu
  aaaaaa = avg_100_a2_Nu
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',aaa[i].(2),'   ', aaaa[i].(2) 
  print,';', aa.(0),'        ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  a  = hfibolo_143_a2_cc
  aa = avg_143_a2_cc
  aaa =   bolo_143_am1_Nu
  aaaa =  bolo_143_a2_Nu
  aaaaa =  avg_143_am1_nu
  aaaaaa = avg_143_a2_Nu
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',aaa[i].(2),'   ', aaaa[i].(2) 
  print,';', aa.(0),'        ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  a  = hfibolo_217_a2_cc
  aa = avg_217_a2_cc
  aaa =   bolo_217_am1_Nu
  aaaa =  bolo_217_a2_Nu
  aaaaa =  avg_217_am1_nu
  aaaaaa = avg_217_a2_Nu
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',aaa[i].(2),'   ', aaaa[i].(2) 
  print,';', aa.(0),'        ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  a  = hfibolo_353_a2_cc
  aa = avg_353_a2_cc
  aaa =   bolo_353_am1_Nu
  aaaa =  bolo_353_a2_Nu
  aaaaa =  avg_353_am1_nu
  aaaaaa = avg_353_a2_Nu
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',aaa[i].(2),'   ', aaaa[i].(2) 
  print,';', aa.(0),'        ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  a  = hfibolo_545_a2_cc
  aa = avg_545_a2_cc
  aaa =   bolo_545_am1_Nu
  aaaa =  bolo_545_a2_Nu
  aaaaa =  avg_545_am1_nu
  aaaaaa = avg_545_a2_Nu
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',aaa[i].(2),'   ', aaaa[i].(2) 
  print,';', aa.(0),'        ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  a  = hfibolo_857_a2_cc
  aa = avg_857_a2_cc
  aaa =   bolo_857_am1_Nu
  aaaa =  bolo_857_a2_Nu
  aaaaa =  avg_857_am1_nu
  aaaaaa = avg_857_a2_Nu
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',aaa[i].(2),'   ', aaaa[i].(2) 
  print,';', aa.(0),'        ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  ;
  ;stop
  ;
  print,';'
  print,';CO correction data:'
  print,';Detector    J+1 -> J      12CO               13CO'
  a    = hfibolo_100_co2
  aa   = hfibolo_100_13co2
  aaa  =     avg_100_co2
  aaaa =     avg_100_13co2
  Nls = N_ELEMENTS(a[0].coline)
  Nds = N_ELEMENTS(a)
  FOR ii=0,Nls - 1 DO FOR i=0,Nds -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],' +/- ',(a[i].er)[ii]  ,'   ',(aa[i].cc)[ii],' +/- ',(aa[i].er)[ii]
  FOR ii=0,Nls -1 DO print,';', aaa.chname,'         ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],' +/- ',(aaa.er)[ii],'   ',(aaaa.cc)[ii],' +/- ',(aaaa.er)[ii]
  a    = hfibolo_143_co2
  aa   = hfibolo_143_13co2
  aaa  =     avg_143_co2
  aaaa =     avg_143_13co2
  Nls = N_ELEMENTS(a[0].coline)
  Nds = N_ELEMENTS(a)
;  FOR ii=0,Nls - 1 DO FOR i=0,Nds -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],'   ',(aa[i].cc)[ii]
;  FOR i=0,Nds - 1 DO FOR ii=0,Nls -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],'   ',(aa[i].cc)[ii]
;  FOR ii=0,Nls -1 DO print,';', aaa.chname,'         ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],'   ',(aaaa.cc)[ii]
  FOR ii=0,Nls - 1 DO FOR i=0,Nds -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],' +/- ',(a[i].er)[ii]  ,'   ',(aa[i].cc)[ii],' +/- ',(aa[i].er)[ii]
  FOR ii=0,Nls -1 DO print,';', aaa.chname,'         ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],' +/- ',(aaa.er)[ii],'   ',(aaaa.cc)[ii],' +/- ',(aaaa.er)[ii]
  a    = hfibolo_217_co2
  aa   = hfibolo_217_13co2
  aaa  =     avg_217_co2
  aaaa =     avg_217_13co2
  Nls = N_ELEMENTS(a[0].coline)
  Nds = N_ELEMENTS(a)
;  FOR ii=0,Nls - 1 DO FOR i=0,Nds -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],'   ',(aa[i].cc)[ii]
;  FOR i=0,Nds - 1 DO FOR ii=0,Nls -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],'   ',(aa[i].cc)[ii]
;  FOR ii=0,Nls -1 DO print,';', aaa.chname,'         ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],'   ',(aaaa.cc)[ii]
  FOR ii=0,Nls - 1 DO FOR i=0,Nds -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],' +/- ',(a[i].er)[ii]  ,'   ',(aa[i].cc)[ii],' +/- ',(aa[i].er)[ii]
  FOR ii=0,Nls -1 DO print,';', aaa.chname,'         ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],' +/- ',(aaa.er)[ii],'   ',(aaaa.cc)[ii],' +/- ',(aaaa.er)[ii]
  a    = hfibolo_353_co2
  aa   = hfibolo_353_13co2
  aaa  =     avg_353_co2
  aaaa =     avg_353_13co2
  Nls = N_ELEMENTS(a[0].coline)
  Nds = N_ELEMENTS(a)
;  FOR ii=0,Nls - 1 DO FOR i=0,Nds -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],'   ',(aa[i].cc)[ii]
;  FOR i=0,Nds - 1 DO FOR ii=0,Nls -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],'   ',(aa[i].cc)[ii]
;  FOR ii=0,Nls -1 DO print,';', aaa.chname,'         ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],'   ',(aaaa.cc)[ii]
  FOR ii=0,Nls - 1 DO FOR i=0,Nds -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],' +/- ',(a[i].er)[ii]  ,'   ',(aa[i].cc)[ii],' +/- ',(aa[i].er)[ii]
  FOR ii=0,Nls -1 DO print,';', aaa.chname,'         ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],' +/- ',(aaa.er)[ii],'   ',(aaaa.cc)[ii],' +/- ',(aaaa.er)[ii]
  a    = hfibolo_545_co2
  aa   = hfibolo_545_13co2
  aaa  =     avg_545_co2
  aaaa =     avg_545_13co2
  Nls = N_ELEMENTS(a[0].coline)
  Nds = N_ELEMENTS(a)
;  FOR ii=0,Nls - 1 DO FOR i=0,Nds -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],'   ',(aa[i].cc)[ii]
;  FOR i=0,Nds - 1 DO FOR ii=0,Nls -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],'   ',(aa[i].cc)[ii]
;  FOR ii=0,Nls -1 DO print,';', aaa.chname,'        ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],'   ',(aaaa.cc)[ii]
  FOR ii=0,Nls - 1 DO FOR i=0,Nds -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],' +/- ',(a[i].er)[ii]  ,'   ',(aa[i].cc)[ii],' +/- ',(aa[i].er)[ii]
  FOR ii=0,Nls -1 DO print,';', aaa.chname,'         ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],' +/- ',(aaa.er)[ii],'   ',(aaaa.cc)[ii],' +/- ',(aaaa.er)[ii]
  a    = hfibolo_857_co2
  aa   = hfibolo_857_13co2
  aaa  =     avg_857_co2
  aaaa =     avg_857_13co2
  Nls = N_ELEMENTS(a[0].coline)
  Nds = N_ELEMENTS(a)
;  FOR ii=0,Nls - 1 DO FOR i=0,Nds -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],'   ',(aa[i].cc)[ii]
;  FOR i=0,Nds - 1 DO FOR ii=0,Nls -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],'   ',(aa[i].cc)[ii]
;  FOR ii=0,Nls -1 DO print,';', aaa.chname,'        ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],'   ',(aaaa.cc)[ii]
  FOR ii=0,Nls - 1 DO FOR i=0,Nds -1 DO print,';', a[i].boloname,'   ',STRTRIM(STRING((a[i].coline)[ii]+1),2)+'->'+STRTRIM(STRING((a[i].coline)[ii]),2),'   ',(a[i].cc)[ii],' +/- ',(a[i].er)[ii]  ,'   ',(aa[i].cc)[ii],' +/- ',(aa[i].er)[ii]
  FOR ii=0,Nls -1 DO print,';', aaa.chname,'         ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],' +/- ',(aaa.er)[ii],'   ',(aaaa.cc)[ii],' +/- ',(aaaa.er)[ii]
  ;
  ;
  print, ';'
  print, ';Combined Unit Conversion / Colour Correction data, Kcmb to MJy/sr, from SI=-1 to SI=+2:'
  print, ';Detector       UC/CC Kcmb to MJy/sr (alpha=2)'
  print,';LFI:'
  a = lfibolo_30_uccc
  aa = avg_30_uccc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1) 
  print,';', aa.(0),'       ',aa.(1)
  a = lfibolo_44_uccc
  aa = avg_44_uccc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1) 
  print,';', aa.(0),'       ',aa.(1)
  a = lfibolo_70_uccc
  aa = avg_70_uccc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1) 
  print,';', aa.(0),'       ',aa.(1)
  print, ';HFI:'
  a  = hfibolo_100_uccc
  aa = avg_100_uccc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1) 
  print,';', aa.(0),'        ',aa.(1)
  a  = hfibolo_143_uccc
  aa = avg_143_uccc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1) 
  print,';', aa.(0),'        ',aa.(1)
  a  = hfibolo_217_uccc
  aa = avg_217_uccc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1) 
  print,';', aa.(0),'        ',aa.(1)
  a  = hfibolo_353_uccc
  aa = avg_353_uccc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1) 
  print,';', aa.(0),'        ',aa.(1)
  a  = hfibolo_545_uccc
  aa = avg_545_uccc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1) 
  print,';', aa.(0),'        ',aa.(1)
  a  = hfibolo_857_uccc
  aa = avg_857_uccc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1) 
  print,';', aa.(0),'        ',aa.(1)
  ;
  ;stop
END
;+
;
; LFI fastcc results:
;Detector        alpha     -2.00000     -1.50000     -1.00000    -0.500000      0.00000     0.500000      1.00000      1.50000      2.00000      2.50000
;      3.00000      3.50000      4.00000
;LFI-18     0.947889     0.960962     0.972064     0.981197     0.988360     0.993553     0.996776     0.998028     0.997311     0.994624
;     0.989967     0.983340     0.974742
;LFI-19     0.855161     0.877891     0.899496     0.919975     0.939330     0.957560     0.974664     0.990644      1.00550      1.01923
;      1.03183      1.04331      1.05367
;LFI-20     0.888581     0.907641     0.925336     0.941665     0.956630     0.970230     0.982464     0.993334      1.00284      1.01098
;      1.01775      1.02316      1.02721
;LFI-21     0.916742     0.932792     0.947251     0.960120     0.971400     0.981090     0.989189     0.995699      1.00062      1.00395
;      1.00569      1.00584      1.00440
;LFI-22      1.02457      1.02643      1.02666      1.02525      1.02220      1.01752      1.01120      1.00325     0.993667     0.982447
;     0.969591     0.955100     0.938975
;LFI-23     0.985392     0.991089     0.995586     0.998883      1.00098      1.00188      1.00157      1.00007     0.997368     0.993465
;     0.988362     0.982059     0.974556
;70GHz     0.937943     0.951267     0.962966     0.973041     0.981490     0.988314     0.993514     0.997088     0.999037     0.999362
;     0.998061     0.995135     0.990585
;LFI-24     0.978061     0.983786     0.988635     0.992610     0.995710     0.997935     0.999285     0.999760     0.999359     0.998084
;     0.995934     0.992909     0.989009
;LFI-25     0.966990     0.973920     0.980045     0.985365     0.989880     0.993590     0.996495     0.998595     0.999890      1.00038
;      1.00006     0.998944     0.997019
;LFI-26     0.957165     0.965579     0.973118     0.979781     0.985570     0.990484     0.994522     0.997686     0.999975      1.00139
;      1.00193      1.00159      1.00038
;44GHz     0.967910     0.974847     0.980940     0.986188     0.990590     0.994147     0.996860     0.998728     0.999750     0.999927
;     0.999260     0.997748     0.995390
;LFI-27     0.947654     0.959183     0.969272     0.977921     0.985130     0.990899     0.995228     0.998117     0.999566     0.999575
;     0.998144     0.995273     0.990962
;LFI-28     0.945519     0.957814     0.968519     0.977635     0.985160     0.991095     0.995440     0.998196     0.999361     0.998936
;     0.996922     0.993317     0.988122
;30GHz     0.946764     0.958638     0.969002     0.977856     0.985200     0.991034     0.995358     0.998172     0.999476     0.999269
;     0.997553     0.994327     0.989591
;LFI-23     0.985392     0.991089     0.995586     0.998883      1.00098      1.00188      1.00157      1.00007     0.997368     0.993465
;     0.988362     0.982059     0.974556
;70GHz     0.937943     0.951267     0.962966     0.973041     0.981490     0.988314     0.993514     0.997088     0.999037     0.999362
;     0.998061     0.995135     0.990585
;% LOADCT: Loading table 16 LEVEL
;case 1 - both detector and band spectra:
;** Structure <79bbe8>, 4 tags, length=40, data length=40, refs=2:
;   CHNAME          STRING    '100'
;   KCMB2MJYSR      DOUBLE           244.09605
;   MJYSR2TB        DOUBLE        0.0032548074
;   KCMB2YSZ        DOUBLE         -0.24814487
;case 2 - only band spectra:
;** Structure <a58ec8>, 4 tags, length=40, data length=40, refs=2:
;   CHNAME          STRING    '100'
;   KCMB2MJYSR      DOUBLE           244.09605
;   MJYSR2TB        DOUBLE        0.0032548074
;   KCMB2YSZ        DOUBLE         -0.24814487
;case 3 - case 1 with an uncertainty estimate:
;** Structure <a810d8>, 6 tags, length=56, data length=56, refs=2:
;   CHNAME          STRING    '100'
;   KCMB2MJYSR      DOUBLE           244.09605
;   MJYSR2TB        DOUBLE        0.0032548074
;   KCMB2YSZ        DOUBLE         -0.24814487
;   ERKCMB2MJYSR    DOUBLE          0.71090131
;   ERKCMB2YSZ      DOUBLE       0.00017364392
;Case 1 and 2 show the same for the band-spectrum.  Case 3 has an additional error component included with the output structure.
;The /GETNUEFF keyword does not change the standard output, but provides the effective frequencies as an additional output.
;** Structure <77ac28>, 3 tags, length=40, data length=40, refs=1:
;   BOLONAME        STRING    '00_100_1a'
;   UNIT            STRING    'GHz'
;   NUEFF           DOUBLE           99.453681
;** Structure <a737c8>, 4 tags, length=48, data length=48, refs=1:
;   BOLONAME        STRING    '00_100_1a'
;   UNIT            STRING    'GHz'
;   NUEFF           DOUBLE           101.92884
;   ER              DOUBLE          0.10075300
;Compare the two CO values, the first without an uncertainty, and the second with one.
;** Structure <a77428>, 3 tags, length=32, data length=26, refs=1:
;   CHNAME          STRING    '100'
;   COLINE          INT       Array[1]
;   CC              DOUBLE    Array[1]
;{ 100       0
;       14.781371
;}
;** Structure <a7b748>, 4 tags, length=40, data length=34, refs=2:
;   CHNAME          STRING    '100'
;   COLINE          INT       Array[1]
;   CC              DOUBLE    Array[1]
;   ER              DOUBLE    Array[1]
;{ 100       0
;       14.781371
;      0.23046190
;}
;Unit conversion data:
;Detector   Kcmb to MJy/sr      MJy/sr to Tb      Kcmb to ySZ
;LFI:
;30_28S          22.644522        0.040354188        -0.18729943
;30_28M          24.416912        0.040354188        -0.18747155
;30_27S          23.593779        0.040354188        -0.18740461
;30_27M          23.450841        0.040354188        -0.18734306
;30             23.509877        0.040354188        -0.18737944
;44_26S          55.803725        0.016735863        -0.19306685
;44_26M          54.800859        0.016735863        -0.19302219
;44_25S          56.093557        0.016735863        -0.19311908
;44_25M          55.320961        0.016735863        -0.19303548
;44_24S          55.369510        0.016735863        -0.19306920
;44_24M          57.005102        0.016735863        -0.19324335
;44             55.734911        0.016735863        -0.19309332
;70_23S          134.70328       0.0065671960        -0.21046684
;70_23M          132.57143       0.0065671960        -0.20998788
;70_22S          141.01550       0.0065671960        -0.21174161
;70_22M          134.74281       0.0065671960        -0.21061418
;70_21S          125.44897       0.0065671960        -0.20913173
;70_21M          128.75164       0.0065671960        -0.20972481
;70_20S          124.99620       0.0065671960        -0.20903845
;70_20M          123.38690       0.0065671960        -0.20869843
;70_19S          126.01674       0.0065671960        -0.20912070
;70_19M          115.71861       0.0065671960        -0.20737482
;70_18S          125.77654       0.0065671960        -0.20948468
;70_18M          135.33161       0.0065671960        -0.21084123
;70             129.18692       0.0065671960        -0.20976774
;HFI:
;00_100_1a          238.28710       0.0032548074        -0.24612330
;01_100_1b          241.85303       0.0032548074        -0.24703110
;20_100_2a          244.23755       0.0032548074        -0.24829497
;21_100_2b          243.35718       0.0032548074        -0.24798481
;40_100_3a          246.07147       0.0032548074        -0.24872684
;41_100_3b          240.17388       0.0032548074        -0.24690003
;80_100_4a          246.73156       0.0032548074        -0.24905638
;81_100_4b          247.62892       0.0032548074        -0.24923779
;100                244.09605       0.0032548074        -0.24814487
;02_143_1a          366.41083       0.0015916707        -0.35501466
;03_143_1b          369.59054       0.0015916707        -0.35735300
;30_143_2a          366.72495       0.0015916707        -0.35551056
;31_143_2b          370.70014       0.0015916707        -0.35818005
;50_143_3a          360.04181       0.0015916707        -0.35015023
;51_143_3b          365.95294       0.0015916707        -0.35477254
;82_143_4a          371.34690       0.0015916707        -0.35964515
;83_143_4b          369.09526       0.0015916707        -0.35685076
;10_143_5          380.11620       0.0015916707        -0.36556768
;42_143_6          373.34134       0.0015916707        -0.36038049
;60_143_7          381.25112       0.0015916707        -0.36660150
;70_143_8     -1.0000000e+09     -1.0000000e+09     -1.0000000e+09
;143                371.73268       0.0015916707        -0.35922296
;11_217_5a          479.80492      0.00069120334          7.4840126
;12_217_5b          480.43636      0.00069120334          6.9767443
;43_217_6a          480.34162      0.00069120334          7.0508221
;44_217_6b          480.35436      0.00069120334          7.0168633
;61_217_7a          481.04863      0.00069120334          6.7975464
;62_217_7b          479.99507      0.00069120334          7.7196475
;71_217_8a          479.80963      0.00069120334          7.2564550
;72_217_8b          480.76858      0.00069120334          6.8621903
;04_217_1          486.03218      0.00069120334          4.3470990
;22_217_2          486.40077      0.00069120334          4.0275920
;52_217_3          486.89237      0.00069120334          4.1183628
;84_217_4          486.01643      0.00069120334          4.4334423
;217                483.68697      0.00069120334          5.1537039
;23_353_3a          289.16164      0.00026120163         0.16225395
;24_353_3b          289.19320      0.00026120163         0.16282742
;32_353_4a          286.62378      0.00026120163         0.15887265
;33_353_4b          286.60036      0.00026120163         0.15939380
;53_353_5a          289.98373      0.00026120163         0.16446016
;54_353_5b          289.89718      0.00026120163         0.16488375
;63_353_6a          288.81774      0.00026120163         0.16298773
;64_353_6b          292.65121      0.00026120163         0.16641383
;05_353_1          288.41816      0.00026120163         0.16229311
;13_353_2          287.86993      0.00026120163         0.16197640
;45_353_7          285.34106      0.00026120163         0.15816234
;85_353_8          283.51491      0.00026120163         0.15708786
;353                287.44556      0.00026120163         0.16109498
;14_545_1          57.083087      0.00010958025        0.068805257
;34_545_2          58.882452      0.00010958025        0.069630064
;55_545_3     -1.0000000e+09     -1.0000000e+09     -1.0000000e+09
;73_545_4          58.059545      0.00010958025        0.069047331
;545                58.035560      0.00010958025        0.069178639
;25_857_1          2.1892087      4.4316316e-05        0.037844453
;35_857_2          2.3456859      4.4316316e-05        0.038160558
;65_857_3          2.2133673      4.4316316e-05        0.037846276
;74_857_4          2.4023450      4.4316316e-05        0.038177835
;857                2.2681256      4.4316316e-05        0.037981062
;
;Colour Correction data, POWERLAW, ALPHA=2.0 (Spectral Index -1 to +2):
;Detector       CC                  Nu_eff (-1) [GHz]  Nu_eff (+2) [GHz]
;LFI:
;30_28S          1.0720293          27.555895          28.154799
;30_28M         0.99331614          28.305574          28.790044
;30_27S          1.0283302          27.944282          28.535575
;30_27M          1.0349350          27.947989          28.345034
;30              1.0321373          27.931726          28.455889
;44_26S          1.0184672          43.710835          44.074033
;44_26M          1.0373265          43.371636          43.949271
;44_25S          1.0129400          43.774626          44.183717
;44_25M          1.0275119          43.556458          43.995483
;44_24S          1.0264349          43.548954          44.059792
;44_24M         0.99612572          44.009542          44.451492
;44              1.0195863          43.663183          44.120709
;70_23S         0.99243238          70.194185          71.322348
;70_23M          1.0104834          69.843631          70.764392
;70_22S         0.94285090          71.277121          72.787859
;70_22M         0.99148230          70.148518          71.483303
;70_21S          1.0716931          68.343764          69.696489
;70_21M          1.0415354          68.969292          70.412259
;70_20S          1.0760260          68.250706          69.585078
;70_20M          1.0916787          67.965482          69.173569
;70_19S          1.0669566          68.485464          69.694898
;70_19M          1.1707515          66.441366          67.513380
;70_18S          1.0671979          68.293865          70.095786
;70_18M         0.98616394          70.225983          71.737696
;70              1.0378434          69.062723          70.467299
;HFI:
;00_100_1a         0.99179519          99.453681          101.92884
;01_100_1b         0.97434208          100.05710          102.51048
;20_100_2a         0.96064331          100.37545          103.34302
;21_100_2b         0.96513917          100.23073          103.13765
;40_100_3a         0.95227075          100.67724          103.59861
;41_100_3b         0.98139436          99.736911          102.44528
;80_100_4a         0.94863988          100.77240          103.81640
;81_100_4b         0.94472171          100.92481          103.91929
;100              0.96173861          100.36149          103.23782
;02_143_1a          1.0274265          140.37274          144.48359
;03_143_1b          1.0149215          140.96564          145.01514
;30_143_2a          1.0256636          140.42427          144.61331
;31_143_2b          1.0105579          141.17268          145.21261
;50_143_3a          1.0540336          139.16910          143.27545
;51_143_3b          1.0291866          140.27783          144.40885
;82_143_4a          1.0061104          141.28702          145.61336
;83_143_4b          1.0172143          140.86844          144.87449
;10_143_5         0.97451098          142.93137          146.86308
;42_143_6         0.99995884          141.66320          145.72121
;60_143_7         0.97009064          143.14353          147.08990
;70_143_8     -1.0000000e+09     -1.0000000e+09     -1.0000000e+09
;143               1.0060688          141.36240          145.45727
;11_217_5a         0.95420519          218.48941          224.29331
;12_217_5b         0.95119898          218.74555          224.46526
;43_217_6a         0.95166708          218.70492          224.44075
;44_217_6b         0.95169476          218.70748          224.40815
;61_217_7a         0.94972948          218.92451          224.45823
;62_217_7b         0.95549596          218.46132          224.03851
;71_217_8a         0.95308358          218.54436          224.43201
;72_217_8b         0.95048273          218.83700          224.44053
;04_217_1         0.92372880          221.09911          226.27404
;22_217_2         0.91865594          221.43011          226.81371
;52_217_3         0.92005774          221.44031          226.45811
;84_217_4         0.92495511          221.03335          226.11565
;217              0.93509079          220.11102          225.51546
;23_353_3a         0.94525258          357.23622          364.63976
;24_353_3b         0.94547560          357.01747          365.01442
;32_353_4a         0.92556041          359.93838          366.84353
;33_353_4b         0.92562884          359.73711          367.21510
;53_353_5a         0.95294775          355.87591          364.41411
;54_353_5b         0.95195590          355.80144          364.98808
;63_353_6a         0.94352632          357.08381          365.57918
;64_353_6b         0.97272517          353.82451          361.24630
;05_353_1         0.94057913          357.54598          365.76238
;13_353_2         0.93604130          358.00476          366.61476
;45_353_7         0.91695162          360.83797          368.39645
;85_353_8         0.90386346          362.25020          370.82054
;353              0.93271094          358.56805          366.77299
;14_545_1         0.92302463          554.43948          570.00070
;34_545_2         0.94195693          550.60607          566.36563
;55_545_3     -1.0000000e+09     -1.0000000e+09     -1.0000000e+09
;73_545_4         0.93789658          551.76273          566.48176
;545              0.93444602          552.21948          567.59628
;25_857_1         0.96944439          858.12873          880.88541
;35_857_2         0.98810254          852.27277          876.21871
;65_857_3         0.97322234          856.95061          879.90013
;74_857_4          1.0083172          847.23219          868.93969
;857              0.98082798          854.68501          877.72420
;
;CO correction data:
;Detector    J+1 -> J      12CO               13CO
;00_100_1a   1->0          10.873565 +/-       0.43156729          16.965922 +/-       0.86361829
;01_100_1b   1->0          12.609543 +/-       0.52700422          16.405345 +/-       0.78047743
;20_100_2a   1->0          14.698090 +/-       0.49820705          14.081151 +/-       0.78438057
;21_100_2b   1->0          12.017624 +/-       0.52099475          17.503830 +/-       0.91888241
;40_100_3a   1->0          16.359971 +/-       0.73440571          14.517842 +/-       0.80829321
;41_100_3b   1->0          11.784310 +/-       0.51054299          13.778121 +/-       0.74451730
;80_100_4a   1->0          19.099223 +/-       0.80341367          18.656492 +/-        1.0737397
;81_100_4b   1->0          16.108041 +/-       0.64715582          17.573116 +/-        1.0537438
;100         1->0          14.781371 +/-       0.25478486          15.548153 +/-       0.34636356
;02_143_1a   1->0        0.061284370 +/-     0.0040160394       0.0022280277 +/-    0.00044370396
;03_143_1b   1->0        0.043679395 +/-     0.0029832442       0.0017537164 +/-    0.00043481607
;30_143_2a   1->0        0.052327719 +/-     0.0036414629       0.0020480152 +/-    0.00042632944
;31_143_2b   1->0        0.055694880 +/-     0.0032012749       0.0022610539 +/-    0.00040300248
;50_143_3a   1->0        0.088060561 +/-     0.0066845357       0.0030871724 +/-    0.00038757117
;51_143_3b   1->0        0.073741721 +/-     0.0055923625       0.0024970386 +/-    0.00085120362
;82_143_4a   1->0        0.048888341 +/-     0.0030892445       0.0018470141 +/-    0.00040398879
;83_143_4b   1->0        0.049270618 +/-     0.0037152722       0.0018870800 +/-    0.00041745742
;10_143_5   1->0        0.020977058 +/-     0.0037663159       0.0012098875 +/-    0.00063808697
;42_143_6   1->0        0.057931473 +/-     0.0035589681       0.0020073957 +/-    0.00052097696
;60_143_7   1->0       0.0099021553 +/-    0.00058626810      0.00050880584 +/-    0.00020009015
;70_143_8   1->0     -1.0000000e+09 +/-     0.0039487650     -1.0000000e+09 +/-    0.00053366342
;02_143_1a   2->1        0.016682181 +/-    0.00052235767        0.025020669 +/-    0.00066742697
;03_143_1b   2->1        0.012608484 +/-     0.0036183630        0.025802457 +/-     0.0029400262
;30_143_2a   2->1        0.018061717 +/-     0.0044145641        0.036610644 +/-     0.0035396596
;31_143_2b   2->1        0.023435272 +/-     0.0032699161        0.042975703 +/-     0.0026482297
;50_143_3a   2->1       0.0040523037 +/-     0.0024113704        0.013213155 +/-     0.0025230951
;51_143_3b   2->1       0.0085426472 +/-     0.0024823886        0.018917069 +/-     0.0020561595
;82_143_4a   2->1        0.019845943 +/-     0.0021586233        0.035063225 +/-     0.0017771262
;83_143_4b   2->1        0.019550992 +/-     0.0027321194        0.025363934 +/-     0.0033652157
;10_143_5   2->1        0.023629988 +/-     0.0015560311        0.039552864 +/-     0.0012496755
;42_143_6   2->1        0.027749227 +/-     0.0014470834        0.046258000 +/-     0.0011559462
;60_143_7   2->1        0.013694609 +/-     0.0034912728        0.027745169 +/-     0.0028080943
;70_143_8   2->1     -1.0000000e+09 +/-     0.0010203237     -1.0000000e+09 +/-     0.0010796652
;143         1->0        0.046990209 +/-     0.0016492403       0.0017660848 +/-    0.00042616977
;143         2->1        0.017681967 +/-     0.0021530477        0.031734788 +/-     0.0017230927
;11_217_5a   2->1          43.977747 +/-       0.23903613          35.854445 +/-       0.31762624
;12_217_5b   2->1          43.685628 +/-       0.34670753          38.539078 +/-       0.23172754
;43_217_6a   2->1          38.915595 +/-       0.30868648          41.207816 +/-       0.25834986
;44_217_6b   2->1          40.744882 +/-       0.57164558          33.320968 +/-       0.22035827
;61_217_7a   2->1          45.509033 +/-       0.26315081          41.576614 +/-       0.35083756
;62_217_7b   2->1          43.587942 +/-       0.24675987          33.191508 +/-       0.22381257
;71_217_8a   2->1          45.307255 +/-       0.29975627          41.482995 +/-       0.34378464
;72_217_8b   2->1          41.785535 +/-       0.23901447          34.157357 +/-       0.26777834
;04_217_1   2->1          50.221777 +/-       0.27414078          34.416231 +/-       0.19565171
;22_217_2   2->1          42.470084 +/-       0.23369785          32.730356 +/-       0.24013414
;52_217_3   2->1          51.231089 +/-       0.29993646          37.367171 +/-       0.25340058
;84_217_4   2->1          47.750367 +/-       0.25574549          30.870055 +/-       0.24932395
;217         2->1          45.848892 +/-       0.13484196          35.375149 +/-      0.070762790
;23_353_3a   3->2          185.30842 +/-        1.9259444          133.25766 +/-        1.2174351
;24_353_3b   3->2          200.72854 +/-        2.1074054          166.58063 +/-       0.90642528
;32_353_4a   3->2          172.90573 +/-        1.1486221          120.97384 +/-        1.6730239
;33_353_4b   3->2          140.88208 +/-        2.3522368          125.14595 +/-        1.1310885
;53_353_5a   3->2          150.32681 +/-        1.2303712          138.05433 +/-       0.88473207
;54_353_5b   3->2          159.78650 +/-        1.6723575          143.90474 +/-       0.95918252
;63_353_6a   3->2          148.92286 +/-        2.3177244          142.95773 +/-       0.86059763
;64_353_6b   3->2          166.22641 +/-        1.2837987          166.86416 +/-        1.3718907
;05_353_1   3->2          170.32482 +/-        1.2324884          82.481385 +/-        1.1735347
;13_353_2   3->2          173.96652 +/-        1.2595755          130.81835 +/-        1.2312132
;45_353_7   3->2          196.91708 +/-        1.4512827          110.92494 +/-       0.65544299
;85_353_8   3->2          185.32158 +/-        1.6985804          99.881088 +/-       0.57130491
;353         3->2          175.09601 +/-       0.52080249          117.12316 +/-       0.51509174
;14_545_1   4->3          256.46494 +/-       0.88451884          47.775126 +/-       0.25552971
;34_545_2   4->3          268.28943 +/-        1.9948036          83.881390 +/-        1.1351645
;55_545_3   4->3     -1.0000000e+09 +/-        3.4408555     -1.0000000e+09 +/-       0.99462625
;73_545_4   4->3          230.72510 +/-        1.0751112          35.511449 +/-        1.5578499
;14_545_1   5->4          2216.0774 +/-        6.6110721          1144.5157 +/-        4.0672467
;34_545_2   5->4          2281.7047 +/-        6.8804863          1422.3663 +/-        15.086892
;55_545_3   5->4     -1.0000000e+09 +/-        6.8801221     -1.0000000e+09 +/-        7.1348931
;73_545_4   5->4          2473.5603 +/-        7.5380496          1492.1819 +/-        2.7779864
;545         4->3          252.49448 +/-       0.57946491          56.881515 +/-       0.20198065
;545         5->4          2322.1718 +/-        3.9032425          1356.0689 +/-        4.7398204
;25_857_1   6->5          7793.9265 +/-        23.233019          3264.5150 +/-        43.077760
;35_857_2   6->5          6701.7786 +/-        129.59289          1700.4582 +/-        18.131397
;65_857_3   6->5          6978.6630 +/-        87.564330          1417.8441 +/-        21.320292
;74_857_4   6->5          7565.6515 +/-        136.95747          1439.8122 +/-        24.649387
;25_857_1   7->6          72287.699 +/-        579.17972          61485.358 +/-        243.37674
;35_857_2   7->6          62773.359 +/-        725.80877          64154.547 +/-        144.29122
;65_857_3   7->6          82316.444 +/-        632.20911          57721.661 +/-        163.06853
;74_857_4   7->6          87559.045 +/-        264.40868          63469.266 +/-        367.74688
;25_857_1   8->7          168433.35 +/-        970.32905          136678.43 +/-        636.70655
;35_857_2   8->7          145618.13 +/-        498.75383          117747.02 +/-        335.79323
;65_857_3   8->7          154860.24 +/-        499.54633          126684.48 +/-        889.82663
;74_857_4   8->7          125210.41 +/-        354.80694          123686.75 +/-        350.71713
;25_857_1   9->8          4941.5215 +/-        357.78920          102256.05 +/-        366.25341
;35_857_2   9->8          5619.6731 +/-        74.219308          80170.029 +/-        862.69075
;65_857_3   9->8          6897.1493 +/-        125.21566          100933.42 +/-        479.48580
;74_857_4   9->8          144.73400 +/-        32.959934          51735.206 +/-        135.45856
;857         6->5          7217.1207 +/-        71.589380          2016.5076 +/-        4.6220072
;857         7->6          74874.417 +/-        142.33363          61405.178 +/-        86.889988
;857         8->7          151723.07 +/-        371.21922          126567.48 +/-        211.44498
;857         9->8          4998.1345 +/-        34.695681          88288.414 +/-        282.61030
;
;Combined Unit Conversion / Colour Correction data, Kcmb to MJy/sr, from SI=-1 to SI=+2:
;Detector       UC/CC Kcmb to MJy/sr (alpha=2)
;LFI:
;30_28S          24.275592
;30_28M          24.253713
;30_27S          24.262195
;30_27M          24.270096
;30              24.265421
;44_26S          56.834262
;44_26M          56.846385
;44_25S          56.819405
;44_25M          56.842947
;44_24S          56.833198
;44_24M          56.784248
;44              56.826554
;70_23S          133.68390
;70_23M          133.96122
;70_22S          132.95659
;70_22M          133.59511
;70_21S          134.44280
;70_21M          134.09939
;70_20S          134.49916
;70_20M          134.69885
;70_19S          134.45439
;70_19M          135.47774
;70_18S          134.22846
;70_18M          133.45916
;70              134.07579
;HFI:
;00_100_1a          236.33200
;01_100_1b          235.64759
;20_100_2a          234.62517
;21_100_2b          234.87355
;40_100_3a          234.32666
;41_100_3b          235.70529
;80_100_4a          234.05940
;81_100_4b          233.94042
;100               234.75659
;02_143_1a          376.46018
;03_143_1b          375.10539
;30_143_2a          376.13644
;31_143_2b          374.61395
;50_143_3a          379.49616
;51_143_3b          376.63385
;82_143_4a          373.61597
;83_143_4b          375.44899
;10_143_5          370.42741
;42_143_6          373.32598
;60_143_7          369.84814
;70_143_8      1.0000000e+18
;143               373.98863
;11_217_5a          457.83234
;12_217_5b          456.99057
;43_217_6a          457.12531
;44_217_6b          457.15072
;61_217_7a          456.86607
;62_217_7b          458.63335
;71_217_8a          457.29868
;72_217_8b          456.96223
;04_217_1          448.96192
;22_217_2          446.83496
;52_217_3          447.96909
;84_217_4          449.54338
;217               452.29123
;23_353_3a          273.33079
;24_353_3b          273.42512
;32_353_4a          265.28762
;33_353_4b          265.28556
;53_353_5a          276.33934
;54_353_5b          275.96934
;63_353_6a          272.50714
;64_353_6b          284.66920
;05_353_1          271.28010
;13_353_2          269.45814
;45_353_7          261.64394
;85_353_8          256.25876
;353               268.10362
;14_545_1          52.689095
;34_545_2          55.464733
;55_545_3      1.0000000e+18
;73_545_4          54.453849
;545               54.231098
;25_857_1          2.1223161
;35_857_2          2.3177782
;65_857_3          2.1540985
;74_857_4          2.4223259
;857               2.2246411
;
;IDL>
;
;-
