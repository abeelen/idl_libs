;+
; NAME:
;
;   lfi_read_bandpass
;
; PURPOSE:
;
;   Read LFI bandpass from official RIMO .fits files, or from the IMO
;
; CATEGORY:
;
;
;
; CALLING SEQUENCE:
;
;   bp = lfi_read_bandpass(/RIMO + Keywords )
;   bp = lfi_read_bandpass(/IMO + Keywords )
;
; INPUTS:
;
;   
; OPTIONAL INPUTS:
;
;   None
;
; KEYWORD PARAMETERS:
;  INPUT:
;   RIMO:         If a RIMO fits file is to be read.
;   PATH_RIMO:    The file path to the RIMO fits file.
;   NAME_RIMO:    The file name for the RIMO fits file, including the .fits suffix.
;   
;  OUTPUT:
;   ER_INFO:      The same info structure as BP_INFO, but with the uncertainty values in place of the transmission values.
;   NOTE: The ERR and ER_INFO keywords do not, at present, do much.  There is a zero-valued placeholder (in the RIMO file), 
;	or the data is not even there (IMO), so it isn't really there for either case.
;
; OUTPUTS:
;
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;    Planck collaboration only.
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;   bp = lfi_read_bandpass(/RIMO, PATH_RIMO='/path/to/RIMO/fits/file/', NAME_RIMO='LFI_RIMO_03062011_DX7.fits') 
;     ; Gets the spectra from the RIMO .fits file in RIMO_PATH, flg is the flag structre, er is the uncertainty structure.
;
; MODIFICATION HISTORY:
;
;   Created by L. Spencer, Sept, 2011
;   Modified by L. Spencer 2013/01/18:
;     shift the LFI frequency sampling by df/2
;     change the 
;   
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   A copy of the GNU General Public License is available at 
;   <http://www.gnu.org/licenses/>.
;   
;   Copyright Locke D. Spencer, 2013
;   
;-

FUNCTION lfi_read_bandpass, IMO = IMO, LBL_IMO=LBL_IMO, PATH_IMO=PATH_IMO, $
  RIMO = RIMO, PATH_RIMO=PATH_RIMO, NAME_RIMO=NAME_RIMO   ;, FLAG=FLAG, ERR=ERR, FLG_INFO=FLG_INFO, ER_INFO=ER_INFO
  ;
  ; -- Definitions
  CONST = { C: 2.99792458d+08, H: 6.6260755d-34, HBAR: 1.0545726691251021d-34, K: 1.3806580d-23, TCMB: 2.725d0 }
  ;
  N_DETECT = 22 ; Planck LFI
  ;
  N_MEASUREMENTS = 252l ; 17000l
  ;
  nuc = [28.4d0, 44.1d0,70.4d0] ; I need this to scale the transmission by (nu/nuc)^(-2)
  ;
  BP_INFO = replicate( { NAME: '', FREQ: dblarr( n_measurements), TRANS: dblarr( n_measurements) }, n_detect )
  ;FLG_INFO = BP_INFO
  ER_INFO = BP_INFO
  If Keyword_set( RIMO ) then begin
    If NOT Keyword_set( PATH_RIMO ) then PATH_RIMO =''
    IF ~KEYWORD_SET(NAME_RIMO) THEN NAME_RIMO = 'LFI_RIMO_18062012_DX9.fits'	;	'LFI_RIMO_03062011_DX7.fits'
    RIMO_FITS = NAME_RIMO
    FITS_RIMO = mrdfits( path_rimo + rimo_fits, 'CHANNEL_PARAMETERS', HDR_RIMO, /SILENT )
    FITS_RIMO_1 = FITS_RIMO
    ;stop
    ;If strpos( hdr_rimo[ 4 ], '52' ) eq -1 then stop,'Is it an HFI RIMO file?'
    FITS_RIMO_det = fits_rimo.detector
    FITS_RIMO_det = STRMID(STRTRIM(FITS_RIMO_DET,2),3)
    BOLO_ID_TMP = ['30_28S','30_28M','30_27S','30_27M',$
                  '44_26S','44_26M', '44_25S','44_25M', '44_24S','44_24M',$
                  '70_23S','70_23M', '70_22S','70_22M', '70_21S','70_21M', '70_20S','70_20M', '70_19S','70_19M', '70_18S','70_18M'];,$
                  ;'F30','F44','F70']
     For i = 0, N_Detect - 1 do begin
     ;stop
      NAME_RIMO_TMP = BOLO_ID_TMP[i]    ; strmid( fits_rimo[ i ].detector, 4  )
      Q_NAME = where( FITS_RIMO_det eq STRMID(NAME_RIMO_TMP,3) )
      BP_INFO[ i ].NAME = NAME_RIMO_TMP    ;   bolo_id_tmp[ q_name[ 0 ] ]  ; completing the name of the detector (e.g. 100-1a in the RIMO)
      ;FLG_INFO[i].name = BP_INFO[i].name
      ER_INFO[i].name = BP_INFO[i].name
      ;stop
    Endfor
    ;stop
    ; Joining the information of the bandpasses in the bp_info structure
    For i = 0, n_detect - 1 do begin
      ;
      ;FITS_RIMO = mrdfits( path_rimo + rimo_fits, fitsshift + i )
      ;stop
      BND_STR = STRMID(BP_INFO[i].NAME,0,2)
      CASE BND_STR OF
        '30': NUC_ = NUC[0]
        '44': NUC_ = NUC[1]
        '70': NUC_ = NUC[2]
        ELSE: ;
      ENDCASE
      FITS_RIMO = mrdfits( path_rimo + rimo_fits, 'BANDPASS_0'+STRMID(BP_INFO[i].NAME,0,2)+'-'+STRMID(BP_INFO[i].name,3,3), hdri, /SILENT)
        ;fits_RIMO_1[i].detector, hdri);fitsshift + i )
      Nms = TAG_NAMES(FITS_RIMO)
      Ns = N_TAGS(FITS_RIMO)
      NmStr = ''
      FOR ii = 0, Ns - 1 DO NmStr = NmStr+Nms[ii]
      FND_Er  = MAX([STRPOS(NmStr,'UNCERTAINTY'),STRPOS(NmStr,'uncertainty'),STRPOS(NmStr,'Uncertainty')])
      ;
      wn = fits_rimo.wavenumber   ;   in GHz
      ;  look for positive frequency
      PosFreq = WHERE(wn GT 0d, Npos)
      df = (wn[PosFreq[Npos - 1]] - wn[PosFreq[0]])/DOUBLE(Npos - 1d)
      wn[PosFreq] = wn[PosFreq] + df/2d
      trans = fits_rimo.transmission
      trans[PosFreq] = trans[PosFreq]*(wn[PosFreq]/NUC_)^(-2d)
      IF FND_ER GE 0 THEN yer = fits_rimo.uncertainty
      ;stop
      BP_INFO[ i ].FREQ  = wn * 1d9   ;   now in Hz    ;   * 1d2 * const.c ; wave number given in cm^-1
      BP_INFO[ i ].TRANS = trans  ; values of the transmission (normalized later)
      ER_INFO[i].freq = BP_INFO[i].freq
      IF FND_ER GE 0 THEN ER_INFO[i].trans = yer
      ;
    Endfor
    ; IF KEYWORD_SET(FLAG) THEN BP_INFO = FLG_INFO
    ;stop
  ENDIF   ; End the RIMO part...
  ;
  ; IMO
  ;
  IF Keyword_set( IMO ) THEN BEGIN
    IF KEYWORD_SET(LBL_IMO) THEN IMO_LBL = LBL_IMO ELSE IMO_LBL = '3_20_detilt_t2_ptcor6'	;	'2_67'
    IF KEYWORD_SET(PATH_IMO) THEN ImoFileMD = PATH_IMO ELSE ImoFileMD = '/data/dmc/MISS03/METADATA'
    ImoFile = ImoFileMD + '%lbl:IMO_' + imo_lbl
    ; Open Imo
    IMO_GROUP = PIOOpenIMOFile(ImoFile, 'r')
    print,'IMO_GROUP: ',imo_group
    IF imo_group LE 0 THEN pioerrmess, imo_group
    ; Joining the information of the bandpasses in the bp_info structure
    G_TYPE = 'PIOSTRING'
    TAIL_X = 'SpectralResp:SpecTransmissions:VectX'
    Tail_Flag = 'SpectralResp:SpecTransmissions:VectYFlag'
    Tail_Y = 'SpectralResp:SpecTransmissions:VectY'
    Tail_YError = 'SpectralResp:SpecTransmissions:VectYError'
    ;IF KEYWORD_SET(ERR) THEN Tail_Y = Tail_YError    ; = 'SpectralResp:SpecTransmissions:VectYError'
    CHANNEL_TMP = [ '30', '44', '70']
    N_DETECT = 0
    For i = 0, 2 do begin
      BOLO_LIST = get_hfibolo_list( CHANNEL = channel_tmp[ i ], /LFI )
      N_LIST = n_elements( bolo_list )
      For j = 0, n_list - 1 DO BEGIN
        BP_INFO[ n_detect + j ].NAME = bolo_list[ j ]
        ;FLG_INFO[n_detect + j].NAME = bolo_list[ j ]
        ;ER_INFO[n_detect + j].NAME = bolo_list[ j ]
        bName = STRMID(bolo_list[j],3,3)+'_'+STRMID(bolo_list[j],0,2)
        G_NAME_X = 'IMO:LFI:RAD:Rad_Pixel Name="' + bName  + '":' + tail_x
        DUMMY  = PIOGetValue( G_VALUE, gerror, g_type, gunit, gcomment, $
                                g_name_x, imo_group)
        ;stop
        Q = strpos( g_value, ':' , /reverse_s)
        G_VALUE_READ = strmid( g_value, 0, q )
        WN = pioread( g_value_read ) ; Wave number in cm^-1
        PosFreq = WHERE(WN GT 0d, Npos)
        WN = WN[PosFreq]
        Npts = N_ELEMENTS(WN)
        DF = (WN[Npts - 1d] - WN[0])/DOUBLE(Npts - 1d)
        WN = WN + df/2d
        G_NAME_Y = 'IMO:LFI:RAD:Rad_Pixel Name="' + bName  + '":' + tail_y
        DUMMY  = PIOGetValue( G_VALUE, gerror, g_type, gunit, gcomment, $
                                g_name_y, imo_group)
        Q = strpos( g_value, ':' , /reverse_s)
        G_VALUE_READ = strmid( g_value, 0, q )
        trans = pioread( g_value_read )
        trans = trans[PosFreq]
        trans = trans*(wn/nuc[i])^(-2d)
        ;print, N_ELEMENTS(WN)
        ;
        ;   FIXME: must check that uncertainty is present in the IMO....(what happens if it is not?)
        G_NAME_YER = 'IMO:LFI:RAD:Rad_Pixel Name="' + bName  + '":' + tail_yError
        DUMMY  = PIOGetValue( G_VALUE, gerror, g_type, gunit, gcomment, $
                                g_name_yEr, imo_group)
        ;stop
        Q = strpos( g_value, ':' , /reverse_s)
        G_VALUE_READ = strmid( g_value, 0, q )
        yerror = pioread( g_value_read )
        ;
        G_NAME_flag = 'IMO:LFI:RAD:Rad_Pixel Name="' + bName  + '":' + tail_Flag
        ;DUMMY  = PIOGetValue( G_VALUE, gerror, g_type, gunit, gcomment, $
        ;                            g_name_flag, imo_group)
        ;Q = strpos( g_value, ':' , /reverse_s)
        ;G_VALUE_READ = strmid( g_value, 0, q )
        ;flg = pioread( g_value_read ) ; Wave number in cm^-1
        ;;
        ;stop
        ;print, N_ELEMENTS(WN)
        BP_INFO[ n_detect + j ].FREQ = wn *1d9      ; const.c *1d2 * wn ; freq in Hz
        ;FLG_INFO[ n_detect + j ].FREQ = wn * 1d9    ; const.c *1d2 * wn ; freq in Hz
        ER_INFO[ n_detect + j ].FREQ = wn * 1d9     ; const.c *1d2 * wn ; freq in Hz
        ;
        BP_INFO[ n_detect + j ].TRANS = trans
        ;FLG_INFO[ n_detect + j ].TRANS = flg
        ER_INFO[ n_detect + j ].TRANS = yerror
      Endfor
      N_DETECT = n_detect + n_list
    Endfor
  Endif   ;   End of the IMO section
  ;
  return, bp_info
END