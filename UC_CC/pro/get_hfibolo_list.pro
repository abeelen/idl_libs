;+
; NAME:
;
;   get_hfibolo_list				, dark = dark, thermo = thermo, channel = channel, polar = polar, lfi = lfi
;
; PURPOSE:
;
;   Output a list of HFI/LFI detectors to use with the unit conversion and colour correction scripts.
;
; CATEGORY:
;
;
;
; CALLING SEQUENCE:
;
;	get_hfibolo_list, dark = dark, thermo = thermo, channel = channel, polar = polar, lfi = lfi
;
; INPUTS:
;
;   
;
; OPTIONAL INPUTS:
;
;
;
; KEYWORD PARAMETERS:
; 
;   INPUT:
;     dark :     Also return the dark detectors (HFI only).
;     channel :  Only return the detectors from a specified channel (30,44, or 70 for LFI, 100, 143, 217, 353, 545, or 857 for HFI).  Input as string
;     polar :    Only return the PSB pairs within a given channel (only works if CHANNEL is alsop set, does not work for LFI).
;     lfi :      Return the LFI detectors
;
;   OUTPUT:
;     
; OUTPUTS:
;
;   listbolo :   String array containing the names of the requested detectors.
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;   Planck collaboration only.
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;  hfi = get_hfibolo_list( channel = '100')
;  print, hfi
;  lfi = get_hfibolo_list(/LFI, CHANNEL='30')
;  print, lfi
;
; 00_100_1a 01_100_1b 20_100_2a 21_100_2b 40_100_3a 41_100_4b 80_100_4a 81_100_4b
; 30_28S 30_28M 30_27S 30_27M
;
;
; MODIFICATION HISTORY:
; Created by S. Hildebrandt and J.F. Macias-Perez, Oct, 2010
; Modified by L. Spencer Oct. 2011
;   Switched order of the LFI naming convention (det_band to band_det) to match that of HFI and the LFI RIMO file.
;   Added header information.
;   
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   A copy of the GNU General Public License is available at 
;   <http://www.gnu.org/licenses/>.
;   
;   Copyright S. Hildebrandt, J. Macias-Perez, L. Spencer, 2013
;   
;-

FUNCTION  get_hfibolo_list, dark = dark, channel = channel, polar = polar, lfi = lfi
  
  if keyword_set(lfi) then begin
     boloname=['18M_70', '18S_70', '19M_70', '19S_70', '20M_70', '20S_70', '21M_70', '21S_70', '22M_70', '22S_70', '23M_70', '23S_70',$
                '24M_44', '24S_44', '25M_44', '25S_44', '26M_44', '26S_44', $
                '27M_30', '27S_30', '28M_30', '28S_30']
     boloname = ['30_28S','30_28M','30_27S','30_27M',$
                  '44_26S','44_26M', '44_25S','44_25M', '44_24S','44_24M',$
                  '70_23S','70_23M', '70_22S','70_22M', '70_21S','70_21M', '70_20S','70_20M', '70_19S','70_19M', '70_18S','70_18M']
     listbolo = boloname
    IF keyword_set(channel) THEN BEGIN
      listbolo = boloname[where(strmatch(boloname,'*' + channel + '*',/fold) eq 1, nboloch)]
    ENDIF 
  endif else begin 
  boloname = ['00_100_1a','01_100_1b', '20_100_2a', '21_100_2b', '40_100_3a','41_100_3b', $
            '80_100_4a', '81_100_4b', '02_143_1a', '03_143_1b', '30_143_2a', '31_143_2b', $
            '50_143_3a','51_143_3b', '82_143_4a', '83_143_4b', '10_143_5', '42_143_6', $
            '60_143_7', '70_143_8', '11_217_5a', '12_217_5b','43_217_6a', '44_217_6b', '61_217_7a',$
            '62_217_7b', '71_217_8a', '72_217_8b', '04_217_1', '22_217_2', '52_217_3', '84_217_4', '23_353_3a', '24_353_3b', '32_353_4a', '33_353_4b',$
            '53_353_5a', '54_353_5b', '63_353_6a', '64_353_6b', '05_353_1', '13_353_2', '45_353_7', '85_353_8', '14_545_1', $
            '34_545_2', '55_545_3', '73_545_4', '25_857_1', '35_857_2', '65_857_3', '74_857_4']

  darkname = ['15_Dark1','75_Dark2']
  


  listbolo = boloname
  IF keyword_set(dark) THEN listbolo = [listbolo, darkname]
  IF keyword_set(channel) THEN BEGIN
    IF keyword_set(polar) THEN BEGIN 
     listbolo = boloname[where(strmatch(boloname,'*' + channel + '*',/fold) eq 1 AND $
         (strmatch(boloname,'*a*', /fold) EQ 1 OR strmatch(boloname,'*b*',/fold) EQ 1), nboloch)]
  
    ENDIF ELSE BEGIN 
     listbolo = boloname[where(strmatch(boloname,'*' + channel + '*',/fold) eq 1, nboloch)]
    ENDELSE 
 
  ENDIF 
 endelse


  return, listbolo
END
