PRO hfi_lfi_test_script_PLA_RIMO, FDIR=FDIR, HFI_RIMO_Name=HFI_RIMO_Name, LFI_RIMO_Name=LFI_RIMO_Name
;
;+
; NAME:
;
;   hfi_lfi_test_script_PLA_RIMO
;
; PURPOSE:
;
;   Demonstrate the calling of and output from the various IDL unit conversion / colour correction scripts.
;
; CATEGORY:
;
;
;
; CALLING SEQUENCE:
;
;    hfi_lfi_test_script_PLA_RIMO, FDIR=FDIR
;
; INPUTS:
;
;   
; OPTIONAL INPUTS:
;
;
; KEYWORD PARAMETERS:
; 
;   INPUT:
;     FDIR :     The location of a directory where the RIMO FITS files are located (a default is specified if not set).
;	HFI_RIMO_Name :	The name of the HFI RIMO .fits file.
;	LFI_RIMO_Name :	The name of the LFI RIMO .fits file.
;
;   OUTPUT:
;
;
; OUTPUTS:
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;   Planck collaboration only.
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;  hfi_lfi_test_script
;  ;   look at plot, look at output tables, compare printed numbers to those at the end of this file.
;  .continue
;
;
; MODIFICATION HISTORY:
; Created by L. Spencer August 31, 2011
; Modified by L. Spencer Sept. 17, 2012 to set default values to v300 HFI spectra, and DX9 RIMO .fits files.
; Modified by L. Spencer Jan. 2013 to change default values to v3.02 spectra and   delta-DX9 RIMO .fits files,
;  also following changes to Unit conversion script structure.
; Modified by L. Spencer 2013/02/05:  Added examples for coefficient uncertainties to be output from the code.
;                                     Added the LFI_testcc routine to the UcCC package, and added LFI_fastcc_test example to this example routine.
;
;   
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   A copy of the GNU General Public License is available at 
;   <http://www.gnu.org/licenses/>.
;   
;   Copyright Locke D. Spencer, 2013
;   
;
;-
;
  ; 
  ; This script runs through a basic test of all of the bandpass conversions/corrections, to demonstrate the use of them.
  ; 
  ; The following results, in addition to a plot of all of the bandpasses, should be printed to the screen upon running this script.
  ; I have moved the results printout from the top to the bottom of this script for readability.  
  ; This should run fine from magique3 as is.  Please copy the RIMO .fits files over to the (user selected) 
  ; FDIR keyword directory if beign run on another machine.
  ; 
  ; 
  ; 
  ; 
  ; 
  ; 
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;
  ;  The LFI_fastcc routine does not require any input spectra as it has quadratic coefficients hard-coded inside the program.
  ;
  print, ' LFI fastcc results: '
  LFI_fastcc_test ; this should print out some LFI CC coefficients.
  ;
  ;  Another few examples of using the LFI_fastcc routine directly (taken from the above test script).
  ;
  spectralIndex = [-2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0]
  print,'LFI-23',LFI_fastcc(70.4,spectralIndex,detector=23)
  print,'70GHz',LFI_fastcc(70.4,spectralIndex)
  ; 
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;
  IF ~KEYWORD_SET(FDIR) THEN fDIR = '/space/lspencer/UcCC_CodeUpdate_v302/'	;	'/wrk/lspencer/mywork2/test/'
  IF ~KEYWORD_SET(HFI_RIMO_Name) THEN HFI_RIMO_Name = 'HFI_RIMO_R1.00.fits'	;	
  IF ~KEYWORD_SET(LFI_RIMO_Name) THEN LFI_RIMO_Name = 'LFI_RIMO_DX9v1.fits'
  ;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;
  ;;    Get the spectra from the RIMO files.
  ;
  hfi_avg = hfi_read_avg_bandpass(/RIMO, PATH_RIMO=fDIR, NAME_RIMO=HFI_RIMO_Name, FLG_INFO=flg_avg, ER_INFO=hfi_er_avg, /FLAG)
  hfi_avg_withCO = hfi_read_avg_bandpass(/RIMO, PATH_RIMO=fDIR, NAME_RIMO=HFI_RIMO_Name, FLG_INFO=flg_avg_withCO, ER_INFO=hfi_er_avg_withCO, FLAG=0)
  ;
  lfi_bp = lfi_read_bandpass(/RIMO, PATH_RIMO=fDIR, NAME_RIMO=LFI_RIMO_Name)
  lfi_avg= lfi_read_avg_bandpass(/RIMO, PATH_RIMO=fDIR, NAME_RIMO=LFI_RIMO_Name)
  ;
  loadct, 12
  plot, /xs, /ys, /XLOG, /YLOG, XR=[1d1,3d3], hfi_avg[0].freq/1d9, hfi_avg[0].trans, XTITLE='Frequency [GHz]', YTITLE='Trans. [a.u.]'
  FOR i = 0, N_ELEMENTS(hfi_avg) - 1 DO oplot, hfi_avg[i].freq/1d9, hfi_avg[i].trans, color=240, thick=2
  ;
  FOR i = 0, N_ELEMENTS(lfi_bp) - 1 DO oplot, lfi_bp[i].freq/1d9, lfi_bp[i].trans, color=40
  FOR i = 0, N_ELEMENTS(lfi_avg) - 1 DO oplot, lfi_avg[i].freq/1d9, lfi_avg[i].trans, color=100, thick=2
  ;
  ;
  ;   Now do a unit conversion calculation
  ;  The unit conversions may be done without the detector spectra (just the band-average spectra -- the PLA RIMO does not have the detector spectra) as follows:
  ;
  hfi_100_uc = hfi_unit_conversion('100', ABP=hfi_avg, AVG_UC=AVG_100_UC)
  hfi_143_uc = hfi_unit_conversion('143', ABP=hfi_avg, AVG_UC=AVG_143_UC)
  hfi_217_uc = hfi_unit_conversion('217', ABP=hfi_avg, AVG_UC=AVG_217_UC)
  hfi_353_uc = hfi_unit_conversion('353', ABP=hfi_avg, AVG_UC=AVG_353_UC)
  hfi_545_uc = hfi_unit_conversion('545', ABP=hfi_avg, AVG_UC=AVG_545_UC)
  hfi_857_uc = hfi_unit_conversion('857', ABP=hfi_avg, AVG_UC=AVG_857_UC)
  ;stop
  ;
  lfi_30_uc = hfi_unit_conversion(BP_INFO=lfi_bp, '30', lfibolo_30_uc, /lfi, ABP=lfi_avg, AVG_UC=AVG_30_UC)
  lfi_44_uc = hfi_unit_conversion(BP_INFO=lfi_bp, '44', lfibolo_44_uc, /lfi, ABP=lfi_avg, AVG_UC=AVG_44_UC)
  lfi_70_uc = hfi_unit_conversion(BP_INFO=lfi_bp, '70', lfibolo_70_uc, /lfi, ABP=lfi_avg, AVG_UC=AVG_70_UC)
  ;
  ;  Show an example of unit conversion with an uncertainty estimate employing 100 iterations.
  ;
  hfi_100_uc2 = hfi_unit_conversion('100', ABP=hfi_avg, AVG_UC=AVG_100_UC2, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_143_uc2 = hfi_unit_conversion('143', ABP=hfi_avg, AVG_UC=AVG_143_UC2, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_217_uc2 = hfi_unit_conversion('217', ABP=hfi_avg, AVG_UC=AVG_217_UC2, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_353_uc2 = hfi_unit_conversion('353', ABP=hfi_avg, AVG_UC=AVG_353_UC2, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_545_uc2 = hfi_unit_conversion('545', ABP=hfi_avg, AVG_UC=AVG_545_UC2, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_857_uc2 = hfi_unit_conversion('857', ABP=hfi_avg, AVG_UC=AVG_857_UC2, EABP=hfi_er_avg, /GETERR, NITER=100)
  ;
  print, 'case 1 - no uncertainty estimate: '
  help, avg_100_uc, /struct
  print, 'case 2 - with uncertainty estimate: '
  help, avg_100_uc2, /struct
  print, 'Case 1 and 2 show the same coefficient, but the latter also has an uncertainty estimate component included with the output structure.'
  print, 'Increase the value of NITER to improve the accuracy of this estimate.'
  ;
  ;   Now do the colour correction example, powerlaw, alpha = 2.0
  ;
  hfi_100_a2_cc = hfi_colour_correction('100', /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_100_a2_cc)
  hfi_143_a2_cc = hfi_colour_correction('143', /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_143_a2_cc)
  hfi_217_a2_cc = hfi_colour_correction('217', /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_217_a2_cc)
  hfi_353_a2_cc = hfi_colour_correction('353', /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_353_a2_cc)
  hfi_545_a2_cc = hfi_colour_correction('545', /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_545_a2_cc)
  hfi_857_a2_cc = hfi_colour_correction('857', /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_857_a2_cc)
  ;
  lfi_30_a2_cc = hfi_colour_correction(BP_INFO=lfi_bp, '30', lfibolo_30_a2_cc, /POWERLAW, ALPHA=2.0, ABP=lfi_avg, AVG_CC=AVG_30_a2_cc, /LFI)
  lfi_44_a2_cc = hfi_colour_correction(BP_INFO=lfi_bp, '44', lfibolo_44_a2_cc, /POWERLAW, ALPHA=2.0, ABP=lfi_avg, AVG_CC=AVG_44_a2_cc, /LFI)
  lfi_70_a2_cc = hfi_colour_correction(BP_INFO=lfi_bp, '70', lfibolo_70_a2_cc, /POWERLAW, ALPHA=2.0, ABP=lfi_avg, AVG_CC=AVG_70_a2_cc, /LFI)
  ;
  ;  Repeat the colour correction for HFI with an uncertainty estimate
  hfi_100_a2_cc2 = hfi_colour_correction('100', /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_100_a2_cc2, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_143_a2_cc2 = hfi_colour_correction('143', /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_143_a2_cc2, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_217_a2_cc2 = hfi_colour_correction('217', /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_217_a2_cc2, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_353_a2_cc2 = hfi_colour_correction('353', /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_353_a2_cc2, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_545_a2_cc2 = hfi_colour_correction('545', /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_545_a2_cc2, EABP=hfi_er_avg, /GETERR, NITER=100)
  hfi_857_a2_cc2 = hfi_colour_correction('857', /POWERLAW, ALPHA=2.0, ABP=hfi_avg, AVG_CC=AVG_857_a2_cc2, EABP=hfi_er_avg, /GETERR, NITER=100)
  ;
  ;  Check the Effective Frequencies for the above colour correction, first get it for Spectral Index = -1, then for Spectral Index = +2.
  ;  You can also get the uncertainty of the effective frequency; do this for the alpha=+2 case below.
  ;
  hfi_100_am1_cc = hfi_colour_correction('100',/POWERLAW,ALPHA=-1.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_100_am1_NU)
  hfi_143_am1_cc = hfi_colour_correction('143',/POWERLAW,ALPHA=-1.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_143_am1_NU)
  hfi_217_am1_cc = hfi_colour_correction('217',/POWERLAW,ALPHA=-1.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_217_am1_NU)
  hfi_353_am1_cc = hfi_colour_correction('353',/POWERLAW,ALPHA=-1.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_353_am1_NU)
  hfi_545_am1_cc = hfi_colour_correction('545',/POWERLAW,ALPHA=-1.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_545_am1_NU)
  hfi_857_am1_cc = hfi_colour_correction('857',/POWERLAW,ALPHA=-1.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_857_am1_NU)
  ;
  lfi_30_am1_cc = hfi_colour_correction(BP_INFO=lfi_bp,'30',/POWERLAW,ALPHA=-1.0,ABP=lfi_avg,/GETNUEFF, AVG_NUEFF=AVG_30_am1_NU, CH_NUEFF=CH_30_am1_NU, BOLO_NUEFF=BOLO_30_am1_NU,/LFI)
  lfi_44_am1_cc = hfi_colour_correction(BP_INFO=lfi_bp,'44',/POWERLAW,ALPHA=-1.0,ABP=lfi_avg,/GETNUEFF, AVG_NUEFF=AVG_44_am1_NU, CH_NUEFF=CH_44_am1_NU, BOLO_NUEFF=BOLO_44_am1_NU,/LFI)
  lfi_70_am1_cc = hfi_colour_correction(BP_INFO=lfi_bp,'70',/POWERLAW,ALPHA=-1.0,ABP=lfi_avg,/GETNUEFF, AVG_NUEFF=AVG_70_am1_NU, CH_NUEFF=CH_70_am1_NU, BOLO_NUEFF=BOLO_70_am1_NU,/LFI)
  ;
  hfi_100_a2_cc = hfi_colour_correction('100',/POWERLAW,ALPHA=2.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_100_a2_NU, /GETERR, NITER=100, EABP=hfi_er_avg)
  hfi_143_a2_cc = hfi_colour_correction('143',/POWERLAW,ALPHA=2.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_143_a2_NU, /GETERR, NITER=100, EABP=hfi_er_avg)
  hfi_217_a2_cc = hfi_colour_correction('217',/POWERLAW,ALPHA=2.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_217_a2_NU, /GETERR, NITER=100, EABP=hfi_er_avg)
  hfi_353_a2_cc = hfi_colour_correction('353',/POWERLAW,ALPHA=2.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_353_a2_NU, /GETERR, NITER=100, EABP=hfi_er_avg)
  hfi_545_a2_cc = hfi_colour_correction('545',/POWERLAW,ALPHA=2.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_545_a2_NU, /GETERR, NITER=100, EABP=hfi_er_avg)
  hfi_857_a2_cc = hfi_colour_correction('857',/POWERLAW,ALPHA=2.0,ABP=hfi_avg,/GETNUEFF, AVG_NUEFF=AVG_857_a2_NU, /GETERR, NITER=100, EABP=hfi_er_avg)
  ;
  print, 'The /GETNUEFF keyword does not change the standard output, but provides the effective frequencies as an additional output.'
  help, bolo_100_am1_nu, /struct
  help, bolo_100_a2_nu, /struct
  ;
  lfi_30_a2_cc = hfi_colour_correction(BP_INFO=lfi_bp,'30',/POWERLAW,ALPHA=2.0,ABP=lfi_avg,/GETNUEFF, AVG_NUEFF=AVG_30_a2_NU, CH_NUEFF=CH_30_a2_NU, BOLO_NUEFF=BOLO_30_a2_NU,/LFI)
  lfi_44_a2_cc = hfi_colour_correction(BP_INFO=lfi_bp,'44',/POWERLAW,ALPHA=2.0,ABP=lfi_avg,/GETNUEFF, AVG_NUEFF=AVG_44_a2_NU, CH_NUEFF=CH_44_a2_NU, BOLO_NUEFF=BOLO_44_a2_NU,/LFI)
  lfi_70_a2_cc = hfi_colour_correction(BP_INFO=lfi_bp,'70',/POWERLAW,ALPHA=2.0,ABP=lfi_avg,/GETNUEFF, AVG_NUEFF=AVG_70_a2_NU, CH_NUEFF=CH_70_a2_NU, BOLO_NUEFF=BOLO_70_a2_NU,/LFI)
  ;
  ;
  ;   Now do a CO correction example, HFI only as no CO lines are found in LFI.
  ;
  vrad = 0d   ;   km/s
  hfi_100_co = hfi_co_correction( '100', VRAD=vrad, ABP=hfi_avg_withCO, AVG_CO=AVG_100_CO, AVG_13CO=AVG_100_13CO, FABP=flg_avg_withCO, EABP=hfi_er_avg_withCO)
  ;
  hfi_143_co = hfi_co_correction( '143', VRAD=vrad, ABP=hfi_avg_withCO, AVG_CO=AVG_143_CO, AVG_13CO=AVG_143_13CO, FABP=flg_avg_withCO, EABP=hfi_er_avg_withCO)
  ;
  hfi_217_co = hfi_co_correction( '217', VRAD=vrad, ABP=hfi_avg_withCO, AVG_CO=AVG_217_CO, AVG_13CO=AVG_217_13CO, FABP=flg_avg_withCO, EABP=hfi_er_avg_withCO)
  ;
  hfi_353_co = hfi_co_correction( '353', VRAD=vrad, ABP=hfi_avg_withCO, AVG_CO=AVG_353_CO, AVG_13CO=AVG_353_13CO, FABP=flg_avg_withCO, EABP=hfi_er_avg_withCO)
  ;
  hfi_545_co = hfi_co_correction( '545', VRAD=vrad, ABP=hfi_avg_withCO, AVG_CO=AVG_545_CO, AVG_13CO=AVG_545_13CO, FABP=flg_avg_withCO, EABP=hfi_er_avg_withCO)
  ;
  hfi_857_co = hfi_co_correction( '857', VRAD=vrad, ABP=hfi_avg_withCO, AVG_CO=AVG_857_CO, AVG_13CO=AVG_857_13CO, FABP=flg_avg_withCO, EABP=hfi_er_avg_withCO)
  ;
  ;
  ;
  ;   Now do a CO correction example with uncertainty, HFI only as no CO lines are found in LFI.
  ;   Use the spectral uncertainty, as well as a +/- 30 km/s radial velocity uncertainty (spacecraft velocity wrt line of sight).
  ;
  vrad = 0d   ;   km/s
  hfi_100_co = hfi_co_correction( '100', VRAD=vrad, ABP=hfi_avg_withCO, AVG_CO=AVG_100_CO2, AVG_13CO=AVG_100_13CO2, $
                                   FABP=flg_avg_withCO, EABP=hfi_er_avg_withCO, /GETERR, NITER=100, ER_VRAD = 30d)
  ;
  hfi_143_co = hfi_co_correction( '143', VRAD=vrad, ABP=hfi_avg_withCO, AVG_CO=AVG_143_CO2, AVG_13CO=AVG_143_13CO2, $
                                   FABP=flg_avg_withCO, EABP=hfi_er_avg_withCO, /GETERR, NITER=100, ER_VRAD = 30d)
  ;
  hfi_217_co = hfi_co_correction( '217', VRAD=vrad, ABP=hfi_avg_withCO, AVG_CO=AVG_217_CO2, AVG_13CO=AVG_217_13CO2, $
                                   FABP=flg_avg_withCO, EABP=hfi_er_avg_withCO, /GETERR, NITER=100, ER_VRAD = 30d)
  ;
  hfi_353_co = hfi_co_correction( '353', VRAD=vrad, ABP=hfi_avg_withCO, AVG_CO=AVG_353_CO2, AVG_13CO=AVG_353_13CO2, $
                                   FABP=flg_avg_withCO, EABP=hfi_er_avg_withCO, /GETERR, NITER=100, ER_VRAD = 30d)
  ;
  hfi_545_co = hfi_co_correction( '545', VRAD=vrad, ABP=hfi_avg_withCO, AVG_CO=AVG_545_CO2, AVG_13CO=AVG_545_13CO2, $
                                   FABP=flg_avg_withCO, EABP=hfi_er_avg_withCO, /GETERR, NITER=100, ER_VRAD = 30d)
  ;
  hfi_857_co = hfi_co_correction( '857', VRAD=vrad, ABP=hfi_avg_withCO, AVG_CO=AVG_857_CO2, AVG_13CO=AVG_857_13CO2, $
                                   FABP=flg_avg_withCO, EABP=hfi_er_avg_withCO, /GETERR, NITER=100, ER_VRAD = 30d)
  ;
  print, 'Compare the two CO values, the first without an uncertainty, and the second with one.'
  help, avg_100_co, /struct
  print, avg_100_co
  help, avg_100_co2, /struct
  print, avg_100_co2
  ;
  ;
  ;  By request, I am combining the CMB to IRAS conversion with a spectral index=2 conversion.
  ;  
  ;  Placeholders for a moment...
  lfibolo_30_uccc = lfibolo_30_a2_cc
  lfibolo_44_uccc = lfibolo_44_a2_cc
  lfibolo_70_uccc = lfibolo_70_a2_cc
  avg_30_uccc = avg_30_a2_cc
  avg_44_uccc = avg_44_a2_cc
  avg_70_uccc = avg_70_a2_cc
  ;
  avg_100_uccc = avg_100_a2_cc
  avg_143_uccc = avg_143_a2_cc
  avg_217_uccc = avg_217_a2_cc
  avg_353_uccc = avg_353_a2_cc
  avg_545_uccc = avg_545_a2_cc
  avg_857_uccc = avg_857_a2_cc
  ;
  cc_30_a2 = lfibolo_30_uccc.cc
  cc_44_a2 = lfibolo_44_uccc.cc
  cc_70_a2 = lfibolo_70_uccc.cc
  ;
  cc_30_a2_avg = avg_30_uccc.cc
  cc_44_a2_avg = avg_44_uccc.cc
  cc_70_a2_avg = avg_70_uccc.cc
  ;
  cc_100_a2_avg = avg_100_uccc.cc
  cc_143_a2_avg = avg_143_uccc.cc
  cc_217_a2_avg = avg_217_uccc.cc
  cc_353_a2_avg = avg_353_uccc.cc
  cc_545_a2_avg = avg_545_uccc.cc
  cc_857_a2_avg = avg_857_uccc.cc
  ;
  uc_30_kcmb = lfibolo_30_uc.KCMB2MJYSR
  uc_44_kcmb = lfibolo_44_uc.KCMB2MJYSR
  uc_70_kcmb = lfibolo_70_uc.KCMB2MJYSR
  uc_30_avg_kcmb = avg_30_uc.KCMB2MJYSR
  uc_44_avg_kcmb = avg_44_uc.KCMB2MJYSR
  uc_70_avg_kcmb = avg_70_uc.KCMB2MJYSR
  ;
  uc_100_avg_kcmb = avg_100_uc.KCMB2MJYSR
  uc_143_avg_kcmb = avg_143_uc.KCMB2MJYSR
  uc_217_avg_kcmb = avg_217_uc.KCMB2MJYSR
  uc_353_avg_kcmb = avg_353_uc.KCMB2MJYSR
  uc_545_avg_kcmb = avg_545_uc.KCMB2MJYSR
  uc_857_avg_kcmb = avg_857_uc.KCMB2MJYSR
  ;
  lfibolo_30_uccc.cc = uc_30_kcmb*cc_30_a2
  lfibolo_44_uccc.cc = uc_44_kcmb*cc_44_a2
  lfibolo_70_uccc.cc = uc_70_kcmb*cc_70_a2
  avg_30_uccc.cc = uc_30_avg_kcmb*cc_30_a2_avg
  avg_44_uccc.cc = uc_44_avg_kcmb*cc_44_a2_avg
  avg_70_uccc.cc = uc_70_avg_kcmb*cc_70_a2_avg
  ;
  avg_100_uccc.cc = uc_100_avg_kcmb*cc_100_a2_avg
  avg_143_uccc.cc = uc_143_avg_kcmb*cc_143_a2_avg
  avg_217_uccc.cc = uc_217_avg_kcmb*cc_217_a2_avg
  avg_353_uccc.cc = uc_353_avg_kcmb*cc_353_a2_avg
  avg_545_uccc.cc = uc_545_avg_kcmb*cc_545_a2_avg
  avg_857_uccc.cc = uc_857_avg_kcmb*cc_857_a2_avg
  ;
  ;
  ;
  ;   Print some of the numbers for verification.
  ;
  print, ';Unit conversion data:'
  print, ';Detector   Kcmb to MJy/sr      MJy/sr to Tb      Kcmb to ySZ'
  print,';LFI:'
  FOR i = 0, N_ELEMENTS(lfibolo_30_uc) - 1 DO print, ';',lfibolo_30_uc[i].(0),'   ',lfibolo_30_uc[i].(1),'   ',lfibolo_30_uc[i].(2),'   ',lfibolo_30_uc[i].(3);,'   ',lfibolo_30_uc[i].(4),'   ',lfibolo_30_uc[i].(5) 
  print,';', avg_30_uc.(0),'      ',avg_30_uc.(1),'   ',avg_30_uc.(2),'   ',avg_30_uc.(3);,'   ',avg_30_uc.(4),'   ',avg_30_uc.(5)
  FOR i = 0, N_ELEMENTS(lfibolo_44_uc) - 1 DO print,';', lfibolo_44_uc[i].(0),'   ',lfibolo_44_uc[i].(1),'   ',lfibolo_44_uc[i].(2),'   ',lfibolo_44_uc[i].(3);,'   ',lfibolo_44_uc[i].(4),'   ',lfibolo_44_uc[i].(5) 
  print, ';',avg_44_uc.(0),'      ',avg_44_uc.(1),'   ',avg_44_uc.(2),'   ',avg_44_uc.(3);,'   ',avg_44_uc.(4),'   ',avg_44_uc.(5)
  FOR i = 0, N_ELEMENTS(lfibolo_70_uc) - 1 DO print,';', lfibolo_70_uc[i].(0),'   ',lfibolo_70_uc[i].(1),'   ',lfibolo_70_uc[i].(2),'   ',lfibolo_70_uc[i].(3);,'   ',lfibolo_70_uc[i].(4),'   ',lfibolo_70_uc[i].(5) 
  print, ';',avg_70_uc.(0),'      ',avg_70_uc.(1),'   ',avg_70_uc.(2),'   ',avg_70_uc.(3);,'   ',avg_70_uc.(4),'   ',avg_70_uc.(5)
  print,';HFI:'
  aa = avg_100_uc
  print,';', aa.(0),'         ',aa.(1),'   ',aa.(2),'   ',aa.(3);,'   ',aa.(4),'   ',aa.(5)
  aa = avg_143_uc
  print,';', aa.(0),'         ',aa.(1),'   ',aa.(2),'   ',aa.(3);,'   ',aa.(4),'   ',aa.(5)
  aa = avg_217_uc
  print,';', aa.(0),'         ',aa.(1),'   ',aa.(2),'   ',aa.(3);,'   ',aa.(4),'   ',aa.(5)
  aa = avg_353_uc
  print,';', aa.(0),'         ',aa.(1),'   ',aa.(2),'   ',aa.(3);,'   ',aa.(4),'   ',aa.(5)
  aa = avg_545_uc
  print,';', aa.(0),'         ',aa.(1),'   ',aa.(2),'   ',aa.(3);,'   ',aa.(4),'   ',aa.(5)
  aa = avg_857_uc
  print,';', aa.(0),'         ',aa.(1),'   ',aa.(2),'   ',aa.(3);,'   ',aa.(4),'   ',aa.(5)
  ;
  ;stop
  ;
  print, ';'
  print, ';Colour Correction data, POWERLAW, ALPHA=2.0 (Spectral Index -1 to +2):'
  print, ';Detector       CC                  Nu_eff (-1) [GHz]  Nu_eff (+2) [GHz]'
  print,';LFI:'
  a = lfibolo_30_a2_cc
  aa = avg_30_a2_cc
  aaa = bolo_30_am1_Nu
  aaaa = bolo_30_a2_Nu
  aaaaa = avg_30_am1_nu
  aaaaaa = avg_30_a2_Nu
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',aaa[i].(2),'   ', aaaa[i].(2)
  print,';', aa.(0),'       ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  a = lfibolo_44_a2_cc
  aa = avg_44_a2_cc
  aaa =   bolo_44_am1_Nu
  aaaa =  bolo_44_a2_Nu
  aaaaa =  avg_44_am1_nu
  aaaaaa = avg_44_a2_Nu
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',aaa[i].(2),'   ', aaaa[i].(2) 
  print,';', aa.(0),'       ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  a = lfibolo_70_a2_cc
  aa = avg_70_a2_cc
  aaa =   bolo_70_am1_Nu
  aaaa =  bolo_70_a2_Nu
  aaaaa =  avg_70_am1_nu
  aaaaaa = avg_70_a2_Nu
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1),'   ',aaa[i].(2),'   ', aaaa[i].(2) 
  print,';', aa.(0),'       ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  print, ';HFI:'
  aa = avg_100_a2_cc
  aaaaa =  avg_100_am1_nu
  aaaaaa = avg_100_a2_Nu
  print,';', aa.(0),'        ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  aa = avg_143_a2_cc
  aaaaa =  avg_143_am1_nu
  aaaaaa = avg_143_a2_Nu
  print,';', aa.(0),'        ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  aa = avg_217_a2_cc
  aaaaa =  avg_217_am1_nu
  aaaaaa = avg_217_a2_Nu
  print,';', aa.(0),'        ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  aa = avg_353_a2_cc
  aaaaa =  avg_353_am1_nu
  aaaaaa = avg_353_a2_Nu
  print,';', aa.(0),'        ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  aa = avg_545_a2_cc
  aaaaa =  avg_545_am1_nu
  aaaaaa = avg_545_a2_Nu
  print,';', aa.(0),'        ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  aa = avg_857_a2_cc
  aaaaa =  avg_857_am1_nu
  aaaaaa = avg_857_a2_Nu
  print,';', aa.(0),'        ',aa.(1),'   ',aaaaa.(2),'   ',aaaaaa.(2)
  ;
  ;stop
  ;
  print,';'
  print,';CO correction data:'
  print,';Detector    J+1 -> J      12CO               13CO'
  aaa  =     avg_100_co2
  aaaa =     avg_100_13co2
  Nls = N_ELEMENTS(aaa[0].coline)
  FOR ii=0,Nls -1 DO print,';', aaa.chname,'         ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],' +/- ',(aaa.er)[ii],'   ',(aaaa.cc)[ii],' +/- ',(aaaa.er)[ii]
  aaa  =     avg_143_co2
  aaaa =     avg_143_13co2
  Nls = N_ELEMENTS(aaa[0].coline)
  FOR ii=0,Nls -1 DO print,';', aaa.chname,'         ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],' +/- ',(aaa.er)[ii],'   ',(aaaa.cc)[ii],' +/- ',(aaaa.er)[ii]
  aaa  =     avg_217_co2
  aaaa =     avg_217_13co2
  Nls = N_ELEMENTS(aaa[0].coline)
  FOR ii=0,Nls -1 DO print,';', aaa.chname,'         ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],' +/- ',(aaa.er)[ii],'   ',(aaaa.cc)[ii],' +/- ',(aaaa.er)[ii]
  aaa  =     avg_353_co2
  aaaa =     avg_353_13co2
  Nls = N_ELEMENTS(aaa[0].coline)
  FOR ii=0,Nls -1 DO print,';', aaa.chname,'         ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],' +/- ',(aaa.er)[ii],'   ',(aaaa.cc)[ii],' +/- ',(aaaa.er)[ii]
  aaa  =     avg_545_co2
  aaaa =     avg_545_13co2
  Nls = N_ELEMENTS(aaa[0].coline)
  FOR ii=0,Nls -1 DO print,';', aaa.chname,'         ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],' +/- ',(aaa.er)[ii],'   ',(aaaa.cc)[ii],' +/- ',(aaaa.er)[ii]
  aaa  =     avg_857_co2
  aaaa =     avg_857_13co2
  Nls = N_ELEMENTS(aaa[0].coline)
  FOR ii=0,Nls -1 DO print,';', aaa.chname,'         ',STRTRIM(STRING((aaa.coline)[ii]+1),2)+'->'+STRTRIM(STRING((aaa.coline)[ii]),2),'   ',(aaa.cc)[ii],' +/- ',(aaa.er)[ii],'   ',(aaaa.cc)[ii],' +/- ',(aaaa.er)[ii]
  ;
  ;
  print, ';'
  print, ';Combined Unit Conversion / Colour Correction data, Kcmb to MJy/sr, from SI=-1 to SI=+2:'
  print, ';Detector       UC/CC Kcmb to MJy/sr (alpha=2)'
  print,';LFI:'
  a = lfibolo_30_uccc
  aa = avg_30_uccc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1) 
  print,';', aa.(0),'       ',aa.(1)
  a = lfibolo_44_uccc
  aa = avg_44_uccc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1) 
  print,';', aa.(0),'       ',aa.(1)
  a = lfibolo_70_uccc
  aa = avg_70_uccc
  FOR i = 0, N_ELEMENTS(a) - 1 DO print,';', a[i].(0),'   ',a[i].(1) 
  print,';', aa.(0),'       ',aa.(1)
  print, ';HFI:'
  aa = avg_100_uccc
  print,';', aa.(0),'        ',aa.(1)
  aa = avg_143_uccc
  print,';', aa.(0),'        ',aa.(1)
  aa = avg_217_uccc
  print,';', aa.(0),'        ',aa.(1)
  aa = avg_353_uccc
  print,';', aa.(0),'        ',aa.(1)
  aa = avg_545_uccc
  print,';', aa.(0),'        ',aa.(1)
  aa = avg_857_uccc
  print,';', aa.(0),'        ',aa.(1)
  ;
  ;stop
END
;+
;
; LFI fastcc results:
;Detector        alpha     -2.00000     -1.50000     -1.00000    -0.500000      0.00000     0.500000      1.00000      1.50000      2.00000      2.50000
;      3.00000      3.50000      4.00000
;LFI-18     0.947889     0.960962     0.972064     0.981197     0.988360     0.993553     0.996776     0.998028     0.997311     0.994624
;     0.989967     0.983340     0.974742
;LFI-19     0.855161     0.877891     0.899496     0.919975     0.939330     0.957560     0.974664     0.990644      1.00550      1.01923
;      1.03183      1.04331      1.05367
;LFI-20     0.888581     0.907641     0.925336     0.941665     0.956630     0.970230     0.982464     0.993334      1.00284      1.01098
;      1.01775      1.02316      1.02721
;LFI-21     0.916742     0.932792     0.947251     0.960120     0.971400     0.981090     0.989189     0.995699      1.00062      1.00395
;      1.00569      1.00584      1.00440
;LFI-22      1.02457      1.02643      1.02666      1.02525      1.02220      1.01752      1.01120      1.00325     0.993667     0.982447
;     0.969591     0.955100     0.938975
;LFI-23     0.985392     0.991089     0.995586     0.998883      1.00098      1.00188      1.00157      1.00007     0.997368     0.993465
;     0.988362     0.982059     0.974556
;70GHz     0.937943     0.951267     0.962966     0.973041     0.981490     0.988314     0.993514     0.997088     0.999037     0.999362
;     0.998061     0.995135     0.990585
;LFI-24     0.978061     0.983786     0.988635     0.992610     0.995710     0.997935     0.999285     0.999760     0.999359     0.998084
;     0.995934     0.992909     0.989009
;LFI-25     0.966990     0.973920     0.980045     0.985365     0.989880     0.993590     0.996495     0.998595     0.999890      1.00038
;      1.00006     0.998944     0.997019
;LFI-26     0.957165     0.965579     0.973118     0.979781     0.985570     0.990484     0.994522     0.997686     0.999975      1.00139
;      1.00193      1.00159      1.00038
;44GHz     0.967910     0.974847     0.980940     0.986188     0.990590     0.994147     0.996860     0.998728     0.999750     0.999927
;     0.999260     0.997748     0.995390
;LFI-27     0.947654     0.959183     0.969272     0.977921     0.985130     0.990899     0.995228     0.998117     0.999566     0.999575
;     0.998144     0.995273     0.990962
;LFI-28     0.945519     0.957814     0.968519     0.977635     0.985160     0.991095     0.995440     0.998196     0.999361     0.998936
;     0.996922     0.993317     0.988122
;30GHz     0.946764     0.958638     0.969002     0.977856     0.985200     0.991034     0.995358     0.998172     0.999476     0.999269
;     0.997553     0.994327     0.989591
;LFI-23     0.985392     0.991089     0.995586     0.998883      1.00098      1.00188      1.00157      1.00007     0.997368     0.993465
;     0.988362     0.982059     0.974556
;70GHz     0.937943     0.951267     0.962966     0.973041     0.981490     0.988314     0.993514     0.997088     0.999037     0.999362
;     0.998061     0.995135     0.990585
;% LOADCT: Loading table 16 LEVEL
;case 1 - no uncertainty estimate:
;** Structure <7752c8>, 4 tags, length=40, data length=40, refs=2:
;   CHNAME          STRING    '100'
;   KCMB2MJYSR      DOUBLE           244.09605
;   MJYSR2TB        DOUBLE        0.0032548074
;   KCMB2YSZ        DOUBLE         -0.24814487
;case 2 - with uncertainty estimate:
;** Structure <a80418>, 6 tags, length=56, data length=56, refs=2:
;   CHNAME          STRING    '100'
;   KCMB2MJYSR      DOUBLE           244.09605
;   MJYSR2TB        DOUBLE        0.0032548074
;   KCMB2YSZ        DOUBLE         -0.24814487
;   ERKCMB2MJYSR    DOUBLE       5.1416807e-13
;   ERKCMB2YSZ      DOUBLE       6.1369887e-16
;Case 1 and 2 show the same coefficient, but the latter also has an uncertainty estimate component included with the output structure.
;Increase the value of NITER to improve the accuracy of this estimate.
;The /GETNUEFF keyword does not change the standard output, but provides the effective frequencies as an additional output.
;BOLO_100_AM1_NU UNDEFINED = <Undefined>
;BOLO_100_A2_NU  UNDEFINED = <Undefined>
;Compare the two CO values, the first without an uncertainty, and the second with one.
;** Structure <777348>, 3 tags, length=32, data length=26, refs=2:
;   CHNAME          STRING    '100'
;   COLINE          INT       Array[1]
;   CC              DOUBLE    Array[1]
;{ 100       0
;       14.781371
;}
;** Structure <7786c8>, 4 tags, length=40, data length=34, refs=3:
;   CHNAME          STRING    '100'
;   COLINE          INT       Array[1]
;   CC              DOUBLE    Array[1]
;   ER              DOUBLE    Array[1]
;{ 100       0
;       14.781371
;      0.23453622
;}
;Unit conversion data:
;Detector   Kcmb to MJy/sr      MJy/sr to Tb      Kcmb to ySZ
;LFI:
;30_28S          22.644522        0.040354188        -0.18729943
;30_28M          24.416912        0.040354188        -0.18747155
;30_27S          23.593779        0.040354188        -0.18740461
;30_27M          23.450841        0.040354188        -0.18734306
;30             23.509877        0.040354188        -0.18737944
;44_26S          55.803725        0.016735863        -0.19306685
;44_26M          54.800859        0.016735863        -0.19302219
;44_25S          56.093557        0.016735863        -0.19311908
;44_25M          55.320961        0.016735863        -0.19303548
;44_24S          55.369510        0.016735863        -0.19306920
;44_24M          57.005102        0.016735863        -0.19324335
;44             55.734911        0.016735863        -0.19309332
;70_23S          134.70328       0.0065671960        -0.21046684
;70_23M          132.57143       0.0065671960        -0.20998788
;70_22S          141.01550       0.0065671960        -0.21174161
;70_22M          134.74281       0.0065671960        -0.21061418
;70_21S          125.44897       0.0065671960        -0.20913173
;70_21M          128.75164       0.0065671960        -0.20972481
;70_20S          124.99620       0.0065671960        -0.20903845
;70_20M          123.38690       0.0065671960        -0.20869843
;70_19S          126.01674       0.0065671960        -0.20912070
;70_19M          115.71861       0.0065671960        -0.20737482
;70_18S          125.77654       0.0065671960        -0.20948468
;70_18M          135.33161       0.0065671960        -0.21084123
;70             129.18692       0.0065671960        -0.20976774
;HFI:
;100                244.09605       0.0032548074        -0.24814487
;143                371.73268       0.0015916707        -0.35922296
;217                483.68697      0.00069120334          5.1537039
;353                287.44556      0.00026120163         0.16109498
;545                58.035560      0.00010958025        0.069178639
;857                2.2681256      4.4316316e-05        0.037981062
;
;Colour Correction data, POWERLAW, ALPHA=2.0 (Spectral Index -1 to +2):
;Detector       CC                  Nu_eff (-1) [GHz]  Nu_eff (+2) [GHz]
;LFI:
;30_28S          1.0720293          27.555895          28.154799
;30_28M         0.99331614          28.305574          28.790044
;30_27S          1.0283302          27.944282          28.535575
;30_27M          1.0349350          27.947989          28.345034
;30              1.0321373          27.931726          28.455889
;44_26S          1.0184672          43.710835          44.074033
;44_26M          1.0373265          43.371636          43.949271
;44_25S          1.0129400          43.774626          44.183717
;44_25M          1.0275119          43.556458          43.995483
;44_24S          1.0264349          43.548954          44.059792
;44_24M         0.99612572          44.009542          44.451492
;44              1.0195863          43.663183          44.120709
;70_23S         0.99243238          70.194185          71.322348
;70_23M          1.0104834          69.843631          70.764392
;70_22S         0.94285090          71.277121          72.787859
;70_22M         0.99148230          70.148518          71.483303
;70_21S          1.0716931          68.343764          69.696489
;70_21M          1.0415354          68.969292          70.412259
;70_20S          1.0760260          68.250706          69.585078
;70_20M          1.0916787          67.965482          69.173569
;70_19S          1.0669566          68.485464          69.694898
;70_19M          1.1707515          66.441366          67.513380
;70_18S          1.0671979          68.293865          70.095786
;70_18M         0.98616394          70.225983          71.737696
;70              1.0378434          69.062723          70.467299
;HFI:
;100              0.96173861          100.36149          103.23782
;143               1.0060688          141.36240          145.45727
;217              0.93509079          220.11102          225.51546
;353              0.93271094          358.56805          366.77299
;545              0.93444602          552.21948          567.59628
;857              0.98082798          854.68501          877.72420
;
;CO correction data:
;Detector    J+1 -> J      12CO               13CO
;100         1->0          14.781371 +/-       0.23453622          15.548153 +/-       0.34664636
;143         1->0        0.046990209 +/-     0.0012966591       0.0017660848 +/-    0.00039538897
;143         2->1        0.017681967 +/-     0.0019777424        0.031734788 +/-     0.0015700248
;217         2->1          45.848892 +/-       0.12051145          35.375149 +/-      0.061764888
;353         3->2          175.09601 +/-       0.45359418          117.12316 +/-       0.53503957
;545         4->3          252.49448 +/-       0.64173806          56.881515 +/-       0.25193456
;545         5->4          2322.1718 +/-        4.4815032          1356.0689 +/-        5.0081426
;857         6->5          7217.1207 +/-        81.935048          2016.5076 +/-        5.1241177
;857         7->6          74874.417 +/-        140.19893          61405.178 +/-        94.828549
;857         8->7          151723.07 +/-        389.32909          126567.48 +/-        235.53289
;857         9->8          4998.1345 +/-        38.795148          88288.414 +/-        308.04792
;
;Combined Unit Conversion / Colour Correction data, Kcmb to MJy/sr, from SI=-1 to SI=+2:
;Detector       UC/CC Kcmb to MJy/sr (alpha=2)
;LFI:
;30_28S          24.275592
;30_28M          24.253713
;30_27S          24.262195
;30_27M          24.270096
;30              24.265421
;44_26S          56.834262
;44_26M          56.846385
;44_25S          56.819405
;44_25M          56.842947
;44_24S          56.833198
;44_24M          56.784248
;44              56.826554
;70_23S          133.68390
;70_23M          133.96122
;70_22S          132.95659
;70_22M          133.59511
;70_21S          134.44280
;70_21M          134.09939
;70_20S          134.49916
;70_20M          134.69885
;70_19S          134.45439
;70_19M          135.47774
;70_18S          134.22846
;70_18M          133.45916
;70              134.07579
;HFI:
;100               234.75659
;143               373.98863
;217               452.29123
;353               268.10362
;545               54.231098
;857               2.2246411
;
;IDL>
;
;-
