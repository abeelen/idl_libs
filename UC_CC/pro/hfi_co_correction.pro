;
;   
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   A copy of the GNU General Public License is available at 
;   <http://www.gnu.org/licenses/>.
;   
;   Copyright Locke D. Spencer, 2013
;
;  Modified by L. Spencer 2013/02/05 to add the ability to provide an uncertainty estimate for the coefficient.
;   
PRO CO_conversion, nu_0, freq_d, trans_d, vrad, FCO, F13CO, LINES, NLINES, BT=BT, $
                   ERR_D=ERR_D, GETERR=GETERR, NITER=NITER, ER_FCO=ER_FCO, ER_F13CO=ER_F13CO, MSG=MSG, ER_VRAD = ER_VRAD, FLG_D=FLG_D
	;
	;	Nu_0 is the band centre frequency, in GHz
	;	freq_d is the frequency array, in Hz
	;	trans_d is the spectral transmission
	;	vrad is the radial velocity desired, in km/s, default value is 0 km/s
	;	FCO is the 12CO correction coefficient(s)
	;	F13CO is the 13CO correction coefficient(s)
	;	LINES is an array indicating the lower quantum state of the transitions calculated
	;	NLINES is the number of lines within the band, 1 for < 545 GHz, 2 for 545 and 4 for 857 GHz
	;
	;	ERR_D is the spectral uncertainty, GETERR is the flag to determine an uncertainty, ER_FCO is the error of the FCO (12CO) coefficient,
	;	F13CO is the error of the F13CO (13CO) coefficient, ER_VRAD is the uncertainty in radial velocity (default is zero).
	;	MSG is a keyword to output a diagnostic message to the user (rather than printing things to the screen).
	;
	CONST = { C: 2.99792458d+08, H: 6.6260755d-34, HBAR: 1.0545726691251021d-34, K: 1.3806580d-23, TCMB: 2.7255d0 }
	;
       IF N_ELEMENTS(BT) EQ 0 THEN BT = 1d-7
       ;
	IF N_ELEMENTS(GETERR) EQ 0 THEN GETERR = 0
	IF N_ELEMENTS(NITER) EQ 0 THEN NITER = 2 ; min number, ignored unless GETERR is set.
	IF N_ELEMENTS(VRAD) EQ 0 THEN VRAD = 0d ; km/s
	IF N_ELEMENTS(ER_VRAD) EQ 0 THEN ER_VRAD = 0d
	IF N_ELEMENTS(ERR_D) EQ 0 THEN ERR_D = DBLARR(N_ELEMENTS(freq_d))
	IF N_ELEMENTS(FLG_D) EQ 0 THEN FLG_D = DBLARR(N_ELEMENTS(freq_d))
	IF N_ELEMENTS(MSG) Eq 0 THEN MSG = '' ELSE MSG = STRING(MSG)
	;
	ERR_D_orig = ERR_D
	FREQ_D_orig = FREQ_D
	TRANS_D_orig = TRANS_D
	FLG_D_orig = FLG_D
	;
	IF KEYWORD_SET(GETERR) THEN BEGIN
	  ;
	  IF N_ELEMENTS(ERR_D) LT N_ELEMENTS(trans_d) THEN BEGIN
	    GETERR = 0 ; err_d is not valid
	    MSG = [MSG,'GETERR was set, but ERR_D was invalid.  GETERR was set to zero and the uncertainties returned are set to zero.']
	  ENDIF
	  ER_FCO = 0d    ; set the error to zero to start.
	  ER_F13CO = 0d ; set the error to zero to start.
	ENDIF
	;
	;		Now check that frequency is always positive
	;
	posF = WHERE((freq_d GT 0d), Npos)
	IF Npos GT 0 THEN BEGIN
		freq_d = freq_d[posF]
		trans_d = trans_d[posF]
		err_d = err_d[posF]
		flg_d = flg_d[posF]
	ENDIF
	;
	;
	nu_0 = nu_0*1d9
	;
	Nurest_12 = [115.2712018d9,230.5380000d9,345.7959899d9,461.0407682d9,576.2679305d9,691.4730763d9,806.6518060d9,921.7997000d9,1036.912393d9]  ; GHz
	Nurest_13 = [110.2013541d9,220.3986765d9,330.5879601d9,440.7651668d9,550.9263029d9,661.0672801d9,771.1841376d9,881.2728339d9,991.3293479d9] ; GHz
  ;
  ;
  NUC = nu_0
  CASE NUC OF
    '1d11': COline = [0]
    '1.43d11': COline = [0,1]
    '2.17d11': COline = [1]
    '3.53d11': COline = [2]
    '5.45d11': COline = [3,4]
    '8.57d11': COline = [5,6,7,8]
  ENDCASE
  ;
  LINES = COLINE
  NLS = N_ELEMENTS(LINES)
  NLINES = NLS
  ;
  ;FOR jj = 0, NLS - 1 DO BEGIN
  Nuv = VRAD/(const.C/1d3)*Nurest_12[COline] + Nurest_12[COline]  ; in GHz now, ... so if Vrad = 10 km/s, then Nuv = 10/300000*115GHz + 115 GHz = 115.003 GHz, higher, if Vrad <0 then lower...
  Nuv13 = VRAD/(const.C/1d3)*Nurest_13[COline] + Nurest_13[COline]  ; in GHz now, ... so if Vrad = 10 km/s, then Nuv = 10/300000*115GHz + 115 GHz = 115.003 GHz, higher, if Vrad <0 then lower...
  Nu = freq_d	;
  ;
  ;	Find the trans at the CO line
  TauCO_12 = INTERPOL(trans_d, Nu, Nuv)
  TauCO_13 = INTERPOL(trans_d, Nu, Nuv13)
  ;
  ;	And check that only the larger-amplitude regions are used...
  HighAmp = WHERE(trans_d GE BT, Ngood)
  IF Ngood GT 0 THEN BEGIN
    trans_d_ = trans_d[HighAmp]
    freq_d_ = freq_d[HighAmp]
    err_d_ = err_d[HighAmp]
    flg_d_ = flg_d[HighAmp]
  ENDIF
  ;
  sp = trans_d_
  nu = freq_d_
  er = err_d_
  flg = flg_d_
  ;  
  ;  Remove the flag points for the integral, but keep them for the CO line frequency transmission determination above.
  FLGfilt = WHERE(flg EQ 0d, Nfilt)
  IF Nfilt GT 0 THEN BEGIN
    sp = sp[FLGfilt]
    nu = nu[FLGfilt]
    er = er[FLGfilt]
    flg = flg[FLGfilt]
  ENDIF
  ;
  ;
  Tcmb = const.tcmb	;	2.725d ; K
  ; dB/dT = (2hnu^3/c^2)(e^(hnu/kT))(1/(e(hnu/kT) - 1))^2(hnu/kT^2)
  a = EXP(const.H*Nu/const.K/Tcmb)
  b = 1d/(a - 1d)
  c = (2d*const.H*Nu^3d)/(const.C^2d)
  d = (2d*const.H^2d*Nu^4d)/(const.C^2d*const.K*Tcmb^2d)
  y1 = c*b  ; the Planck function
  y2 = a*d*b^2d ; The Planck Temperature deriv.
  y = y2;*SP
  ;Intval = INT_TABULATED(Nu_, y, /DOUBLE) ; W m-2 sr-1 K_CMB
  Intval = TSUM(Nu, y*SP) ; W m-2 sr-1 K_CMB
  ;
  BBRJ_CO12 = 2d*(Nuv)^2d*const.K*(const.C)^(-2d)  ; W m-2 sr-1 Hz-1 K-1
  BBRJ_CO13 = 2d*(Nuv13)^2d*const.K*(const.C)^(-2d)  ; W m-2 sr-1 Hz-1 K-1
  ;
  Fco = (TauCO_12*BBRJ_CO12*Nuv/(const.C/1d3))/(Intval)*1d6  ; uKcmb/(Krj km/s)
  F13co = (TauCO_13*BBRJ_CO13*Nuv13/(const.C/1d3))/(Intval)*1d6  ; uKcmb/(Krj km/s)
  ;
  IF KEYWORD_SET(GETERR) THEN BEGIN
    ;  
    ;  Run NITER MC-realizations to get an uncertainty estimate of the coefficients provided.
    ;  I need to keep the full spectrum and then remove the flag points from it each time...
    ;
    Nspec = N_ELEMENTS(trans_d)
    ;         
    ;  Set up some arrays for uncertainty
    UC_arr1 = DBLARR(NITER,Nlines)  ;  The FCO array
    UC_arr2 = DBLARR(NITER,Nlines)  ;  The F13CO array
    ;
    FOR ii = 0, NITER - 1 DO BEGIN
      ;
      noise_spec = RANDOMN(seed,Nspec)
      sp_ = trans_d + noise_spec*ERR_D
      nu_ = freq_d
      sp__ = (sp_[HighAmp])[FLGfilt]
      nu__ = (nu_[HighAmp])[FLGfilt]
      ;
      VRAD_ = (VRAD + RANDOMN(seed,1)*ER_VRAD)[0]
      ;
      ;  get the Nuv and Nuv13 arrays prior to down-sampling the spectrum array.
      ;
      Nuv_ = VRAD_/(const.C/1d3)*Nurest_12[COline] + Nurest_12[COline]  ; in GHz now, ... so if Vrad = 10 km/s, then Nuv = 10/300000*115GHz + 115 GHz = 115.003 GHz, higher, if Vrad <0 then lower...
      Nuv13_ = VRAD_/(const.C/1d3)*Nurest_13[COline] + Nurest_13[COline]  ; in GHz now, ... so if Vrad = 10 km/s, then Nuv = 10/300000*115GHz + 115 GHz = 115.003 GHz, higher, if Vrad <0 then lower...
      ;
      ;	Find the trans at the CO line
      TauCO_12_ = INTERPOL(sp_, Nu_, Nuv_)
      TauCO_13_ = INTERPOL(sp_, Nu_, Nuv13_)
      ;
      Intval_ = TSUM(Nu__, y*SP__) ; W m-2 sr-1 K_CMB
      ;
      BBRJ_CO12_ = 2d*(Nuv_)^2d*const.K*(const.C)^(-2d)  ; W m-2 sr-1 Hz-1 K-1
      BBRJ_CO13_ = 2d*(Nuv13_)^2d*const.K*(const.C)^(-2d)  ; W m-2 sr-1 Hz-1 K-1
      ;
      Fco_ = (TauCO_12_*BBRJ_CO12_*Nuv_/(const.C/1d3))/(Intval_)*1d6  ; uKcmb/(Krj km/s)
      F13co_ = (TauCO_13_*BBRJ_CO13_*Nuv13_/(const.C/1d3))/(Intval_)*1d6  ; uKcmb/(Krj km/s)
      ;
      UC_arr1[ii,*] = Fco_
      UC_arr2[ii,*] = F13co_
      ;
    ENDFOR
    ;
    ER_FCO = DBLARR(Nlines)
    ER_F13CO = DBLARR(Nlines)
    FOR ii = 0, Nlines - 1 DO BEGIN
      ER_FCO[ii] = STDEV(UC_arr1[*,ii])
      ER_F13CO[ii] = STDEV(UC_arr2[*,ii])
    ENDFOR
    ;
  ENDIF
  ;
  ERR_D = ERR_D_orig
  FREQ_D = FREQ_D_orig
  TRANS_D = TRANS_D_orig
  FLG_D = FLG_D_orig
  ;
  nu_0 = nu_0/1d9
  ;
  return
END;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
FUNCTION hfi_co_correction, bp_info=BP_INFO, channel, bolo_co, bolo_13co, VRAD=VRAD, ABP=ABP, $
	AVG_CO=AVG_CO, AVG_13CO=AVG_13CO, CH_CO=CH_CO, CH_13CO=CH_13CO, BT=BT, $
	BP_ERR=BP_ERR, EABP=EABP, BP_FLG=BP_FLG, FABP=FABP, GETERR=GETERR, NITER=NITER, ER_VRAD=ER_VRAD
;+
; NAME:
;
;    hfi_co_correction
;
; PURPOSE:
;
; Quick tool to compute co correction for FLUX wrt nominal frequencies in HFI.
;
; CATEGORY:
;
;
;
; CALLING SEQUENCE:
;
;    result = hfi_co_correction( BP_INFO=bp_info, channel, bolo_co, bolo_13co, vrad=VRAD, ABP=ABP, $
;	AVG_CO=AVG_CO, AVG_13CO=AVG_13CO, CH_CO=CH_CO, CH_13CO=CH_13CO)
;
; INPUTS:
;
;    channel : string for HFI channel to be considered
;    bp_info : HFI bandpass structure (from hfi_read_bandpass script) (now an optional keyword input)
;    vrad    : The radial velocity to consider, in km/s (default is 0 km/s)
;
; KEYWORD PARAMETERS:
;
;   INPUT
;    BP_INFO: See above, now an optional keyword input.
;    ABP:  a structure containing the band-average bandpass data (from the hfi_read_avg_bandpass script)
;       NOTE: The ABP keyword must be set for the outputs AVG_CO and AVG_13CO to be produced.
;    BT:   The transmission amplitude threshold, above which the data are included in the integrals (default 1d-7)
;   OUTPUT
;    AVG_CO:  a structure containing the 12CO coefficient for the average spectrum
;               .chname:    a string containing the frequency band of the data
;               .coline:    integer(s) for the lower quantum state(s) of the transition(s) in question
;               .cc:        the CO conversion soefficient, in units of uK_CMB / (K_RJ km/s)
;    AVG_13CO:  The same as AVG_CO for the 13CO transition(s).
;    CH_CO:     The same as AVG_CO in structure, but the average of the individual detector coefficients,
;               rather than the single coefficient derived from the average spectrum.
;    CH_13CO:   The same as CH_CO for the 13CO transition(s).  
;
; OUTPUTS:
;
;   This function returns the CO coefficient for the band-average spectrum.  All other coefficients are returned through keywords.
;
;   bolo_co : a structure containing the 12CO coefficients
;            .name : bolometer name
;			       .coline: array of the lower quantum state of the line, e.g. 0 for CO J1-0
;            .cc   : the co correction coefficients per bolometer (-1 for RTS bolometers)
;   bolo_13co : structure containing the 13CO coefficients for each bolometer, same composition as bolo_co
;            .name : bolometer name
;			       .coline: array of the lower quantum state of the line
;            .cc   : the co correction coefficients per bolometer (-1 for RTS bolometers)
;   ch_co:   Structure containing the average of the detector coefficients for the 12CO transition(s).
;		         Note: This is not the same as the CO correction computed using the bandpass-average spectrum!!!
;
; OPTIONAL OUTPUTS:
;
;   See KEYWORDS
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;  bp = hfi_read_bandpass( /imo, LBLIMO='2_61')     ;   I select an older version intentionally.
;  abp = hfi_read_avg_bandpass(,/IMO, LBLIMO='2_61')
;  vrad = 0d
;  co = hfi_co_correction(BP_INFO=bp,'100',bolo_co, bolo_13co,vrad=VRAD, ABP=ABP, AVG_CO=AVG_CO, AVG_13CO=AVG_13CO, CH_CO=CH_CO, CH_13CO=CH_13CO)
;  help, co, /str
;  nbolo =n_elements(bolo_co)
;  for ibolo=0,nbolo-1 do print, bolo_co[ibolo].boloname+':  ' +strtrim(bolo_co[ibolo].cc,2)
;  for ibolo=0,nbolo-1 do print, bolo_13co[ibolo].boloname+':  ' +strtrim(bolo_13co[ibolo].cc,2)
;  print, AVG_CO[0].chname+':  '+strtrim(AVG_CO[0].cc,2)
;  print, CH_CO[0].chname+':  '+strtrim(CH_CO[0].cc,2)
;  print, AVG_13CO[0].chname+':  '+strtrim(AVG_13CO[0].cc,2)
;  print, CH_13CO[0].chname+':  '+strtrim(CH_13CO[0].cc,2)
;
; ** Structure <a4e328>, 3 tags, length=32, data length=26, refs=5:
;   CHNAME    STRING    '100'
;   COLINE    INT       Array[1]
;   CC        DOUBLE    Array[1]
;
; 00_100_1a:  10.845943
; 01_100_1b:  12.597105
; 20_100_2a:  14.719018
; 21_100_2b:  12.022305
; 40_100_3a:  16.366229
; 41_100_3b:  11.787892
; 80_100_4a:  19.122387
; 81_100_4b:  16.107923
;
; 00_100_1a:  16.922881
; 01_100_1b:  16.389269
; 20_100_2a:  14.101336
; 21_100_2b:  17.510663
; 40_100_3a:  14.523445
; 41_100_3b:  13.782610
; 80_100_4a:  18.679410
; 81_100_4b:  17.572983
;
; 100:  14.442624
; 100:  14.196100
; 100:  16.135543
; 100:  16.185325
; 
; MODIFICATION HISTORY:
;   Created by L. Spencer, August 31, 2011
;	Modified by L. Spencer, 2012/09/17 to require a single compile rather than double (chenged sub-procedure order).
;      Modified by L. Spencer, 2013/01/18:
;        Added a check for zero-valued frequency data points.
;        Changed the BP_INFO input variable to an optional Keyword argument to allow for the external RIMO which may not have the 
;        detector level spectra included.
;      Modified by L. Spencer, 2013/02/05: 
;        - Added the BP_FLG and FABP keywords to accept the CO-interpolation flag as an optional input.
;        This allows the CO-interpolated data to be used where needed, but ignored in the broad frequency integral 
;        (otherwise these points remain in the integral causing very slight deviations from the desired result).
;        - Added the ability to estimate a coefficient uncertainty using the GETERR, NITER, BP_ERR, EABP, ER_VRAD keywords.
;        - Added the MSG keyword to provide diagnostic messages to the user without printing them to screen.
;
;-
;
;
;
  ; -- Definitions
  CONST = { C: 2.99792458d+08, H: 6.6260755d-34, HBAR: 1.0545726691251021d-34, K: 1.3806580d-23, TCMB: 2.7255d0 }
  ;
  IF N_ELEMENTS(BT) EQ 0 THEN BT = 1d-7
  ;
  IF N_ELEMENTS(BP_INFO) GT 0 THEN BEGIN  ; check for the detector level CO flag if the detector level data are provided.
    IF N_ELEMENTS(BP_FLG) EQ 0 THEN BEGIN
      BP_FLG = BP_INFO
      FOR ii = 0, N_ELEMENTS(BP_INFO) - 1 DO BP_FLG[ii].Trans = BP_FLG[ii].Trans*0d ; set all of the flag to zero.
    ENDIF
  ENDIF
  ;
  IF N_ELEMENTS(ABP) GT 0 THEN BEGIN  ; check for the band-average level CO flag if the band-average spectral data are provided.
    IF N_ELEMENTS(FABP) EQ 0 THEN BEGIN
      FABP = ABP
      FOR ii = 0, N_ELEMENTS(ABP) - 1 DO FABP[ii].Trans = FABP[ii].Trans*0d ; set all of the flag to zero.
    ENDIF
  ENDIF
  ;
  ;  Check if the GETERR keyword has been set, if so check that the required uncertainties are also set.
  ;
  IF N_ELEMENTS(vrad) EQ 0 THEN vrad = 0d	;	km/s
  IF N_ELEMENTS(ER_vrad) EQ 0 THEN ER_vrad = 0d	;	km/s
  ;
  IF N_ELEMENTS(NITER) EQ 0 THEN NITER = 2 ; default of 2 iterations if the number of iterations is not set, only valid if GETERR is set, otherwise only one calculation is done.
  IF N_ELEMENTS(GETERR) EQ 0 THEN GETERR = 0 ; set this to not get the errors.
  ERR_DETS = 0  ; internal flag to determine uncertainty of detector coefficients.
  ERR_BNDS = 0  ; internal flag to determine uncertainty of band-average coefficients.  
  IF KEYWORD_SET(GETERR) THEN BEGIN
    ;
    ERR_DETS = 1
    ERR_BNDS = 1
    ;  check if the uncertainties on the spectra are provided.
    IF N_ELEMENTS(BP_ERR) LT N_ELEMENTS(BP_INFO) THEN ERR_DETS = 0
    IF N_ELEMENTS(EABP) LT N_ELEMENTS(ABP) THEN ERR_BNDS = 0
    ;
  ENDIF
  ;
  ;
  ; --- Check channel name first
  channels = ['100','143','217','353','545','857']
  colines = [1,2,1,1,2,4]
  nuc = [100.0d0, 143.0d0, 217.0d0,353.0d0,545.0d0,857.0d0]
  nchannels =  n_elements(channels)
  okchannel = 0
  ichwork = -1
  FOR ich = 0,nchannels-1 DO BEGIN
    IF (channel EQ channels[ich]) THEN BEGIN
      okchannel = 1
      ichwork = ich
      ;print, 'ichwork is: ',ichwork
    ENDIF
  ENDFOR
  IF (okchannel EQ 0) THEN BEGIN
    print, 'Wrong channel name. Available channels are '
    print,  channels
    return, -1
  ENDIF
  ;
  IF KEYWORD_SET(BP_INFO) THEN BEGIN
  ;
  ; Defining bandpass parameters
  ;
  NAME   = bp_info.name      ; name of the detector
  FREQ   = bp_info.freq  	 ; frequencies measured in Hz
  TRANS  = bp_info.trans     ; values of the transmission (normalized later)
  FLG    = BP_FLG.Trans      ; I will assume the same freq sampliing for the flag as for the main spectrum.
  ;
  IF KEYWORD_SET(ERR_DETS) THEN BEGIN
    NAME_E = bp_err.name
    FREQ_E = bp_err.freq
    TRANS_E = bp_err.trans ; I should check that this is on the same grid as the bp_info structure (should be, but may not be).
  ENDIF
  ;
  ; Defining channel parameters
  NU_NOM = double(nuc[ichwork])
  NumLines = colines[ichwork]
  chan = nuc[ichwork]
  CASE chan OF
   	'100': Lns = [0]
   	'143': Lns = [0,1]
   	'217': Lns = [1]
   	'353': Lns = [2]
   	'545': Lns = [3,4]
   	'857': Lns = [5,6,7,8]
   	ELSE: ;
  ENDCASE
  BOLO_LIST = get_hfibolo_list( channel = channel )
  N_BOLO_LIST = n_elements( bolo_list )
  ;
  ;  Define output variables
  IF KEYWORD_SET(ERR_DETS) THEN bolo_co =  {boloname:'',coline: Lns,cc:DBLARR(NumLines),er:DBLARR(NumLines)} ELSE bolo_co =  {boloname:'',coline: Lns,cc:DBLARR(NumLines)}
  bolo_co =  replicate(bolo_co,n_bolo_list)
  bolo_co.boloname = bolo_list
  bolo_13co = bolo_co
  ;
  IF KEYWORD_SET(ERR_DETS) THEN ch_co = {chname:channel,coline:Lns,cc:DBLARR(NumLines),er:DBLARR(NumLines)} ELSE ch_co =  {chname:channel,coline:Lns,cc:DBLARR(NumLines)}
  ch_13co = ch_co
  IF KEYWORD_SET(ERR_BNDS) THEN ch_co_avg = {chname:channel,coline:Lns,cc:DBLARR(NumLines),er:DBLARR(NumLines)} ELSE ch_co_avg =  {chname:channel,coline:Lns,cc:DBLARR(NumLines)}
  ch_13co_avg = ch_co
  ;
  ; starting computation
  For idet = 0 , n_bolo_list - 1 do begin
    ; ----- get transmission ------------
    Q_DET = where(bolo_list[ idet ] eq name)
    Q_DET = q_det(0)
    Q_TRANS = where( trans( * , q_det ) gt 1d-17 )	;	This is done later just in case the CO line falls at a low amplitude...
    FREQ_D = freq( q_trans, q_det ) ; already in Hz
    TRANS_D = trans( q_trans, q_det )
    FLAG_D = FLG( q_trans, q_det)
    PosFreq = WHERE(FREQ_D GT 0d, Npos)
    FREQ_D = FREQ_D[PosFreq]
    TRANS_D = TRANS_D[PosFreq]
    FLAG_D = FLAG_D[PosFreq]
    ;INTTRANS = int_tabulated(freq_d, trans_d , /double)
    ;TRANS_1 = trans_d / inttrans ; normalizing the transmission areas
    ;
    ;
    IF KEYWORD_SET(ERR_DETS) THEN BEGIN
      Q_DET_E = where(bolo_list[ idet ] eq name_E)
      Q_DET_E = q_det_E(0)
      FREQ_DE = freq_E( *, q_det_E ) ; already in Hz
      TRANS_DE = trans_E( *, q_det_E )
      ERR_D = INTERPOL(TRANS_DE, FREQ_DE, FREQ_D) ; now on same grid as the transmission spectrum.
    ENDIF
    ;
    co_conversion, nu_nom, freq_d, trans_d, vrad, FCO, F13CO, LINES, NLINES, BT=BT, $
                   ERR_D=ERR_D, GETERR=ERR_DETS, NITER=NITER, ER_FCO=ER_FCO, ER_F13CO=ER_F13CO, MSG=MSG, ER_VRAD = ER_VRAD, FLG_D=FLAG_D
    ;; ------- choose option ----------------
    ;     IF keyword_set(powerlaw) THEN BEGIN
    ;
    ;         inuint = ( freq_d / nu_nom )^alpha
    ;
    ;     ENDIF ELSE IF keyword_set(modblackbody) THEN BEGIN
    ;
    ;         inuint = mod_blackbody(freq_d,betabd , tbd)/ mod_blackbody( nu_nom,betabd , tbd )
    ;
    ;     ENDIF ELSE BEGIN
    ;
    ;         inuint = INTERPOL( inu, nu, freq_d )
    ;         inuint0 = INTERPOL( inu, nu, [nu_nom] )
    ;         inuint =  inuint/inuint0[0]
    ;     ENDELSE
    ;
    ;     FLUX_INT = int_tabulated( freq_d, inuint * trans_1 , /double)
    ;     IRAS_NOM = int_tabulated( freq_d, ( nu_nom / freq_d ) * trans_1, /double )
    ;
    ;     bolo_cc[ idet].cc = flux_int / iras_nom
    bolo_co[idet].cc = FCO
    bolo_co[idet].coline = LINES
    bolo_13co[idet].cc = F13CO
    bolo_13co[idet].coline = LINES
    ;
    ;
    IF KEYWORD_SET(ERR_DETS) THEN BEGIN
      Bolo_co[idet].er = ER_FCO
      bolo_13co[idet].er = ER_F13CO
    ENDIF
    ;
    ; RTS_DETECTORS (v41)
    If (bolo_list[ idet ] eq '70_143_8') OR ( bolo_list[ idet ] eq '55_545_3' ) then BEGIN
	;	OR ( bolo_list[ idet ] eq '74_857_4' ) ... removed from above for DX9.
      bolo_co[ idet ].cc = -1.0d9
      bolo_13co[ idet ].cc = -1.0d9
    ENDIF
    ;
  ENDFOR
  ;
  ; Computing an average (first approximation to map making)
  Q_D = where( bolo_co.cc GT -1.0)
  FOR ii = 0 , Nlines - 1 DO BEGIN
    ch_co.cc[ii] =  mean(bolo_co[ q_d ].cc[ii] )
    ch_13co.cc[ii] =  mean(bolo_13co[ q_d ].cc[ii] )
    ;
    IF KEYWORD_SET(ERR_DETS) THEN BEGIN
      ch_co.er[ii] = MAX( [ SQRT(TOTAL( (bolo_co[ q_d ].er[ii])^2d ))/(DOUBLE(N_ELEMENTS(bolo_co[ q_d ].er[ii]))), $
                                  STDEV(bolo_co[ q_d ].cc[ii])/SQRT(DOUBLE(N_ELEMENTS(bolo_co[ q_d ].cc[ii])))] ) 
    ; either take the individual uncertainties spread into the mean, or take the spread of the coefficients, whichever is greater.
      ch_13co.er[ii] = MAX( [ SQRT(TOTAL( (bolo_13co[ q_d ].er[ii])^2d ))/(DOUBLE(N_ELEMENTS(bolo_13co[ q_d ].er[ii]))), $
                                    STDEV(bolo_13co[ q_d ].cc[ii])/SQRT(DOUBLE(N_ELEMENTS(bolo_13co[ q_d ].cc[ii])))] ) 
    ; either take the individual uncertainties spread into the mean, or take the spread of the coefficients, whichever is greater.
      ;
    ENDIF
    ;
  ENDFOR
  ENDIF ELSE BEGIN
    ;
    ;
    ;
    ; Defining channel parameters
    NU_NOM = double(nuc[ichwork])
    NumLines = colines[ichwork]
    chan = nuc[ichwork]
    CASE chan OF
     	'100': Lns = [0]
      	'143': Lns = [0,1]
   	'217': Lns = [1]
   	'353': Lns = [2]
   	'545': Lns = [3,4]
   	'857': Lns = [5,6,7,8]
   	ELSE: ;
    ENDCASE
    BOLO_LIST = get_hfibolo_list( channel = channel )
    N_BOLO_LIST = n_elements( bolo_list )
    ;
    ;  Define output variables
    bolo_co =  {boloname:'',coline: Lns,cc:DBLARR(NumLines)}
    bolo_co =  replicate(bolo_co,n_bolo_list)
    bolo_co.boloname = bolo_list
    bolo_13co = bolo_co
    ch_co =  {chname:channel,coline:Lns,cc:DBLARR(NumLines)}
    ch_13co = ch_co
    IF KEYWORD_SET(ERR_BNDS) THEN ch_co_avg =  {chname:channel,coline:Lns,cc:DBLARR(NumLines),er:DBLARR(NumLines)} ELSE ch_co_avg =  {chname:channel,coline:Lns,cc:DBLARR(NumLines)}
    ch_13co_avg = ch_co_avg
    ;
    ; starting computation
    For idet = 0 , n_bolo_list - 1 do begin
      ;
      ; RTS_DETECTORS (v41)
      If (bolo_list[ idet ] eq '70_143_8') OR ( bolo_list[ idet ] eq '55_545_3' ) then BEGIN
       	;	OR ( bolo_list[ idet ] eq '74_857_4' ) ... removed from above for DX9.
        bolo_co[ idet ].cc = -1.0d9
        bolo_13co[ idet ].cc = -1.0d9
      ENDIF
      ;
    ENDFOR
    ;
    ;
  ENDELSE
  ;
  ;	Replace the above average with the calculation based on the average spectrum
  IF KEYWORD_SET(abp) THEN BEGIN		;	abp is the average spectra for the given channel.
    trans_b = abp[ichwork].trans
    freq_b = abp[ichwork].freq
    FLAG_B = FABP[ichwork].Trans
    ;stop
    Q_TRANS = where( trans_b gt 1d-17 )	;	This is done later just in case the CO line falls at a low amplitude...
    FREQ_b = freq_b[q_trans] ; already in Hz
    TRANS_b = trans_b[q_trans]
    FLAG_B = FLAG_B[q_trans]
    PosFreq = WHERE(Freq_b GT 0d, Npos)
    FREQ_b = FREQ_b[PosFreq]
    TRANS_b = TRANS_b[PosFreq]
    FLAG_B = FLAG_B[PosFreq]
    ;
    IF KEYWORD_SET(ERR_BNDS) THEN BEGIN
      FREQ_BE = EABP[ichwork].freq
      TRANS_BE = EABP[ichwork].trans ; I should check that this is on the same grid as the bp_info structure (should be, but may not be).
      ERR_b = INTERPOL(Trans_BE, FREQ_BE, FREQ_b)
    ENDIF
    ;
    co_conversion, nu_nom, freq_b, trans_b, vrad, FCO, F13CO, LINES, NLINES, BT=BT, $
                   ERR_D=ERR_B, GETERR=GETERR, NITER=NITER, ER_FCO=ER_FCO, ER_F13CO=ER_F13CO, MSG=MSG, ER_VRAD = ER_VRAD, FLG_D=FLAG_B
    ch_co_avg.cc = FCO
    ch_13co_avg.cc = F13CO
    IF KEYWORD_SET(ERR_BNDS) THEN BEGIN
      ch_co_avg.er = ER_FCO
      ch_13co_avg.er = ER_F13CO
    ENDIF
    ;
    AVG_CO = ch_co_avg
    AVG_13CO = ch_13co_avg
  ENDIF ELSE BEGIN
    CH_CO_AVG = 0d
  ENDELSE
	CH_CO = ch_co
	CH_13CO = ch_13co
  return, ch_co_avg	;	ch_co
END