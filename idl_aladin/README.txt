IDL/ Aladin interface
---------------------

This package provides a set of IDL functions allowing to control Aladin (http://aladin.u-strasbg.fr/) from an IDL script. As we use IDL Java Bridge underneath to call Aladin Java methods, IDL version 6.0 or greater is required. Some of the functions also require the IDL Astronomy User's Library (http://idlastro.gsfc.nasa.gov/)

The web page http://eurovotech.org/twiki/bin/view/VOTech/AladinIDL provides a full description of the available functions. You might also want to refer to the individual .pro files, each function being documented (inputs, outputs, calling sequence, usage example, ...)

*** List of IDL procedures ***

All the IDL procedures (.pro files) are contained in the pro directory as ASCII files:

   add_ct.pro
   convert_vec.pro
   get_image.pro
   get_pixelval_at_reticle_pos.pro
   get_reticle_pos.pro
   get_table.pro
   launch_aladin.pro
   load_image.pro
   load_table.pro
   select_objects.pro
   send_script_command.pro
   set_ct.pro
   set_reticle_pos.pro
   stop_aladin.pro


*** How to use these procedures ? Initial setup ***

Let's consider you uncompressed idl_aladin_interface.tar.gz in directory $HOME/idl_modules/
Download Aladin.jar (for instance in $HOME/jar/).

You need then to update the IDL path in order to easily find the new module. Edit the file pointed by the environment variable $IDL_STARTUP (usually $HOME/.idl_startup), and add the following line :

!path=expand_path('$HOME/idl_modules/idl_aladin_interface')+ ':' +!path

Before being able to use the API, you need to properly configure the IDL Java Bridge :

    * create a new file $HOME/.idljavabrc with the following line :

      JVM Classpath = $CLASSPATH:$HOME/jar/Aladin.jar

      This property will tell IDL where to find needed Java classes

    * create an environment variable called IDLJAVAB_LIB_LOCATION and pointing the JVM shared library to be used. For instance, your .tcshrc file may have the following lines : 

      setenv JAVA_HOME /usr/java/jdk1.5.0_04
      setenv IDLJAVAB_LIB_LOCATION $JAVA_HOME/jre/lib/i386/client

In order to check your setup, launch idl and type the following command : a = launch_aladin() . If Aladin is starting, your configuration is fine. 

*** Version history ***

   Version 0.8 : February 2007 : 2 new functions were added : get_table and select_sources. kill_aladin has been renamed to stop_aladin

   Version 0.6 : January 2007 : add_colortable_in_aladin renamed to add_ct, set_colortable renamed to set_ct

   Version 0.5 : September 2006 - Kickoff


*** Contact ***

This library has been developed and is maintained by Thomas Boch [CDS].
Send an e-mail to boch at astro.u-strasbg.fr for any comment, question or bug report.
