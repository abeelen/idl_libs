;+
; NAME;
;       STOP_ALADIN
; 
; PURPOSE:
;       quit aladin and  destroy the reference to the aladin object
;       Use this procedure to properly quit Aladin
;
; CALLING SEQUENCE:
;       STOP_ALADIN, aladin_ref
;
; INPUTS
;       aladin_ref : reference to the aladin object (return value of launch_aladin function)
;
; EXAMPLE:
;       a = launch_aladin()
;       ....
;       stop_aladin, a
;
; PROCEDURES CALLED:
;      SEND_SCRIPT_COMMAND
;
; MINIMUM IDL VERSION:
;       v6.0 (relies on IDL Java Bridge)
;
; HISTORY
;       Written        Thomas Boch [CDS]        September 2006
;       Procedure named changed from kill_aladin to stop_aladin, TB, January 2007
;
;-
PRO stop_aladin, aladin_ref
	ON_ERROR, 2

	; check number of parameters
	n = n_params() 
	if n lt 1 then begin
		print, ' Usage: stop_aladin, aladin_ref'
		return
	endif

	send_script_command, aladin_ref, 'quit'
	; TODO : we need a way to kill the Command thread ...

	; delete the temporary file
	home = GETENV('HOME')
	temp_file = home+'/.idl_temp_image'
	file_delete, temp_file, /QUIET
	
	; cleanup
	OBJ_DESTROY, aladin_ref
END
