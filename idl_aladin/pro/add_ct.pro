;+
; NAME:
;       ADD_CT
;
; PURPOSE:
;       Add a new color table to the list of Aladin color tables, and use it
;       for the current reference image in Aladin
;
; CALLING SEQUENCE:
;       ct_id = ADD_CT(aladin_ref, red, blue, green, ct_name)
;
; INPUTS:
;       aladin_ref -  reference to the Aladin instance (return value of launch_aladin function)
;       red - array of 256 elements, representing the red components of the
;             color table
;       blue - array of 256 elements, representing the blue componenets of
;              the color table
;       green - array of 256 elements, representing the green components of
;               the color table
;       ct_name - name associated with the color table (for Aladin)
;
; OUTPUTS:
;       a String variable is returned, identifying the color table in Aladin
;       (this String will be ct_name if ct_name is not already used for an
;       existing color table in Aladin)
;
; EXAMPLE:
;       loadct, 27
;       tvlct, r, g, b, /get
;       ctname = add_ct(aladin, r, g, b, 'my_color_table')
;
; PROCEDURES CALLED
;       SEND_SCRIPT_COMMAND
;
; MINIMUM IDL VERSION:
;       v6.0 (relies on IDL Java Bridge)
;
; HISTORY
;       Written        Thomas Boch [CDS]        September 2006
;       Modified, TB, January 2007 : function renamed from add_colortable_in_aladin to add_ct
;
;-
FUNCTION add_ct, aladin_ref, red, blue, green, ct_name
	ON_ERROR, 2

	; check number of parameters
	n = n_params() 
	if n lt 5 then begin
		print, ' Usage: ct_id = add_ct(aladin_ref, red, blue, green, ct_name)'
		return, ''
	endif

	colormap_static = OBJ_NEW('IDLjavaObject$Static$CDS_ALADIN_COLORMAP', 'cds.aladin.ColorMap')
	my_new_colormap = OBJ_NEW('IDLjavaObject$CDS_ALADIN_MYCOLORMAP', 'cds.aladin.MyColorMap', ct_name, red, blue, green)

	name = colormap_static->addCustomCM(my_new_colormap)

	set_ct, aladin_ref, name
	
	;cleanup
	OBJ_DESTROY, colormap_static, my_new_colormap

	return, name
END

