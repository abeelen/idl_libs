;+
; NAME:
;       LOAD_IMAGE
;
; PURPOSE:
;       Load a FITS image in Aladin
;
; CALLING SEQUENCE:
;       LOAD_IMAGE, aladin_ref, im, hdr[, PLANENAME=planename]
;
; INPUTS:
;       aladin_ref -  reference to the Aladin instance (return value of launch_aladin function)
;       im - FITS data array
;       hdr - FITS header
;
; OPTIONAL INPUT KEYWORD
;       PLANENAME - name of the Aladin image plane
;                   If not set, the plane will be labelled 'IDL_image'
; EXAMPLE:
;       load_image, aladin, im, hdr, PLANENAME='my_image'
;
; MINIMUM IDL VERSION:
;       v6.0 (relies on IDL Java Bridge)
;
; DEPENDENCY:
;       IDL Astronomy User's Library (uses procedure writefits)
;
; HISTORY
;       Written        Thomas Boch [CDS]        September 2006
;-
PRO LOAD_IMAGE, aladin_ref, im, hdr, PLANENAME=planename
	ON_ERROR, 2

	if N_params() lt 3 then begin
		print,'Syntax - LOAD_IMAGE, aladin_ref, im, hdr[, PLANENAME=planename]'
		return
	endif


	if not keyword_set(planename) then planename = 'IDL_image'


	home = GETENV('HOME')
	; at the time, this is uglier but much simpler than passing the data array
	file = home+'/.idl_temp_image'
	writefits, file, im, hdr

	aladin_ref->loadImageFromFile, file, planename

END

