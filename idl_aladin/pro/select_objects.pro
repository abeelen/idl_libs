;+
; NAME:
;       SELECT_OBJECTS
;
; PURPOSE:
;       select in Aladin objects of a given table according to their row indexes
;
; CALLING SEQUENCE:
;       SELECT_OBJECTS, aladin_ref, planename, indexes
;
; INPUTS:
;       aladin_ref - reference to the aladin object (return value of launch_aladin function)
;       planename - name of the plane containing objects to be selected.
;                   The name may contain the wildcard character '*'
;       indexes - array of row indexes of each object to be selected in plane planename
;
; EXAMPLE:
;       load_table, aladin, ra, dec, jmag, hmag, kmag, VECNAMES=['ra','dec','jmag','hmag','kmag'], PLANENAME='2MASS'
;       t = where(jmag ne 0 AND jmag gt 18.0)
;       select_objects, aladin, '2MASS', t
;
; MINIMUM IDL VERSION:
;       v6.0 (relies on IDL Java Bridge)
;
; HISTORY
;       Written        Thomas Boch [CDS]        February 2007
;
;-
PRO select_objects, aladin_ref, planename, indexes
	ON_ERROR, 2

	if N_params() lt 3 then begin
		print, 'Usage: select_objects, aladin_ref, planename, indexes'
		return
	end

	aladin_ref->selectSourcesByRowNumber, planename, indexes

END
