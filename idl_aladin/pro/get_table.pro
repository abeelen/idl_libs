;+
; NAME:
;       GET_TABLE
;
; PURPOSE:
;       Retrieve table values from an Aladin catalogue plane
;
; CALLING SEQUENCE:
;       GET_TABLE, aladin_ref, v1[, v2, v3, ..., v8, v9, PLANENAME=planename, COLNAMES=colnames
;
; INPUTS:
;       aladin_ref - reference to the Aladin instance (return value of launch_aladin function)
;
; INPUT KEYWORDS:
;      PLANENAME - name of the catalogue plan to retrieve. This name may contain
;                  the wildcard character '*'
;                  The PLANENAME input keyword is compulsory.
;
; OPTIONAL INPUT KEYWORDS:
;      COLNAMES - array of strings being the names of the fields to retrieve.
;                 If not present, the k first columns will be retrieved, k being
;                 the number of vectors requested (v1, v2, ..., vk)
;
; OUTPUTS:
;      v1, v2, ..., v9 - IDL vectors holding values of requested COLNAMES
;
; EXAMPLE:
;      get_table, aladin, ra, dec, jmag, hmag, kmag, PLANENAME='2MASS', COLNAMES=['RAJ2000', 'DEJ2000', 'Jmag', 'Hmag', 'Kmag']
;
; PROCEDURES CALLED:
;      CONVERT_VEC
;
; MINIMUM IDL VERSION:
;       v6.0 (relies on IDL Java Bridge)
;
; HISTORY
;       Written        Thomas Boch [CDS]        February 2007
;
;-
PRO GET_TABLE, aladin_ref, v1, v2, v3, v4, v5, v6, v7, v8, v9, PLANENAME=planename, COLNAMES=colnames
	ON_ERROR, 2
	
	if not keyword_set(planename) or N_params() lt 2 then begin
		print, 'Usage: get_table, aladin_ref, v1[, v2, v3, ..., v8, v9], PLANENAME=planename[, COLNAMES=colnames]';
		return
	endif

	nb_vec = N_params()-1

	if not keyword_set(colnames) then colnames = strarr(nb_vec)

	data_types = strarr(nb_vec)
	data = aladin_ref->getTableVectors(planename,colnames,data_types)
	data = transpose(data)

	k = 0
	v1 = convert_vec(data[k,*],data_types[k])
	k++
	if arg_present(v2) then v2 = convert_vec(data[k,*],data_types[k])
	k++
	if arg_present(v3) then v3 = convert_vec(data[k,*],data_types[k])
	k++
	if arg_present(v4) then v4 = convert_vec(data[k,*],data_types[k])
	k++
	if arg_present(v5) then v5 = convert_vec(data[k,*],data_types[k])
	k++
	if arg_present(v6) then v6 = convert_vec(data[k,*],data_types[k])
	k++
	if arg_present(v7) then v7 = convert_vec(data[k,*],data_types[k])
	k++
	if arg_present(v8) then v8 = convert_vec(data[k,*],data_types[k])
	k++
	if arg_present(v9) then v9 = convert_vec(data[k,*],data_types[k])
END

