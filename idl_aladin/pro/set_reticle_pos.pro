;+
; NAME:
;       SET_RETICLE_POS
;
; PURPOSE:
;       Set the position of the reticle in Aladin at a given position on the sky
;
; CALLING SEQUENCE:
;       SET_RETICLE_POS, aladin_ref, ra, dec
;
; INPUTS:
;       aladin_ref - reference to the aladin object (return value of launch_aladin function)
;       ra - right ascension (J2000), in decimal degrees
;       dec - variable holding declination (J2000), in decimal degrees
;
; EXAMPLE:
;       set_reticle_pos, aladin, 202.47, -23.29
;
; PROCEDURES CALLED:
;       SEND_SCRIPT_COMMAND
;
; MINIMUM IDL VERSION:
;       v6.0 (relies on IDL Java Bridge)
;
; HISTORY
;       Written        Thomas Boch [CDS]        September 2006
;
;-
PRO set_reticle_pos, aladin_ref, ra, dec
	ON_ERROR, 2

	; check number of parameters
	n = n_params()
	if n lt 3 then begin
		print, ' Usage: set_reticle_pos, aladin_ref, ra, dec'
		return
	endif
	
	coord_static_ref = OBJ_NEW('IDLjavaObject$Static$CDS_ALADIN_COORD', 'cds.aladin.Coord')
	coord_sexa = coord_static_ref->getSexa(ra,dec)
	; cleanup
	OBJ_DESTROY, coord_static_ref
	
	send_script_command, aladin_ref, coord_sexa
END

