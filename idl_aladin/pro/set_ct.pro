;+
; NAME:
;       SET_CT
;
; PURPOSE:
;       set  a given color table for the current reference image in Aladin
;
; CALLING SEQUENCE:
;		SET_CT, aladin_ref, colortable_name
;
; INPUTS:
;       aladin_ref -  reference to the Aladin instance (return value of launch_aladin function)
;       colortable_name - name of the color table to set. This name is the one
;                         returned by function add_ct
;
; EXAMPLE:
;       ctname = add_ct(aladin, r, g, b, 'eosb')
;       set_ct, aladin, ctname
;
; PROCEDURES CALLED:
;       SEND_SCRIPT_COMMAND
;
; aladin_ref : reference to the aladin object (return value of launch_aladin function)
; ct_name : name of the color table to set
;
; PROCEDURES CALLED
;       SEND_SCRIPT_COMMAND
;
; MINIMUM IDL VERSION:
;       v6.0 (relies on IDL Java Bridge)
;
; HISTORY
;       Written        Thomas Boch [CDS]        September 2006
;       Modified, TB, January 2007 : function renamed from set_colortable to set_ct
;
;-
PRO set_ct, aladin_ref, ct_name
	ON_ERROR, 2

	; check number of parameters
	n = n_params() 
	if n lt 2 then begin
		print, ' Usage: set_ct, aladin_ref, colortable_name'
		return
	endif

	send_script_command, aladin_ref, 'cm '+ct_name

END
