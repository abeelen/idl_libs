;+
; NAME:
;       SEND_SCRIPT_COMMAND
;
; PURPOSE:
;       send a script command to an instance of Aladin
;
; CALLING SEQUENCE:
;      SEND_SCRIPT_COMMAND, aladin_ref, cmd
;
; INPUTS:
;       aladin_ref - reference to the aladin object (return value of launch_aladin function)
;       cmd - the command to send to Aladin
;
; EXAMPLE:
;       send_script_command, aladin, 'contour 4'
;
; MINIMUM IDL VERSION:
;       v6.0 (relies on IDL Java Bridge);
;
; HISTORY
;       Written        Thomas Boch [CDS]        September 2006
;
;-
PRO send_script_command, aladin_ref, cmd
	ON_ERROR, 2

	; check number of parameters
	n = n_params() 
	if n lt 2 then begin
		print, ' Usage: send_script_command, aladin_ref, cmd'
		return
	endif

	; ajouter une vérification du type de aladin_ref
	err_msg = aladin_ref->execCommand(cmd)
END
