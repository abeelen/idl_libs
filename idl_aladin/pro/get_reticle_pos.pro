;+
; NAME:
;       GET_RETICLE_POS
;
; PURPOSE:
;       retrieve the current position of the reticle in Aladin
;
; CALLING SEQUENCE:
;       GET_RETICLE_POS, aladin_ref, ra, dec
;
; INPUTS:
;       aladin_ref - reference to the aladin object
;
; OUPUTS:
;       ra - value of right ascension (J2000), in decimal degrees
;       dec - value of declination (J2000), in decimal degrees
;
; EXAMPLE:
;      get_reticle_pos, aladin, ra, dec
;      print, 'Aladin reticle is located at position '+string(ra)+', '+string(dec)
;
; MINIMUM IDL VERSION:
;       v6.0 (relies on IDL Java Bridge);
;
; HISTORY
;       Written        Thomas Boch [CDS]        September 2006
;
;-
PRO get_reticle_pos, aladin_ref, ra, dec
	ON_ERROR, 2

	; check number of parameters
	n = n_params()
	if n lt 3 then begin
		print, ' Usage: get_reticle_pos, aladin_ref, ra, dec'
		return
	endif

	pos = aladin_ref->getReticlePos()
	ra = pos[0]
	dec = pos[1]

END

