;+
; NAME:
;       GET_IMAGE
;
; PURPOSE:
;       Retrieve FITS data array and header from an Aladin image plane
;
; CALLING SEQUENCE:
;       GET_IMAGE, aladin_ref, plane_name, im, hdr
;
; INPUTS:
;       aladin_ref - reference to the Aladin instance (return value of launch_aladin function)
;       plane_name - name of the Aladin image plane to retrieve. Wildcard character '*' can be used ; in this case, the first plane matching the expression will be returned
; 
; OUTPUTS:
;       im - array of image data
;       hdr - String array containing the FITS header
;
; EXAMPLE:
;       get_image, aladin, 'IRAS*', im, hdr
;
; PROCEDURES CALLED
;       SEND_SCRIPT_COMMAND
;
; MINIMUM IDL VERSION:
;       v6.0 (relies on IDL Java Bridge)
;
; DEPENDENCY:
;       IDL Astronomy User's Library (uses procedure readfits)
;
; HISTORY
;       Written        Thomas Boch [CDS]        September 2006
;-
PRO get_image, aladin_ref, plane_name, im, hdr
	ON_ERROR, 2

	if N_params() lt 4 then begin
		print,'Syntax - GET_IMAGE, aladin_ref, plane_name, im, hdr'
		return
	endif

	home = GETENV('HOME')
	file = home+'/.idl_temp_image'

	cmd = 'export "'+plane_name+'"'+' '+file
	send_script_command, aladin_ref, cmd
	im = readfits(file, hdr)
	
END
