; internal function used by GET_TABLE to convert vector data types
FUNCTION convert_vec, vec, data_type
	if data_type eq 'int' or data_type eq 'float' or data_type eq 'short' then return, float(vec)
	if data_type eq 'double' then return, double(vec)

	return, vec
END
