;+
; NAME:
;       LOAD_TABLE
;
; PURPOSE:
;       Load a table into Aladin (ie create a new catalogue plane) from IDL vectors
;
; CALLING SEQUENCE:
;       LOAD_TABLE, aladin_ref, v1, v2, [v3,...v9, 
;                   VECNAMES = , PLANENAME=]
;
; INPUTS:
;       aladin_ref - reference to the Aladin instance (return value of launch_aladin function)
;
; OPTIONAL INPUT KEYWORDS:
;       VECNAMES - array of strings reflecting the name of each column in Aladin.
;                  If not set, the first 2 columns will be named 'ra' and 'dec', and the following ones 'col2', 'col3', ...
;       PLANENAME - name of the newly created Aladin plane
;                   If not set, the plane will be labelled 'IDL_table'
;
; OUTPUTS:
;       v1, v2 - right ascension and declination vectors.
;                These 2 variables are compulsory (otherwise, Aladin won't be able to plot the source position)
;       v3, v4, ..., v9 - other vectors
;
;
; EXAMPLE:
;       load_table, aladin, ra, dec, Umag, Bmag, Vmag, 
;                   VECNAMES=['RA','DEC',umag','bmag','vmag'], PLANENAME='my_table'
;
; MINIMUM IDL VERSION:
;       v6.0 (relies on IDL Java Bridge)
;
; HISTORY
;       Written        Thomas Boch [CDS]        September 2006
;-
PRO LOAD_TABLE, aladin_ref, v1, v2, v3, v4, v5, v6, v7, v8, v9, VECNAMES=vecnames, PLANENAME=planename
	ON_ERROR, 2

	if N_params() lt 3 then begin
		print,'Syntax - LOAD_TABLE, aladin_ref, v1, v2, [v3,...v9, '
		print,'                     VECNAMES = , PLANENAME=]'
		return
	endif

	if not keyword_set(planename) then planename = 'IDL_table'
	if not keyword_set(vecnames) then vecnames =OBJ_NEW()
	
	if keyword_set( v9 ) then begin array = [ [v1], [v2], [v3], [v4], [v5], [v6], [v7], [v8], [v9] ]
	endif else if keyword_set( v8 ) then begin array = [ [v1], [v2], [v3], [v4], [v5], [v6], [v7], [v8] ]
	endif else if keyword_set( v7 ) then begin array = [ [v1], [v2], [v3], [v4], [v5], [v6], [v7] ]
	endif else if keyword_set( v6 ) then begin array = [ [v1], [v2], [v3], [v4], [v5], [v6] ]
	endif else if keyword_set( v5 ) then begin array = [ [v1], [v2], [v3], [v4], [v5] ]
	endif else if keyword_set( v4 ) then begin array = [ [v1], [v2], [v3], [v4] ]
	endif else if keyword_set( v3 ) then begin array = [ [v1], [v2], [v3] ]
	endif else if keyword_set( v2 ) then begin array = [ [v1], [v2] ]
	endif else begin
		print,'Syntax - LOAD_TABLE, aladin_ref, v1, v2, [v3,...v9, '
		print,'                     VECNAMES = , PLANENAME=]'
		return
	endelse

	array = transpose(strtrim(string(array)))

	
	aladin_ref->loadTableFromVectors, array, vecnames, planename

END
