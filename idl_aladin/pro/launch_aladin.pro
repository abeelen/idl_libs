;+
; NAME:
;       LAUNCH_ALADIN
;
; PURPOSE:
;       Launch the Aladin Java application and returns a pointer to this instance
; (this pointer will be needed later to send tables or images from IDL to Aladin)
;
; EXAMPLE:
;       a = launch_aladin()
;
; MINIMUM IDL VERSION:
;       v6.0 (relies on IDL Java Bridge)
;
; HISTORY
;       Written        Thomas Boch [CDS]        September 2006
;-
FUNCTION launch_aladin
	ON_ERROR, 2

	idl_version = !version.release
	
	if idl_version lt '6.0' then begin
		print, 'This function can only be used with IDL >= 6.0'
		return, OBJ_NEW()
	endif

	; request special object to invoke static method
	aladin_static = OBJ_NEW('IDLjavaObject$Static$CDS_ALADIN_ALADIN', 'cds.aladin.Aladin')
	aladin_voapp = aladin_static->launch()

	; cleanup
	OBJ_DESTROY, aladin_static

	return, aladin_voapp
END
