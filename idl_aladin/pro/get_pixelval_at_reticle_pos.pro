;+
; NAME:
;       GET_PIXELVAL_AT_RETICLE_POS
;
; PURPOSE:
;       retrieve the pixel value of the current reference image at the position
;       of the reticle
;
; WARNING: 
;       To make this procedure work properly, the type of pixel value to be
;       displayed has to be set to "Full" in the Pixel panel in Aladin 
;       (top right, next to the position panel). This can also be set once
;       and for all from the Tools-->User Preferences window)
;
; CALLING SEQUENCE:
;       get_pixelval_at_reticle_pos, aladin_ref, pix_val
;
; INPUTS:
;       aladin_ref - reference to the aladin object
;
; OUPUTS:
;       pix_val - value of the pixel of the current reference image located at
;                 the reticle position
;
; EXAMPLE:
;       ra=47.62 & dec=57.45 & set_reticle_pos, aladin, ra, dec
;       get_pixelval_at_reticle_pos, aladin, value
;       print, 'Pixel value at position '+string(ra)+', '+string(dec)+' is '+string(value)
;       
; MINIMUM IDL VERSION:
;       v6.0 (relies on IDL Java Bridge);
;
; HISTORY
;       Written        Thomas Boch [CDS]        September 2006
;
;-
PRO get_pixelval_at_reticle_pos, aladin_ref, pix_val
	ON_ERROR, 2

	; check number of parameters
	n = n_params()
	if n lt 2 then begin
		print, ' Usage: get_pixelval_at_reticle_pos, aladin_ref, pix_val'
		return
	endif
	
	pix_val = aladin_ref->getPixelValAtReticlePos()
END
