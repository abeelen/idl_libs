#!/bin/bash

# Get the directory where the script sits...
export IDL_LIBDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export ASTRON_DIR="$IDL_LIBDIR/astron"
export ATV_DIR="$IDL_LIBDIR/atv"
export COLORTOOLS_DIR="$IDL_LIBDIR/colortools"
export COYOTE_DIR="$IDL_LIBDIR/coyote"
export IDLUTILS_DIR="$IDL_LIBDIR/idlutils"
export IRSA_DIR="$IDL_LIBDIR/IRSA"
export MARKWARDT_DIR="$IDL_LIBDIR/markwardt"
export OAR_DIR="$IDL_LIBDIR/OAR"
export PIP_DIR="$IDL_LIBDIR/PIP"
export SDSSIDL_DIR="$IDL_LIBDIR/sdssidl"
export STARFINDER_DIR="$IDL_LIBDIR/starfinder"
export TEXTOIDL_DIR="$IDL_LIBDIR/textoidl"
export UC_CC_DIR="$IDL_LIBDIR/UC_CC"
export SLUGS_DIR="$IDL_LIBDIR/slugs"
