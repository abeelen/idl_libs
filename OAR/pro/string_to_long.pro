;
;+
; NAME:
;
;       STRING_TO_LONG
;
; PURPOSE:
;
;		Converts all the numeric characters of a string into a Long type variable.
;
;
; CATEGORY:
;
;       String Manipulation.
;
; CALLING SEQUENCE:
;
;		Result = STRING_TO_LONG(string)
;
;
; INPUTS:
;
;		String: scalar or array string to convert.
;
; OUTPUTS:
;
;       The function returns a Long type variable of the same size as String,
;		which value is the number written inside String.
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;
;-

FUNCTION String_to_Long, stringa

;Converte tutti i caratteri numerici di una stringa in un long.

n_strings = n_elements(stringa)

result = lonarr(n_strings)

FOR j = 0, n_strings-1 DO BEGIN

	IF strlen(stringa[j]) EQ 0 THEN result[j] = 0 ELSE BEGIN

		new_stringa = ''
		FOR i = 0, strlen(stringa[j])-1 DO BEGIN

			char = strmid(stringa[j], i, 1)
			IF byte(char) GE 48 AND byte(char) LE 57 THEN new_stringa = new_stringa + char

		ENDFOR

		result[j] = long(new_stringa)

	ENDELSE

ENDFOR

IF n_strings EQ 1 THEN result = result[0]

RETURN, result

END