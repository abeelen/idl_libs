;+
; NAME:
;       GRID_IMAGE
;
; PURPOSE:
;   	Creates the image of a regular grid of points or segments.
;		The grid can be flat or distorted (barrel or pincushion) by a cubic distortion.
;		Mainly useful to test distortion programs.
;
; CALLING SEQUENCE:
;
;		Result = GRID_IMAGE(sizex, sizey, nx, ny, border, DISTORTION=distortion, FWHM=fwhm, $
;						TRASL=trasl, ROT=rot, CENTRAL=central, SQUARES=squares, $
;						HIGH_PRECISION=HIGH_PRECISION, [xdef, ydef], [xs, ys])
;
; INPUTS:
;       sizex:     x size of the final image
;
;       sizey:     y size of the final image
;
;       nx, ny:    number of grid points in x and y (greater or equal 2)
;
;       border:    optional border to leave around the grid (pixels)
;
; KEYWORDS:
;
;       DISTORTION: distortion at the image corner:
;			      (if < 0 you have a barrel distortion, if > 0 a pincushion distortion,
;                 if = 0 there is no distortion).
;				  Distortion is computed respect to the image center.
;
;       FWHM:     full width at half maximum of the gaussian PSF to convolve with the grid points
;
;       TRASL:    2-element vector of the form [offx, offy] with the x and y translation of optical
;				  axis, i.e. the center of the distortion.
;
;       ROT:	  angle of rotation around image center, expressed in degrees, positive clockwise.
;
;       CENTRAL:  if set, the central point of the grid is enhanced (value = 2.0 instead of 1.0)
;
;		SQUARES:  Set this keyword to produce an image of a reticle instead of single points.
;				  In this case the fwhm value is the lines width.
;				  Attention: the grid nodes will be connected by straight segments, i.e. the nodes
;				  positions follow the distortion, but the segments are not distorted.
;
;		HIGH_PRECISION: integer value between 2 and 10: if provided, the image of the grid
;					is computed with the given sub-pixel precision.
;					With high value the computation can be very slow.
;
; OUTPUTS:
;
;       The function returns a float 2-D array of sizex times sizey with value 0 but in the points,
;		which value is 1.0.
;		If FHHM is not zero then the total energy of each point is 1.0
;
; OPTIONAL OUTPUTS:
;
;       xdef,ydef:	x and y coordinates of the final grid (pixels)
;
;       xs,ys:		x and y coordinates of the grid without distortion and translation (pixels)
;
; PROCEDURE:
;
;		A lens optical distortion is a radial deformation which can be written in power series with
;		odd exponents, starting with r^3. Here only this cubic term is considered for the distortion.
;
; CALLS:
;
;		The function uses FSHIFT and SEGMENT.
;
; MODIFICATION HISTORY:
;       February 2004, G. Li Causi, Rome Astronomical Observatory
;       August 2004, G. Li Causi - Added ROT keyword
;
; 		Planned Improvements:
;		- Restore DEFORM keyword
;-


FUNCTION GRID_IMAGE, latox, latoy, nx, ny, bordo, DISTORTION=distortion, FWHM=fwhm, DEFORM=deform, $
		TRASL=trasl, ROT=rot, CENTRAL=central, SQUARES=squares, HIGH_PRECISION=HIGH_PRECISION, xdef,ydef,xs,ys

ON_ERROR, 2

;****************
;PARAMETERS CHECK
;****************
IF latox LE 0 OR latoy LE 0 THEN Message, 'LATOX and LATOY must be greater than 0'

IF nx GT (latox-2*bordo) OR ny GT (latoy -2*bordo) OR nx LT 2 OR ny LT 2 THEN $
								Message, 'NX and NY must be > 2 and < (LATO-2*BORDO)'


IF keyword_set(HIGH_PRECISION) THEN HIGH_PRECISION=FIX(HIGH_PRECISION) > 1 < 10

;**********
;GRID SETUP
;**********
lx2 = latox/2.
ly2 = latoy/2.

passo_x = ( (latox-2*bordo) / (nx-1.) )		;grid gap
passo_y = ( (latoy-2*bordo) / (ny-1.) )

x = findgen(nx) * passo_x + bordo
y = findgen(ny) * passo_y + bordo

nstars = nx * ny			;number of grid points or reticle nodes
xs = fltarr(nstars)
ys = fltarr(nstars)


;****************
;GRID TRANSLATION
;****************
IF KEYWORD_SET(TRASL) THEN BEGIN		;traslazione
	sx = float(trasl[0])
	sy = float(trasl[1])
ENDIF ELSE BEGIN
	sx = 0.
	sy = 0.
ENDELSE

nn = 0
FOR n1 = 0, ny-1 DO BEGIN
	FOR n2 = 0, nx-1 DO BEGIN
  		xs[nn] = x[n2] + sx
 		ys[nn] = y[n1] + sy
 		nn = nn + 1
  ENDFOR
ENDFOR


;*************
;GRID ROTATION
;*************
IF KEYWORD_SET(ROT) THEN BEGIN		;rotazione
	th = rot * !dtor
	FOR n = 0, nn-1 DO BEGIN
  		dx = xs[n] - lx2
  		dy = ys[n]-ly2
  		ro = SQRT(dx^2 + dy^2)
  		angle = ATAN(dy, dx) + th
  		xs[n] = ro * cos(angle) + lx2
 		ys[n] = ro * sin(angle) + ly2
	ENDFOR
ENDIF


;******************
;IMAGE CONSTRUCTION
;******************
img = fltarr(latox,latoy)
FOR n=0, nstars-1 DO BEGIN			;set fluxes
	IF xs[n] LE latox-1 AND  ys[n] LE latoy-1 AND  xs[n] GE 0 and  ys[n] GE 0 THEN BEGIN
		img[xs[n], ys[n]] = 1.0
	ENDIF
ENDFOR

nc = (ny/2)*nx + nx/2
IF KEYWORD_SET(CENTRAL) AND xs[nc] LE latox-1 AND  ys[nc] LE latoy-1 AND $
						xs[nc] GT 0 AND ys[nc] GT 0 THEN img[xs[nc],ys[nc]]=2.0		;set central flux

IF NOT KEYWORD_SET(DISTORTION) AND NOT KEYWORD_SET(DEFORM) AND NOT keyword_set(FWHM) AND NOT keyword_set(squares) THEN RETURN, img	;image of 1 pixel points


;***************
;GRID DISTORTION
;***************
IF KEYWORD_SET(DISTORTION) THEN BEGIN

	img = fltarr(latox,latoy)

	xs = xs - lx2	;punti rispetto al pixel centrale
	ys = ys - ly2

	r = sqrt(xs^2 + ys^2)
	fi = atan(ys/xs)

	wge = where(xs eq 0 and ys ge 0)
	wlt = where(xs eq 0 and ys lt 0)
	IF max(wge) GE 0 THEN fi(wge) = 90 * !dtor
	IF max(wlt) GE 0 THEN fi(wlt) = -90 * !dtor

	ng=(where(xs lt 0))
	IF max(ng) GE 0 THEN fi(ng) = fi(ng) + 180 * !dtor

	IF KEYWORD_SET(DEFORM) THEN distortion = -distortion

	;Cubic radial distortion:
	r_corner = sqrt(lx2^2+ly2^2)
	aa = distortion / r_corner^3			;distortion e' la percentuale di distorsione all'angolo -barilotto +cuscinetto
	rdef = r * (1 + aa * r^3)

	xdef = rdef * cos(fi) + lx2
	ydef = rdef * sin(fi) + ly2

ENDIF ELSE BEGIN

	xdef = xs
	ydef = ys

ENDELSE


;***************
;PSF CONVOLUTION
;***************
IF keyword_set(FWHM) THEN BEGIN


	;IF keyword_set(DEFORM) THEN BEGIN		;<-------ricontrollare deform
	;	;DEFORM:   if set, the PSF of the grid points are distorted with the grid.
	;	;
	;
	;	;*****************
	;	;GAUSSIAN + DEFORM
	;	;*****************
	;	; (Chiama la function REPIX con la keyword /FAST.)
	;
	;	scal = 1.
	;	frame = 1*fwhm*scal
	;	lxs = latox*scal
	;	lys = latoy*scal
	;	xss = xs*scal
	;	yss = ys*scal
	;	lx2s = lx2*scal
	;	ly2s = ly2*scal
	;
	;	llx = lxs + 2*frame
	;	lly = lys + 2*frame
	;	img_big = fltarr(llx, lly)						;bordo per la convoluzione
	;	img_big(frame:frame+lxs-1,frame:frame+lys-1) = rebin(temporary(img), lxs,lys)
	;	stella = psf(fwhm*scal, 2 * frame)
	;
	;   print, 'Convolving with gaussian...'
	;	img_big = convol(temporary(img_big), stella, /CENTER)
	;
 	;    print, 'Computing deformation...'
	;	xdef = xdef*scal + frame
	;	ydef = ydef*scal + frame
	;
	;	xx = MCS( xdef, xs+lx2, ys+ly2, BOUNDS=[0, 0, llx, lly], nx=(llx+1), ny=(lly+1) )
  	;   yy = MCS( ydef, xs+lx2, ys+ly2, BOUNDS=[0, 0, llx, lly], nx=(llx+1), ny=(lly+1) )
	;
    ;	print, 'Deforming grid...'
    ;	img_big = repix(temporary(img_big), xx,yy,FAST=0)
	;
	;	img_big = temporary(img_big(frame:frame+lxs-1,frame:frame+lys-1))			;ritaglio l'immagine finale
	;	img = rebin(temporary(img_big), latox, latoy)
	;
  	;ENDIF ELSE BEGIN

		scal = 1.
		IF KEYWORD_SET(HIGH_PRECISION) THEN scal = float(HIGH_PRECISION)		;fattore di resampling (scal=6 -> precisione nel centro = 0.0005 pixel in simulazioni)

		lxs = latox * scal
		lys = latoy * scal

		img_big = fltarr(lxs, lys)

		IF KEYWORD_SET(SQUARES) THEN BEGIN

			;*****************
			;SQUARES NO DEFORM
			;*****************

		  	FOR nyy=1, ny-1 DO BEGIN
				n1 = nyy * nx + indgen(nx)
		  		xd1 = (xdef(n1) * scal)
		  		yd1 = (ydef(n1) * scal)
				n0 = (nyy-1) * nx + indgen(nx)
		  		xd0 = (xdef(n0) * scal)
		  		yd0 = (ydef(n0) * scal)
			  	img_big = segment(img_big, xd0, yd0, xd1, yd1, 1.0)
			ENDFOR
		  	FOR nxx=1, nx-1 DO BEGIN
				n1 = indgen(ny) * nx + nxx
		  		xd1 = (xdef(n1) * scal)
		  		yd1 = (ydef(n1) * scal)
				n0 = indgen(ny) * nx + (nxx-1)
		  		xd0 = (xdef(n0) * scal)
		  		yd0 = (ydef(n0) * scal)
			  	img_big = segment(img_big, xd0, yd0, xd1, yd1, 1.0)
			ENDFOR

			fwhm_factor = 5.
			pp = psf(fwhm*scal, fwhm_factor * fwhm * scal)
			pp = pp / max(pp)
			img_big = convol(img_big, pp)


		ENDIF ELSE BEGIN

			;******************
			;GAUSSIAN NO DEFORM
			;******************

			fwhm_factor = 5.
			ls = fwhm_factor * fwhm * scal		;box per la PSF
			stella = psf(fwhm * scal, ls)

		  	FOR n=0, nstars-1 DO BEGIN
		  		xds = xdef(n) * scal
		  		yds = ydef(n) * scal
		  		fxds = fix(xds)
		  		fyds = fix(yds)
		  		IF xds GT ls/2. AND yds GT ls/2. AND xds LT (lxs-1 - ls/2.) AND yds LT (lys-1 - ls/2.) THEN BEGIN
			  		;ATTENZIONE!: qui devo TOGLIERE 0.5 pixel allo shift perche' la coordinata del centro pixel
			  		; corrisponde all'angolo in basso a sinistra del pixel, percio' se la mia stella e' centrata
			  		; su un array di lato dispari e la devo assegnare ad es. alla coordinata 20.5 vuol dire
			  		; che devo sovrapporre il suo pixel centrale al pixel 20, cioe' NON devo shiftarla. Al contrario
			  		; se la coordinata e' intera ad es. 21.0, cioe' al bordo pixel, devo assegnare il pixel centrale di stella
			  		; suddividendolo tra i pixel 20 e 21 e quindi devo shiftare di -0.5.
			  		img_big(fxds-ls/2:fxds+ls/2, fyds-ls/2:fyds+ls/2) = $
			  			img_big(fxds-ls/2:fxds+ls/2, fyds-ls/2:fyds+ls/2) + fshift(stella, xds-fxds-0.5, yds-fyds-0.5)
			  		IF keyword_set(central) AND n EQ nc THEN $
				  		img_big(fxds-ls/2:fxds+ls/2, fyds-ls/2:fyds+ls/2) = $
				  			img_big(fxds-ls/2:fxds+ls/2, fyds-ls/2:fyds+ls/2) + fshift(stella, xds-fxds-0.5, yds-fyds-0.5)
				ENDIF
		  	ENDFOR

		ENDELSE

		img = rebin(temporary(img_big), latox, latoy)

  	;ENDELSE



ENDIF ELSE BEGIN



	IF KEYWORD_SET(SQUARES) THEN BEGIN

		;*****************
		;SQUARES NO DEFORM
		;*****************

	  	FOR nyy=1, ny-1 DO BEGIN
			n1 = nyy * nx + indgen(nx)
	  		xd1 = (xdef(n1))
	  		yd1 = (ydef(n1))
			n0 = (nyy-1) * nx + indgen(nx)
	  		xd0 = (xdef(n0))
	  		yd0 = (ydef(n0))
		  	img = segment(img, xd0, yd0, xd1, yd1, 1.0)
		ENDFOR
	  	FOR nxx=1, nx-1 DO BEGIN
			n1 = indgen(ny) * nx + nxx
	  		xd1 = (xdef(n1))
	  		yd1 = (ydef(n1))
			n0 = indgen(ny) * nx + (nxx-1)
	  		xd0 = (xdef(n0))
	  		yd0 = (ydef(n0))
		  	img = segment(img, xd0, yd0, xd1, yd1, 1.0)
		ENDFOR

		;FOR n = 0, nstars-1 DO BEGIN
		;	IF xdef(n) LE latox-1 AND  ydef(n) LE latoy-1 AND  xdef(n) GE 0 AND $
		;			ydef(n) GE 0 THEN img(xdef(n), ydef(n)) = 1.0
		;ENDFOR

		  	img = segment(img, xd0, yd0, xd1, yd1, 1.0)
		  	img = segment(img, xd0, yd0, xd1, yd1, 1.0)

	ENDIF ELSE BEGIN

		;*************
		;SIMPLE POINTS
		;*************

		;IF keyword_set(deform) THEN BEGIN	;<------?????			;deform but no fwhm

		;	xx = MCS( xdef, xs+lx2+sx, ys+ly2+sy, BOUNDS=[0, 0, latox, latoy], nx=(latox+1), ny=(latoy+1) )
		;  	yy = MCS( ydef, xs+lx2+sx, ys+ly2+sy, BOUNDS=[0, 0, latox, latoy], nx=(latox+1), ny=(latoy+1) )

	  	;	print, 'Deforming grid...'
	  	;	img_big = repix(temporary(img), xx,yy,/FAST)

		;ENDIF ELSE BEGIN								;no deform and no fwhm

			FOR n = 0, nstars-1 DO BEGIN
				IF xdef(n) LE latox-1 AND  ydef(n) LE latoy-1 AND  xdef(n) GE 0 AND $
						ydef(n) GE 0 THEN img(xdef(n), ydef(n)) = 1.0
			ENDFOR

		;ENDELSE

	ENDELSE

ENDELSE


RETURN, img

END
