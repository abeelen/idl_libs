;+
;
; NAME:
;
;       PLOT_LOG
;
;
; PURPOSE:
;
;		The same of PLOT, but if the keywords /XLOG or /YLOG are used, makes a logarithm plot with
;		better scaling.
;
; CALLING SEQUENCE:
;
;		PLOT_LOG, y		or		PLOT_LOG, x, y
;
; INPUTS:
;
;       x, y:	Same meaning of PLOT: x and y vectors to plot, y can also be used alone.
;
; KEYWORDS:
;
;		All the keywords of the PLOT routine can be used here.
;
;
; MODIFICATION HISTORY:
;
;       September 2004, G. Li Causi, Rome Astronomical Observatory
;						licausi@mporzio.astro.it
;						http://www.mporzio.astro.it/~licausi/
;
;-


PRO Plot_Log, x, y, _EXTRA=EXTRA


;************
;INPUT CHECK:
;************
n_pars = N_PARAMS()

IF n_pars EQ 1 THEN BEGIN
	data_y = x
	data_x = dindgen(n_elements(data_y))
ENDIF ELSE BEGIN
	data_x = x
	data_y = y
ENDELSE

IF KEYWORD_SET(EXTRA) THEN BEGIN
	IF where(tag_names(EXTRA) EQ 'XLOG') NE -1 THEN XLOG = EXTRA.XLOG
	IF where(tag_names(EXTRA) EQ 'YLOG') NE -1 THEN YLOG = EXTRA.YLOG
ENDIF



;*******************
;INPUT CONDITIONING:
;*******************

data_x_plot = data_x
data_y_plot = data_y
yy = data_y

IF KEYWORD_SET(XLOG) THEN BEGIN
	indx = where(data_x_plot GT 0, countx)
	IF countx NE 0 THEN BEGIN
		data_x_plot = data_x_plot[indx]
		yy = data_y_plot[indx]
	ENDIF ELSE Message,'X vector has no positive values: cannot do the plot!'
ENDIF

IF KEYWORD_SET(YLOG) THEN BEGIN
	indy = where(data_y_plot GT 0, county)
	IF county NE 0 THEN BEGIN
		data_y_plot = data_y_plot[indy]
	ENDIF ELSE Message,'Y vector has no positive values: cannot do the plot!

	indyy = where(yy GT 0, countyy)
	IF countyy EQ 0 THEN Message, 'Data has no points in the first cartesian quadrant: cannot do the plot!'
	IF countyy EQ 1 THEN Message, 'Warning: data has only one point in the first cartesian quadrant.'
ENDIF


;*************
;PLOT OF DATA:
;*************

xmin = min(data_x_plot)
ymin = min(data_y_plot)
xmax = max(data_x_plot)
ymax = max(data_y_plot)

IF KEYWORD_SET(XLOG) OR KEYWORD_SET(YLOG) THEN BEGIN
	PLOT, data_x, data_y, yrange=[ymin,ymax], xrange=[xmin, xmax], _extra=extra
ENDIF ELSE BEGIN
	PLOT, data_x, data_y,  _EXTRA=EXTRA
ENDELSE



END
