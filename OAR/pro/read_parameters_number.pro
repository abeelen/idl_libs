;
;+
; NAME:
;
;       READ_PARAMETERS_NUMBER
;
; PURPOSE:
;
;		Returns the number (and optionally the names) of tags in a parameter file of the form:
;
;			# Comment
;			TAG1 = value
;			TAG2 = value
;			...
;
; CATEGORY:
;
;       Input/Output.
;
; CALLING SEQUENCE:
;
;		READ_PARAMETERS_NUMBER, filename, number [, TAGS=TAGS]
;
;
; INPUTS:
;
;		Filename: name of the text file to read.
;
; KEYWORDS:
;
;       TAGS: output 1-D array with will contain the names of the found tags.
;
; OUTPUTS:
;
;       Number: The function returns the number of tags in the file.
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;       Apr 2004 - 	Added TAGS keyword.
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;
;-

PRO Read_Parameters_Number, filename, number, tags=tags

ON_ERROR, 2

OPENR, unit, filename, /GET_LUN

number = 0
tags = ''
line = ''
WHILE NOT EOF(unit) DO BEGIN

    	READF, unit, line
		line = strtrim(line, 2)		;elimina eventuali spazi iniziali o finali

		pos1 = strpos( line, '#')
		pos2 = strpos( line, ';')
		pos3 = strpos( line, ' ')
		pos4 = strpos( line, '=')

		IF strlen(line) GT 1 AND pos1 NE 0 AND pos2 NE 0 AND pos3 NE 0 AND pos4 NE 0 THEN BEGIN		;linee di commento o scritte male
			pos = strpos(line, '=')
			IF pos GT 0 THEN BEGIN
				number = number + 1
				tags = [tags, strmid(line, 0, pos)]
			ENDIF
		ENDIF

ENDWHILE

CLOSE, unit

tags = tags[1:number-1]

END
