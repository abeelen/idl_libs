;+
; NAME
;        MED_NEIGBORHOOD
; PURPOSE
;       Compute the median of an array avoiding the central point.
;		The array can of even length.
;		Useful to make running filters on data.
;
; CALLING SEQUENCE:
;       Result = MED_NEIGBORHOOD(A [,/MEAN])
;
;
; KEYWORDS:
;		MEAN: If set then the MEAN instead of the MEDIAN is computed on the same points.
;
; OUTPUTS
;       The median (or the mean) of the array A without the central point.
;
; REVISION HISTORY:
;       H.T. Freudenreich, 1989
;       G. Li Causi, 2004
;-

FUNCTION MED_NEIGBORHOOD, A, MEAN=MEAN

ON_ERROR,2

NUM = N_ELEMENTS(A)

INDEXES = WHERE(LINDGEN(NUM) NE NUM/2.)

AA = A[INDEXES]		;copy of A without the central point

NUM = N_ELEMENTS(AA)

IF KEYWORD_SET(MEAN) THEN RETURN, MEAN(AA)

IF NUM MOD 2 EQ 0 THEN BEGIN  ; even # points. Can't call MEDIAN.
   B = AA   &  B = B( SORT(B) )  &  I0  = (NUM-1)/2  & MED =.5*(B(I0)+B(I0+1))
ENDIF ELSE MED = MEDIAN(AA)

RETURN, MED

END
