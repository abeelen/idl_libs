;+
; NAME:
;       PLOT_LON_LAT
;
; PURPOSE:
;		Plot data on a spherical map autoscaling map center and limits.
;
; CATEGORY:
;		Mapping
;
; CALLING SEQUENCE:
;
;	PLOT_LON_LAT, lon, lat [,PSYM=PSYM] [,BORDER=BORDER] [, any MAP_SET keyword]
;
; INPUTS:
;       lon, lat: 1-D arrays with the longitude and latitude positions of the data points
;
; KEYWORDS:
;
;       PSYM: plot symbol to use.
;
;		BORDER: border around the data before the edges of the map (degrees);
;			if not set, then a border of 5% of the maximum data extents are used.
;
;		CENTRAL_LONGITUDE: if set, then the map is centered on this longitude
;
;		Any keyword accepted by MAP_SET is also accepted here.
;		If the keyword LIMIT is not set, then the procedure compute the best limits to fit the data.
;		The center of the map is computed as the geometrical center of the data.
;
; OUTPUTS:
;
;		The selected Map Projection with the correct limit is set and the data are plotted.
;
; CALLS:
;
;		The following routines are needed:
;		- ANG_BAR
;
;
; MODIFICATION HISTORY:
;       Marzo 2004 - Gianluca Li Causi, INAF - Osservatorio Astronomico di Roma
;
;-

PRO Plot_Lon_Lat, lon, lat, psym=psym, border=border, _EXTRA=EXTRA, limit=limit, central_longitude=central_longitude



center = Ang_Bar(transpose([[lon], [lat]]), /DEGREES, /CENTER)	;computes the center of the data distribution

IF n_elements(central_longitude) NE 0 THEN center[0] = central_longitude

inizio:
IF n_elements(border) EQ 0 THEN border2 = 0 ELSE border2 = border

lat_min = min(lat) - border2
lat_max = max(lat) + border2

lonp = lon + border2
lonm = lon - border2
lonp = lonp - 360*(lonp GT (center[0]+180)) + 360*(lonp LT (center[0]-180))		;divido in +-180 attorno a center[0]
lonm = lonm - 360*(lonm GT (center[0]+180)) + 360*(lonm LT (center[0]-180))
lon_min = min(lonm)
lon_max = max(lonp)

IF n_elements(border) EQ 0 THEN	BEGIN
	border = (lon_max-lon_min)*.05*cos(center[1]) > (lat_max-lat_min)*.05
	GOTO, inizio
ENDIF


limit = [lat_min > (-90), lon_min, lat_max < 90, lon_max]

MAP_SET, center[1], center[0], 0, _EXTRA=EXTRA, limit=limit

plots, lon, lat, psym=psym


END
