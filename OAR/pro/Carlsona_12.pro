function CarlsonA_12,n,tab
;
;+
; NAME:
;       CarlsonA_12
;PURPOSE:
;	This function returns in a double precision array  the director cosines and the relative weights for CarlsonA quadrature
; 	3D integration scheme. In cases n=4,6,8 data are taken from table 2, pag.15 in
;	Carlson B.G.,1963, in Alder B., Fernbach S.(eds.) Methods in Computational Physics. Vol. 1 p.1
; 	In case n=12, angles and weights are computed as indicated in the article by calling subroutine Wconstruct.
;   Because of symmetry properties (see reference), only angles in an octant are evaluated, other directions being
;   easily retreived by multiples of 90 degrees rotations.
;   Note that weights are normalized to one ON THE OCTANT, so that a proper renormalization has to be performed if
;   integration is performed on the whole sphere.

;CATEGORY:
;		Math.

;CALLING SEQUENZE:
;		Result=CarlsonA_12(n)

;INPUT:
; 		n=ordine della quadratura. n=[4,6,8,12]

;OUTPUT:
;		Result= array of size [n*(n+2)/8,4].
;       Each column contains the weight (first entry), the director cosines (second, third, fourth entries).
;       Angles are defined moving 'away' from axes.
;		Note that because of symmetries is not necessary to specify the axes of reference when defining the angles.


;EXAMPLE
; I want to evaluate the flux through an octant of an Intensity  Radiation field I(mu,csi), where mu and csi are direction cosines. Than:

;		R=CarlsonA_12(6)

;		Flux=0.d0
; 		for k=0,6*(6+2)/8 -1 do begin
;			Flux=R(k,0)*I(R(k,1),R(k,2))+Flux
; 		endfor


; MODIFICATION HISTORY
;		S. Criscuoli, H.A.O.-O.A.R.: 15-11-2004.




;if n eq 2 then begin
	;tab=fltarr(1,3)
	;tab(0,0)=1.
	;tab(0,1)=0.57735027
	;tab(0,2)=1
;endif

if n eq 4 then begin
	tab=fltarr(2,3)
	tab(0,0)=0.3333333
	tab(0,1)=0.3333333
	tab(0,2)=3

	tab(1,0)=0.
	tab(1,1)=0.88191710
	tab(1,2)=0
endif

if n eq 6 then begin
	tab=fltarr(3,3)
	tab(0,0)=0.15
	tab(0,1)=0.25819889
	tab(0,2)=3

	tab(1,0)=0.1833333
	tab(1,1)=0.68313005
	tab(1,2)=3

	tab(2,0)=0.
	tab(2,1)=0.93094934
	tab(2,2)=0
endif
if n eq 8 then begin
	tab=fltarr(4,3)
	tab(0,0)=0.07075470
	tab(0,1)=0.21821790
	tab(0,2)=1

	tab(1,0)=0.09138352
	tab(1,1)=0.57735027
	tab(1,2)=6

	tab(2,0)=0.12698139
	tab(2,1)=0.7879579
	tab(2,2)=3

	tab(3,0)=0.
	tab(3,1)=0.95118972
	tab(3,2)=0.
endif

if n eq 12 then begin
	tab=fltarr(6,3)
endif
dim=total(tab(*,2))
bigtab=dblarr(n*(n+2)/8,4)
k1=0
l=where(tab(0,*) ne 0,count)

;return,bigtab
if n eq 4 then begin
	bigtab(0,0)=tab(0,0)
	bigtab(0,1)=tab(0,1)
	bigtab(0,2)=tab(1,1)
	bigtab(0,3)=tab(0,1)
	bigtab(1,0)=tab(0,0)
	bigtab(1,1)=tab(1,1)
	bigtab(1,2)=tab(0,1)
	bigtab(1,3)=tab(0,1)
	bigtab(2,0)=tab(0,0)
	bigtab(2,1)=tab(0,1)
	bigtab(2,2)=tab(0,1)
	bigtab(2,3)=tab(1,1)
endif


if n eq 6 then begin
	bigtab(0,0)=tab(0,0)
	bigtab(0,1)=tab(0,1)
	bigtab(0,2)=tab(1,1)
	bigtab(0,3)=tab(1,1)
	bigtab(1,0)=tab(0,0)
	bigtab(1,1)=tab(1,1)
	bigtab(1,2)=tab(0,1)
	bigtab(1,3)=tab(1,1)
	bigtab(2,0)=tab(0,0)
	bigtab(2,1)=tab(1,1)
	bigtab(2,2)=tab(1,1)
	bigtab(2,3)=tab(0,1)

	bigtab(3,0)=tab(1,0)
	bigtab(3,1)=tab(0,1)
	bigtab(3,2)=tab(2,1)
	bigtab(3,3)=tab(0,1)
	bigtab(4,0)=tab(1,0)
	bigtab(4,1)=tab(0,1)
	bigtab(4,2)=tab(0,1)
	bigtab(4,3)=tab(2,1)
	bigtab(5,0)=tab(1,0)
	bigtab(5,1)=tab(2,1)
	bigtab(5,2)=tab(0,1)
	bigtab(5,3)=tab(0,1)

endif

if n eq 8 then begin
	bigtab(0,0)=tab(2,0)
	bigtab(0,1)=tab(0,1)
	bigtab(0,2)=tab(0,1)
	bigtab(0,3)=tab(0,1)

	bigtab(1,0)=tab(1,0)
	bigtab(1,1)=tab(0,1)
	bigtab(1,2)=tab(1,1)
	bigtab(1,3)=tab(2,1)

	bigtab(2,0)=tab(1,0)
	bigtab(2,1)=tab(0,1)
	bigtab(2,2)=tab(2,1)
	bigtab(2,3)=tab(1,1)

	bigtab(3,0)=tab(1,0)
	bigtab(3,1)=tab(1,1)
	bigtab(3,2)=tab(0,1)
	bigtab(3,3)=tab(2,1)

	bigtab(4,0)=tab(1,0)
	bigtab(4,1)=tab(1,1)
	bigtab(4,2)=tab(2,1)
	bigtab(4,3)=tab(0,1)

	bigtab(5,0)=tab(1,0)
	bigtab(5,1)=tab(2,1)
	bigtab(5,2)=tab(1,1)
	bigtab(5,3)=tab(0,1)

	bigtab(6,0)=tab(1,0)
	bigtab(6,1)=tab(2,1)
	bigtab(6,2)=tab(0,1)
	bigtab(6,3)=tab(1,1)

	bigtab(7,0)=tab(2,0)
	bigtab(7,1)=tab(3,1)
	bigtab(7,2)=tab(0,1)
	bigtab(7,3)=tab(0,1)

	bigtab(8,0)=tab(2,0)
	bigtab(8,1)=tab(0,1)
	bigtab(8,2)=tab(3,1)
	bigtab(8,3)=tab(0,1)

	bigtab(9,0)=tab(0,0)
	bigtab(9,1)=tab(1,1)
	bigtab(9,2)=tab(1,1)
	bigtab(9,3)=tab(3,1)

endif

if n eq 12 then begin
Wconstruct,mu,v
	bigtab(0,0)=v(0)
	bigtab(0,1)=mu(5)
	bigtab(0,2)=mu(0)
	bigtab(0,3)=mu(0)

	bigtab(1,0)=v(0)
	bigtab(1,1)=mu(0)
	bigtab(1,2)=mu(5)
	bigtab(1,3)=mu(0)

	bigtab(2,0)=v(0)
	bigtab(2,1)=mu(0)
	bigtab(2,2)=mu(0)
	bigtab(2,3)=mu(5)

	bigtab(3,0)=v(1)
	bigtab(3,1)=mu(4)
	bigtab(3,2)=mu(0)
	bigtab(3,3)=mu(1)

	bigtab(4,0)=v(1)
	bigtab(4,1)=mu(4)
	bigtab(4,2)=mu(1)
	bigtab(4,3)=mu(0)

	bigtab(5,0)=v(1)
	bigtab(5,1)=mu(1)
	bigtab(5,2)=mu(4)
	bigtab(5,3)=mu(0)

	bigtab(6,0)=v(1)
	bigtab(6,1)=mu(1)
	bigtab(6,2)=mu(0)
	bigtab(6,3)=mu(4)

	bigtab(7,0)=v(1)
	bigtab(7,1)=mu(0)
	bigtab(7,2)=mu(4)
	bigtab(7,3)=mu(1)

	bigtab(8,0)=v(1)
	bigtab(8,1)=mu(0)
	bigtab(8,2)=mu(1)
	bigtab(8,3)=mu(4)

	bigtab(9,0)=v(2)
	bigtab(9,1)=mu(0)
	bigtab(9,2)=mu(2)
	bigtab(9,3)=mu(3)

	bigtab(10,0)=v(2)
	bigtab(10,1)=mu(0)
	bigtab(10,2)=mu(3)
	bigtab(10,3)=mu(2)

	bigtab(11,0)=v(2)
	bigtab(11,1)=mu(2)
	bigtab(11,2)=mu(3)
	bigtab(11,3)=mu(0)

	bigtab(12,0)=v(2)
	bigtab(12,1)=mu(2)
	bigtab(12,2)=mu(0)
	bigtab(12,3)=mu(3)

	bigtab(13,0)=v(2)
	bigtab(13,1)=mu(3)
	bigtab(13,2)=mu(0)
	bigtab(13,3)=mu(2)

	bigtab(14,0)=v(2)
	bigtab(14,1)=mu(3)
	bigtab(14,2)=mu(2)
	bigtab(14,3)=mu(0)

	bigtab(15,0)=v(3)
	bigtab(15,1)=mu(2)
	bigtab(15,2)=mu(2)
	bigtab(15,3)=mu(1)

	bigtab(16,0)=v(3)
	bigtab(16,1)=mu(2)
	bigtab(16,2)=mu(1)
	bigtab(16,3)=mu(2)

	bigtab(17,0)=v(3)
	bigtab(17,1)=mu(1)
	bigtab(17,2)=mu(2)
	bigtab(17,3)=mu(2)

	bigtab(18,0)=v(4)
	bigtab(18,1)=mu(1)
	bigtab(18,2)=mu(1)
	bigtab(18,3)=mu(3)

	bigtab(19,0)=v(4)
	bigtab(19,1)=mu(3)
	bigtab(19,2)=mu(1)
	bigtab(19,3)=mu(1)

	bigtab(20,0)=v(4)
	bigtab(20,1)=mu(1)
	bigtab(20,2)=mu(3)
	bigtab(20,3)=mu(1)

endif


return,bigtab

end



pro Wconstruct,mu,v

w2=dblarr(6)
mu2=dblarr(6)
wl=dblarr(6)

w2(0)=(4.d0+0.00592059)/33.d0
;w2(0)=(4.d0+0.00416667)/15.d0
mu2(0)=1.d0/33.d0
;mu2(0)=1.d0/15.d0
wl(0)=sqrt(w2(0))

for i=1,5 do begin
	w2(i)=w2(0)+i*2.d0/11.d0
	mu2(i)=mu2(0)+i*2.d0/11.d0
	wl(i)=sqrt(w2(i))-(sqrt(w2(i-1)))
endfor

w2(5)=0.d0
wl(5)=1.d0-total(wl(0:4))
mu=sqrt(mu2)
W=sqrt(W2)

v=dblarr(5)
v(0)=wl(5)
v(1)=wl(4)/2.d0
v(2)=0.1d0*(4*wl(3)+wl(2)-2*wl(1)+2*wl(4));0.5d0*(4.d0*wl(3)/5.d0 + 3.d0*wl(4)/5.d0 - 2.d0*wl(1)/5.d0)
v(3)=(wl(1)+2*wl(2)-2.d0*wl(3)-wl(4))/5.d0;(wl(1)+wl(4)-2.d0*wl(3))/5.d0
v(4)=(wl(3)-wl(2) + 2.d0*wl(1)-2*wl(4))/5.d0;(wl(3)-3.d0*wl(4) + 2.d0*wl(1))/5.d0


; Angles classification in the notaton of Carlson, 1963 for n=12 case.

;611,116,161                    v1 v(0)
;134,143,431,413,314,341        v3 v(2)
;512,521,251,215,125,152        v2 v(1)
;422,224,242                    v5 v(4)
;332,233,323                    v4 v(3)
end



