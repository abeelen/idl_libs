;
;+
; NAME:
;
;       MYCOLORS
;
; PURPOSE:
;
;		Returns a structure to easily use the basic colours in screen or postscript.
;
; CATEGORY:
;
;       Plotting.
;
; CALLING SEQUENCE:
;
;		Result = MYCOLORS([PS=PS])
;
;
; KEYWORD PARAMETERS:
;
;		PS: If set then postscript colors are returned.
;
;
; OUTPUTS:
;
;		Structure with the following tags containing the respective color code:
;			Result.black
;			Result.white
;			Result.red
;			Result.green
;			Result.blue
;			Result.yellow
;			Result.magenta
;			Result.cyan
;
;
; MODIFICATION HISTORY:
;
;		Modified from Agostino Fiorani, INAF - Rome Astronomical Observatory
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;
;-

FUNCTION mycolors, PS=PS

wincolors = {	black: 0L, $
			white: 0L, $
			red: 0L, $
			green: 0L, $
			blue: 0L, $
			yellow: 0L, $
			magenta: 0L, $
			cyan: 0L }

pscolors = {	black: 0b, $
			white: 0b, $
			red: 0b, $
			green: 0b, $
			blue: 0b, $
			yellow: 0b, $
			magenta: 0b, $
			cyan: 0b }

r = [0,255,255,0,0,255,255,0]
g =	[0,255,0,255,0,255,0,255]
b =	[0,255,0,0,255,0,255,255]

TVLCT,	r, g, b

pscolors.black = 0
pscolors.white = 1
pscolors.red = 2
pscolors.green = 3
pscolors.blue = 4
pscolors.yellow = 5
pscolors.magenta = 6
pscolors.cyan = 7

wincolors.black =	r[0] + g[0]*256L + b[0]*65536L
wincolors.white =	r[1] + g[1]*256L + b[1]*65536L
wincolors.red =		r[2] + g[2]*256L + b[2]*65536L
wincolors.green =	r[3] + g[3]*256L + b[3]*65536L
wincolors.blue =	r[4] + g[4]*256L + b[4]*65536L
wincolors.yellow =	r[5] + g[5]*256L + b[5]*65536L
wincolors.magenta =	r[6] + g[6]*256L + b[6]*65536L
wincolors.cyan =	r[7] + g[7]*256L + b[7]*65536L


colors = wincolors
IF KEYWORD_SET(PS) THEN colors = pscolors


RETURN, colors

END