;
;+
; NAME:
;
;       DRAW_INVERSE_BOX
;
; PURPOSE:
;
;		Draws a rectangle over the current window with colors complementary to the
;		underlaying ixels.
;
; CATEGORY:
;
;       Graphics.
;
; CALLING SEQUENCE:
;
;		DRAW_INVERSE_BOX, x0, y0, x1, y1, [WINDOW_CONTENT=WINDOW_CONTENT]
;
;
; KEYWORDS:
;
;		WINDOW_CONTENT: optional keyword used to avoid DRAW_INVERSE_BOX to read the
;			content of the window each time it is called. Useful to speed execution
;			when DRAW_INVERSE_BOX has to be called repeatedly: in this case, simply
;			read the window *before* to call DRAW_INVERSE_BOX and pass it to the
;			WINDOW_CONTENT keyword.
;
;			Example: a=TVRD() & DRAW_INVERSE_BOX, x0,y0,x1,y1,WINDOW_CONTENT=a
;
;			ATTENTION: after program execution WINDOW_CONTENT will contain the current
;			window's image i.e. the original one *with* the box overimposed.
;
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;		Sept 2005 - Gianluca Li Causi: added WINDOW_CONTENT keyword to enhance speed.
;-


PRO Draw_Inverse_Box, x0, y0, x1, y1, WINDOW_CONTENT=WINDOW_CONTENT

	IF NOT KEYWORD_SET(WINDOW_CONTENT) THEN window_content = tvrd()

	s = size(window_content)

	x_0 = x0 > 0 < s[1]
	x_1 = x1 > 0 < s[1]
	y_0 = y0 > 0 < s[2]
	y_1 = y1 > 0 < s[2]

	xx0 = x_0 < x_1
	yy0 = y_0 < y_1
	xx1 = x_0 > x_1
	yy1 = y_0 > y_1

	window_content[xx0:xx0, yy0:yy1] = 255 - window_content[xx0:xx0, yy0:yy1]
	window_content[xx0:xx1, yy0:yy0] = 255 - window_content[xx0:xx1, yy0:yy0]
	window_content[xx0:xx1, yy1:yy1] = 255 - window_content[xx0:xx1, yy1:yy1]
	window_content[xx1:xx1, yy0:yy1] = 255 - window_content[xx1:xx1, yy0:yy1]

	tv, window_content[xx0:xx0, yy0:yy1], xx0, yy0
	tv, window_content[xx0:xx1, yy0:yy0], xx0, yy0
	tv, window_content[xx0:xx1, yy1:yy1], xx0, yy1
	tv, window_content[xx1:xx1, yy0:yy1], xx1, yy0

END
