;
;+
; NAME:
;
;       IS_NUMBER
;
; PURPOSE:
;
;		Check if a string has only numerical characters (also scientific notation).
;
; CATEGORY:
;
;       String processing.
;
; CALLING SEQUENCE:
;
;		Result = IS_NUMBER(string)
;
;
; INPUTS:
;
;		string: any scalar string
;
; OUTPUTS:
;
;       The function returns 1 if only numerical characters, 0 otherwise.
;
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;
;-

FUNCTION IS_NUMBER, str


len = STRLEN(str)


IF len EQ 0 THEN RETURN, 0

numeric_chars = ['+', '-', '.', '0','1','2','3','4','5','6','7','8','9', 'e']   ;+,-,.,digits,e

previous = ''
dot1 = 0
dot2 = 0
e = 0

FOR c = 0, len-1 DO BEGIN

	char = STRMID(str, c, 1)
	ok = where(char EQ numeric_chars, count)

	IF count EQ 0 THEN RETURN, 0						;if not a numeric character

	IF char EQ '.' AND e EQ 0 THEN dot1 = dot1 + 1
	IF char EQ '.' AND e EQ 1 THEN dot2 = dot2 + 1

	IF char EQ 'e' THEN e = e + 1

	IF dot1 GT 1 THEN RETURN, 0
	IF dot2 GT 1 THEN RETURN, 0
	IF e GT 1 THEN RETURN, 0	;if more than one 'e'

	IF char EQ 'e' AND (c EQ 0 OR c EQ len-1) THEN RETURN, 0			;if e is first or last character
	IF char EQ 'e' AND c EQ 1 AND previous EQ '.' THEN RETURN, 0		;if . is first and e is second character
	IF char EQ '.' AND c EQ len-1 AND (previous EQ 'e' OR previous EQ '+' OR previous EQ '-') THEN RETURN, 0						;if . is last character
	IF (char EQ '+' OR char EQ '-') AND c EQ len-1 THEN RETURN, 0			;if . is last character
	IF char EQ 'e' AND (previous EQ 'e' OR previous EQ '+' OR previous EQ '-') THEN RETURN, 0 ;if e and previous is + or -
	IF (char EQ '+' OR char EQ '-') AND c GT 0 AND previous NE 'e' THEN RETURN, 0	;if + or - not first and previous is not e

	previous = char

ENDFOR

RETURN, 1

END
