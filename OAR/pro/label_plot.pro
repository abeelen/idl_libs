;
;+
; NAME:
;
;       LABEL_PLOT
;
; PURPOSE:
;
;		This procedure marks choosen points of a plot by automatically managing
;		their position to avoid overlapping among labels and between labels and plotted data.
;		If it's not possible to find the space for a label, that label is neglected.
;		A segment is drawn from each label to the corresponding data point.
;
; CATEGORY:
;
;       Plotting
;
; CALLING SEQUENCE:
;
;		LABEL_PLOT, datax, datay, label, step
;
; INPUT:
;
;		Datax, Datay: x,y data coordinates of the points which correspond to the label vector
;
;		Label: string type vector with the labels for each Datax, Datay points
;
;		Step: setting the step value makes it possible to have Datax,Datay and Label vectors
;				with a different number of elements.
;				For example if Datax,Datay containd 1000 elements and the user wants to label
;				just on point on one hundred, than the Label vector is a 100 elements vector
;				and Step is 10.
;
;
; PROCEDURE:
;
;		For each label the routine computes the minimum distance from four possible positions
;		of the text box and each data point and from the text box and any previously written label.
;		If this distance is less than a threshold than the label is written else it is neglected.
;		A segment is then drawn from the label to the corresponding data point.
;
; MODIFICATION HISTORY:
;
;       March 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;-

PRO Label_Plot, datax, datay, label, steps

charsize = 3
plot_factor = 1.1
characters = max(strlen(label))
xgap = 25       ;x distance from text box and graph
ygap = 20

dati = n_elements(datax)

xy = CONVERT_COORD(datax, datay, /DATA, /TO_DEVICE)

xtext = characters * charsize * plot_factor
gap = 10
min_dist = SQRT((xgap-gap)^2 + (ygap-gap)^2)			;minima dist tra le scritte e i punti
min_text_dist = xtext + min_dist					;minima dist tra le scritte
xy1 = xy
xy1[0,*] = xy[0,*] + xgap
xy1[1,*] = xy[1,*] + ygap
xy2 = xy
xy2[0,*] = xy[0,*] - xgap - xtext
xy2[1,*] = xy[1,*] + ygap
xy3 = xy
xy3[0,*] = xy[0,*] + xgap
xy3[1,*] = xy[1,*] - ygap
xy4 = xy
xy4[0,*] = xy[0,*] - xgap - xtext
xy4[1,*] = xy[1,*] - ygap
dist = fltarr(4)
w = 0
dist_w = 1e9
FOR i = 0., dati-1, steps DO BEGIN
	dist[0] = min(SQRT((xy[0,*]-xy1[0,i])^2 + (xy[1,*]-xy1[1,i])^2))
	dist[1] = min(SQRT((xy[0,*]-(xy2[0,i]+xtext))^2 + (xy[1,*]-xy2[1,i])^2))
	dist[2] = min(SQRT((xy[0,*]-xy3[0,i])^2 + (xy[1,*]-xy3[1,i])^2))
	dist[3] = min(SQRT((xy[0,*]-(xy4[0,i]+xtext))^2 + (xy[1,*]-xy4[1,i])^2))
	max_dist = max(dist, index)
	xf = ([ xy1[0,i], xy2[0,i], xy3[0,i], xy4[0,i] ])[index]
	yf = ([ xy1[1,i], xy2[1,i], xy3[1,i], xy4[1,i] ])[index]
	IF n_elements(xfin) NE 0 THEN dist_w = min(SQRT((xfin-xf)^2 + (yfin-yf)^2))		;minima distanza dal testo gi� scritto
	IF dist_w GT min_text_dist AND max_dist GT min_dist THEN BEGIN
		XYOUTS, xf, yf, label[i/steps], /DEVICE, charthick=2, charsize=plot_factor
		plots, [xy[0,i], xf], [xy[1,i], yf], /DEVICE		;line
		plots, xy[0,i], xy[1,i], /DEVICE, psym=2, thick=2	;mark
	ENDIF
	IF n_elements(xfin) NE 0 THEN xfin = [xfin, xf] ELSE xfin = xf
	IF n_elements(yfin) NE 0 THEN yfin = [yfin, yf] ELSE yfin = yf
	w = w + 1
ENDFOR


END