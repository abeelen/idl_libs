;+
; NAME:
;       SGN
;
; PURPOSE:
;	Return the sign of its argument.
;
; DESCRIPTION:
;
;	Return the sign of its argument:
;
;		1  for positive numbers,
;		0  for zeroes,
;		-1 for negative numbers
;
;
; CALLING SEQUENCE:
;
;	Result = SGN(X, /NO_ZERO)
;
; INPUTS:
;       X:  	Any scalar or array number
;
; KEYWORDS:
;
;		NO_ZERO: if set, returns 1 for zero arguments.
;
; OUTPUTS:
;       Result: -1, 0 or 1.
;
; MODIFICATION HISTORY:
;          G. Li Causi, O.A.R.: March 2004
;-

FUNCTION sgn, x, no_zero=no_zero

s = x * 0
zero = where(x EQ 0, count)
not_zero = where(x NE 0, n_not_zero)
IF count NE 0 THEN IF KEYWORD_SET(NO_ZERO) THEN s[zero] = 1 ELSE s[zero] = 0
IF n_not_zero NE 0 THEN s[not_zero] = x[not_zero] / ABS(x[not_zero])

RETURN,s

END
