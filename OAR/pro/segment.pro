;
;+
; NAME:
;
;       SEGMENT
;
; PURPOSE:
;
;		Draws one or more segments inside a 2-D image.
;
; CATEGORY:
;
;       Graphics.
;
; CALLING SEQUENCE:
;
;		Result = SEGMENT(image, x0, y0, x1, y1, value)
;
;
; INPUTS:
;
;		Image: 2-D array
;
;		x0, y0:	starting point coordinates, scalar or vector
;
;		x1, y1:	ending point coordinates, scalar or vector, same number of elements as x0, y0
;
;		Value: value to assign to the pixels of the segment
;
; OUTPUTS:
;
;       The function returns Image with the segments overimposed
;
;
; PROCEDURE:
;
;		All the array elements within a straight line from [x0,y0] to [x1, y1] are set
;		to Value. The number of segments is the number of elements of x0.
;
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;
;		Planned improvements:
;		- Work with segments which cross the image edges.
;-

FUNCTION segment, image, x0cr, y0cr, x1cr, y1cr, cr_value

s = size(image)
IF min(x0cr) LT 0 OR min(y0cr) LT 0 OR max(x0cr) GT (s[1]-1) OR max(y0cr) GT (s[2]-1) THEN $
					MESSAGE, 'Both initial and final point must be inside the image'
IF min(x1cr) LT 0 OR min(y1cr) LT 0 OR max(x1cr) GT (s[1]-1) OR max(y1cr) GT (s[2]-1) THEN $
					MESSAGE, 'Both initial and final point must be inside the image'

ncr = n_elements(x0cr)

FOR i = 0, ncr-1 DO BEGIN
	FOR x = x0cr[i], x1cr[i] DO BEGIN
		frac = (x-x0cr[i])/float(x1cr[i]-x0cr[i])
		y = y0cr[i] + frac*(y1cr[i]-y0cr[i])
		image[x,y] = cr_value
	ENDFOR
	FOR y = y0cr[i], y1cr[i] DO BEGIN
		frac = (y-y0cr[i])/float(y1cr[i]-y0cr[i])
		x = x0cr[i] + frac*(x1cr[i]-x0cr[i])
		image[x,y] = cr_value
	ENDFOR
ENDFOR

RETURN, image

END
