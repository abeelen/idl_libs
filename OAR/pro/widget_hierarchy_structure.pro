;
;+
; NAME:
;
;       WIDGET_HIERARCHY_STRUCTURE
;
; PURPOSE:
;
;		Return a structure with the widget IDs of all widget elements belonging to a given widget's hiararchy.
;
;
; CATEGORY:
;
;       Widgets.
;
; CALLING SEQUENCE:
;
;		Result = Widget_Hierarchy_Structure(widget_id)
;
;
; INPUTS:
;
;		widget_id:	the id of the widget whose hierarchy is to be explored.
;
;
; OUTPUTS:
;
;       Result:	an idl structure containing a sub-structure for each widget type present in the
;				hierarchy. Each sub-structure contains a number of tags whose names are the
;				UVALUE names of the widgets and whose values are their IDs.
;				The Result structure is made as follows:
;
;				Result = {
;							BASE:		{uname_base_widget_1:	ID_base_widget_1,
;														....
;														....
;														....
;										uname_base_widget_x:	ID_base_widget_x},
;							BUTTON:		{uname_button_widget_1:	ID_button_widget_1,
;														....
;														....
;														....
;										uname_button_widget_y:	ID_button_widget_y},
;							COMBOBOX:	{....},
;							DRAW:	{....},
;							DROPLIST:	{....},
;							LABEL:	{....},
;							LIST:	{....},
;							PROPERTYSHEET:	{....},
;							SLIDER:	{....},
;							TAB:	{....},
;							TABLE:	{....},
;							TEXT:	{....},
;							TREE:	{....}
;						}
;
;				This way e.g. the widget ID of a droplist whose UNAME is 'my_droplist'
;				is simply given by:
;								my_droplist_ID = Result.DROPLIST.my_droplist
;
;
; PROCEDURE:
;
;		The given widget is checked for its children and their sibling until all the widgets in the
;		hierarchy has been controlled.
;
;
; MODIFICATION HISTORY:
;
;       Jun 2005 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;
;-


FUNCTION Widget_Hierarchy_Structure, base_widget


	;Create a structure with all the widgets' IDs:
	base_widget_name = Widget_Info(base_widget, /UNAME)
	WID = CREATE_STRUCT(base_widget_name, base_widget)


	;Find the unames, types and IDs of all the widgets in the hierarchy whose parent is base_widget.
	unames = ''
	types = ''
	ids = 0L
	sibling_checked = 1
	child_checked = 1
	current_widget = base_widget
	REPEAT BEGIN

		check_for_sibling = where(sibling_checked EQ 0, count_sibling)
		IF count_sibling EQ 0 THEN check_for_child = where(child_checked EQ 0, count_child) ELSE count_child = 0
		count = count_sibling > count_child
		IF n_elements(ids) EQ 1 THEN count = 1

		FOR i = 0, count-1 DO BEGIN

			IF n_elements(ids) GT 1 THEN BEGIN						;if the base_widget has already been checked

				IF count_sibling NE 0 THEN BEGIN
					id = ids[check_for_sibling[i]]						;id of the widget to check for sibling
					current_widget = Widget_Info(id, /SIBLING)			;id of the sibling
					sibling_checked[check_for_sibling[i]] = 1
				ENDIF

				IF count_child NE 0 THEN BEGIN
					id = ids[check_for_child[i]]						;id of the widget to check for child
					current_widget = Widget_Info(id, /CHILD)			;id of the child
					child_checked[check_for_child[i]] = 1
				ENDIF

			ENDIF


			IF current_widget NE 0 THEN BEGIN						;if there is a child or sibling

				uname = Widget_Info(current_widget, /UNAME)			;user name of widget
				type = Widget_Info(current_widget, /NAME)			;type of widget

				;Check the uname to build the correct structure tag name:
				uname = strtrim(uname, 2)
				IF uname EQ '' THEN uname = strtrim(string(current_widget),2)		;se non esiste uname, ci mette l'id
				first_char = BYTE(strmid(uname, 0, 1))
				IF first_char LT 65 OR (first_char GT 90 AND first_char LT 97) OR first_char GT 122 THEN uname = 'w_' + uname	;se il primo carattere non e' alfabetico lo precede con "w_"
				chars = BYTE(uname)
				pos = where(chars LT 48 OR (chars GT 57 AND chars LT 65) OR (chars GT 90 AND chars LT 95) OR (chars GT 95 AND chars LT 97) OR chars GT 122, count)		;caratteri non alfanumerici e non underscore
				IF count THEN chars[pos] = 95b
				uname = string(uname)

				unames = [unames, uname]
				types = [types, type]
				ids = [ids, current_widget]
				sibling_checked = [sibling_checked, 0]				;set the current widget to check for sibling
				child_checked = [child_checked, 0]					;set the current widget to check for child

			ENDIF

		ENDFOR

	ENDREP UNTIL min([sibling_checked, child_checked]) EQ 1				;repeat until there are no more widgets to check


	unames = unames[1:*]
	types = types[1:*]
	ids = ids[1:*]

	utypes = types[uniq(types, sort(types))]		;uniqe type names

	n_widgets = n_elements(unames)
	n_types = n_elements(utypes)


	;Builds the organized structure containing the previous information:
	FOR t = 0, n_types-1 DO BEGIN

		type = utypes[t]		;current type
		index = where(types EQ type, n_widgets_this_type)

		FOR w = 0, n_widgets_this_type - 1 DO BEGIN

			uname = unames[index[w]]	;current uname
			id = ids[index[w]]			;current id

			IF w NE 0 THEN s = CREATE_STRUCT(s, uname, id) ELSE s = CREATE_STRUCT(uname, id)			;build a structure for each uniqe type

		ENDFOR

		IF t NE 0 THEN WID = CREATE_STRUCT(WID, type, s) ELSE WID = CREATE_STRUCT(type, s)		;add the structure for each type

	ENDFOR


	RETURN, WID


END
