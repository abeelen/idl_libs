;
;+
; NAME:
;
;       STR_WRAP
;
; PURPOSE:
;
;		Divide one or more strings in pieces of a given lenght.
;
; CATEGORY:
;
;       String Manipulation.
;
; CALLING SEQUENCE:
;
;		Result = STR_WRAP(string, lenght)
;
; INPUTS:
;
;		String: scalar or array string to wrap.
;
;		Lenght: maximum lenght of substrings in characters.
;
; OUTPUTS:
;
;       The function returns a string array in which each element is a piece of String.
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;
;-

FUNCTION Str_Wrap, str, len

IF len LT 1 THEN MESSAGE, 'ERROR: wrapping lenght has to be greater than one character.'
n_string = n_elements(str)

orig = str
result = ''

FOR i = 0, n_string-1 DO BEGIN
	REPEAT BEGIN
		a = strmid(orig[i], 0, len)
		b = strmid(orig[i], len)
		result = [result, a]
		orig[i] = b
	ENDREP UNTIL strlen(orig[i]) LE len
	IF b NE '' THEN result = [result, b]
ENDFOR

result = result[1:n_elements(result)-1]


RETURN, result

END
