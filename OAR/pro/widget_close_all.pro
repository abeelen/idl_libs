;
;+
; NAME:
;
;       WIDGET_CLOSE_ALL
;
; PURPOSE:
;
;		Delete all the widgets currently managed by XMANAGER, but the ones which IDs are in the SKIP vector.
;
;
; CATEGORY:
;
;       Widgets.
;
; CALLING SEQUENCE:
;
;		WIDGET_CLOSE_ALL [, /SKIP]
;
;
; KEYWORD PARAMETERS:
;
;		SKIP: 1-D long type array with the Widget IDs of the widget to skip from deleting.
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;
;-

PRO Widget_Close_All, skip=skip

COMMON MANAGED,	IDs, $		; IDs of widgets being managed
	  			names, $	; and their names
				modalList	; list of active modal widgets

n_widgets = n_elements(IDs)

FOR i = 0, n_widgets-1 DO BEGIN
	IF n_elements(skip) NE 0 THEN BEGIN
		pos = WHERE(IDs[i] EQ skip, count)
		IF count EQ 0 THEN IF Widget_Info(IDs[i], /valid) THEN Widget_Control, IDs[i], /DESTROY
	ENDIF ELSE IF Widget_Info(IDs[i], /valid) THEN Widget_Control, IDs[i], /DESTROY
ENDFOR

END
