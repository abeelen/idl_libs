;
;+
; NAME:
;
;       START
;
; PURPOSE:
;
;		Generic procedure to automatically set the paths environment and run a procedure.
;		By simply writing in a Configuration File the name of the procedure to run and the paths of
;		the used libraries, the user can easily work with different projects,
;		without to manually editing the IDL search and working directories at each time.
;		The routine will work on any operative system.
;
; CATEGORY:
;
;       Program Control.
;
; CALLING SEQUENCE:
;
;		START [,NO_START=NO_START] [,CFG_FILE=CFG_FILE]
;
;
; KEYWORD PARAMETERS:
;
;		NO_START: Tells the program to don't run the procedure after setting the environment paths.
;
;       CFG_FILE: Filename of the Configuration File in which to read the paths and the name of
;			the procedure to run.
;			If not set, a file named 'start,cfg' is searched in the same directory of START.PRO.
;			If no configuration file at all has been prepared, the path will be restricted to the
;			only directory of START.PRO.
;			Hereafter an example of a Configuration File (it is an ASCII file):
;
;				------------------------------------------------------------------------------------
;				##### Begin the paths with "+" if you want to include the subdirectories tree ######
;
;				# Procedure to run:
;				procedure = "my_procedure"
;				
;				# Procedure related paths:
;				programs_path = "+C:\my_procedure_directory"
;				
;				# Library path:
;				library_path ="+C:\my_libraries_directory"
;				------------------------------------------------------------------------------------
;
;			With the above example file, the user tells IDL to set 'C:\my_procedure_directory' as the working
;			directory and to add all its subdirectory tree to the IDL search path (because of the '+'
;			sign, then to add all the libraries in '+C:\my_libraries_directory' to the search path and
;			finally to run the program 'my_procedure' (to be written whitout the .pro extension).
;			No other directories, but the IDL system directories, will be added to the path defined in the fine.
;
; SIDE EFFECTS:
;
;		The common block:  COMMON system, system  is created with the structure 'system' of the following form:
;
;		system = 		{	os: 	!VERSION.OS_FAMILY, $	;current operative system
;							slash: 					'', $	;directory separation character for the current operative system
;							path_separator:			'', $	;IDL path separation character for the current operative system
;							copy_command:			'', $	;shell COPY command for the current operative system
;							move_command:			'', $	;shell MOVE / RENAME command for the current operative system
;							delete_command:			''}		;shell DELETE command for the current operative system
;
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;       Mar 2004 - 	Gianluca Li Causi - added possibility to don't use any configuration file
;-

PRO Start, no_start=no_start, cfg_file=cfg_file


COMMON system, system


FORWARD_FUNCTION ___str_subst, ___read_parameters


;Imposta i parametri per il sistema operativo corrente:
system = 		{	os: 	!VERSION.OS_FAMILY, $
					slash: 					'', $
					path_separator:			'', $
					copy_command:			'', $
					move_command:			'', $
					delete_command:			''}

CASE StrUpCase(system.os) of
	'WINDOWS': BEGIN
		system.slash = '\'
		system.path_separator = ';'
		system.copy_command = 'copy'
		system.move_command = 'move'
		system.delete_command = 'del'
		END
	'UNIX': BEGIN
		system.slash = '/'
		system.path_separator = ':'
		system.copy_command = 'cp'
		system.move_command = 'mv'
		system.delete_command = 'rm'
		END
	'MACOS': BEGIN
		system.slash = ':'
		system.path_separator = ':'
		system.copy_command = ''
		system.move_command = ''
		system.delete_command = ''
		END
	'VMS': BEGIN
		system.slash = ']'
		system.path_separator = ','
		system.copy_command = ''
		system.move_command = ''
		system.delete_command = ''
		END
	ELSE: BEGIN
		system.slash = ''
		system.path_separator = ''
		system.copy_command = ''
		system.move_command = ''
		system.delete_command = ''
	END
ENDCASE


;Chiude tutti i file eventualmente lasciati aperti in sessioni precedenti:
close, /all


;Legge la directory corrente:
;Ref: SourceRoot function
Help, Calls = Calls
UpperRoutine = (StrTok(Calls[0], ' ', /Extract))[0]
Skip = 0
Catch, ErrorNumber
If (ErrorNumber ne 0) then Begin
	Catch, /Cancel
	ThisRoutine = Routine_Info(UpperRoutine, /Source)
	Skip = 1
EndIf
If (Skip eq 0) then Begin
	ThisRoutine = Routine_Info(UpperRoutine, /Source)
EndIf
current_dir = StrMid(ThisRoutine.Path, 0, StrPos(ThisRoutine.Path, system.slash, /Reverse_Search) + 1)
print, 'DETECTED CURRENT DIRECTORY: ', current_dir


;Setta la directory corrente:
CD, current_dir


;Definisce il PATH:
!PATH = EXPAND_PATH('+' + !DIR)															;IDL path
!PATH = !PATH + system.path_separator + EXPAND_PATH(current_dir)					;Current dir

;il "+" per aggiungere le subdirectories deve essere messo in start.cfg
Start = 	{	procedure:		'', $
				programs_path:	'', $
				library_path:	''}


;Aggiunge i paths descritti nel file 'start.cfg'
IF NOT KEYWORD_SET(cfg_file) THEN cfg_file = 'start.cfg'
IF FILE_TEST(cfg_file) THEN BEGIN

	;il "+" per aggiungere le subdirectories deve essere messo in start.cfg
	Start = 	{	procedure:		'', $
					programs_path:	'', $
					library_path:	''}


	;Legge i parametri nel file installation.cfg:
	___read_parameters, cfg_file, start

	n_paths = N_TAGS(Start)
	FOR i = 1, n_paths-1 DO BEGIN
		IF FILE_TEST(___str_subst(Start.(i), '+', ''), /DIR) EQ 0 AND STRTRIM(Start.(i),2) NE '' THEN message, 'The path number ' + string(i) + 'in the configuration file does not exist!'
		!PATH = !PATH + system.path_separator + EXPAND_PATH(Start.(i))
	ENDFOR

	IF NOT KEYWORD_SET(no_start) THEN IF Start.Procedure NE '' THEN Call_Procedure, strlowcase(Start.Procedure)

ENDIF


END






FUNCTION ___str_subst, s, in, out, start = start

;Sostituisce tutte le occorrenze della sottostringa IN
;con la stringa OUT nelle stringhe S

IF KEYWORD_SET(start) THEN inizio = start ELSE inizio = 0

ns = n_elements(s)
result = s
;IF ns EQ 1 THEN s = strarr(ns) + s

FOR i = 0, ns-1 DO BEGIN

	pos = strpos(result[i], in, inizio)
	IF pos LT 0 THEN GOTO, next

	REPEAT BEGIN
		s1 = strmid(result[i], inizio, pos)
		s2 = strmid(result[i], pos+strlen(in), strlen(result[i]))
		result[i] = s1 + out + s2
		pos = strpos(result[i], in, pos+strlen(out))
	ENDREP UNTIL pos EQ (-1)

next:
ENDFOR

RETURN, result

END






PRO ___read_parameters, filename, structure

;Legge un file di parametri identificati da tags
;
;Filename = nome del file
;structure = struttura con i nomi dei tags uguali a quelli da leggere
;
;Ritorna structure con i valori letti assegnati ai tag corretti
;
;Il programma non fa distinzione tra maiuscolo e minuscolo

tags = tag_names(structure)
tags = strlowcase(tags)
ntags = n_elements(tags)


openr, unit, filename, /GET_LUN

line = ""
i = 0
WHILE i LT ntags DO BEGIN

	IF NOT EOF(unit) THEN BEGIN

    	READF, unit, line
		line = strtrim(line, 2)		;elimina eventuali spazi iniziali o finali

		pos = strpos( strlowcase(line), tags[i])

		IF pos EQ 0 AND strlen(line) GE 3 THEN BEGIN		;il tag deve essere la prima scritta sulla riga

			pos = strpos(line, '=', pos)
			value_string = strmid(line, pos+1, strlen(line))
			value_string = ___str_subst(value_string, '"', '')	;elimina eventuali virgolette
			value_string = strtrim(value_string, 2)			;elimina eventuali spazi iniziali o finali
			size = size([structure.(i)])
			n_el = n_elements(structure.(i))

			IF n_el GT 1 THEN BEGIN			;array di piu' elementi
				value_string = ___str_subst(value_string, '[', '')		;elimina eventuali "["
				value_string = ___str_subst(value_string, ']', '')		;elimina eventuali "]"
				value_string = strsplit(value_string, ',', /extract)	;elementi separati da virgola
			ENDIF

			value = structure.(i)

			IF size[size[0]+1] EQ 1 THEN BEGIN				;dato booleano --> byte per idl
				FOR j = 0, n_el-1 DO value[j] = where(value_string[j] EQ ['no', 'yes'])
			ENDIF ELSE BEGIN
				value[*] = value_string[*]
			ENDELSE

			structure.(i) = value

			i = i + 1				;next tag
			point_lun, unit, 0		;rewind the file

		ENDIF

	ENDIF ELSE BEGIN

		i = i + 1				;next tag
		point_lun, unit, 0		;rewind the file

	ENDELSE

ENDWHILE

close, unit

END




