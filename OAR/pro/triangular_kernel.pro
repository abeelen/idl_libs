pro triangular_kernel, LENGTH, PEAK_POS, K_TR = K_TR

;
;+
; NAME:
;
;       triangular_kernel
;
; PURPOSE:
;
;		Compute a triangular normalized kernel. Useful to produce asimmetric
;		distorsion of lines by convolution.
;
; CATEGORY:
;
;       VAR-ARRAYS.
;
; CALLING SEQUENCE:
;
;		TRIANGULAR_KERNEL, LENGTH, PEAK_POS, K_TR = K_TR
;
;INPUTS:
;
;       LENGTH:	array length
;
;		PEAK_POS: position in pixels of the peak of the triangle from the central pixel of the array.
;		PEAK_POS can contain a fractional part and must be: PEAK_POS < LENGTH.
;		Positive values for PEAK_POS means a right peak triangle.
;
; KEYWORDS:
;
;		 K_TR:	triangular kernel
;
; MODIFICATION HISTORY:
;
;       Dec  2004 - Massimo De Luca, INAF - Rome Astronomical Observatory
;					deluca@mporzio.astro.it
;
;
;-

K_TR = dblarr(LENGTH)
c = LENGTH / 2
if PEAK_POS eq 0 then begin
	K_TR[c] = 1
	return
endif
sign = PEAK_POS / abs(PEAK_POS)
a1 = abs(PEAK_POS)
l = 2 * a1 + 1						;triangle basis   (+1 to have 1 pixel for a=0)
ai = fix(a1)
af = a1 - ai
n = 2 * ai + 2 - 1					;number of heigths to compute
h = dindgen(n + 1)				;vector of heigths
for j = 0, n do h[j] = (af + j) / float(l)
if af gt 0 then K_TR[c - sign * (ai + 1)] = h[0] * af / 2			;first pixel
if af gt 0 then K_TR[c + sign * (ai + 1)] = (1 + h[n]) * af / 2		;last pixel
for j = 1, n do K_TR[c - sign * (ai + 1 - j)] = (h[j] + h[j - 1]) / 2	;central pixels

end
