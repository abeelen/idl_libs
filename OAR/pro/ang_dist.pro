;+
; NAME:
;       ANG_DIST
;
; PURPOSE:
;	Computes the angolar distance, on the sphere, between two points of
;	coordinates C1 and C2.
;
; CALLING SEQUENCE:
;
;	Result = ANG_DIST(C1, C2, [/DEGREES])
;
; INPUTS:
;       C1, C2:  2-element arrays (=[lon, lat])with the coordinates of the
;		two points expressed in RADIANS.
;
; KEYWORDS:
;       DEGREES: If set, indicates that C1 and C2 are expressed in degrees.
;				Also the result will be in degrees.
;
; OUTPUTS:
;       Result: Scalar value containing the minimum angular distance along the
;		maximum circle connecting the two points.
;
; MODIFICATION HISTORY:
;          G. Li Causi, O.A.R.: 25-08-2000
;-

FUNCTION ang_dist, C1, C2, degrees=degrees

IF C1(0) EQ C2(0) AND C1(1) EQ C2(1) THEN RETURN,0		;se sono le stesse coordinate

c1 = double(c1)
c2 = double(c2)

IF KEYWORD_SET(DEGREES) THEN BEGIN
	C1 = C1 * !DTOR
	C2 = C2 * !DTOR
ENDIF


;Conversione da coord. angolari a coord. x,y,z:
x1 = cos(C1(0))*cos(C1(1))
y1 = sin(C1(0))*cos(C1(1))
z1 = sin(C1(1))

x2 = cos(C2(0))*cos(C2(1))
y2 = sin(C2(0))*cos(C2(1))
z2 = sin(C2(1))

;Calcola l'angolo tra due rette:
dist = ACOS( (1 + (y1*y2+ z1*z2)/(x1*x2) )/SQRT( (1 + (y1/x1)^2 + (z1/x1)^2 ) * (1 + (y2/x2)^2 + (z2/x2)^2 ) ) < 1 >(-1) )

;IF dist LT 0 THEN dist = dist + !PI
verso = x1*x2 + y1*y2 + z1*z2
dist = dist * sgn(verso)

IF KEYWORD_SET(DEGREES) THEN dist = dist * !RADEG

RETURN, dist

END
