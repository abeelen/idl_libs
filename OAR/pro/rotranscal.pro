;+
; NAME:
;
;       ROTRANSCAL
;
;
; PURPOSE:
;
;	Computes the roto-translation and scaling parameters between two corresponding sets of points.
;
; DESCRIPTION:
;
;	Computes the rotation, translation and scaling parameters between two corresponding sets of points.
;	Also works with coordinates inversion and with different x and y scaling.
;	In case of coordinates affected by random errors, the best solution is given in the
;	chi-squared sense.
;
;	Useful e.g. to compute the best rototranslation and scale to match two images from the coordinates
;	of a set of corresponding control points, for example stars in astronomical images.
;
;
; CATEGORY:
;
;       ANALYTHIC GEOMETRY
;
;
; CALLING SEQUENCE:
;
;		Result = ROTRANSCAL(x1, y1, x2, y2, /REVERSE)
;
; INPUTS:
;
;	x1, y1:	1-d vectors: coordinates of the points in reference frame n. 1
;
;	x2, y2:	1-d vectors: coordinates of the points in reference frame n. 2
;
;
; KEYWORDS:
;
;	REVERSE: if set, then Result is the roto-translation and scaling to pass back from reference n. 2
;			to reference n. 1.
;
;
; OUTPUTS:
;
;	Result:	5-element vector of the form: [theta,  dx, dy,  kx, ky], where theta is the rotation angle in degrees,
;			dx, dy is the translation and kx, ky are the x and y scaling factor to pass from reference n. 1
;			to reference n. 2.
;			Theta will range in the interval [-180, 180) if /REVERSE is not set and in the
;			interval (-180, 180] if /REVERSE is set.
;
;
; PROCEDURE:
;
;	Singular value decomposition (SVD) is used to solve a linear system in which the elements of Result
;	are the unknowns. SVD automatically gives the best solution, in the chi-squared sense, if the linear system
;	has not an analythic solution.
;
;
; IDL VERSION AT WRITING TIME:
;
;	IDL v. 6.0 Win32
;
;
; MODIFICATION HISTORY:
;
;	October 2004 - 	Gianlcua Li Causi, INAF - Rome Astronomical Observatory
;			licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;
; TESTED BY:
;
;	G. Li Causi, Oct 2004
;	A. Caratti o Garatti, Oct 2004
;
;
; PLANNED IMPROVEMENTS:
;
;	- Add possibility to request only rotation, or translation, or scale with identical
;		or different x and y scaling and to choose if allow coordinate inversion or not.
;	- So far it works with 3 or more points: generalize to less than 3 points.
;	- This function weights all points the same: find out how to add different points weighting.
;	- Add computation of errors propagation to the result.
;
;-

FUNCTION ROTRANSCAL, x1, y1, x2, y2, REVERSE=REVERSE

on_error, 2		;return to caller on errors.


;***********
;INPUT CHECK
;***********
n = n_elements(x1)

IF n_elements(x2) NE n OR n_elements(y1) NE n OR n_elements(y2) NE n THEN $
		Mesage, 'ERROR: One or more input vectors are not same size!'

IF n LT 3 THEN Message, 'ERROR: Input vectors must contain at least 3 elements!'

sx1 = size(x1, /n_dimensions)
sx2 = size(x2, /n_dimensions)
sy1 = size(y1, /n_dimensions)
sy2 = size(y2, /n_dimensions)

IF (sx1 > sx2 > sy1 > sy2) GT 1 OR (sx1 < sx2 < sy1 < sy2) LT 1 THEN $
		Message, 'ERROR: One ore more input vectors are not 1-dimesnional!'


rx1 = ABS(max(x1)-min(x1))
rx2 = ABS(max(x2)-min(x2))
IF rx1 > rx2 NE 0 AND rx1 < rx2 EQ 0 THEN MESSAGE, 'ERROR: One of the two coordinates yields null x-scaling!'

ry1 = ABS(max(y1)-min(y1))
ry2 = ABS(max(y2)-min(y2))
IF ry1 > ry2 NE 0 AND ry1 < ry2 EQ 0 THEN MESSAGE, 'ERROR: One of the two coordinates yields null y-scaling!'



;*******************
;LINEAR SYSTEM SOLVE
;*******************

;Soluzione di A*v1=x1 e A*v2=y1

x2t = transpose(x2)
y2t = transpose(y2)
it = transpose(fltarr(n) + 1)

A = [ x2t, y2t, it ]


; Decompose A:
SVDC, A, W, U, V, /DOUBLE

; Compute the solution:
;dove v1 = [kx*C, -ky*S, dx*C-dy*S]
;e v2 = [kx*S, ky*C, dx*S+dy*C]

v1 = SVSOL(U, W, V, x1, /DOUBLE)
v2 = SVSOL(U, W, V, y1, /DOUBLE)


;Rotation:
;tg(theta) = v2[0]/v1[0] & tg(theta) = -v1[1]/v2[1]
IF float(v1[0]) EQ 0 THEN BEGIN
	theta1 = ATAN(-v1[1],v2[1])
	theta2 = theta1
ENDIF
IF float(v2[1]) EQ 0 THEN BEGIN
	theta1 = ATAN(v2[0],v1[0])
	theta2 = theta1
ENDIF
IF float(v1[0]) NE 0 AND float(v2[1]) NE 0 THEN BEGIN
	theta1 = ATAN(-v1[1],v2[1])
	theta2 = ATAN(v2[0],v1[0])
	dift = float( (theta1>theta2) - (theta1<theta2) )
	IF dift GT .5*!PI AND dift LT 1.5*!PI THEN inv = 1 ELSE inv = 0		;c'e' una inversione di coordinate
ENDIF


;Scale:
S = sin(theta1)
C = cos(theta1)
IF ABS(C+S) GT 1e-6 THEN BEGIN
	M = [[C+S, 0], [0, C+S]]
	SVDC, M, W, U, V
	k1 = SVSOL(U, W, V, [v1[0]+v2[0], v2[1]-v1[1]], /DOUBLE)
ENDIF ELSE BEGIN
	M = [[C-S, 0], [0, C-S]]
	SVDC, M, W, U, V
	k1 = SVSOL(U, W, V, [v1[0]-v2[0], v2[1]+v1[1]], /DOUBLE)
ENDELSE


S = sin(theta2)
C = cos(theta2)
IF ABS(C+S) GT 1e-6 THEN BEGIN
	M = [[C+S, 0], [0, C+S]]
	SVDC, M, W, U, V
	k2 = SVSOL(U, W, V, [v1[0]+v2[0], v2[1]-v1[1]])
ENDIF ELSE BEGIN
	M = [[C-S, 0], [0, C-S]]
	SVDC, M, W, U, V
	k2 = SVSOL(U, W, V, [v1[0]-v2[0], v2[1]+v1[1]])
ENDELSE



IF inv THEN BEGIN
		IF ABS(theta1) LE ABS(theta2) THEN BEGIN		;prende l'inversione che implica la rotazione minore
		k2 = -k2
		theta2 = theta2 + !PI
	ENDIF ELSE BEGIN
		k1 = -k1
		theta1 = theta1 + !PI
	ENDELSE
ENDIF

IF float(theta1) GE !PI THEN theta1 = theta1 - 2*!PI
IF float(theta1) LT (-!PI) THEN theta1 = theta1 + 2*!PI
IF float(theta2) GE !PI THEN theta2 = theta2 - 2*!PI
IF float(theta2) LT (-!PI) THEN theta2 = theta2 + 2*!PI

k = (k1 + k2) / 2.
theta = (theta1 + theta2) / 2.



;Translation:
S = sin(theta)
C = cos(theta)

M = [[C, -S], [S, C]]
SVDC, M, W, U, V, DOUBLE=DOUBLE
dxy = SVSOL(U, W, V, [v1[2], v2[2]], DOUBLE=DOUBLE)


;******
;OUTPUT
;******
theta = theta * !RADEG
IF float(theta) GE 180 THEN theta = theta - 360
IF float(theta) LT (-180) THEN theta = theta + 360

IF NOT KEYWORD_SET(REVERSE) THEN RTS = [theta, dxy, k] ELSE RTS = [-theta, -dxy, 1./k]


RETURN, RTS

END
