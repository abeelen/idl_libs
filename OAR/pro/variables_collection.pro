;
;+
; NAME:
;
;       VARIABLES_COLLECTION
;
; PURPOSE:
;		Create and manage pseudo-arrays with different type/dimensions for each element.
;
;
; DESCRIPTION:
;
;		Create, modify or delete an array of pointers avoiding explicit pointers usage.
;		A variables collection can be considered as an array which can have a different
;		type and/or dimensions for each element.
;
; CATEGORY:
;
;       Arrays Management.
;
; CALLING SEQUENCE:
;
;		Result = VARIABLES_COLLECTION(array, element, value, DIMENSIONS=DIMENSIONS, DELETE=DELETE)
;
;
; INPUT:
;
;		array:	an existing variables collection to modify or delete.
;
;		element: scalar or n-dim vector specifying the 1-dim or n-dim index of element to modify.
;
;		value:	if set, value to assign to the selected element: can be any of variable type and any dimension
;				If undefined, is will output the current value of the selected element.
;
; OUTPUT:
;
;		Result:	if the DIMENSIONS keyword is set, will contain the created variable collection.
;				Else is 1 if the operations have succeded and 0 if error messages are reported.
;
; KEYWORDS:
;
;		DIMENSIONS:	used to create a new variable collection: n-element vector specifying the array
;				size on each dimension.
;
;		DELETE:	if set, deletes the array. In practice it will delete all the heap variables
;				created by the variable collection.
;				Its VERY IMPORTANT to always delete all the variables collections before to exit a program,
;				else the menory allocated for them will not be released.
;
;				NOTE: This is the ONLY way to delete the memory allocated for a variables collection;
;				this means that writing e.g. "array=0" only deletes the possibility to ever access that
;				variables collection, but does not free its used memory.
;
;
; MODIFICATION HISTORY:
;
;       Jun 2005 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;
;-


FUNCTION Variables_Collection, array, element, value, DIMENSIONS=DIMENSIONS, DELETE=DELETE


on_error, 2

;Canella le Heap variables referenziate da array.
;Se array non e' definito ritorna.
IF KEYWORD_SET(DELETE) THEN BEGIN
	n = n_elements(array)
	FOR i = 0, n-1 DO PTR_FREE, array[i]
	RETURN, 1
ENDIF

;Restituisce un pointer array vuoto:
IF KEYWORD_SET(DIMENSIONS) THEN BEGIN
	array = MAKE_ARRAY(DIMENSIONS, /PTR)
	RETURN, array
ENDIF ELSE IF n_elements(array) EQ 0 THEN Message, 'Array parameter is needed! Syntax: Result = Variable_Length_Array(array, element [, value])'

IF n_elements(element) NE 0 AND n_elements(value) NE 0 THEN BEGIN
	index = ARRAY_SCALAR_INDEX(array, element)
	array[index] = PTR_NEW(value)
	RETURN, 1
ENDIF


IF n_elements(element) NE 0 THEN BEGIN
	s = size(array)
	IF n_elements(element) EQ s[0] THEN BEGIN
		index = ARRAY_SCALAR_INDEX(array, element)
		RETURN, *array[index]
	ENDIF ELSE Message, 'Element must have same number of elements as array dimensions! Only one element at a time can be read.'
ENDIF

RETURN, 0

END
