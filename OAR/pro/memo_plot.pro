;+
;
; NAME:
;
;       MEMO_PLOT
;
;
; PURPOSE:
;
;		Allow to use OPLOT or PLOTS over 2D or 3D plots on many windows at the same time.
;
; DESCRIPTION:
;
;		If you make a plot on a window and a second plot on another window then you cannot
;		use OPLOT on the first graph unless the two have identical axis: with MEMO_PLOT you
;		can draw as many 2d or 3d plots or surfaces as you want and then recall them at any
;		time to make overplots.
;		Similarly, you must use MEMO_PLOT when you want to read Cursor position in a previous
;		plot window.
;
;		For surfaces, it is like using /SAVE and, when overplotting,
;		the keyword /T3D must be set in any case.
;
;		MEMO_PLOT must be called soon after the first plotting command in a window and then
;		each time an overplotting is required after having plotted on other windows.
;
;
; CALLING SEQUENCE:
;
;		MEMO_PLOT, S, [/RECALL]
;
; INPUT and OUTPUT:
;
;       S:	Output structure type variable in which the current plot settings are saved.
;			If /RECALL is set, S is to be given as input.
;
;
; KEYWORDS:
;
;		RECALL:	If set then the plotting settings previously saved in variable S will be restored in
;				order to allow a subsequent call of OPLOT or PLOTS.
;				The involved window is also automatically set and showed.
;
; PROCEDURE:
;
;		Save in a structure the current graphics variables for future use.
;		When /RECALL is set, the structure's values are assigned back to the graphic keywords.
;
;
; EXAMPLE:
;
;		;Define an y(x) function to plot:
;
;		x = findgen(100)/99.
;		y = sin(x * 2 * !PI * 4)
;
;		;Plot the function in window 1 and store the plot settings in structure p1:
;
;		window, 1
;		plot, x, y
;
;		MEMO_PLOT, p1
;
;		;Make a new plot in window 2 and store the plot settings in structure p2:
;
;		window, 2
;		plot_oo, x+1, y+1, /xstyle, /ystyle
;
;		MEMO_PLOT, p2
;
;
;		;Recall plot settings p1 and overplot on window 1:
;
;		wset, 1
;		MEMO_PLOT, p1, /RECALL
;		oplot, x, y, color=255, psym=1
;
;		;Recall plot settings p2 and overplot on window 2:
;
;		wset, 2
;		MEMO_PLOT, p2, /RECALL
;		oplot, x+1, y+1, color=255, psym=2
;
;
; IDL VERSION AT WRITING TIME:
;
;	IDL v. 6.0 Win32
;
;
; MODIFICATION HISTORY:
;
;       October 2004, G. Li Causi, Rome Astronomical Observatory
;						licausi@mporzio.astro.it
;						http://www.mporzio.astro.it/~licausi/
;
;-

PRO MEMO_PLOT, value, RECALL=RECALL

;Salva le variabili grafiche per poter fare oplot su un grafico vecchio

IF KEYWORD_SET(RECALL) THEN BEGIN

	wset, value.W
	wshow, value.W

	!ORDER = value.ORDER
	!P = value.P
	!X = value.X
	!Y = value.Y
	!Z = value.Z

ENDIF ELSE BEGIN

	value = CREATE_STRUCT('ORDER', !ORDER, 'W', !D.window, 'P', !P, 'X', !X, 'Y', !Y, 'Z', !Z)

ENDELSE


END
