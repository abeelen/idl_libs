;+
;
; NAME:
;       PIX_SAMPLED_POLAR_SURFACE
;
;
; PURPOSE:
;   	Computes the pixel sampled z-axis revolution of a profile z(r) given on a regular or irregular grid.
;
;
; DESCRIPTION:
;		This function does conceptually the same as the IDL function POLAR_SURFACE, i.e. it builds up
;		a square array with the cartesian z=f(x,y) representation of a polar function described
;		by z=f(r) where the r vector can also be non uniform.
;
;		The difference is that POLAR_SURFACE just spline interpolates the radial profile data onto a 2D
;		cartesian grid and integrate over pixel area, while PIX_SAMPLED_POLAR_SURFACE gives the pixel
;		sampled image of such a	polar surface.
;		This makes difference when r contains peak features narrower than the choosed
;		sample interval (e.g. r is logarithmically spaced and is the intensity profile of a bright
;		central star with an extended faint envelope).
;		In this case the PIX_SAMPLED_POLAR_SURFACE will conserve energy in each pixel whatever the
;		choosed sampling resolution.
;
;		Very useful when Fourier transforms are required for irregularly spacing of r: the transform of
;		the 2-D surface will depend on the sampling unless pixel sampling is used.
;
;
; CALLING SEQUENCE:
;
;		Result = PIX_SAMPLED_POLAR_SURFACE(r, z, SAMPLE=SAMPLE, XY_AXIS=XY_AXIS, $
;									NUM_THETA_SAMPLES=NUM_THETA_SAMPLES, _EXTRA=EXTRA)
;
; INPUTS:
;       r:		1d array of the radial coordinates of the profile; may be irregularly spaced.
;				If r (and z) are not defined at the origin (r=0), the z(0) point is linearly extrapolated
;				in order to compute the final central pixel value.
;
;		z:		1d array of the profile of the same number of elements as r.
;
;
; KEYWORDS:
;
;		SAMPLE:		Sampling interval (i.e. pixel size).
;					If the whole range of the radius 'r' cannot be sampled with an integer number of
;					pixels, the larger integer multiple	of the 'sample' value will be taken as max(r).
;
;		XY_AXIS: 	Output the X axis vector (which is the same of Y axis vector) for the computed 2D surface.
;
;		NUM_THETA_SAMPLES: Number of radial sectors for the computation: minimum 36 sectors.
;
;		_EXTRA:		The "MISSING" and "QUINTIC" keywords of POLAR_SURFACE can be
;					used in calling this function.
;
;
; OUTPUTS:
;
;       Result:	The function returns a 2-D array with the polar surface.
;				Result will be a square array with ODD NUMBER of elements, whose CENTRAL ELEMENT correspond
;				to the axis origin.
;
;
; PROCEDURE:
;
;		Computes the integral of the surface on each pixel by means of a Delaunay triangulation of
;		the profile points z(r) along the x-axis and on the lines connecting the origin with the pixel corners,
;		as shown below:
;
;		- Pixel at origin: computes the integral over triangle ABC from data points z(r) on lines AB and AC,
;		then multiplies by 8 and divides by pixel area:
;
;					+-----+C
;					|    /|
;					| A*--|B
;					| orig|
;					+-----+
;
;		- Pixel not at origin: computes the integral over rectangle ABCD from data points z(r) on lines OB and OC
;		and point D, then doubles it and divides by pixel area:
;
;					      D     C
;					+-----+-----+
;					|     |     |
;					|  *  A-----B
;					| orig|     |
;					+-----+-----+
;
;
;		The pixel sampled profile along the x-axis is then put into the IDL function POLAR_SURFACE in order to
;		get final the 2D array.
;
;
; CALLS:
;
;		Needs SPLINE_DOUBLE, which is the same as SPLINE of IDL but with double precision.
;
;
; MODIFICATION HISTORY:
;
;       September 2004, G. Li Causi, Rome Astronomical Observatory
;						licausi@mporzio.astro.it
;						http://www.mporzio.astro.it/~licausi/
;       April 2005, G. Li Causi
;						- fix some typos in the header
;						- fix errors with sample greater than max of r
;       June 2005, G. Li Causi
;						- substitute SPLINE_DOUBLE to INTERPOL for a better behaviour.
;
;
; PLANNED IMPROVEMENTS:
;		>>>>rivedere per due o tre pixel perche' da errore triangulate <<<-----
;
;-


FUNCTION Pix_Sampled_Polar_Surface, r, z, sample=sample, xy_axis=xy_axis, $
						NUM_THETA_SAMPLES=NUM_THETA_SAMPLES, _EXTRA=EXTRA

ON_ERROR, 2


;**********
;Parameters
;**********
minimum_theta_samples = 36


;***********
;Input Check
;***********
IF SIZE(r, /N_DIMENSIONS) NE 1 THEN MESSAGE, 'RADIUS r must be a 1-D array!'
IF SIZE(z, /N_DIMENSIONS) NE 1 THEN MESSAGE, 'PROFILE z must be a 1-D array!'
IF min(r) LT 0 THEN MESSAGE, 'The RADIUS coordinates r must be positive!'
IF n_elements(z) NE n_elements(r) THEN MESSAGE, 'PROFILE z mut have same number of elements as RADIUS r!'

IF NOT KEYWORD_SET(NUM_THETA_SAMPLES) THEN num_theta_samples = minimum_theta_samples
num_theta_samples = ABS(num_theta_samples)
IF num_theta_samples LT 36 THEN num_theta_samples = minimum_theta_samples


IF NOT KEYWORD_SET(EXTRA) THEN EXTRA = CREATE_STRUCT('EXTRA', 'EXTRA')

ind = where(tag_names(EXTRA) EQ 'MISSING', count)
IF count EQ 0 THEN EXTRA = CREATE_STRUCT(EXTRA, 'MISSING', 0)

ind = where(tag_names(EXTRA) EQ 'QUINTIC', count)
IF count EQ 0 THEN EXTRA = CREATE_STRUCT(EXTRA, 'QUINTIC', 0)



;************************************
;Add the origin if it is not defined:
;************************************
IF min(r) NE 0 THEN BEGIN
	z0 = INTERPOL(z, r, 0.)		;linear extrapolation
	r = [0., r]
	z = [z0, z]
ENDIF


;**********************************************************
;Ricampionamento ed integrazione del profilo z(r) sui pixel
;**********************************************************

IF NOT KEYWORD_SET(sample) THEN BEGIN
	dr = r - shift(r, 1)
	dr = dr[1:*]
	sample = min(abs(dr))
ENDIF ELSE sample = ABS(sample)

num_r_samples = long(max(r)/sample)				;number of pixels
IF num_r_samples LT 1 THEN BEGIN
	data_z = total(z)/(max(r)-min(r)) * 2 * !PI * sample/2.		;tutto il flusso in un singolo pixel  <<<<<--rivedere per due o tre pixel perche' da errore triangulate
	xy_axis = 0
	RETURN, data_z
ENDIF

ro = (dindgen(num_r_samples) + .5d) * sample  	;r of pixels' right borders
z_res_interp = SPLINE_DOUBLE(r, z, ro, 1)				;spline interpolation at pixel right borders

ro_corner = SQRT((sample / 2d)^2 + ro^2)					;r at pixel corners
z_corner_interp = SPLINE_DOUBLE(r, z, ro_corner, 1)	;spline interpolation at pixel corners


z_res = dblarr(num_r_samples)
FOR jj = 0L, num_r_samples-1 DO BEGIN

	IF jj EQ 0 THEN BEGIN			;Pixel at origin

		aindex = 1
		bindex = max(where(r LT ro[0], count))
		xa = 0
		ya = 0
		za = z[0]
		xb = ro[0]
		yb = 0
		zb = z_res_interp[0]
		xc = xb
		yc = sample / 2d
		zc = z_corner_interp[0]
		IF bindex LT aindex THEN BEGIN
			xpoints = [xa, xb, xc]
			ypoints = [ya, yb, yc]
			zpoints = [za, zb, zc]
		ENDIF ELSE BEGIN
			xpoints = [xa, xb, xc, r[aindex:bindex]]
			ypoints = [ya, yb, yc, r[aindex:bindex]*0]
			zpoints = [za, zb, zc, z[aindex:bindex]]
		ENDELSE

		ro_c = xb * SQRT(2d)			;bindex_inters e'lo stesso indice di bindex ma sul raggio che va fino al punto C
		bindex_inters = max(where(r LT ro_c, count))
		IF count EQ 0 THEN bindex_inters = 0
		IF bindex_inters GE aindex THEN BEGIN
			xpoints = [xpoints, r[aindex:bindex_inters]/SQRT(2d)]
			ypoints = [ypoints, r[aindex:bindex_inters]/SQRT(2d)]
			zpoints = [zpoints, z[aindex:bindex_inters]]
		ENDIF

		TRIANGULATE, xpoints, ypoints, Triangles				;calcola i triangoli
		st = size(Triangles)
		IF st[0] EQ 1 THEN nt = 1 ELSE nt = st[2]				;numero di triangoli
		sum = 0.
		FOR t = 0L, nt-1 DO BEGIN
			xx = xpoints[Triangles[*, t]]						;coordinate dei vertici
			yy = ypoints[Triangles[*, t]]
			zz = zpoints[Triangles[*, t]]
			zm = mean(zz)
			vol = ABS( (xx[2] - xx[0])*(yy[1] - yy[0]) - (xx[1] - xx[0])*(yy[2] - yy[0]) ) / 2d * zm
			sum = sum + vol
		ENDFOR
		z_res[jj] = sum * 8. / sample^2


	ENDIF ELSE BEGIN				;Pixel not at origin

		aindex = min(where(r GT ro[jj-1], count))
		IF count EQ 0 THEN aindex = 0
		bindex = max(where(r LT ro[jj], count))
		IF count EQ 0 THEN bindex = 0
		xa = ro[jj-1]
		ya = 0
		za = z_res_interp[jj-1]
		xb = ro[jj]
		yb = 0
		zb = z_res_interp[jj]
		xc = xb
		yc = sample / 2d
		zc = z_corner_interp[jj]
		xd = xa
		yd = sample / 2d
		zd = z_corner_interp[jj-1]
		theta = ATAN(yc, xc)
		IF bindex LT aindex THEN BEGIN
			xpoints = [xa, xb, xc, xd]
			ypoints = [ya, yb, yc, yd]
			zpoints = [za, zb, zc, zd]
		ENDIF ELSE BEGIN
			xpoints = [xa, xb, xc, xd, r[aindex:bindex]]
			ypoints = [ya, yb, yc, yd, r[aindex:bindex]*0]
			zpoints = [za, zb, zc, zd, z[aindex:bindex]]
		ENDELSE

		ro_inters = xa / cos(theta)
		aindex_inters = min(where(r GT ro_inters, count))		;aindex_inters e bindex_inters sono gli stessi indici di aindex e bindex ma sul raggio che va fino al punto C
		IF count EQ 0 THEN aindex_inters = 0
		ro_c = xb / cos(theta)
		bindex_inters = max(where(r LT ro_c, count))
		IF count EQ 0 THEN bindex_inters = 0
		IF bindex_inters GE aindex_inters THEN BEGIN
			xpoints = [xpoints, r[aindex_inters:bindex_inters]*cos(theta)]
			ypoints = [ypoints, r[aindex_inters:bindex_inters]*sin(theta)]
			zpoints = [zpoints, z[aindex_inters:bindex_inters]]
		ENDIF

		TRIANGULATE, xpoints, ypoints, Triangles			;do the triangulation
		st = size(Triangles)
		IF st[0] EQ 1 THEN nt = 1 ELSE nt = st[2]			;number of triangles
		sum = 0.
		FOR t = 0L, nt-1 DO BEGIN
			xx = xpoints[Triangles[*, t]]					;vertex coordinates
			yy = ypoints[Triangles[*, t]]
			zz = zpoints[Triangles[*, t]]
			zm = mean(zz)
			vol = ABS( (xx[2] - xx[0])*(yy[1] - yy[0]) - (xx[1] - xx[0])*(yy[2] - yy[0]) ) / 2d * zm
			sum = sum + vol
		ENDFOR
		z_res[jj] = sum * 2. / sample^2

	ENDELSE

ENDFOR
ro = ro - min(ro)			;return sampled coordinates from origin.


;****************
;Create 2-D grid:
;****************
theta = dindgen(num_theta_samples)/num_theta_samples * 2.d * !DPI
data_polar_z = z_res # replicate(1d, num_theta_samples)									;Polar surface

data_z = POLAR_SURFACE(data_polar_z, indgen(num_r_samples), theta, /GRID, SPACING=[1,1], $
			QUINTIC=EXTRA.QUINTIC, MISSING=EXTRA.MISSING)								;Cartesian surface

s = size(data_z)
xy_axis = (findgen(s[1])/(s[1]-1) * 2 - 1) * max(ro)		;from -max(r) to max(r)



RETURN, data_z

END

