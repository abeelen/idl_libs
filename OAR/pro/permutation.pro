;+
; NAME:
;       PERMUTATION
;
; PURPOSE:
;		Computes all the permutation whithout repetition of the elements in the input array.
;
; CATEGORY:
;       Combinatorial
;
; CALLING SEQUENCE:
;
;		Result = PERMUTATION(Array)
;
; INPUTS:
;       Array: is a 1-dimensional array of any type whose elements will be permutated.
;
; OUTPUTS:
;       Result: is a 2-D array each row of which is a different permutation of the input array.
;		Its dimensions is n_elements(Array) x Factorial(n_elements(Array)).
;
; PROCEDURE:
;		The function is called recursively untill all the permutations are computed.
;
; CALLS:
;		The FACTORIAL routine of IDL is called.
;
; NOTE:	Attention: when the number of elements in input array is greater than 7 or 8 the program will
;		take long time to finish and the total number of permutation is very huge.
;
; MODIFICATION HISTORY:
;          G. Li Causi, Rome Astronomical Observatory: 15-11-2000
;-


FUNCTION PERMUTATION, oggetti

so = size(oggetti)
type = so(2)
no = n_elements(oggetti)
liberi = oggetti
perm_loc = make_array(no, factorial(no), type = type)	;matrice che contiene le permutazioni

p_id_loc = 0
nlib_c = n_elements(liberi)
FOR c = 0, nlib_c-1 DO BEGIN
	perm_loc(no - nlib_c, p_id_loc:(p_id_loc+factorial(nlib_c-1)-1)) = liberi(c)
	liberi_prima_c = liberi
	index = where(indgen(nlib_c) ne c, count)
	IF count NE 0 THEN BEGIN
		liberi = liberi(index)
		sotto = PERMUTATION(liberi)						;RECURSION
		perm_loc((no-nlib_c+1):(no-1), p_id_loc:(p_id_loc+factorial(nlib_c-1)-1)) = sotto
		p_id_loc = p_id_loc+factorial(nlib_c-1)
	ENDIF
	liberi = liberi_prima_c
ENDFOR

RETURN, perm_loc

END
