;+
; NAME:
;
;       WRITE_PARAMETERS
;
; PURPOSE:
;
;		Updates a text file with a tagged list of parameters in the form:
;
;			# Comment
;			TAG1 = value
;			TAG2 = value
;			...
;
;		with the values of a structure which have the same tag names.
;
;
; CATEGORY:
;
;       Input/Output.
;
; CALLING SEQUENCE:
;
;		WRITE_PARAMETERS, filename, structure [, /NO_FIRST]
;
; INPUTS:
;
;		Filename: name of the text file to update: the file must already exist.
;
;		Structure: structure with the tag names identical to the tags in the parameter file.
;				The routine is not case sensitive.
;
;
; OUTPUTS:
;
;       The parameter file is updated.
;
;
; PROCEDURE:
;
;		The file is searched for any tag in the input structure.
;		If the tag is not found its value remains unchanged.
;		The file can contain more tags than the structure: in this case they are ignored.
;		Once found a tag, the value of the corresponding tag in Structure is written in
;		the file after the '=' sign; array elements are separated by commas.
;		Any byte type tag in the input structure is interpreted as boolean type:
;		0 -> FALSE, other values -> TRUE, and correspondingly in the file are written
;		the strings "NO" or "YES".
;
;
; See Also:
;
;		READ_PARAMETERS
;
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;       May 2004 - 	Gianluca Li Causi - Fix a bug with tag names that contains each other.
;
;-

PRO write_parameters, filename, structure, no_first=no_first

tags = tag_names(structure)
tags = strlowcase(tags)
ntags = n_elements(tags)

;close, /all

openr, 1, filename

;Reads all the file into a string array:
rows = [' ']
line = ''
WHILE NOT EOF(1) DO BEGIN
   	READF, 1, line
   	line = strtrim(line, 2)		;elimina eventuali spazi iniziali o finali
   	rows = [rows, line]		;aggiunge la riga all'array
ENDWHILE

rows = rows[1:n_elements(rows)-1]	;elimina il primo spazio vuoto
nrows = n_elements(rows)        ;nmero di righe nel file

close, 1


new_rows = rows

line = ""
i = 0
WHILE i LT ntags DO BEGIN			;Ciclo sui TAG

	r = 0
	WHILE r LE (nrows-1) DO BEGIN		;Ciclo sulle righe

		line = rows[r]

		pos_equal = strpos(line, "=")

		IF pos_equal GE 0 THEN BEGIN		;Se trova il segno "="

			tag_side = strmid(line, 0, pos_equal)				;parte prima dell'uguale
			pos = strpos(strlowcase(tag_side), tags[i])			;posizione del tag cercato
			;tag_string = strtrim(strlowcase(tag_side), 2)		;elimina gli spazi iniziali e finali
			;IF tag_string EQ tags[i] AND ((n_elements(NO_FIRST) EQ 0 AND pos EQ 0 AND strlen(line) GE 3) OR (Keyword_Set(NO_FIRST) AND pos GE 0)) THEN BEGIN	;il tag deve essere la PRIMA SCRITTA sulla riga

			IF ((n_elements(NO_FIRST) EQ 0 AND pos EQ 0 AND strlen(line) GE 3) OR (Keyword_Set(NO_FIRST) AND pos GE 0)) THEN BEGIN	;il tag deve essere la PRIMA SCRITTA sulla riga
				old_value_string = strmid(line, pos_equal + 1)
				old_len = strlen(old_value_string)

				value = structure.(i)
				sz = size(value)
				tipo = sz[sz[0] + 1]
				elementi = n_elements(value)		;numero elementi se e' un vettore
				value_string = ''
				FOR e = 0, elementi-1 DO BEGIN
					v_string = string(value[e])
					v_string = strtrim(v_string, 2)	;elimina eventuali spazi iniziali o finali
					IF tipo EQ 7 THEN v_string = '"' + v_string + '"'	;tipo stringa: aggiunge le virgolette per iraf
					IF tipo EQ 1 THEN v_string = (['no', 'yes'])[value NE 0]		;tipo byte: sostituisce con booleano per iraf
					IF e GT 0 THEN value_string = value_string + ','
					value_string = value_string + v_string
				ENDFOR
				newline = strmid(line, 0, pos_equal + 1) + value_string

				new_rows[r] = newline

				;Legge il resto del file
				;reminder = ''
				;WHILE NOT EOF(1) DO BEGIN
			    ;	READF, 1, line
			    ;	reminder = reminder + line
				;ENDWHILE

			ENDIF

		ENDIF

		r = r +1

	ENDWHILE
	i = i + 1			;next tag

ENDWHILE



;Sovrascrive il nuovo file
openw, 1, filename
FOR r = 0, nrows-1 DO printf, 1, new_rows[r]
close, 1


END
