;
;+
; NAME:
;
;       SX_RIORDINA
;
; PURPOSE:
;
;		Sort the FITS header so that the last keywords are HISTORY and COMMENTS
;
; CATEGORY:
;
;       FITS functions.
;
; CALLING SEQUENCE:
;
;		Result = SX_RIORDINA(header_string)
;
;
; INPUTS:
;
;		Header_string: string array in which each line is a row of a FITS header.
;
; OUTPUTS:
;
;       Returns the input string array with the keywords re-ordered.
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;-

FUNCTION SX_RIORDINA, stringa

header_string = stringa

comments = SXPAR(header_string, 'COMMENT', count=comments_count)
histories = SXPAR(header_string, 'HISTORY', count=histories_count)

SXDELPAR, header_string, 'COMMENT'
SXDELPAR, header_string, 'HISTORY'
SXDELPAR, header_string, ' '


IF histories_count GT 0 THEN $
	FOR l=0, n_elements(histories)-1 DO SXADDPAR, header_string, 'HISTORY', histories[l]

IF comments_count GT 0 THEN $
	FOR l=0, n_elements(comments)-1 DO SXADDPAR, header_string, 'COMMENT', comments[l]

RETURN, header_string

END
