;+
; NAME:
;       OPLOTERR_XY
;
; PURPOSE:
;		Overplot both X and Y error bars or draw an error eclipse over an existing plot.
;
; DESCRIPTION:
;
;		Error bars are drawn in the standard "I" shape. Linestyle is selectable and
;		clipping of error bars or ellipses can be choosed.
;
; CATEGORY:
;
;		Plotting
;
; CALLING SEQUENCE:
;
;		OPLOTERR_XY, x,y, errx,erry, color=color, thick=thick, linestyle=linestyle, $
;				ellipse=ellipse, noclip=noclip
;
; INPUTS:
;       X, Y:  X and Y vectors for the plot
;
;       ERRX, ERRY:  X and Y error vectors for the error boxes.
;
; KEYWORD PARAMETERS:
;
;		COLOR:	The color to plot the errors
;
;		THICK:	The line thickness to plot the errors
;
;		LINESTYLE:	The line style to plot the errors
;
;		ELLIPSE:	If set, the errors are plotted as ellipses.
;
;		NOCLIP:	If set, the parts of the errors which lay outside the plot region are not clipped.
;
; CALLS:
;		Calls the function TV_ELLIPSE.
;
; MODIFICATION HISTORY:
;       February 2004, G. Li Causi, Rome Astronomical Observatory
;       March 2005, G. Li Causi - corrections in this header; make the "I" shape.
;-

PRO oploterr_xy, x,y, errx,erry, color=color, thick=thick, linestyle=linestyle, ellipse=ellipse, noclip=noclip

npoints = n_elements(x)

IF n_elements(errx) LT npoints THEN $
	errx2 = replicate(errx, npoints) $
ELSE errx2 = errx

IF n_elements(erry) LT npoints THEN $
	erry2 = replicate(erry, npoints) $
ELSE erry2 = erry


dd = ((!d.x_ch_size + !d.y_ch_size) / 2) / 2

xyd = CONVERT_COORD( x, y, /DATA, /TO_DEVICE)
xd = xyd[0,*]
yd = xyd[1,*]
xmd = (CONVERT_COORD( xd-dd, yd, /DEVICE, /TO_DATA))[0,*]
xpd = (CONVERT_COORD( xd+dd, yd, /DEVICE, /TO_DATA))[0,*]
ymd = (CONVERT_COORD( xd, yd-dd, /DEVICE, /TO_DATA))[1,*]
ypd = (CONVERT_COORD( xd, yd+dd, /DEVICE, /TO_DATA))[1,*]

FOR i = 0, npoints-1 DO BEGIN
	IF KEYWORD_SET(ellipse) THEN BEGIN
		;Ellipse shape:
		tv_ellipse, errx2[i], erry2[i], x[i], y[i], color=color, /data, $
				thick=thick, linestyle=linestyle, noclip=noclip
	ENDIF ELSE BEGIN
		;Error bars:
		oplot, [x[i]-errx2[i], x[i]+errx2[i]], [y[i], y[i]], color=color, thick=thick, linestyle=linestyle
		oplot, [x[i], x[i]], [y[i]-erry2[i], y[i]+erry2[i]], color=color, thick=thick, linestyle=linestyle
		;"I" shapes:
		oplot, [x[i]-errx2[i], x[i]-errx2[i]], [ymd[i], ypd[i]], color=color, thick=thick, linestyle=linestyle
		oplot, [x[i]+errx2[i], x[i]+errx2[i]], [ymd[i], ypd[i]], color=color, thick=thick, linestyle=linestyle
		oplot, [xmd[i], xpd[i]], [y[i]-erry2[i], y[i]-erry2[i]], color=color, thick=thick, linestyle=linestyle
		oplot, [xmd[i], xpd[i]], [y[i]+erry2[i], y[i]+erry2[i]], color=color, thick=thick, linestyle=linestyle
	ENDELSE
ENDFOR

END
