;
;+
; NAME:
;
;       WINDOWS_CLOSE_ALL
;
; PURPOSE:
;
;		Deletes all the curretly opened windows, but the ones written in the SKIP keyword.
;
;
; CATEGORY:
;
;       Graphics
;
; CALLING SEQUENCE:
;
;		WINDOWS_CLOSE_ALL [, SKIP]
;
; KEYWORDS:
;
;		SKIP: 1-D long type array with the window numbers of the windows not to close.
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;
;-

PRO Windows_Close_All, skip=skip

DEVICE, window_state = ws
index = where(ws EQ 1, count)

IF n_elements(skip) EQ 0 THEN skip = .5

FOR i = 0, n_elements(index)-1 DO BEGIN
	no = WHERE(index[i] EQ skip, count)
	IF count EQ 0 THEN WDELETE, index[i]
ENDFOR


END
