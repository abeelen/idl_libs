;
;+
; NAME:
;
;       STAT
;
; PURPOSE:
;
;		Computes an extended statistics of an array.
;
; DESCRIPTION:
;
;		The function returns a structure with minimum, maximum, mean, median,
;		mode (i.e. peak of the gaussian fit around the median), standard deviation,
;		variance, sigma of the gaussian fit around the median, variance of the
;		gaussian fit aroud the median.
;
; CATEGORY:
;
;       Statistics.
;
; CALLING SEQUENCE:
;
;		Result = STAT(Data, [GFIT=GFIT], [BS=BS], [/SILENT], [/ADU], [/HELP])
;
;
; INPUTS:
;
;		Data: Array for which statistics are desired.
;
;
; KEYWORD PARAMETERS:
;
;		SILENT:	If set, the routine does not print the result on the log window.
;
;		GFIT: Output variable which contains the gaussian fit of the image histogram:
;			2-D array of the form: gfit[0,*]=bin, gfit[1,*]=fit.
;			When GFIT is set, also BS must be set.
;
;		BS: Binseze for the histogram.
;
;		ADU: If set tells to the routine that the Data is a quantized to integers.
;

; OUTPUTS:
;
;       The function returns the structure Result of the following form:
;
;			Result.min:			minimum
;			Result.max:			maximum
;			Result.mean:		mean
;			Result.med:			median
;			Result.mode:		mode (i.e. peak of the gaussian fit around the median)
;			Result.sig:			standard deviation
;			Result.var:			variance
;			Result.gauss_sig:	sigma of the gaussian fit around the median
;			Result.gauss_var:	variance of the gaussian fit aroud the median
;
;
; PROCEDURE:
;
;
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;
;-

FUNCTION stat, data, silent=silent, gfit=gfit, bs=binsize, adu=adu, help=help


IF KEYWORD_SET(help) THEN BEGIN
	DOC_LIBRARY, 'stat'
	RETURN, -1
ENDIF

IF n_elements(data) EQ 0 THEN Message, '! INPUT ARRAY IS UNDEFINED !'

ok_ind = where(finite(data) NE 0, count)
IF count EQ 0 THEN Message, '! INPUT ARRAY IS NOT FINITE !'

array = data[ok_ind]

stat = {min: 0., max: 0., mean: 0., med: 0., $
		mode: 0., sig: 0., var: 0., gauss_sig: 0., gauss_var: 0.}


IF max(array) EQ min(array) THEN BEGIN
	value = min(array)
	stat.min = value
	stat.max = value
	stat.mean = value
	stat.med = value
	stat.sig = 0.
	stat.var = 0.
	stat.gauss_sig = 0.
	stat.gauss_var = 0.
	RETURN, stat
ENDIF


;Statistica:
stat.min = min(array)
stat.max = max(array)
stat.mean = mean(array)
stat.med = median(array)
stat.sig = stdev(array)
stat.var = stat.sig^2

nbin = 100
Nsigma=3		;clipping per il fit gaussiano

IF n_elements(adu) EQ 0 THEN adu = 0

;Calcola la mediana e la stdev approssimata dell'immagine
IF n_elements(array) GT 20 THEN BEGIN
	sky, array, back, sig_back
	IF sig_back LT 0 THEN BEGIN
		back = median(array)
		sig_back = stdev(array)
	ENDIF
ENDIF ELSE BEGIN
	back = median(array)
	sig_back = stdev(array)
ENDELSE


exception = 0
IF NOT FINITE(sig_back) THEN BEGIN			;se la sigma approssimata non e' misurabile
	nn = n_elements(array)
	nb = 10000
	xmin = stat.min
	xmax = stat.max
	binsize = ((xmax - xmin)/float(nb)) > adu
	nbin = round(float(xmax - xmin)/binsize)
	minh = xmin
	maxh = minh + float(nb*binsize)
	xhist_parziale0 = indgen(nb+1)*binsize + minh
	istogramma_parziale0 = histogram(float(array), max=maxh, min=minh, bin=binsize)
	xhist_parziale0 = xhist_parziale0[0:n_elements(istogramma_parziale0)-1]

	;Supporto dell'istogramma:
	index = where(istogramma_parziale0 GT 0, count)

	IF count GE 3 THEN BEGIN		;se non ci sono almeno tre punti non fa il fit

		;Clean extreme outlier:
		REJECT_1D, xhist_parziale0, istogramma_parziale0, xhist_parziale0, istogramma_parziale0, 7, 7, 1, /MEDIAN

		;Fit gaussiano dell'istogramma:
		fit0 = gaussfit(xhist_parziale0, istogramma_parziale0, coeff, nterms=3)

		IF min(FINITE(coeff)) EQ 0 THEN BEGIN		;se tutti i coefficienti sono infiniti
			fit1 = gaussfit(findgen(n_elements(istogramma_parziale0)), istogramma_parziale0, coeff, nterms=3)
		ENDIF ELSE BEGIN
			fit1 = fit0
		ENDELSE
	ENDIF ELSE fit1 = 1./0.

	IF NOT FINITE(fit1[0]) THEN BEGIN		;se fit1 non e' finito prende il max e la fwhm approssimata per calcolare i limiti per l'istogramma parziale
		mh = max(istogramma_parziale0, centro)
		fwhm = binsize * n_elements(where(istogramma_parziale0 GT (mh*.5)))
		xmin = centro - Nsigma * fwhm / (2*sqrt(2*alog(2)))
		xmax = centro + Nsigma * fwhm / (2*sqrt(2*alog(2)))
		exception = 1
	ENDIF ELSE BEGIN						;se fit1 e' finito usa i coefficienti gaussiani
		mode = coeff[1]
		gauss_sig = coeff[2]
		xmin = mode - Nsigma * gauss_sig
		xmax = mode + Nsigma * gauss_sig
	ENDELSE

ENDIF ELSE BEGIN					;se la sigma approssimata e' misurabile

	;Calcola i limiti low e high per l'istogramma parziale:
	IF NOT FINITE(stat.sig) THEN BEGIN		;se la stdev di tutto array e' infinita
		back_sig_fac = 10			;volte la sigma del fondo
		xmin = stat.med - Nsigma * back_sig_fac * sig_back
		xmax = stat.med + Nsigma * back_sig_fac * sig_back
	ENDIF ELSE BEGIN						;se la stdev e' finita
		xmin = stat.med - Nsigma * stat.sig
		xmax = stat.med + Nsigma * stat.sig
		;seconda iterazione per sigma clipping:
		sig_parziale = stdev(array[where(array LE xmax AND array GE xmin)])
		xmin = stat.med - 3 * sig_parziale
		xmax = stat.med + 3 * sig_parziale
		;terza iterazione per sigma clipping:
		sig_parziale = stdev(array[where(array LE xmax AND array GE xmin)])
		xmin = stat.med - 3 * sig_parziale
		xmax = stat.med + 3 * sig_parziale
		;quarta iterazione per sigma clipping:
		sig_parziale = stdev(array[where(array LE xmax AND array GE xmin)])
		xmin = stat.med - 3 * sig_parziale
		xmax = stat.med + 3 * sig_parziale
	ENDELSE

ENDELSE

IF xmax EQ xmin THEN BEGIN
 xmax = xmax + 1
 xmin = xmin - 1
ENDIF

binsize = ((xmax - xmin)/float(nbin)) > adu
nbin = round(float(xmax - xmin)/binsize)
minh = xmin
maxh = minh + float(nbin*binsize)
xhist_parziale = indgen(nbin+1)*binsize + minh
istogramma_parziale = histogram(float(array), max=maxh, min=minh, bin=binsize)
xhist_parziale = xhist_parziale[0:n_elements(istogramma_parziale)-1]


IF exception EQ 1 THEN BEGIN
	fit = gaussfit(findgen(n_elements(istogramma_parziale)), istogramma_parziale, coeff, nterms=3)
	stat.mode = coeff[1]
	stat.gauss_sig = coeff[2]
	stat.gauss_var = stat.gauss_sig^2
	gfit = -1
ENDIF ELSE BEGIN
	fit = gaussfit(xhist_parziale, istogramma_parziale, coeff, nterms=3)
	stat.mode = coeff[1]
	stat.gauss_sig = coeff[2]
	stat.gauss_var = stat.gauss_sig^2
	gfit=fltarr(2, n_elements(istogramma_parziale))
	gfit[0,*] = xhist_parziale
	gfit[1,*] = fit
ENDELSE


IF NOT KEYWORD_SET(silent) THEN BEGIN
	titles = '       min               max             mean            med           mode           sig            var           gauss_sigma  gauss_variance'
	print
	print, titles
	print, stat
	print
ENDIF


RETURN, stat

END
