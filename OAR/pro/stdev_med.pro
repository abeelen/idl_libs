;
;+
; NAME:
;
;       STDEV_MED
;
; PURPOSE:
;
;		Compute the Standard Deviation as the MEAN or the MEDIAN of the squared deviations
;		from the MEDIAN.
;
; CATEGORY:
;
;       Statistics.
;
; CALLING SEQUENCE:
;
;		Result = STDEV_MED(array [, /USE_MEDIAN] [, /DOUBLE])
;
;
; INPUTS:
;
;		Array: any numerical array
;
; KEYWORD PARAMETERS:
;
;		USE_MEDIAN: if set, compute the MEDIAN of the squared deviations
;				from the MEDIAN (default: computes the MEAN of them).
;
;		DOUBLE: converts the array to double precision.
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;-

FUNCTION stdev_med, array, double=double, use_median=use_median

IF KEYWORD_SET(DOUBLE) THEN new_array = double(array)

IF KEYWORD_SET(USE_MEDIAN) THEN $
	RETURN, SQRT(MEDIAN( (array - MEDIAN(array))^2 )) $
ELSE $
	RETURN, SQRT(MEAN( (array - MEDIAN(array))^2 ))

END

