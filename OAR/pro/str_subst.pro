;
;+
; NAME:
;
;       STR_SUBST
;
; PURPOSE:
;
;		Replace all the occurrences of the string IN with the string OUT within
;		the given string.
;
;
; CATEGORY:
;
;       String Manipulation.
;
; CALLING SEQUENCE:
;
;		Result = STR_SUBST(string, in, out, START=START)
;
;
; INPUTS:
;
;		String: scalar or array string to search for occurrences of substring to replace.
;
;		In: scalar substring to search for.
;
;		Out: scalar substring that will replace the searched substring.
;
;
; KEYWORD PARAMETERS:
;
;		START:	Starting position within String to begin searching.
;
;
; OUTPUTS:
;
;       The function returns String with all occurrences of In after the START position replaced with Out.
;
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;
;-

FUNCTION str_subst, s, in, out, start = start


IF KEYWORD_SET(start) THEN inizio = start ELSE inizio = 0

ns = n_elements(s)
result = s
;IF ns EQ 1 THEN s = strarr(ns) + s

FOR i = 0, ns-1 DO BEGIN

	pos = strpos(result[i], in, inizio)
	IF pos LT 0 THEN GOTO, next

	REPEAT BEGIN
		s1 = strmid(result[i], inizio, pos)
		s2 = strmid(result[i], pos+strlen(in), strlen(result[i]))
		result[i] = s1 + out + s2
		pos = strpos(result[i], in, pos+strlen(out))
	ENDREP UNTIL pos EQ (-1)

next:
ENDFOR

RETURN, result

END
