
FUNCTION POLY_NEW,X,C

;+
; NAME:
;	POLY
;
; PURPOSE:
;	Evaluate a polynomial function of a variable.
;
;	MODIFIED "POLY.pro" FUNCTION.
;		If degree of polynomial = 0 then this new function
;		returns a costant vector of the same length of X instead
;		of a single float number!
;
; CATEGORY:
;		VAR-ARRAYS.
;
; CALLING SEQUENCE:
;	Result = POLY_NEW(X,C)
;
; INPUTS:
;	X:	The variable.  This value can be a scalar, vector or array.
;
;	C:	The vector of polynomial coefficients.  The degree of
;		of the polynomial is N_ELEMENTS(C) - 1.
;
; OUTPUTS:
;	POLY_NEW returns a result equal to:
;  --->	 (X * 0 + C[0]) + c[1] * X + c[2]*x^2 + ...   <-------- difference from POLY.pro
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; RESTRICTIONS:
;	None.
;
; PROCEDURE:
;	Straightforward.
;
; MODIFICATION HISTORY:
;	DMS, Written, January, 1983.
;	Massimo De Luca, July, 2003
;-


on_error,2		;Return to caller if an error occurs
N = N_ELEMENTS(C)-1	;Find degree of polynomial
Y = REPLICATE(c[n], N_ELEMENTS(X))
for i=n-1,0,-1 do y = y * x + c[i]
return,y
end

