;
;+
; NAME:
;
;       SELECT_RECTANGULAR_BOX
;
; PURPOSE:
;
;		Interactively draws with the mouse a selection rectangle on the given graphic window.
;
; CATEGORY:
;
;       Graphics.
;
; CALLING SEQUENCE:
;
;		Result = SELECT_RECTANGULAR_BOX([window], [scale=scale], [verbose=verbose], [cancel=cancel])
;
;
; INPUTS:
;
;		Window: window number; if not given, the current active window is used (if no window exists
;				an error message is given).
;
;
; OUTPUTS:
;
;       The function returns a four element vector with the coordinates of the
;		corner of the selected box: [x0, y0, x1, y1]
;
;
; KEYWORD PARAMETERS:
;
;		SCALE: Visualization scale. Useful when the image is not displayed 1:1, e.g. for a big
;			image of 2000x2000 pixel which is displayed in a 500x500 window, the scxale is 4.
;
;		CANCEL: Set on this keyword to hyde the selection box in the window after selection
;			has been completed
;
;
; CALLS:
;
;		DRAW_INVERSE_BOX is called to draw a box with colors complementary to the underlaying pixels.
;
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;		Sept 2005 - Massimo De Luca & Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					deluca@mporzio.astro.it
;					Added CANCEL keyword, plus correct a bug on the scale plus enhance speed.
;
; PLANNED IMPROVEMENTS:
;		- Constrain box to maximum width of window when mouse exit from right or top edges.
;
;-

FUNCTION Select_Rectangular_Box, window, verbose=verbose, scale=scale, cancel=cancel


IF n_elements(scale) EQ 0 THEN scale = 1.
IF scale LE 0 THEN scale = 1

IF n_elements(window) EQ 0 THEN window = !D.window
IF window GE 0 THEN wset, window ELSE Message, 'No active windows!'

image = tvrd()

s = size(image)
lx = s[1]
ly = s[2]

x0 = lx/2
y0 = ly/2

tvcrs, x0,y0, /device	;plot the cursor at the center

REPEAT BEGIN

	cursor, x,y, 2, /device					;read mouse position (no wait)

	IF KEYWORD_SET(VERBOSE) THEN BEGIN
		print
		print, '(x,y) = (' + strtrim(string(x*scale),2) + ',' + strtrim(string(y*scale),2) + ')'
	ENDIF

	xprec = x
	yprec = y

	x0 = x
	y0 = y

	started = 0

	WHILE !MOUSE.BUTTON EQ 1 DO BEGIN		;mentre e' premuto il left button

		started = 1

		cursor, x,y, 0, /device				;get coordinates (wait for change)

		IF x NE xprec OR y NE yprec THEN BEGIN

			Draw_Inverse_Box, x0, y0, xprec, yprec, window_content=image		;cancel previous box

			cursor, x,y, 0, /device						;get coordinates, nowait (mouse can have moved again)
			xprec = x
			yprec = y

			Draw_Inverse_Box, x0, y0, x, y, window_content=image				;draw new box

			IF KEYWORD_SET(VERBOSE) THEN BEGIN
				print
				print, '(X center,Y center) = (' + strtrim(string((x+x0)/2.*scale),2) + ',' + strtrim(string((y+y0)/2.*scale),2) + ')'
				print, '(X size, Y size) = (' + strtrim(string(abs(x-x0)*scale),2) + ',' + strtrim(string(abs(y-y0)*scale),2) + ')'
			ENDIF
		ENDIF

	ENDWHILE

ENDREP UNTIL started EQ 1 AND !MOUSE.BUTTON NE 1


IF KEYWORD_SET(CANCEL) THEN Draw_Inverse_Box, x0, y0, x, y, window_content=image

RETURN, [x0<x, y0<y, x0>x, y0>y] * scale

END
