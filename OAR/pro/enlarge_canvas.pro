;
;+
; NAME:
;
;       ENLARGE_CANVAS
;
; PURPOSE:
;
;		This function enlarges the size of a 2-D array and returns an array of (2*xgrow, 2*ygrow)
;		larger than the input array and having the input array at its center.
;		The added border are filled with 'value', but with /mirror_edge you can have
;		the edges mirrored out.
;
;		Very useful in conjunction with MEDIAN, SMOOTH, CONVOL, etc.
;
; CATEGORY:
;
;       Image processing.
;
; CALLING SEQUENCE:
;
;		Result = ENLARGE_CANVAS(image, xgrow, ygrow, value, mirror_edge=mirror_edge, to_size=to_size)
;
;
; INPUTS:
;
;		IMAGE: 2-D array of any type.
;
;		XGROW, YGROW: border width in X and Y direction, only if TO_SIZE keyword is not specified.
;
;		VALUE: value to fill the border, default = 0.
;
;
; KEYWORD PARAMETERS:
;
;		MIRROR_EDGE: if set the borders are filled with the mirrored edges of IMAGE.
;
;		TO_SIZE: final size of the enlarged image in the form [x-size, y-size].
;				If set, the xgrow and ygrow values will be computed by the procedure.
;
; OUTPUTS:
;
;       The function returns the original array enlarged of the desired border or to the desired size.
;
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;
;		Planned improvements:
;		- Extend functionality to n-Dim arrays
;-

FUNCTION Enlarge_Canvas, image, xgrow, ygrow, value, mirror_edge=mirror_edge, to_size=to_size

ON_ERROR, 2

s = size(image)
IF s[0] NE 2 THEN MESSAGE, 'Image must be 2-dimensional !'

IF xgrow EQ 0 AND ygrow EQ 0 THEN RETURN, image

IF n_elements(value) EQ 0 THEN value = 0

IF KEYWORD_SET(to_size) THEN BEGIN
	xgrow = (to_size[0] - s[1]) / 2.
	ygrow = (to_size[0] - s[1]) / 2.
	IF xgrow < ygrow LT 0 THEN Message, 'Final image size expressed in TO_SIZE keyword must be LARGER than original Image!'
ENDIF $
ELSE IF n_elements(xgrow) EQ 0 OR n_elements(ygrow) EQ 0 THEN Message, 'Missing XGROW o YGROW values !'

new_image = fltarr(s[1]+2*xgrow, s[2]+2*ygrow) + value

ns = size(new_image)

new_image[xgrow:xgrow+s[1]-1, ygrow:ygrow+s[2]-1] = image

IF KEYWORD_SET(MIRROR_EDGE) THEN BEGIN

	new_image[0:(xgrow-1)>0, ygrow:ygrow+s[2]-1] = REVERSE( image[0:(xgrow-1)>0, *] , 1)
	new_image[xgrow+s[1]-1:ns[1]-1, ygrow:ygrow+s[2]-1] = REVERSE( image[s[1]-1-xgrow:s[1]-1, *] , 1)

	new_image[xgrow:xgrow+s[1]-1, 0:(ygrow-1)>0] = REVERSE( image[*, 0:(ygrow-1)>0] , 2-(ygrow EQ 0))
	new_image[xgrow:xgrow+s[1]-1, ygrow+s[2]-1:ns[2]-1] = REVERSE( image[*, s[2]-1-ygrow:s[2]-1] , 2-(ygrow EQ 0))

	new_image[0:(xgrow-1)>0, 0:(ygrow-1)>0] = ROTATE( image[0:(xgrow-1)>0, 0:(ygrow-1)>0] , 2-(ygrow EQ 0 OR xgrow EQ 0))
	new_image[0:(xgrow-1)>0, ygrow+s[2]-1:ns[2]-1] = ROTATE( image[0:(xgrow-1)>0, s[2]-1-ygrow:s[2]-1] , 2-(xgrow EQ 0))
	new_image[xgrow+s[1]-1:ns[1]-1, 0:(ygrow-1)>0] = ROTATE( image[s[1]-1-xgrow:s[1]-1, 0:(ygrow-1)>0] , 2-(ygrow EQ 0))
	new_image[xgrow+s[1]-1:ns[1]-1, ygrow+s[2]-1:ns[2]-1] = ROTATE( image[s[1]-1-xgrow:s[1]-1, s[2]-1-ygrow:s[2]-1] , 2-(ygrow EQ 0))

ENDIF


RETURN, new_image

END
