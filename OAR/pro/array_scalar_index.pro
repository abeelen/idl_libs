;
;+
; NAME:
;
;       ARRAY_SCALAR_INDEX
;
; PURPOSE:
;
;		Returns the scalar index of the element in a multi-dimensional array.
;
; CATEGORY:
;
;       Arrays Management.
;
; CALLING SEQUENCE:
;
;		Result = ARRAY_SCALAR_INDEX(array, element)
;
;
; INPUT:
;
;		Array:	array of any type and dimensions
;
;		Element: n-dim elements vector specifying the element location
;
; OUTPUT:
;
;		Result: the scalar index (long type) of the spedified element is returned.
;
;
; MODIFICATION HISTORY:
;
;       Jun 2005 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;
;-


FUNCTION ARRAY_SCALAR_INDEX, array, element

ON_ERROR, 2

s = size(array)
n_dim = s[0]

IF n_elements(array) EQ 0 THEN MESSAGE, 'Array is undefined!'

IF n_elements(element) NE n_dim THEN MESSAGE, 'Element vector MUST have same number of elements as Array dimensions!'
IF min(element) LT 0 THEN MESSAGE, 'Element values MUST be positive!'

element = long(element)

s[0] = 1L
index = 0L
p = 1L
FOR i = 0, n_dim-1 DO BEGIN
	IF element[i] GE s[i+1] THEN MESSAGE, 'Element values MUST be within Array size!'
	p = p * s[i]
	index = index + element[i] * p
ENDFOR

RETURN, index

END
