;
;+
; NAME:
;
;       STRING_NUM
;
; PURPOSE:
;
;		Return a string representation of an integer with the digits right justified
;		and the leading characters filled with zeroes, or with a user selected char.
;
; CATEGORY:
;
;       String Manipulation.
;
; CALLING SEQUENCE:
;
;		Result = STR_WRAP(string, lenght)
;
; INPUTS:
;
;		String: scalar or array string to wrap.
;
;		Lenght: maximum lenght of substrings in characters.
;
; OUTPUTS:
;
;       The function returns a string array in which each element is a piece of String.
;
; MODIFICATION HISTORY:
;
;       Feb 2004 - 	Gianluca Li Causi, INAF - Rome Astronomical Observatory
;					licausi@mporzio.astro.it
;					http://www.mporzio.astro.it/~licausi/
;
;-

FUNCTION string_num, number, fillchar, digits

str = strcompress(string(ABS(number)),/REMOVE)

IF FIX(alog10(MAX(ABS(number)))) GT digits-1 THEN RETURN, -1

FOR i = 0, n_elements(number)-1 DO BEGIN
	IF number[i] LT 0 THEN segno = '-' ELSE segno = ''
	fill_digits = digits - FIX(alog10(ABS(number[i]))) - 1
	IF fill_digits GT 0 THEN str[i] = segno + strjoin(replicate(fillchar, fill_digits)) + str[i] $
		ELSE str[i] = segno + str[i]
ENDFOR

RETURN, str

END
