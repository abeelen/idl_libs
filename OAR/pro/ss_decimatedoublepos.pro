pro ss_decimatedoublepos,xx,yy,isurv,idouble,xdec,ydec $
                        ,searchradius=searchradius,degrees=degrees

;+
;
; PURPOSE: 
;         This procedure finds the elements in a list of positions
;         x,y (or ra,dec) that are closer than a given
;         searchradius. Its purpose is to get rid of double/multiple
;         positions in the list. 
;
; INPUT: 
;         XX: vector of x cartesian coordinate of objects (or ra in decimal degrees)
;
;         YY: vector of y cartesian coordinate of objects (or dec in decimal degrees)
;
; OUTPUT:
;         ISURV: index of the survived objects in the x,y list (single
;         entries within the given searchradius)
;
;         IDOUBLE: index of multiple detections that have been decimated.
;
;         XDEC: vector of x coordinate of the objects survived to the
;               decimation
;
;         YDEC: vector of y coordinate of the objects survived to the
;               decimation
;
; OPTIONAL:
;
;         SEARCHRADIUS: Keyword to set with a value of the maximum allowed
;                  distance for a couple of points to be considered the same
;                  detection. The default value is 0.00001.
;
;         DEGREES: set this keyword when using ra,dec instead of x,y
;                coordinates in order to calculate spherical distances.
;
; MODIFICATION HISTORY:
;
;         Written by:    S. Sabatini, Sept 2001
;
;         SS: updated with the searchradius keyword 27 Nov 2001 
;         SS: updated with idouble output 16 Jun 2002
;         SS: updated with the degrees keyword 10 Jul 2002
;         SS: done some 'make-up' to the procedure, Feb 2005
;
;-

       	t0=systime(1)               ;starting time of the program


        if n_elements(searchradius) eq 0. then begin
            searchradius=0.00001
        endif

	nx=n_elements(xx)

	xtmp=xx
        ytmp=yy

	for ii=0,nx-2 do begin

            xxlist=xx[ii+1:nx-1]*1D
            yylist=yy[ii+1:nx-1]*1D

            if keyword_set(degrees) then begin

                dist=sphdist(xxlist,yylist,xx[ii]*1D,yy[ii]*1D,/degrees)

            endif else begin
                
                dist=sqrt((xxlist-xx[ii]*1D)^2.+(yylist-yy[ii]*1D)^2.)
                
            endelse

            jj=where(dist le searchradius, cc)

            if cc ne 0 then begin
                
                imatch=ii+1+jj
                xtmp[imatch]=0
                ytmp[imatch]=0

            endif
           
	endfor

	idouble=where((xtmp eq 0.) and (ytmp eq 0.), complement=isurv)
	
	xdec=xtmp[isurv]
	ydec=ytmp[isurv]

        t1=systime(1)       ;time of the end of run

	print,'Running SS_DECIMATEDOUBLEPOS took (in secs):',t1-t0,$
		' (in mins):',(t1-t0)/60.0  



end
