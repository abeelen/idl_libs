rgb = [[0,   0,   0  ], $   ;;; start w/black                                               
       [125, 0,   175], $     ; then purple                                                 
       [255, 0, 255  ], $       ; 2nd middle will be magenta                                
       [255, 0,  125 ], $       ; middle will be red                                        
       [255, 255, 255]]         ; last is white                                             

colorvecs = get_colortable(rgb)
TVLCT, colorvecs(*,0), colorvecs(*,1), colorvecs(*,2)

mind = 0.0
maxd = 1.0

;steps2 = 50.

;step = (maxd - mind)/(steps2-1)
;userLevels = dindgen(steps2)*step + mind

x1 = 0.1
x2 = 0.9
y1 = 0.1
y2 = 0.95

csize_bar = 2.0
;steps2 = step


window, 1, xsize=100, ysize=500, xpos = 100, ypos = 100
colorbar, divisions=3, range=[round(mind),round(maxd)], $
   position=[x1, y1, x2, y2], $
  /vertical,  charsize=csize_bar, /noerase;, color=255



END
