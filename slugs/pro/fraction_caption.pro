;+
;PURPOSE
;	to make an axis label which has a fraction in it
;	replaces use of xtitle and ytitle
;
;SYNTAX
; fraction_caption, pre=pre, post=post, top=top, bottom=bottom,
;         [/x, /y,_extra=_extra,
;        nch_gap=nch_gap, charsize=charsize, /rotate, frac_fac=frac_fac,
;        sep_fac=sep_fac, linoff_fac=linoff_fac, thick=thick, offx=xoff, offy=yoff]
;
;INPUTS [defaults]
;	charsize: size of letters
;	pre: the part of the title that goes before the fraction['']
;	post: the part of the title that goes after the fraction['']
;	top: numerator of the fraction
;	bottom: denominator of the fraction
;	/x: if a x axis label 
;	/y: if a y axis label (either /x or /y must be set)
;	/rotate if you want it 90 degrees
;	_extra: keywords passed to xyouts
;
;KEYWORDS FOR TWEAKING POSITION
;	sep_fac: multiplicative factor tuning the separation of the 
;		numerator and denominator, [0.5]
;       frac_fac: multiplicative factor tuning the size of the fraction [0.9]
;       linoff_fac: multiplicative factor tuning the offset
;		of the line between the numerator and denomicator [0.2]
;	nch_grap: gap between the axis and the title [4]
;	offx: xoffset in normal coordinates [0]
;	offy: yoffset in normal coordinates [0]
;	parens: set to a string with a parenthesis type you like ['(', ')', '[',']','{', or '}']
;		to draw a vector parenthesis around the fraction
;	parfac: scale factor for the parentheses
;	poffsety: parentheses offset in y direction in normal coordinates
;	poffsetx: parentheses offset in y direction in normal coordinates
;NOTES
;       *DOESNT work with font=0 since font=0 fonts have no well-defined size
;	*see test_frac.pro for example
;	*this is still very much in development
;	*the same logic this code uses can in principle be easily
;		expanded to include more than one fraction
;	*you may have to tweak the various keywords to get it all to fit right on your screen
;		but the defaults should work out quite well for almost all cases
;	*doesn't support !p.multi currently... if I ever need it for that then I might do it.
;		in principle this could be done using the offx, and offy keywords as well
;		as maybe scaling the xone_ch and yone_ch or passing them after they are found
;		for one plot... very doable... I just haven't put in the time to do it... You are
;		of course welcome to try!.... this should involve some manipulation of the 
;		!D structure
;
;Written by R. da Silva, UCSC, 9-8-10
;-
pro fraction_caption, pre=pre, post=post, top=top, bottom=bottom,$
	 x=x, y=y,_extra=_extra,$
	nch_gap=nch_gap, charsize=charsize, rotate=rotate, frac_fac=frac_fac,$
	sep_fac=sep_fac, linoff_fac=linoff_fac, thick=thick, offx=xoff, offy=yoff, $
	parens=parens, parfac=parfac, poffsetx=poffsetx, poffsety=poffsety
;--
; Set some keyword defaults
if not keyword_set(poffsetx) then poffsetx=0
if not keyword_set(poffsety) then poffsety=0
if not keyword_set(xoff) then xoff=0
if not keyword_set(yoff) then yoff=0
if not keyword_set(thick) then thick=!p.thick
if not keyword_set(frac_fac) then frac_fac=0.9
if not keyword_set(parfac) then parfac=frac_fac*.7
if not keyword_set(sep_fac) then sep_fac=0.5
if not keyword_set(linoff_fac) then linoff_fac=0.20
if not keyword_set(charsize) then charsize=!p.charsize
if not keyword_set(post) then post=''
if not keyword_set(pre) then pre=''
axis_ratio=float(!D.x_size)/!D.y_size;find the axis ratio of the device

;grab some info about the size of characters
if !x.margin[0] NE 0 then $
	xone_ch=(convert_coord(min(!x.crange),min(!y.crange) , /data, /to_normal)$
	/!x.margin[0])[0] else $
     if !x.margin[1] NE 0 then $
	xone_ch=((1-convert_coord(max(!x.crange),min(!y.crange) , /data, /to_normal))$
        /!x.margin[1])[0] else xone_ch=0.05

if !y.margin[0] NE 0 then $
        yone_ch=(convert_coord(min(!x.crange),min(!y.crange), /data, /to_normal)$
        /!y.margin[0])[1] else $
  if !y.margin[1] NE 0 then $
        yone_ch=((1-convert_coord(min(!x.crange),max(!y.crange) , /data, /to_normal))$
        /!y.margin[1])[0] else yone_ch=0.05
	
if keyword_set(x) then begin ;if this is an x label
if not keyword_set(nch_gap) then nch_gap=1.5
	;number of charcter size spaces between bottom of plot and table
co1=convert_coord(mean(!x.crange), min(!y.crange), /data, /to_normal) ;find center of data plot
x0=co1[0]								
;y0=co1[1]-xone_ch*nch_gap*charsize
y0=co1[1]-yone_ch*nch_gap*charsize;*axis_ratio
			;move down nch_gap* size of one character down
endif 
if keyword_set(y) then begin ;if this is a y label
if not keyword_set(nch_gap) then nch_gap=1.5
co1=convert_coord(min(!x.crange),mean(!y.crange), /data, /to_normal)
y0=co1[1]
;x0=co1[0]-yone_ch*nch_gap*charsize*axis_ratio
x0=co1[0]-xone_ch*nch_gap*charsize;*axis_ratio
endif

;apply any offsets you like to get your position just right
x0+=xoff
y0+=yoff
lp='!M'
rp='!M'
if keyword_set(parens) then begin
case parens of
 '(': begin
	lp=lp+'('
	rp=rp+')'
     end
 ')': begin
        lp=lp+'('
        rp=rp+')'
     end
 '[': begin
        lp=lp+'['
        rp=rp+']'
      end
 '[': begin
        lp=lp+'['
        rp=rp+']'
      end
 '{': begin
        lp=lp+'<'
        rp=rp+'>'
      end
 '}': begin
        lp=lp+'<'
        rp=rp+'>'
      end
endcase
endif

;find out the lengths of each of your strings by setting charsize to be negative
xyouts, x0, y0, pre, width=wpre, charsize=-charsize
xyouts, x0, y0, post, width=wpost, charsize=-charsize
xyouts, x0, y0, top, width=wtop, charsize=-charsize*frac_fac
xyouts, x0, y0, bottom, width=wbottom, charsize=-charsize*frac_fac
xyouts, x0, y0, rp, width=wlp, charsize=-charsize*frac_fac*parfac, font=-1
xyouts, x0, y0, lp, width=wrp, charsize=-charsize*frac_fac*parfac, font=-1
if NOT keyword_set(parens) then begin
wlp=0
wrp=0
endif 
wpre+=wlp
wpost+=wrp

total_length=wpre+wpost+(wtop>wbottom); the total length of the string

;draw the pre portion
xyouts, x0-(keyword_set(rotate) EQ 0?total_length/2.:0), $
	y0-(keyword_set(rotate)?axis_ratio*(total_length/2.):0),$
	pre, _extra=_extra, orient=(keyword_set(rotate)?90:0),$
	/normal, charsize=charsize

;draw the post portion
xyouts, x0-(keyword_set(rotate) EQ 0?total_length/2.-wpre-(wtop>wbottom)-wlp:0), $
        y0-(keyword_set(rotate)?axis_ratio*(total_length/2.-wpre-(wtop>wbottom)-wlp):0),$
        post, _extra=_extra, orient=(keyword_set(rotate)?90:0),$
        /normal, charsize=charsize
;draw the parentheses if you wnat
if keyword_set(parens) then begin
pcharthick=!p.charthick
;if charthick EQ 0 AND pcharthick EQ 0 then pcharthick=1
if pcharthick EQ 0 then pcharthick=1
;if pcharthick EQ 0 then pcharthick=charthick
xyouts, poffsetx + x0-(keyword_set(rotate) EQ 0?total_length/2.-(wpre-wlp):0), $
        poffsety + y0-(keyword_set(rotate)?axis_ratio*(total_length/2.-(wpre-wlp)):0),$
        lp, _extra=_extra, orient=(keyword_set(rotate)?90:0),$
        /normal, charsize=charsize*parfac, font=-1, charthick=2>pcharthick

xyouts, poffsetx + x0-(keyword_set(rotate) EQ 0?total_length/2.-wpre-(wtop>wbottom):0), $
        poffsety + y0-(keyword_set(rotate)?axis_ratio*(total_length/2.-wpre-(wtop>wbottom)):0),$
        rp, _extra=_extra, orient=(keyword_set(rotate)?90:0),$
        /normal, charsize=charsize*parfac, font=-1, charthick=2>pcharthick
endif

sep=(keyword_set(rotate)?xone_ch:yone_ch)*sep_fac;the separation of the numerator and denominator
if keyword_set(rotate) then sep*=1.5
if keyword_set(rotate) then linoff_fac*=1.7
;draw the top (numerator)
if wtop GE wbottom then buff=0 else buff=(wbottom-wtop)/2.
xyouts, x0-(keyword_set(rotate) EQ 0?total_length/2.-wpre-buff:0)-$
	(keyword_set(rotate)?sep:0 ), $
        y0-(keyword_set(rotate)?(total_length/2.-wpre-buff)*axis_ratio:0)+$
	(keyword_set(rotate) EQ 0?sep:0 ),$
        top, _extra=_extra, orient=(keyword_set(rotate)?90:0),$
        /normal, charsize=charsize*frac_fac

;draw the bottom (denominator)
if wbottom GE wtop then buff=0 else buff=(wtop-wbottom)/2.
xyouts, x0-(keyword_set(rotate) EQ 0?total_length/2.-wpre-buff:0)+$
	(keyword_set(rotate)?sep:0 ), $
        y0-(keyword_set(rotate)?axis_ratio*(total_length/2.-wpre-buff):0)-$
	(keyword_set(rotate) EQ 0?sep:0 ),$
        bottom, _extra=_extra, orient=(keyword_set(rotate)?90:0),$
        /normal, charsize=charsize*frac_fac

;draw the line between them
if not keyword_set(rotate) then begin
y00=(convert_coord(0, y0, /normal, /to_data))[1]
x00=(convert_coord(x0-total_length/2.+wpre, 0, /normal, /to_data))[0]
len00=(convert_coord(0, 0, /normal,/to_data))[0]  -  $
	(convert_coord((wtop>wbottom), 0, /normal, /to_data))[0]
plots, x00+[0,abs(len00)], replicate(y00+$
	abs((convert_coord(0,0, /normal, /to_data))[1]-$
	(convert_coord(0,yone_ch, /normal, /to_data))[1])$;*charsize
	*linoff_fac,2)
endif

if keyword_set(rotate) then begin
;linoff_fac*=axis_ratio
y00=(convert_coord(0, y0-(total_length/2.-wpre)*axis_ratio, /normal, /to_data))[1]
x00=(convert_coord(x0, 0, /normal, /to_data))[0]
zero0=convert_coord(0, 0, /normal, /to_device)
ylen0=convert_coord(zero0[0], (convert_coord((wtop>wbottom),0, /normal, /to_device))[0],$
	/device, /to_normal)
;len00=(convert_coord(0, 0, /normal,/to_data))[1]  -ylen0
len00=(convert_coord(0, 0, /normal,/to_data))[1]  -  $
        (convert_coord(0,(wtop>wbottom), /normal, /to_data))[1]
plots,  replicate(x00-$
        abs((convert_coord(0,0, /normal, /to_data))[0]-$
        (convert_coord(xone_ch,0, /normal, /to_data))[0])$;*charsize
        *linoff_fac,2), $
	y00+[0,abs(len00)*axis_ratio], thick=thick
endif


end
