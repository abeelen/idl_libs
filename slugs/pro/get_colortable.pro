FUNCTION get_colortable, rgb
; this picks returns the rgb vectors from a list of colors
;Written by J. Naiman
   steps = 256

   nc = n_elements(rgb(0,*))-1.
   p = fltarr(nc+1)
   for i=1,nc do begin
       p(i) = round(i*steps/nc)-1
   endfor
   
   redVector = fltarr(steps)
   greenVector = fltarr(steps)
   blueVector = fltarr(steps)
   
   for i=0,nc-1 do begin
       p1 = p(i)
       p2 = p(i+1)
       c1 = rgb(0,i)
       c2 = rgb(0,i+1)
       redVector(p1:p2) = findgen(p2-p1+1)/(p2-p1+1.-1.)*(c2 - c1) + c1
       
       c1 = rgb(1,i)
       c2 = rgb(1,i+1)
       greenVector(p1:p2) = findgen(p2-p1+1)/(p2-p1+1.-1.)*(c2 - c1) + c1
       
       c1 = rgb(2,i)
       c2 = rgb(2,i+1)
       blueVector(p1:p2) = findgen(p2-p1+1)/(p2-p1+1.-1.)*(c2 - c1) + c1
   endfor

   rgbout = [[redVector], [greenVector], [blueVector]]
   return, rgbout
END
