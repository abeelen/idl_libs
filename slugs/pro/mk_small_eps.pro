;+
;PURPOSE
;	to convert a big eps file to a smaller one (by 2 orders of magnitude)
;	by first converting to a png and then back to eps
;SYNTAX
;	mk_small_eps, name
;INPUTS
;	name: name of the file
;Written by R. da Silva, UCSC, 7-27-10
;-
pro mk_small_eps, name
splog, 'converting big ps file to smaller one:', name
spawn, 'convert '+name+' dummy_123_mkSMALLeps.png'
spawn, 'convert dummy_123_mkSMALLeps.png '+name

end
