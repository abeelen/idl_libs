;+
;PURPOSE
;	to restore IDL save files in a better syntax
;SYNTAX
;	res=restore(filename, [choice=choice, _extra=_extra])
;INPUTS
;	filename: name of file
;	choice: the name or number of the variable from the
;		save file you want to return, if
;		not set, then returns the first variable
;KEYWORDS:
;	_extra: keywords for restore
;OUTPUTS
;	res: the variable you chose
;NOTES
;	see the procedure r_restore for a better procedure
;	version of restore
;
;	won't work if you have a varible named x123x689*
;Written by R. da Silva, UCSC, 12-2-10
;-


FUNCTION restore, x123x689__filename, choice=x123x689__choice,$
	_extra=x123x689__extra
;x123x689__bad=scope_varname()
;x123x689__bad=where(strmatch(x123x689__bad, 'x123x689*', /fold) EQ 0]
restore, x123x689__filename, _extra=x123x689__extra

;goofy variable names are to prevent name collisions
x123x689__res=scope_varname()
x123x689__res=x123x689__res[where(strmatch(x123x689__res, 'x123x689*',/fold) EQ 0)]
if total( isdefined(x123x689__res)) EQ 0 then begin
	print, 'RESTORE: no variables found'
	return, -1
endif
x123x689__res=x123x689__res[where(isdefined(x123x689__res) EQ 1)]



if n_elements(x123x689__choice) EQ 0 then begin
	return, scope_varfetch(x123x689__res[0], level=0)
endif else begin
           if n_elements(x123x689__choice) NE 1 then begin
                  print, 'You can only choose one element, for more call as a procedure'
                  return, -1
                endif
	   if size(x123x689__choice, /type) EQ 7 then $
           x123x689__wh=where(strmatch(x123x689__res, x123x689__choice, /fold), x123x689__ct) $
	   else begin
		x123x689__wh=x123x689__choice
		x123x689__ct=1
	   endelse
         if x123x689__ct EQ 0 then begin
                  print, 'Choice not found!'
                  return, -1
                endif
           return, scope_varfetch(x123x689__res[x123x689__wh[0]], level=0)
endelse
end

