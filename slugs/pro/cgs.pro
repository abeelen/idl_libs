;+
;PURPOSE
;	to define CGS constants
;SYNTAX
;	cgs, a
;OUTPUTS
;	a: a structure with cgs constants
; TAG:value,name
;  g:6.674d-8, gravitational constant
;  pc:3.086d18, parsec
;  kpc:1d3*3.086d18, kiloparsec
;  k:1.3807d-16, bolztmann constant
;  c:2.9979d10, speed of light
;  h:6.6261d-27, planck's constant
;  hbar:6.6261d-27/2/!pi, hbar= planck/s constant /2 pi
;  msun:1.989d33, mass of sun
;  lsun:3.85d33, luminosity of sun
;  rsun:6.96d10,radius of sun
;  me:9.1094d-28, mass of election
;  mp:1.6726d-24, mass of proton
;  eV:1.60219d-12, electron volt
;  a0:5.2918d-9,bohr radius
;  au:1.49598d13,astronomical unit
;
;Written by R. da Silva, UCSC, 9-10-10
;-

pro cgs, a

a={ $
g:6.674d-8, $ ;gravitational constant
pc:3.086d18, $ ;parsec
kpc:1d3*3.086d18, $ ;kiloparsec
k:1.3807d-16, $ ;bolztmann constant
c:2.9979d10, $ ;speed of light
h:6.6261d-27, $ ;planck's constant
hbar:6.6261d-27/2/!pi, $ ;hbar planck/s constant /2 pi
msun:1.989d33, $ ;mass of sun
lsun:3.85d33, $ ;luminosity of sun
rsun:6.96d10,$  ;radius of sun
me:9.1094d-28, $ ;mass of election
mp:1.6726d-24, $ ;mass of proton
eV:1.60219d-12, $ ;electron volt
a0:5.2918d-9,$ ;bohr radius
au:1.49598d13} ;astronomical unit


end
