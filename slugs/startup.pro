DEFSYSV, '!SLUGS_DIR', GETENV("SLUGS_DIR")
!PATH = !PATH + ":"+ EXPAND_PATH('+'+!SLUGS_DIR+'/pro')
PRINT,"Slugs Guide to IDL parallel routine now available"
;; See also https://svn.ssec.wisc.edu/repos/bennartz_group/LIBRARY/idl/group_libs/parallel/
