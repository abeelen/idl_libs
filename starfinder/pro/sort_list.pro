; $Id: sort_list.pro, v 1.1 Mar 2012 e.d. $
;
;+
; NAME:
;	SORT_LIST
;
; PURPOSE:
;	Sort stars by decreasing flux.
;
; CATEGORY:
;	STARFINDER auxiliary procedures.
;
; CALLING SEQUENCE:
;	Result = SORT_LIST(List, SUBSCRIPTS = S)
;
; INPUTS:
;	List:	list of stars
;
; KEYWORD PARAMETERS:
;	SUBSCRIPTS:	subscripts of stars to be sorted. If undefined, sort
;		all stars in the list
;
; OUTPUTS:
;	Return sorted list of sublist, if SUBSCRIPTS is set.
;
; MODIFICATION HISTORY:
; 	Written by:	Emiliano Diolaiti, June 2001.
;   1) Created this file (E. D., March 2012).
;-

FUNCTION sort_list, list, SUBSCRIPTS = s

    on_error, 2
    if  n_tags(list) eq 0  then  return, list
    s = reverse(sort(list.f))
    return, list[s]
end
