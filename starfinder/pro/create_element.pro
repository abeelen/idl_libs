; $Id: create_element.pro, v 1.1 Mar 2012 e.d. $
;
;+
; NAME:
;	CREATE_ELEMENT
;
; PURPOSE:
;	Generate new element of star list and initialize it.
;
; CATEGORY:
;	STARFINDER auxiliary procedures.
;
; CALLING SEQUENCE:
;	Result = CREATE_ELEMENT(X, Y, F)
;
; INPUTS:
;	X, Y:	x- and y- position of object.
;
; OPTIONAL INPUTS:
;	F:	object flux.
;
; OUTPUTS:
;	Return initialized element, representing a (possibly)
;	presumed star
;
; MODIFICATION HISTORY:
; 	Written by:	Emiliano Diolaiti, June 2001.
; 	1) Created this file (E. D., March 2012).
;-

FUNCTION create_element, x, y, f

    on_error, 2
    element = star()
    update_list, element, x, y, f
    return, element
end
