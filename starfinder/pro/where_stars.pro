; $Id: where_stars.pro, v 1.1 Mar 2012 e.d. $
;
;+
; NAME:
;	WHERE_STARS
;
; PURPOSE:
;	Find subscripts of stars in a given star list, which might also
;	include presumed stars.
;
; CATEGORY:
;	STARFINDER auxiliary procedures.
;
; CALLING SEQUENCE:
;	Result = WHERE_STARS(List, LX = Lx, UX = Ux, LY = Ly, UY = Uy, N)
;
; INPUTS:
;	List:	star list
;
; KEYWORD PARAMETERS:
;	LX, UX, LY, UY:	fix lower and upper x- and y- bounds of image
;		region where the stars in the list have to searched
;
; OUTPUTS:
;	Return subscripts of stars, possibly falling within specified
;	region
;
; OPTIONAL OUTPUTS:
;	N:	number of found stars
;
; MODIFICATION HISTORY:
; 	Written by:	Emiliano Diolaiti, June 2001.
;   1) Created this file (E. D., March 2012).
;-

FUNCTION where_stars, list, LX = lx, UX = ux, LY = ly, UY = uy, n

    on_error, 2
    if  n_tags(list) eq 0  then begin
       n = 0L  &  return, -1L
    endif
    flag = list.is_a_star
    if  n_elements(lx) ne 0  then $
       flag = flag and list.x ge lx and list.x le ux $
                   and list.y ge ly and list.y le uy
    return, where(flag and 1B, n)
end
