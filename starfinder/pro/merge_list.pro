; $Id: merge_list.pro, v 1.1 Mar 2001 e.d. $
;
;+
; NAME:
;	MERGE_LIST
;
; PURPOSE:
;	Merge two lists of stars.
;
; CATEGORY:
;	STARFINDER auxiliary procedures.
;
; CALLING SEQUENCE:
;	Result = MERGE_LIST(L1, L2)
;
; INPUTS:
;	L1, L2:	lists to merge. If L1 is empty, return L2.
;
; OUTPUTS:
;	Return merged list
;
; MODIFICATION HISTORY:
; 	Written by:	Emiliano Diolaiti, June 2001.
; 	1) Created this file (E. D., March 2012).
; 	2) Fixed bug when input list L1 is empty (E. D., April 2012).
;-

FUNCTION merge_list, l1, l2

    on_error, 2
    if n_tags(l1) eq 0 then $
       l = l2 else $
    begin
       n1 = n_elements(l1)
;       if  n1 eq 0  then  l = l2  else $
;       begin
       n2 = n_elements(l2)
       l = starlist(n1 + n2)
       l[0] = l1  &  l[n1] = l2
;       endelse
    endelse
    return, l
end
