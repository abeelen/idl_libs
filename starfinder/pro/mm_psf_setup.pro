; $Id: mm_psf_setup.pro, v 1.0 March 2012 e.d. $
;
;+
; NAME:
; MM_PSF_SETUP
;
; PURPOSE:
; Define auxiliary PSF data to be used by main routine of "starfinder.pro" 
; when a parametric space-variant PSF model called is adopted.
; The PSF model defined here is based on a sum of Moffat functions. 
; The relative weight, width and position angle of the Moffat components are 
; assumed to change across the image which the PSF model refers to. 
; The variation model makes the following assumptions:
; - all elongated Moffat components point to the same reference position 
;   in the image
; - relative weight and width of Moffat components change across the image 
;   according to a polynomial law depending on the distance of the considered 
;   location in the image from the reference position.
; 
; CATEGORY:
; Stellar fields astrometry and photometry.
;
; CALLING SEQUENCE:
; MM_PSF_SETUP, C, Pow, Xref, Yref, Xsize_ima, Ysize_ima, Nx, Ny, $
;               Xsize_psf, Ysize_psf, Xsize_svpsf, Ysize_svpsf, $
;               Psf_data, Psf, Sv_par
;               
; INPUTS:
; C: 2-dim or 3-dim floating-point array of coefficients defining the multi-Moffat PSF 
;    model and its variation across the image. The variation of each parameter is 
;    assumed to be described by a polynomial function of the distance of the PSF 
;    location in the image from the reference position.
;    The array C is defined as follows:
;    C[i,j,k], 0 <= i <= maxdeg+2, 0 <= j <= Nparcomp-1, 0 <= k <= Ncomp-1, where
;    - Ncomp: number of Moffat functions in the PSF model
;    - Nparcomp: number of parameters per Moffat component (3)
;    - maxdeg: maximum degree among polynomials defining parameters variations across the image
;    It is assumed that each Moffat function is defined by 3 parameters:
;    - relative weight (scale factor)
;    - half-light radius along elongated axis
;    - half-light radius along axis perpendicular to elongation
;    The elongation axis is defined as the line connecting the PSF position in the image
;    and the reference position.
;    The element C[0,j,k] is the degree of the polynomial associated to parameter j 
;    of Moffat function k.
;    The elements C[i,j,k] with 1 <= i <= C[0,j,k]+1 are the coefficients of the 
;    polynomial associated to parameter j of Moffat function k.
; 
; Pow: scalar or floating-point array with power law of Moffat functions. If scalar, 
;    the same power law is assumed for all components. If array, the number of elements 
;    must be the same as the number of components.
;
; Xref, Yref: pixel coordinates of reference position in the image.
; 
; Xsize_ima, Ysize_ima: integers, number of columns and rows of image array.
; 
; Nx, Ny: number of regions along X and Y axes of image to determine cube of
;   piecewise constant PSFs.
; 
; Xsize_psf, Ysize_psf: integers, number of columns and rows of PSF array to be 
;   computed at any desired location in the image according to the continuous 
;   variation model.
; 
; Xsize_svpsf, Ysize_svpsf: integers, number of columns and rows of PSF arrays 
;   in the cube of piecewise constant PSFs (see Nx, Ny above).
; 
; OUTPUTS:
; Psf_data: pointer to structure of PSF model parameters used to compute the PSF 
;    at any position in the image.
; 
; Psf: floating-point array of piece-wise constant PSFs; cube of size 
;    Xsize_svpsf * Ysize_svpsf * (Nx*Ny).
; 
; Sv_par: structure defining the boundaries of the Nx*Ny sub-regions into which the 
;    image is virtually partitioned and over which the piece-wise constant PSFs are 
;    defined.
;
; EXAMPLE:
; 1) Define PSF model based on 2 Moffat components with following parameters:
; 
; 
; IDL> c=[[[0,0.5,0,0],[2,1.0,1e-3,1e-4],[1,1.0,1e-3,0.0]],$
;         [[0,0.5,0,0],[0,4.0,0,0],[0,4.0,0,0]]]
; IDL> help,c
; C               FLOAT     = Array[4, 3, 2]
; IDL> pow=[1.5,2.0]
; IDL> xref=128.0 & yref=128.0
; IDL> xsize_ima=256
; IDL> ysize_ima=256
; IDL> nx=4
; IDL> ny=4
; IDL> xsize_psf=64
; IDL> ysize_psf=64
; IDL> xsize_svpsf=32
; IDL> ysize_svpsf=32
; IDL> mm_psf_setup, c, pow, xref, yref, xsize_ima, ysize_ima, nx, ny, $
;                    xsize_psf, ysize_psf, xsize_svpsf, ysize_svpsf, psf_data, psf, sv_par 
; IDL> help,*psf_data,/STRUCTURE
; ** Structure <1374398>, 10 tags, length=132, data length=130, refs=1:
;    XREF            FLOAT           128.000
;    YREF            FLOAT           128.000
;    XORI            FLOAT          0.000000
;    YORI            FLOAT          0.000000
;    C               FLOAT     Array[4, 3, 2]
;    POW             FLOAT     Array[2]
;    NCOMP           LONG                 2
;    NPARCOMP        INT              3
;    X_SIZE          INT             64
;    Y_SIZE          INT             64
; IDL> help,psf
; PSF             FLOAT     = Array[32, 32, 16];
; IDL> help,sv_par,/STRUCTURE
; ** Structure <181f8a0>, 4 tags, length=64, data length=64, refs=1:
;    LX              LONG      Array[4]
;    UX              LONG      Array[4]
;    LY              LONG      Array[4]
;    UY              LONG      Array[4]
;
; MODIFICATION HISTORY:
;   Written by: Emiliano Diolaiti, March 2012.
;-

PRO mm_psf_setup, c, pow, xref, yref, xsize_ima, ysize_ima, nx, ny, $
                  xsize_psf, ysize_psf, xsize_svpsf, ysize_svpsf, $
                  psf_data, psf, sv_par

;catch, error
;if error ne 0 then begin
;   if  ptr_valid(psf_data) then ptr_free, psf_data
;   return
;endif

if size(c, /N_DIM) gt 2 then ncomp = (size(c, /DIM))[2] else ncomp = 1
if n_elements(pow) eq ncomp then k = pow else k = replicate(pow[0], ncomp)
nparcomp = 3 ; number of parameters per component
psf_data = {xref: float(xref), yref: float(yref), xori: 0.0, yori: 0.0, $
            c: c, pow: k, ncomp: ncomp, nparcomp: nparcomp, $
            x_size: xsize_psf, y_size: ysize_psf}
psf_data = ptr_new(psf_data)

array_partition, xsize_ima, nx, lx, ux, /OVERLAP
array_partition, ysize_ima, ny, ly, uy, /OVERLAP
sv_par = {lx: lx, ux: ux, ly: ly, uy: uy}
psf = fltarr(xsize_svpsf, ysize_svpsf, nx*ny)
for j = 0L, nx-1 do for i = 0L, ny-1 do begin
   (*psf_data).xori = (lx[j] + ux[j]) / 2 - xsize_svpsf/2
   (*psf_data).yori = (ly[i] + uy[i]) / 2 - ysize_svpsf/2
   psf[*,*,i*nx+j] = $
   image_model(xsize_svpsf/2, ysize_svpsf/2, 1.0, xsize_svpsf, ysize_svpsf, "mm_psf", psf_data)
endfor
(*psf_data).xori = 0.0
(*psf_data).yori = 0.0

return
end
