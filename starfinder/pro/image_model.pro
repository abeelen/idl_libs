; $Id: image_model.pro, v 2.3 March 2012 e.d. $
;
;+
; NAME:
;   IMAGE_MODEL
;
; PURPOSE:
;   Create a synthetic image given by a sum of shifted scaled replicas
;   of a PSF. The PSF may either be a replica of the input template
;   ("fixed PSF" option) or may be extracted from a set of local PSFs
;   ("space-variant PSF" option) or computed for each location, according
;   to some user-defined model ("PSF model" option).
;
; CATEGORY:
;   Models.
;
; CALLING SEQUENCE:
;   Result = IMAGE_MODEL(X, Y, F, X_size, Y_size, Psf, Data)
;
; INPUTS:
;   X, Y: Vectors of stars positions
;
;   F:    Vector of stellar fluxes
;
;   X_size, Y_size:   First and second size of the output array
;
;   Psf:  When the "fixed PSF" option is used, Psf must be a 2D array
;     containing the image of the PSF to be replicated in the output image
;     model.
;     When the "space-variant PSF" option is used, Psf must be a 3D stack
;     of PSF images. In this case it is necessary to supply the bounds of
;     image domain partition (see KEYWORDS LX, UX, LY, UY).
;     When the "PSF model" option is used, Psf must be a string parameter 
;     with the name of the function used to computer the desired PSF model. 
;     The following PSF models are available:
;     - Sum of Moffat functions elongated to a common point; the width in the 
;       elongated axis is described by a polynomial function.
;       Set Psf = "mm_psf"
;     - Airy diffraction pattern. Set Psf = "airy"
;     - Resampled version of an input numerical PSF. Set Psf = "resampled". This 
;       model may be useful if a PSF is known on a fine pixel grid and it has to 
;       be resampled on a coarser grid.
;     If the "PSF model" option is applied, the user may have to supply
;     additional information with the parameter Data (see OPTIONAL INPUTS),
;     depending on how the procedure which actually computes the PSF model
;     is defined.
;
; OPTIONAL INPUTS:
;   Data: Use this variable to provide a pointer to the additional information
;     required by the "model" option.
;     In general the heap variable pointed by Data is a structure, containing
;     miscellaneous information.
;     For the supported model types, the structure pointed by Data must be
;     defined as follows:
;     1) Psf type = 'mm_psf'
;      *Data = {Xref: xref, Yref: yref, Xori: xori, Yori: yori, $
;               C: c, Pow: k, Ncomp: ncomp, Nparcomp: nparcomp, $
;               X_size: x_size, Y_size: y_size}
;      where
;      Xref, Yref: (float) coordinates of reference position in the image
;      Xori, Yori: (float) coordinates of pixel [0,0] of PSF model array with respect to 
;         image array
;      C: (float) array of coefficients defining the multi-Moffat PSF model and its variation 
;         across the image. For more details, see 'mm_psf_setup.pro'
;      Pow: vector with power law of Moffat functions
;      Ncomp: number of Moffat components in the model
;      Nparcomp: number of parameters per component (3)
;      X_size, Y_size: size of the PSF model array
;     2) Psf type = 'airy'
;      *Data = {X_size: x_size, Y_size: y_size, Sampling_factor: sampling_factor},
;      where
;      X_size, Y_size: size of the PSF model array
;      Sampling_factor: ratio of the actual sampling step to the critical step size
;     3) Psf type = 'resampled'
;      *Data = {X_size0: x_size0, Y_size0: y_size0, X_size: x_size, Y_size: y_size, $
;               Psf: psf, Xpsf: xpsf, Ypsf: ypsf}
;      where
;      X_size0, Y_size0: integer scalars, X and Y size of given PSF
;      X_size, Y_size: integer scalars, X and Y size of resampled PSF
;      Psf: 2D or 3D cube of PSFs; size x_size0*y_size0*npsf, where npsf is the number 
;         of input PSFs
;      Xpsf, Ypsf: vectors of npsf elements representing the X and Y positions of the 
;         PSFs in the psf cube.
;         
;     When the "fixed PSF" options is chosen, the variable Data may also be
;     used to provide additional information for the Psf shift, which has
;     been released on output in a previous call to IMAGE_MODEL (see OPTIONAL
;     OUTPUTS below).
;     The parameter Data must NOT be supplied when a stack of space-variant
;     PSFs is supplied.
;
; KEYWORD PARAMETERS:
;   XPSF, YPSF:   Vectors specifying the positions of the local PSFs in 
;     the image when the "space-variant PSF" option is used.
;
;   LX, UX, LY, UY:   Vectors specifying the bounds of the image domain
;     partition when the "space-variant PSF" option is used. In this case
;     the sub-domain  [LX[j]: Ux[j], LY[i]: UY[i]]  must correspond to the
;     (i * X_size + j)-th  PSF in the input stack.
;
;   INTERP_TYPE:  Set this keyword to a string identifying one of the
;     interpolation techniques supported by the function IMAGE_SHIFT
;     (for more details see the file 'image_shift.pro'). This keyword
;     is neglected if the "PSF model" option is used.
;
;   REFERENCE_PIX:    Set this keyword to a two-elements integer vector with
;     the coordinates of the reference pixel in the Psf array which must
;     be placed at the positions (X, Y) in the output array.
;     The default is the Psf maximum.
;
; OUTPUTS:
;   Result:   2D array containing the synthetic model
;
; OPTIONAL OUTPUTS:
;   Data: The heap variable pointed by Data may be modified by the
;     procedures called by IMAGE_MODEL to shift the input Psf ("input PSF"
;     option) or to compute a PSF model ("PSF model" option).
;     This pointer can be used as a useful input/output variable to
;     provide or retrieve information for subsequent calls to IMAGE_MODEL.
;     Let us consider the following example: suppose IMAGE_MODEL is
;     called the first time with the heap variable (*Data) undefined and
;     using the "input PSF" option. The pointer variable Data released on
;     output will reference an anonymous structure (created by IMAGE_SHIFT)
;     with useful information which can be recycled in a further call to
;     IMAGE_MODEL (provided all the options are the same).
;
; RESTRICTIONS:
;   The "fixed" and "space-variant" PSF options are suited to a well sampled
;   PSF: in this case it is possible to interpolate the template when a
;   fractional shift is required for sub-pixel positioning. When the data are
;   sub-sampled, interpolation errors may occur. In this case a PSF model should 
;   be used instead, if available (with the "PSF model" option).
;
; PROCEDURE:
;   For each input position and stellar flux, put one PSF in the output image.
;   The PSF may be simply a replica of the input template or may be extracted
;   from an stack of local PSFs or even computed for each location according to 
;   a given model. In the first two cases, sub-pixel positioning is performed 
;   by interpolating the input PSF (see the function IMAGE_SHIFT in the file 
;   'image_shift.pro' for more details).
;   If the user wishes to define new model options, he/she must write a new
;   procedure according to the following template:
;
;   PRO model_psf, X, Y, Aux, Psf, Reference_pixel
;     dx = X - round(X)  &  dy = Y - round(Y)
;     "function call" to define the PSF model,
;      having its maximum at (x_size/2,y_size/2)
;     Psf = Psf / total(Psf)
;     Reference_pixel = [Aux.x_size, Aux.y_size] / 2
;     return
;   end
;
;   where X and Y represent the location of a star in the image, Aux is a
;   structure passed to IMAGE_MODEL through the pointer Data (see OPTIONAL
;   INPUTS above), Psf is the output PSF model and Reference_pixel will be
;   used by IMAGE_MODEL to position the computed PSF in the output array.
;   The line "function call" in the above template is a call to some
;   procedure/function which actually computes the PSF model. The maximum
;   intensity pixel should lie at (x_size/2,y_size/2). For more details,
;   see the procedures 'gaussian_psf' and 'airy_psf' in this file.
;
;
; MODIFICATION HISTORY:
;   Written by:  Emiliano Diolaiti, August 1999.
;   Updates:
;   1) Added REFERENCE_PIX keyword (Emiliano Diolaiti, December 1999)
;   2) Space-Variant PSF option (Emiliano Diolaiti, January 2000)
;   3) Added MODEL keyword (Emiliano Diolaiti, December 2000)
;   4) Space-Variant PSF option: fixed error in call to PICK_REGION
;      and adjusted call to IMAGE_SHIFT (Emiliano Diolaiti, December 2004)
;   5) Added "multigaussian_psf" and "powergau_psf" options
;      (E. D., January 2005)
;   6) Added keywords XPSF, YPSF for space-variant option
;      (E. D., May 2008)
;   7) Added MOFFAT_PSF (E. D., June 2008)
;   8) Added RESAMPLED_PSF (E. D., May 2009)
;   9) Removed "CASE" instruction in main routine. Now the value of the input
;      Psf in the "PSF model" option must be equal to the name of the function 
;      used to compute the PSF model (E. D., January 2012).
;   10) Updated multi-Moffat PSF function, now called MMR_PSF (E. D., January 2012).
;   11) Updated multi-Moffat PSF function, now called MM_PSF and temporarly removed 
;       other PSF models that need verification and documentation (moved to block 
;       comment at the end of the file) (E. D., March 2012).
;-


;;; Auxiliary procedures to compute PSF model.


; MM_PSF: Sum of N Moffat functions elongated to a common point.
; All Moffat functions have same center and position angle.

PRO mm_psf, x, y, data, psf, ref_pix

    on_error, 2
    xglo = x + data.xori
    yglo = y + data.yori
    dx = x - round(x)  &  dy = y - round(y)
    ref_pix = [data.x_size/2, data.y_size/2]
    psf = fltarr(data.x_size, data.y_size)
    r = distance(data.xref, data.yref, xglo, yglo)
    if xglo ne data.xref then $
       tilt = atan(float(yglo - data.yref) / float(xglo - data.xref)) else tilt = !pi/2
    par = fltarr(data.nparcomp, data.ncomp)
    for k = 0, data.ncomp-1 do $
       for j = 0, data.nparcomp-1 do $
          for d = 1, round(data.c[0,j,k])+1 do par[j,k] = par[j,k] + data.c[d,j,k] * r^(d-1)
    for k = 0, data.ncomp-1 do $
       psf = temporary(psf) + par[0,k] * $
       moffat2d(data.x_size, data.y_size, ref_pix[0] + dx, ref_pix[1] + dy, $
                par[1,k], par[2,k], tilt, POWER = data.pow[k], /NORMAL)

    return
end

; AIRY_PSF: Airy diffraction pattern.

PRO airy_psf, x, y, data, psf, ref_pix

    on_error, 2
    dx = x - round(x)  &  dy = y - round(y)
    psf = airy_pattern(data.x_size, data.y_size, $
                 data.x_size/2+dx, data.y_size/2+dy, data.sampling_factor)
    psf = psf / total(psf)
    ref_pix = [data.x_size, data.y_size] / 2
    return
end

; RESAMPLED_PSF: resample given PSF to desired size.

PRO resampled_psf, x, y, data, psf, ref_pix

    on_error, 2
    dx = x - round(x)  &  dy = y - round(y)
    dx = dx * float(data.x_size0) / float(data.x_size)
    dy = dy * float(data.y_size0) / float(data.y_size)
    m = min(distance(x, y, data.xpsf, data.ypsf), w)
    psf = image_shift(data.psf[*,*,w], dx, dy)
    norm = (float(data.x_size0) / data.x_size) * (float(data.y_size0) / data.y_size)
    psf = norm * congrid(psf, data.x_size, data.y_size, CUBIC = -0.5)
    ref_pix = [data.x_size, data.y_size] / 2
    return
end

; TEMPLATE_PSF: template procedure for user-written PSF model procedures.

PRO template_psf, x, y, data, psf, ref_pix

    on_error, 2
    dx = x - round(x)  &  dy = y - round(y)
;   psf = "function call"
    psf = psf / total(psf)
    ref_pix = [data.x_size, data.y_size] / 2
    return
end



;;; The main routine.

FUNCTION image_model, x, y, f, x_size, y_size, psf, data, $
                      REFERENCE_PIX = psf_ref_pix, _EXTRA = extra, $
                      LX = lx, UX = ux, LY = ly, UY = uy, $
                      XPSF = xpsf, YPSF = ypsf, MODEL = image

   on_error, 2
    ; Define output image model
    if  n_elements(image) eq 0  then  image = fltarr(x_size, y_size)
    nstar = n_elements(f)
    ; Define PSF option ("fixed" or "space variant" or "model")
    fixed_psf = size52(psf, /N_DIM) eq 2
    space_var = not fixed_psf
    if  not fixed_psf  then begin
       space_var = size52(psf, /N_DIM) eq 3
       if space_var then $
          space_var = (n_elements(lx) ne 0 and n_elements(ux) ne 0 and $
                       n_elements(ly) ne 0 and n_elements(uy) ne 0) or $
                      (n_elements(xpsf) ne 0 and n_elements(ypsf) ne 0)
    endif
    if  fixed_psf or space_var  then begin
       psf_size = (size52(psf, /DIM))[0:1]
       if  n_elements(psf_ref_pix) eq 0  then $
          psf_ref_pix = get_max(psf[*, *, 0])
    endif else $
       psf_pro = strlowcase(psf)
    ; If there are some additional data defined, de-reference them
    if  ptr_valid(data)  then $
       if  n_elements(*data) ne 0  then  aux = *data
    ; Compute image model
    for  n = 0L, nstar - 1  do begin
       ix = round(x[n])  &  iy = round(y[n])
       if  fixed_psf  then $
          psf_xy = image_shift(psf, x[n] - ix, y[n] - iy, _EXTRA = extra, aux) $
       else  if  space_var  then $
          psf_xy = image_shift(psf[*,*,pick_region(lx, ux, ly, uy, x[n], y[n])], $
                               x[n] - ix, y[n] - iy, _EXTRA = extra) $
       else $
          call_procedure, psf_pro, x[n], y[n], aux, psf_xy, psf_ref_pix
       add_overlap, image, f[n] * psf_xy, [ix, iy], psf_ref_pix
    endfor
    ; Update additional data if necessary
    if  ptr_valid(data)  then $
       if  n_elements(aux) ne 0  then  *data = aux
    return, image
end





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; MGR_PSF: Sum of N Gaussian functions elongated to a common point.
; All Gaussian functions have same center and position angle.

;PRO mgr_psf, x, y, data, psf, ref_pix
;
;    on_error, 2
;;    data.xglo = x + data.xori
;;    data.yglo = y + data.yori
;    xglo = x + data.xori
;    yglo = y + data.yori
;    dx = x - round(x)  &  dy = y - round(y)
;    ref_pix = [data.dim/2, data.dim/2]
;    psf = fltarr(data.dim, data.dim)
;    r = distance(data.xref, data.yref, xglo, yglo)
;    if x ne data.xref then $
;       tilt = atan(float(yglo - data.yref) / float(xglo - data.xref)) else tilt = !pi/2
;    npc = 3
;    par = fltarr(npc * data.ncomp)
;    for k = 0, data.ncomp-1 do begin
;       j = npc * k
;       for d = 1, round(data.m[0, k]) + 1  do par[j] = par[j] + data.m[d, k] * r^(d-1)
;       for d = 1, round(data.rx[0, k]) + 1 do par[j+1] = par[j+1] + data.rx[d, k] * r^(d-1)
;       for d = 1, round(data.ry[0, k]) + 1 do par[j+2] = par[j+2] + data.ry[d, k] * r^(d-1)
;    endfor
;    k = lindgen(data.ncomp) * npc
;    par[k] = par[k] / total(2 * !pi * par[k] * par[k+1] * par[k+2])
;    for k = 0, data.ncomp-1 do begin
;       j = npc * k
;       psf = temporary(psf) + par[j] * $
;       gaussian2d(data.dim, data.dim, ref_pix[0] + dx, ref_pix[1] + dy, $
;                  par[j + 1], par[j + 2], tilt)
;    endfor
;
;    return
;end


;;;;; GAUSSIAN_PSF: 2D (elliptical) gaussian PSF.
;;;;
;;;;PRO gaussian_psf, x, y, data, psf, ref_pix
;;;;
;;;;    on_error, 2
;;;;    dx = x - round(x)  &  dy = y - round(y)
;;;;    psf = gaussian2d(data.x_size, data.y_size, $
;;;;               data.x_size/2+dx, data.y_size/2+dy, $
;;;;               data.sigma_x, data.sigma_y, data.angle)
;;;;    psf = psf / total(psf)
;;;;    ref_pix = [data.x_size, data.y_size] / 2
;;;;    return
;;;;end

;;;;;; MULTIGAUSSIAN_PSF: Sum of 2D gaussians.
;;;;;; All gaussians have same center and position angle.
;;;;;
;;;;;PRO multigaussian_psf, x, y, data, psf, ref_pix
;;;;;
;;;;;    on_error, 2
;;;;;    dx = x - round(x)  &  dy = y - round(y)
;;;;;    ref_pix = [data.x_size/2, data.y_size/2]
;;;;;    npar = data.ngau * data.npar_gau + 1
;;;;;    psf = fltarr(data.x_size, data.y_size)
;;;;;    for g = 0, data.ngau-1 do begin
;;;;;       k = g * data.npar_gau
;;;;;       psf = temporary(psf) + data.p[k] * $
;;;;;       gaussian2d(data.x_size, data.y_size, ref_pix[0] + dx, ref_pix[1] + dy, $
;;;;;                  data.p[k + 1], data.p[k + 2], data.p[npar - 1])
;;;;;    endfor
;;;;;    return
;;;;;end
;;;;;
;;;;;; MULTIGAUSSIAN_PSF: Sum of 2D gaussians.
;;;;;; Each gaussian may have different center and position angle.
;;;;;
;;;;;PRO multigaussian_psf_, x, y, data, psf, ref_pix
;;;;;
;;;;;    on_error, 2
;;;;;    psf = fltarr(data.x_size, data.y_size)
;;;;;    dx = x - round(x)  &  dy = y - round(y)
;;;;;    for g = 0, data.ngau-1 do begin
;;;;;       k = g * data.npar_gau
;;;;;       psf = temporary(psf) + data.p[k] * $
;;;;;       gaussian2d(data.x_size, data.y_size, $
;;;;;       data.x_size/2 + data.p[k + 3] + dx, data.y_size/2 + data.p[k + 4] + dy, $
;;;;;       data.p[k + 1], data.p[k + 2], data.p[k + 5])
;;;;;    endfor
;;;;;    ref_pix = [data.x_size/2, data.y_size/2]
;;;;;    return
;;;;;end
;;;;;
;;;;;; POWERGAU_PSF: Gaussian with power profile.
;;;;;
;;;;;PRO powergau_psf, x, y, data, psf, ref_pix
;;;;;
;;;;;    on_error, 2
;;;;;    psf = fltarr(data.x_size, data.y_size)
;;;;;    dx = x - round(x)  &  dy = y - round(y)
;;;;;    psf = powergau(data.x_size, data.y_size, data.x_size/2 + dx, data.y_size/2 + dy, $
;;;;;                   data.p[0], data.p[1], data.p[2], data.p[3])
;;;;;    psf = psf / total(psf)
;;;;;    ref_pix = [data.x_size/2, data.y_size/2]
;;;;;    return
;;;;;end
;;;;;
;;;;;; MULTIPOWERGAU_PSF: Sum of 2D gaussians with power profile.
;;;;;; All gaussians have same center and position angle.
;;;;;
;;;;;PRO multipowergau_psf, x, y, data, psf, ref_pix
;;;;;
;;;;;    on_error, 2
;;;;;    dx = x - round(x)  &  dy = y - round(y)
;;;;;    ref_pix = [data.x_size/2, data.y_size/2]
;;;;;    npar = data.ngau * data.npar_gau + 2
;;;;;    phi = data.p[npar - 2]
;;;;;    pow = data.p[npar - 1]
;;;;;    psf = fltarr(data.x_size, data.y_size)
;;;;;    for g = 0, data.ngau-1 do begin
;;;;;       k = g * data.npar_gau
;;;;;       psf = temporary(psf) + data.p[k] * $
;;;;;             powergau(data.x_size, data.y_size, ref_pix[0] + dx, ref_pix[1] + dy, $
;;;;;                      data.p[k + 1], data.p[k + 2], phi, pow)
;;;;;    endfor
;;;;;    psf = psf / total(psf)
;;;;;    return
;;;;;end
