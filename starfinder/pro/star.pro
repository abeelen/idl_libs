; $Id: star.pro, v 1.1 Mar 2012 e.d. $
;
;+
; NAME:
;	STAR
;
; PURPOSE:
;	Create named structure, called "starlet", representing a star.
;	This structure is the basic element of a list of stars, which
;	might both accepted stars and still presumed ones.
;
; CATEGORY:
;	STARFINDER auxiliary procedures.
;
; CALLING SEQUENCE:
;	Result = STAR()
;
; OUTPUTS:
;	Return "starlet" structure
;
; MODIFICATION HISTORY:
; 	Written by:	Emiliano Diolaiti, June 2001.
; 	1) Created this file (E. D., March 2012).
;-

FUNCTION star

    return, {starlet, $
             x: 0., sigma_x: 0., $   ; x- coordinate and error
             y: 0., sigma_y: 0., $   ; y- coordinate and error
             f: 0., sigma_f: 0., $   ; flux and error
             c: -1., $              ; correlation
             is_a_star: 0B}            ; flag
end
