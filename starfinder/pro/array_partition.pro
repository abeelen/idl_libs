; $Id: array_partition.pro, v 1.2 Mar 2012 e.d. $
;
;+
; NAME:
;   ARRAY_PARTITION
;
; PURPOSE:
;   Define bounds to partition an array into a pre-fixed number of
;   parts along one dimension.
;
; CATEGORY:
;   Array manipulation.
;
; CALLING SEQUENCE:
;   ARRAY_PARTITION, Siz, N, L, U, S
;
; INPUTS:
;   Siz:  (Long) integer, representing the array size along the
;      dimension of interest.
;
;   N:    Number of parts into which the dimension must be partitioned.
;     The size of the sub-domains may be defined using the keyword STEP.
;     In this case, the input N may be left undefined; on output, it is
;     set to the actual number of sub-domains.
;
; KEYWORD INPUTS:
;   STEP: Set this keyword to the integer value of the desired size of each
;     sub-domain. If STEP is set, the parameter N is set on output to
;     the actual number of sub-domains.
;     
;   OVERLAP: Set this keyword to overlap sub-domains (upper boundary of 
;     a sub-domain coincides with lower boundary of next sub-domain).
;
; OUTPUTS:
;   L, U: N-element long integer vectors, defined as follows:
;      [L[i], U[i]] are the bounds of the i-th partition, i = 0, N - 1.
;      Notice that L[0] = 0, U[N - 1] = Siz - 1.
;      The following relation holds:
;      L[i + 1] = U[i] + 1, if the keyword OVERLAP is not set
;      L[i + 1] = U[i], if the keyword OVERLAP is set
;
;   S:    N-element long integer vector, with the size of each sub-domain.
;
; EXAMPLE:
;   Given a 512x512 array, partition it into 2x2 sub-regions. Define the
;   bounds of the sub-arrays along one dimension:
;   
;   IDL> ARRAY_PARTITION, 512, 2, L, U, S
;   IDL> PRINT, L, U
;        0      256
;        255    511
;   IDL> ARRAY_PARTITION, 512, 2, L, U, S, /OVERLAP
;   IDL> PRINT, L, U
;        0      256
;        256    512
;
; MODIFICATION HISTORY:
;   Written by:  Emiliano Diolaiti, December 1999.
;   1) Added keyword STEP (E.D., July 2007).
;   2) Added keyword OVERLAP (E.D., March 2012).
;-

PRO array_partition, siz, n, l, u, s, STEP = step, OVERLAP = overlap

    on_error, 2
    if keyword_set(step) then begin
       n = siz / step
       if siz mod step ne 0 then n = n + 1
    endif else $
       step = siz / n
    if keyword_set(overlap) then begin
       l = lindgen(n + 1) * step
       u = l[1: *]
       u[n - 1] = siz
       l = l[0: n - 1]
    endif else begin
       l = lindgen(n) * step
       u = l + step - 1
       u[n - 1] = siz - 1
    endelse
    s = u - l + 1
    return
end


;    on_error, 2
;    if keyword_set(step) then begin
;       n = siz / step
;       if siz mod step ne 0 then n = n + 1
;    endif else $
;       step = siz / n
;    l = lindgen(n) * step
;    u = l + step - 1
;    u[n - 1] = siz - 1
;    s = u - l + 1
;    return
;end
