; $Id: starlist.pro, v 1.1 Mar 2012 e.d. $
;
;+
; NAME:
;	STARLIST
;
; PURPOSE:
;	Create a new list of elements representing either
;	true or presumed stars.
;
; CATEGORY:
;	STARFINDER auxiliary procedures.
;
; CALLING SEQUENCE:
;	Result = STARLIST(N, X, Y, F, C)
;
; INPUTS:
;	N:	number of elements in the list
;
; OPTIONAL INPUTS:
;	X, Y, F:	1D vectors with position and flux of new elements
;	
;	C: 1D vector with correlation coefficients of new elements (if available)
;
; OUTPUTS:
;	Return possibly initialized list of N elements
;
; MODIFICATION HISTORY:
; 	Written by:	Emiliano Diolaiti, June 2001.
; 	1) Added optional input C in STARLIST (E.D., March 2012).
; 	2) Created this file (E. D., March 2012).
;-

FUNCTION starlist, n, x, y, f, c

    on_error, 2
    element = star()
    list = replicate(element, n)
    if  n_elements(x) ne 0 and n_elements(y) ne 0  then $
       update_list, list, x, y, f, c
    return, list
end
