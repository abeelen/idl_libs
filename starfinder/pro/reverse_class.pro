; $Id: reverse_class.pro, v 1.1 Mar 2012 e.d. $
;
;+
; NAME:
;	REVERSE_CLASS
;
; PURPOSE:
;	Reverse classification (star/not star) of an element in a list.
;
; CATEGORY:
;	STARFINDER auxiliary procedures.
;
; CALLING SEQUENCE:
;	Result = REVERSE_CLASS(List, SUBSCRIPTS = S)
;
; INPUTS:
;	List:	list of stars and presumed stars
;
; KEYWORD PARAMETERS:
;	SUBSCRIPTS:	subscripts of elements whose classification
;		must be reversed. If undefined, reverse classification
;		of all elements in the list
;
; OUTPUTS:
;	Return list where the classification of the subscripted elements;
;	is reversed
;
; MODIFICATION HISTORY:
; 	Written by:	Emiliano Diolaiti, June 2001.
; 	1) Created this file (E.D., March 2012).
;-

FUNCTION reverse_class, list, SUBSCRIPTS = s

    on_error, 2
    if  n_elements(s) eq 0  then  s = lindgen(n_elements(list))
    l = list
    l[s].is_a_star = (not l[s].is_a_star) and 1B
    return, l
end
