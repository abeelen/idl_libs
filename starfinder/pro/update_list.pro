; $Id: update_list.pro, v 1.1 Mar 2012 e.d. $
;
;+
; NAME:
;	UPDATE_LIST
;
; PURPOSE:
;	Update list of stars.
;
; CATEGORY:
;	STARFINDER auxiliary procedures.
;
; CALLING SEQUENCE:
;	UPDATE_LIST, List, SUBSCRIPTS = S, X, Y, F, C, $
;	             Sigma_X, Sigma_Y, Sigma_F, IS_STAR = Is_Star
;
; INPUTS:
;	List:	list of stars.
;
;	X, Y:	1D vectors with x- and y- position of stars to update.
;
; OPTIONAL INPUTS:
;	F, C:	1D vectors with flux and correlation coefficient of stars to update.
;
;	Sigma_X, Sigma_Y, Sigma_F:	errors on position and flux.
;
; KEYWORD PARAMETERS:
;	SUBSCRIPTS:	1D vector of subscripts of stars to update. If not defined,
;		update all the stars in the list.;
;
;	IS_STAR:	set this keyword to say that the stars to update have already
;		been accepted as true stars.
;
; OUTPUTS:
;	List:	updated list.
;
; MODIFICATION HISTORY:
; 	Written by:	Emiliano Diolaiti, June 2001.
; 	1) Created this file (E. D., March 2012).
;-

PRO update_list, list, SUBSCRIPTS = s, x, y, f, c, $
                  sigma_x, sigma_y, sigma_f, IS_STAR = is_star

    on_error, 2
    n = n_elements(s)
    if  n eq 0  then  s = lindgen(n_elements(x))
    if  s[0] ge 0  then begin
       list[s].x = x  &  list[s].y = y
       if  n_elements(f) ne 0  then  list[s].f = f
       if  n_elements(c) ne 0  then  list[s].c = c
       if  n_elements(sigma_x) ne 0  then  list[s].sigma_x = sigma_x
       if  n_elements(sigma_y) ne 0  then  list[s].sigma_y = sigma_y
       if  n_elements(sigma_f) ne 0  then  list[s].sigma_f = sigma_f
       list[s].is_a_star = keyword_set(is_star) and 1B
    endif
    return
end
