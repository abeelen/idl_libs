; $Id: moffat2d.pro, v 1.0 March 2012 e.d. $
;
;+
; NAME:
;   MOFFAT2D
;
; PURPOSE:
;   Compute a 2-dim Moffat function on a 2-dim (X, Y) reference frame.
;
; CATEGORY:
;   Models.
;
; CALLING SEQUENCE:
;   Result = MOFFAT2D(Nx, Ny, Cx, Cy, R1, R2, A)
;
; INPUTS:
;   Nx, Ny: Size of output array (Nx columns * Ny rows).
;
;   Cx, Cy: Coordinates of center of Moffat function.
;
;   R1, R2: Radius of Moffat function along principal axes.
;   
; OPTIONAL INPUTS:
;   A:  Angle of first axis (see R1) with respect to X axis; it is 
;     measured counter-clockwise in radians. If undefined, it is set to 0.
;
; KEYWORD PARAMETERS:
;   POWER:  Power law of Moffat function. Default POWER = 1.5.
;
;   NORMAL: Set this keyword to normalize the output Moffat function 
;     to unit energy over the whole (X, Y) plane.
;     Note: setting /NORMAL does not mean that the function is normalized 
;     to unit energy over the output array.
;
; OUTPUTS:
;   Result:   2D floating-point array containing the Moffat function.
;
; PROCEDURE:
;   Moffat function is defined by
;   z(x,y) = 1 / (1 + (xr/R1)^2 + (yr/R2)^2)^k,
;   where
;   xr and yr are coordinates in the reference frame defined by the 
;   principal axes of the Moffat function (rotated by an angle A with 
;   respect to the X, Y reference frame);
;   k is the power law defined by keyword POWER.
;   The normalization coefficient is
;   (k-1) / (!pi*R1*R2)
;   
; MODIFICATION HISTORY:
;   Written by:  Emiliano Diolaiti, March 2012.
;-

FUNCTION moffat2d, nx, ny, cx, cy, r1, r2, a, POWER = k, NORMAL = normal

on_error, 2

if n_elements(a) eq 0 then a = 0.0
if n_elements(k) eq 0 then k = 3.0/2.0

x = findgen(nx) - cx
y = findgen(ny) - cy
x = temporary(x) # make_array(ny, VALUE = 1)
y = make_array(nx, VALUE = 1) # temporary(y)
xtemp = x
ytemp = y
x =  xtemp * cos(a) + ytemp * sin(a)
y = -xtemp * sin(a) + ytemp * cos(a)
z = 1 / (1 + (x / r1)^2 + (y / r2)^2)^k

if keyword_set(normal) then z = z * (k-1) / (!pi*r1*r2)

return, z
end
