; $Id: extract_stars.pro, v 1.1 Mar 2012 e.d. $
;
;+
; NAME:
;	EXTRACT_STARS
;
; PURPOSE:
;	Return sub-list of stars, extracted from a list which might include
;	presumed stars.
;
; CATEGORY:
;	STARFINDER auxiliary procedures.
;
; CALLING SEQUENCE:
;	Result = EXTRACT_STARS(List, N)
;
; INPUTS:
;	List:	star list
;
; OUTPUTS:
;	Return sublist of true stars
;
; OPTIONAL OUTPUTS:
;	N:	number of extracted stars
;
; MODIFICATION HISTORY:
; 	Written by:	Emiliano Diolaiti, June 2001.
;   1) Created this file (E. D., March 2012).
;-

FUNCTION extract_stars, list, n

    on_error, 2
    s = where_stars(list, n)
    if  n ne 0  then  return, list[s]  else  return, -1
end
