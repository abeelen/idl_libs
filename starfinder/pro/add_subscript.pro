; $Id: add_subscript.pro, v 1.1 Mar 2012 e.d. $
;
;+
; NAME:
;	ADD_SUBSCRIPT
;
; PURPOSE:
;	Add new subscript to subscript vector.
;
; CATEGORY:
;	STARFINDER auxiliary procedures.
;
; CALLING SEQUENCE:
;	Result = ADD_SUBSCRIPT(Subscripts, S)
;
; INPUTS:
;	Subscripts:	1D vector of subscripts.
;
;	S:	new subscripts to append to Subscripts
;
; OUTPUTS:
;	Return appended vector of subscripts.
;	If input vector Subscripts is not valid, return S.
;
; MODIFICATION HISTORY:
; 	Written by:	Emiliano Diolaiti, June 2001.
;   1) Created this file (E. D., March 2012).
;-

FUNCTION add_subscript, subscripts, s

    on_error, 2
    if  subscripts[0] lt 0  then $
       w = s  else  w = [subscripts, s]
    return, w
end
