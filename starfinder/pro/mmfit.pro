; $Id: mmfit.pro, v 1.0 March 2012 e.d. $
;
;+
; NAME:
;   MMFIT
;
; PURPOSE:
;   Fit a 2D data array defined over a regular (X,Y) grid with a parametric 
;   model given by the sum of 2D Moffat functions. The Moffat functions have 
;   all the same position angle with respect to the X axis and have all the 
;   same center. One of the Moffat functions (typically the wider one) may 
;   have round simmetry. The fit is carried out by the iterative 
;   Newton-Gauss method.
;   
; CATEGORY:
;   Fitting.
;   
; CALLING SEQUENCE:
;   MMFIT, Image, Ncomp, P, Sigma_p, Model, Converged
;
; INPUTS:
;   Image:  2D floating-point array with data to be fitted (e.g. Point 
;     Spread Function of an optical system).
;   
;   Ncomp:  Integer, number of Moffat components.
;
; KEYWORD PARAMETERS:
;   NOISE:  Scalar or 2D array with the same size as Image with an estimate 
;     of the 1-sigma uncertainties on the data. This keyword parameter is 
;     used to perform a weighted least-squares fit to the data. If missing, 
;     the fit is unweighted.
;   
;   P0: Floating point vector of optional initial values of model parameters. 
;     For Ncomp components the vector has the following structure:
;     P0 = [f_1, r_1_1, r_1_2, ...., f_Ncomp, r_1_Ncomp, r_2_Ncomp, $
;           x0, y0, phi, b, bx, by]
;     where
;     f_n: scale factor of the n-th component (n = 1, ..., Ncomp)
;     r_1_n, r_2_n: radius along major and minor axes of n-th component
;     x0, y0: coordinates in pixel units of the center of the model
;     phi: angle of major axis of Moffat components with respect to X axis; 
;       it is measured counter-clockwise in radians
;     b, bx, by: background terms (constant, X-gradient, Y-gradient).
;   
;   PVAR: Integer vector with the same length as P0 defined as follows:
;     PVAR[n] = 1, if n-th parameter has to be optimized
;     PVAR[n] = 0, if n-th parameter has to be kept fixed to its initial value
;     By default, all parameters are optimized with the following exceptions:
;     * background parameters (see b, bx, by under P0 above)
;     * position angle phi (see P0 above) when only one component is used 
;       and it is round
;   
;   POWER:  Vector of Ncomp elements describing the power law of each 
;     Moffat component (see procedure 'moffat2d.pro' for details). 
;     If undefined, it is assumed that all components follow the same 
;     power 1.5.
;   
;   LMROUND:  Set this binary keyword to consider the last component in 
;     the model round. By default all Moffat functions are elongated.
;     
;   MAXIT:  Maximum number of iterations allowed. The default is 30.
;   
;   TOL:  Floating-point tolerance used as convergence criterion in 
;     iterative fitting process. The tolerance is absolute for all the 
;     parameters, with the exception of the scaling factors of the Moffat 
;     components (see parameters f_n under P0 above) for which it is 
;     relative. Default TOL = 1e-2. 
;   
; OUTPUTS:
;   P: Floating-point vector of best fit model parameters. Same length and 
;     structure as P0. If fit does not converge, the output value of P is 
;     set to the initial estimates P0.
;   
;   Sigma_p:  Vector with estimates of errors on model parameters P. 
;     Available only if keyword NOISE is set.
;      
;   Model: 2D array with the best fit model. Same size as input Image.
;     The model is a sum of scaled Moffat functions, as defined by the procedure 
;     'moffat2d.pro' (NOTE: without /NORMAL keyword).
;   
;   Converged:  Byte. True if the fit has converged.
;
; RESTRICTIONS:
;
; PROCEDURE:
;   Find initial estimate of model parameters (if not given on input) and 
;   refine this estimate by the iterative Newton-Gauss method (see 
;   'newton_gauss.pro').
;   
; MODIFICATION HISTORY:
;   Written by:  Emiliano Diolaiti, March 2012.
;-



;;; Auxiliary procedures/functions.

FUNCTION mmfit_model, p, DATA = data

    on_error, 2
    if (*data).lmround then begin
       k = ((*data).ncomp - 1) * (*data).npc
       p[k + 2] = p[k + 1]
    endif
    npar = n_elements(p)
    k = (*data).ncomp * (*data).npc
    x0 = p[k]
    y0 = p[k + 1]
    phi = p[k + 2]
    (*data).model = fltarr((*data).siz[0], (*data).siz[1])
    for  n = 0, (*data).ncomp - 1  do begin
       k = (*data).npc * n
       (*data).mof[*,*,n] = moffat2d((*data).siz[0], (*data).siz[1], x0, y0, $
                            p[k + 1], p[k + 2], phi, POWER = (*data).pow[n])
       (*data).model = (*data).model + p[k] * (*data).mof[*,*,n]
    endfor
    k = (*data).ncomp * (*data).npc + 3
    (*data).model = (*data).model + p[k] + $
                    plane(0.0, p[k + 1], p[k + 2], (*data).siz[0], (*data).siz[1])
    return, (*data).model
end

FUNCTION mmfit_iacobi, p, PVAR = pvar, DATA = data

    on_error, 2
    if (*data).lmround then begin
       k = ((*data).ncomp - 1) * (*data).npc
       p[k + 2] = p[k + 1]
    endif
    npix = (*data).siz[0] * (*data).siz[1]
    npar = total(pvar)
    k = (*data).ncomp * (*data).npc
    x0 = p[k]
    y0 = p[k + 1]
    phi = p[k + 2]
    x = rebin(findgen((*data).siz[0]) - x0, (*data).siz[0], (*data).siz[1])
    y = rebin(transpose(findgen((*data).siz[1])) - y0, (*data).siz[0], (*data).siz[1])
    rot_trans, x, y, [0, 0], phi, xr, yr

    iacobi = fltarr(npix, npar)
    dmdx0 = fltarr((*data).siz[0], (*data).siz[1])
    dmdy0 = fltarr((*data).siz[0], (*data).siz[1])
    dmdphi = fltarr((*data).siz[0], (*data).siz[1])
    k = -1
    j = 0
    for  n = 0, (*data).ncomp - 1  do begin
       k = k + 1
       norm = p[k]
       deriv = (*data).mof[*,*,n]
       if pvar[k] eq 1 then begin
          iacobi[*, j] = reform(deriv, npix)
          j = j + 1
       endif
       k = k + 1
       rx = p[k]
       ry = p[k + 1]
       rone = 1 + (xr / rx)^2 + (yr / ry)^2
       deriv = 2 * (*data).pow[n] * norm * rone^(-(*data).pow[n] - 1.0) * xr^2 / rx^3
       if pvar[k] eq 1 then begin
          iacobi[*, j] = reform(deriv, npix)
          if n lt (*data).ncomp - 1 or not (*data).lmround then j = j + 1
       endif
       k = k + 1
       ;ry = p[k]
       deriv = 2 * (*data).pow[n] * norm * rone^(-(*data).pow[n] - 1.0) * yr^2 / ry^3
       if pvar[k] eq 1 then begin
          iacobi[*, j] = reform(deriv, npix)
          j = j + 1
       endif else $
       if n eq (*data).ncomp - 1 and (*data).lmround then begin
          iacobi[*, j] = iacobi[*, j] + reform(deriv, npix)
          j = j + 1
       endif
       dmdx0 = temporary(dmdx0) + 2 * (*data).pow[n] * norm * $
               rone^(-(*data).pow[n] - 1.0) * (xr*cos(phi)/rx^2-yr*sin(phi)/ry^2)
       dmdy0 = temporary(dmdy0) + 2 * (*data).pow[n] * norm * $
               rone^(-(*data).pow[n] - 1.0) * (xr*sin(phi)/rx^2+yr*cos(phi)/ry^2)
       dmdphi = temporary(dmdphi) + 2 * (*data).pow[n] * norm * $
                rone^(-(*data).pow[n] - 1.0) * xr * yr * (1/ry^2 - 1/rx^2)
    endfor
    k = k + 1
    if pvar[k] eq 1 then begin
       iacobi[*, j] = reform(dmdx0, npix)
       j = j + 1
    endif
    k = k + 1
    if pvar[k] eq 1 then begin
       iacobi[*, j] = reform(dmdy0, npix)
       j = j + 1
    endif
    k = k + 1
    if pvar[k] eq 1 then begin
       iacobi[*, j] = reform(dmdphi, npix)
       j = j + 1
    endif
    k = k + 1
    if pvar[k] eq 1 then begin
       iacobi[*, j] = 1.0
       j = j + 1
    endif
    k = k + 1
    if pvar[k] eq 1 then begin
       iacobi[*, j] = reform(plane(0, 1, 0, (*data).siz[0], (*data).siz[1]), npix)
       j = j + 1
    endif
    k = k + 1
    if pvar[k] eq 1 then begin
       iacobi[*, j] = reform(plane(0, 0, 1, (*data).siz[0], (*data).siz[1]), npix)
       j = j + 1
    endif

    return, transpose(iacobi)
end

FUNCTION mmfit_converg, p0, p1, DATA = data

    on_error, 2
    if (*data).lmround then begin
       k = ((*data).ncomp - 1) * (*data).npc
       p0[k + 2] = p0[k + 1]
       p1[k + 2] = p1[k + 1]
    endif
    ;print, p0
    type_p = (*data).type_p
    wr = where(type_p, nr)
    wa = where(not type_p, na)
    test = 1B
    if nr ne 0 then test = test and convergence(p0[wr], p1[wr], (*data).tol)
    if na ne 0 then test = test and convergence(p0[wa], p1[wa], (*data).tol, /ABSOLUTE)
    return, test
end



;;; The main routine.

PRO mmfit, image, NOISE = noise, ncomp, P0 = p0_in, PVAR = pvar_in, $
           POWER = power, LMROUND = lmround, TOL = tol, _EXTRA = extra, $
           p, sigma_p, model, converged

    catch, error
    if  error ne 0  then begin
       ;msg = dialog_message(/ERROR, !err_string)
       converged = 0B
       heap_gc
       return
    endif
    ; Setup
    if  n_elements(tol) eq 0  then  tol = 1e-2
    siz = size(image, /DIM)
    if  n_elements(noise) ne 0  then begin
       var = noise
       w = where(var gt 0)
       var[w] = 1 / (var[w])^2
    endif
    npc = 3
    ; Parameter guess
    if n_elements(p0_in) eq 0 then begin
       peak = max(image)
       rc = fwhm(image) / 2
       m = get_max(image)  &  x0 = m[0]  &  y0 = m[1]
       npar = npc * ncomp
       p0 = fltarr(npar)
       for  n = 0, ncomp - 1  do begin
          k = npc * n
          p0[k] = peak / (n + 1)
          p0[k + 1] = rc * (n + 1)
          p0[k + 2] = rc * (n + 1)
       endfor
       p0 = [p0, x0, y0, 0.0, replicate(0.0, 3)]
    endif else $
       p0 = p0_in
    npar = n_elements(p0)
    type_p = bytarr(npar)
    k = lindgen(ncomp) * npc
    type_p[k] = 1B
    if n_elements(pvar_in) eq 0 then $
       pvar = [replicate(1, npar-3), 0, 0, 0] else $
       pvar = pvar_in
    k = (ncomp - 1) * npc
    if keyword_set(lmround) then begin
       pvar[k + 2] = 0
       if ncomp eq 1 then pvar[k + 5] = 0
    endif
    ; Power law of Moffat functions
    if n_elements(power) eq 0 then $
       pow = replicate(1.5, ncomp) else pow = power
    
    ; Iterative optimization
    data = {ncomp: ncomp, siz: siz, $
            model: fltarr(siz[0], siz[1]), tol: tol, type_p: type_p, $
            npc: npc, mof: fltarr(siz[0], siz[1], ncomp), $
            pow: pow, lmround: keyword_set(lmround) and 1B}
    data = ptr_new(data, /NO_COPY)
    newton_gauss, "mmfit_model", "mmfit_iacobi", "mmfit_converg", $
                  p0, PVAR = pvar, image, converged, p, sigma_p, model, $
                  DATA = data, _EXTRA = extra, INVERSE_DATA_VAR = var
    if  not converged  then begin
       p = p0
       model = fltarr(siz[0], siz[1])
    endif
    if n_elements(sigma_p) ne 0 then begin
       sigma_p[where(pvar eq 0)] = 0.0
       if keyword_set(lmround) then sigma_p[k + 2] = sigma_p[k + 1]
    endif
    u = lindgen(ncomp) * npc + 1
    v = lindgen(ncomp) * npc + 2
    p[u] = abs(p[u])
    p[v] = abs(p[v])
    k = npc * ncomp + 2
    if pvar[k] eq 1 then begin
       if abs(p[1]) lt abs(p[2]) then begin
          temp = p[u]
          p[u] = p[v]
          p[v] = temp
          p[k] = (p[k] + !pi/2) mod !pi
          if n_elements(sigma_p) ne 0 then begin
             temp = sigma_p[u]
             sigma_p[u] = sigma_p[v]
             sigma_p[v] = temp
          endif
       endif
       p[k] = ((p[k] mod !pi) + 2*!pi) mod !pi
    endif

    ptr_free, data
    return
end
