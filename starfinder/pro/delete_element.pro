; $Id: delete_element.pro, v 1.1 Mar 2012 e.d. $
;
;+
; NAME:
;	DELETE_ELEMENT
;
; PURPOSE:
;	Delete last element from list of stars.
;
; CATEGORY:
;	STARFINDER auxiliary procedures.
;
; CALLING SEQUENCE:
;	Result = DELETE_ELEMENT(List)
;
; INPUTS:
;	List:	input list to trim.
;
; OUTPUTS:
;	Return trimmed list.
;	
; MODIFICATION HISTORY:
; 	Written by:	Emiliano Diolaiti, June 2001.
;   1) Created this file (E. D., March 2012).
;-

FUNCTION delete_element, list

    on_error, 2
    l = list  &  n = n_elements(l)
    return, l[0:n-2]
end
