; $Id: star_param.pro, v 1.1 Mar 2012 e.d. $
;
;+
; NAME:
;	STAR_PARAM
;
; PURPOSE:
;	Extract stars parameters from star list, possibly including
;	presumed stars.
;
; CATEGORY:
;	STARFINDER auxiliary procedures.
;
; CALLING SEQUENCE:
;	STAR_PARAM, List, SUBSCRIPTS = S, $
;	            N, X, Y, F, C, Sigma_X, Sigma_Y, Sigma_F
;
; INPUTS:
;	List:	list of stars
;
; KEYWORD PARAMETERS:
;	SUBSCRIPTS:	1D vector of subscript of stars to be extracted.
;		If undefined, extract parameters of all true stars in the list.
;
; OUTPUTS:
;	N:	number of extracted stars
;
;	X, Y, F:	position and flux of stars
;
;	C:	correlation coefficienf
;
;	Sigma_X, Sigma_Y, Sigma_F:	errors on position and flux
;
; MODIFICATION HISTORY:
; 	Written by:	Emiliano Diolaiti, June 2001.
;   1) Created this file (E. D., March 2012).
;-

PRO star_param, list, SUBSCRIPTS = s, $
                 n, x, y, f, c, sigma_x, sigma_y, sigma_f

    on_error, 2
    if  n_tags(list) eq 0  then begin
       n = 0L  &  s = -1L  &  return
    endif
    n = n_elements(s)
    if  n eq 0  then begin
       n = n_elements(list)  &  s = lindgen(n) ; extract all elements
    endif
    if  s[0] lt 0  then  n = 0  else begin $
       x = list[s].x  &  sigma_x = list[s].sigma_x
       y = list[s].y  &  sigma_y = list[s].sigma_y
       f = list[s].f  &  sigma_f = list[s].sigma_f
       c = list[s].c
    endelse
    return
end
